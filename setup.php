<?php
# setup.php

# Notes:

# Changelog:
# 
# 2014/03/02  FS  Added descriptive error message to jpgraph.php setup failure
# 2014/01/11  FS  Corrected wrong description for the include folder and the config.php file
# 2013/11/20  FS  Added check for write access for jpgraph.php and jpgraph.bak files in jpgraph directory
# 2013/11/18  FS  Corrected dieout() to die()
# 2007/08/08  FS  Spelling error corrected, changed print statement for the action form
#                 changed redirect at the end to not call non functional manage_db.php
# 2004/09/19  FS  Changed the rest of HTML to PHP code, added some file checks
# 2004/09/12  FS  Changed parts of HTML to PHP code
# 2004/08/18  EW  Finished Windows configuration.
# 2004/08/11  NK  Initial Version


extract($_POST);
extract($_GET);

$server_os="";

if (strstr(PHP_OS, "WIN")){
  $server_os="Windows";
} else { //change to else if to support other OS's
  $server_os="Linux";  
}

$base_dir=substr($_SERVER[PATH_TRANSLATED],0,-9); //-9 due to length of "setup.php"
if (empty(${base_dir})) {
  $base_dir="./";
}

$path = $base_dir;

$conf_dir=$base_dir.'include/';
$conf_file=$conf_dir.'config.php'; 
$jpgraph_dir=$base_dir.'jpgraph-1.12.1/src/';
$jpgraph_file=$jpgraph_dir.'jpgraph.php';
$jpgraph_bak=$jpgraph_dir.'jpgraph.bak';
//print "<br>";
//print_r($_POST);
//print "<br>";
//print "<p>${base_dir}</p><p>${path}</p><p>${conf_dir}</p><p>${conf_file}</p><p>${jpgraph_file}</p><p>${jpgraph_bak}</p>";
//run checks to make sure the environment is set up correctly. 
//need GD library for charts
if (function_exists("imagejpeg")) {
//  print "GD Library has been found";
} else {
  die("GD Library not found.  Please follow the instructions here to install it");
}

//need to make sure we can write to the directory
if (!is_writable(${conf_dir})) {
  die( "<h2>Can't write config file, aborting</h2>
  <p>In order to configure this program you have to make the <tt>include</tt> subdirectory writable by the web server.</p>
  <p>To make the directory writable on a Unix/Linux system:</p>
  <pre>
  cd <i>/path/to/wwwsite</i>
  chmod a+w include
  </pre>
  <p>Afterwards reload the page to continue</p>");
}         

if (!is_writable(${jpgraph_dir}) || (file_exists(${jpgraph_file}) && !is_writable(${jpgraph_file}))) {
  die ("<h2>Can not write to jpgraph file, aborting!</h2>
  <p>In order to configure the jpgraph component you have to make the <tt>${jpgraph_dir}</tt> subdirectory and the <tt>${jpgraph_file}</tt> file writable by the web server.</p>
  <p>To make the directory writable on a Unix/Linux system:</p>
  <pre>
  cd <i>/path/to/wwwsite</i>
  chmod a+w ${jpgraph_dir}
  </pre>
  <p>To make the file writable on a Unix/Linux system:</p>
  <pre>
  cd <i>/path/to/wwwsite</i>
  chmod a+w ${jpgraph_file}
  </pre>
  <p>Afterwards reload the page to continue</p>");
}

if (file_exists(${jpgraph_bak}) && !is_writable(${jpgraph_bak})) {
  die ("<h2>Can not write jpgraph backup file, aborting!</h2>
  <p>In order to configure the jpgraph component you have to make the  <tt>${jpgraph_bak}</tt> file writable by the web server.</p>
  <p>To make the file writable on a Unix/Linux system:</p>
  <pre>
  cd <i>/path/to/wwwsite</i>
  chmod a+w ${jpgraph_bak}
  </pre>
  <p>Afterwards reload the page to continue</p>");
}

if ((!isset($_POST['action'])) || $_POST['action'] == "Back"){
  print "<form action=setup.php method=POST>
         <table border=0 cellpadding=5>
           <tr align=center valign=middle><td colspan=2><h2>Welcome to AIStockBot Setup</h2></td></tr>
           <tr align=center valign=middle><td colspan=2><b>Configuration settings:</b><br></td></tr>
           <tr valign=middle><td>Server OS:</td>
             <td>${server_os}<br></td></tr>
           <tr valign=middle><td>Install Directory:</td>
             <td>${base_dir}<br></td></tr>
           <tr valign=middle><td>Database hostname:</td>
             <td><input type=text name=dbhost value=localhost><br></td></tr>
           <tr valign=middle><td>Database name:<br></td>
             <td><input type=text name=dbname value=aistockbot><br></td></tr>
           <tr valign=middle><td>AIStockBot Account:</td>
             <td><input type=radio name=account value=new onClick=\"dbrootuser.disabled=false; dbrootpass.disabled=false\" checked>Create a new account<br>
               <input type=radio name=account value=existing onClick=\"dbrootuser.disabled=true; dbrootpass.disabled=true\">Use an existing account</td></tr>
           <tr valign=middle><td>MySQL Admin Username:<br></td>
             <td><input type=text name=dbrootuser value=root><br></td></tr>
           <tr valign=middle><td>MySQL Admin Password:<br>(Admin Account is only used here, then discarded)</td>
             <td><input type=password name=dbrootpass><br></td></tr>
           <tr valign=middle><td>AIStockBot MySQL Username:<br></td>
             <td><input type=text name=dbuser value=aisb><br></td></tr>
           <tr valign=middle><td>AIStockBot MySQL Password:<br></td>
             <td><input type=password name=dbpass value=aisb><br></td></tr>
           <tr align=left valign=middle><td colspan=2><b>Warning:</b><br>
             When you click <b>\"Save Configuration\"</b> this system will update MySQL and your AIStockbot Configuration files.<br>
             Clicking <b>\"Skip This Step\"</b> will bring you to the Database Import page using your current configuration.</td></tr>
           <tr align=center valign=middle><td colspan=2><br><input type=submit name=action value=\"Save Configuration\">
             <input type=submit name=action value=\"Skip This Step\"><br></td></tr>
         </table>
         </form>";
} else if ($_POST['action'] == "Save Configuration") {
  $dbname = $_POST['dbname'];
  $dbhost = $_POST['dbhost'];
  $account = $_POST['account'];
  $dbrootuser = $_POST['dbrootuser'];
  $dbrootpass = $_POST['dbrootpass'];
  $dbuser = $_POST['dbuser'];
  $dbpass = $_POST['dbpass'];
  
  if($account == 'existing'){
    $dbrootuser = $dbuser; // technically this isn't quite right, but we have
    $dbrootpass = $dbpass; // to use *some* account to try to create the database
  }

  print "<center>Configuration</center><br><br>";
  print "<br>Testing MySQL connectivity...<br>";

  $link = mysql_connect( $dbhost, $dbrootuser, $dbrootpass );
  if (!$link){
    die ( 'Unable to connect to server: '. mysql_error());
  }

  print "Connected to MySQL on '$dbhost' as '$dbrootuser'<br>Creating database '$dbname'...<br>";
  $result = mysql_query("CREATE DATABASE IF NOT EXISTS $dbname");
  if (!$result) {
    die('Invalid query: ' . mysql_error());
  }

  if($account == 'new'){
    print "Database '$dbname' created.<br>Adding user '$dbuser'...<br>";
    $result = mysql_query("GRANT ALL PRIVILEGES ON $dbname.* to '$dbuser'@'localhost' IDENTIFIED BY '$dbpass'");
    if (!$result) {
      die('Invalid query: ' . mysql_error());
    } 
    $result = mysql_query("GRANT ALL PRIVILEGES ON $dbname.* to '$dbuser'@'%' IDENTIFIED BY '$dbpass'");
    if (!$result) {
      die('Invalid query: ' . mysql_error());
    } 
    print "User '$dbuser' added. Privileges set.<br>";
  }

  include_once($base_dir.'include/install_functions.php');

  mysql_select_db($dbname,$link);
  $errmsg= "";
  $mydir = dir($path . 'sql/');
  while(($file = $mydir->read()) !== false) {
    if (ereg(".sql",$file))	{
      if (mysql_import_file($path . 'sql/' . $file, $errmsg)) {
      	echo $file . " was loaded in succesfully<br>";
    	}	else {
        echo "failure: ".$errmsg."<br/>".mysql_error();
      }
    }
  }
  $mydir->close();
  mysql_close($link);

  unset($dbrootuser);
  unset($dbrootpass);

  print "Writing configuration files...<br>";
  $bak_file=substr($conf_file, 0, -3).'bak';
  if(file_exists($conf_file)){
    if(file_exists($bak_file)){
      unlink($bak_file);
    }
    if (file_exists($bak_file)) {
      die("Couldn't remove $bak_file. <b>Stop!</b><br>");
    };
    rename($conf_file, $bak_file);
    if (file_exists($conf_file)) {
      die("Couldn't move $conf_file to $bak_file. <b>Stop!</b><br>");
    };
    print "'$conf_file' moved to '$bak_file'<br>";
  }
  $handle = fopen($conf_file, 'wt');
  fwrite($handle, '<?php'."\n");
  fwrite($handle, '// Created by setup.php on '.date("Y.m.d")."\n\n");
  fwrite($handle, '$global_database="'.$dbname."\";\n");
  fwrite($handle, '$global_hostname="'.$dbhost."\";\n");
  fwrite($handle, '$global_username="'.$dbuser."\";\n");
  fwrite($handle, '$global_password="'.$dbpass."\";\n");
  //fwrite($handle, '$base_dir="'.$base_dir."\";\n");

  if($server_os == "Linux"){
    fwrite($handle, '// Linux-only additions'."\n");
    fwrite($handle, '// Probably unnecessary'."\n");
    fwrite($handle, '$base_dir="'.$base_dir."\";\n");
  }
  fwrite($handle, '?>');
  fclose($handle); 
  if (!file_exists($conf_file)) {
    die("Couldn't create $conf_file. <b>Stop!</b><br>");
  };
  print "File '$conf_file' written.<br><br>";

// Linux also needs to open jpgraph1.x.x/src/jpgraph.php

  if($server_os == "Linux"){  
    $bak_file=substr($jpgraph_file, 0, -3).'bak';

    if(file_exists($jpgraph_file)) {
      $jpgraph_text = file_get_contents($jpgraph_file);
      if(file_exists($bak_file)) {
        unlink($bak_file);
      }
      if (file_exists($bak_file)) {
        die("Couldn't remove $bak_file. <b>Stop!</b><br>");
      };
      copy($jpgraph_file, $bak_file);
      if (!file_exists($bak_file)) {
        die("Couldn't copy $jpgraph_file to $bak_file. <b>Stop!</b><br>");
      };
      print "'$jpgraph_file' copied to '$bak_file'<br>";
      if(jpgraph_text) {
        $old = array("CHANGECACHE", "CHANGETTF");
        $new = array("${base_dir}".'tmp/jpgraph_cache/' , "${base_dir}".'ttf/'); 
        $jpgraph_new = str_replace($old, $new, $jpgraph_text);
        //file_put_contents($jpgraph_file, $jpgraph_new); //(only in php5)
        $handle = fopen($jpgraph_file, 'w');
        fwrite($handle, $jpgraph_new);
        fclose($handle);
        print "'${jpgraph_file}' written<br>";
      } else {
        print "<h2>Unable to read from '$jpgraph_file'</h2><br>";
      }
    } else {
      print "<h2>File '$jpgraph_file' not found. Update Failed.<h2><br>";
    }
  }
  print "<br><form action={$_SERVER['PHP_SELF']} method=POST>";
  print "<input type=submit name=action value=Proceed>";
  print "</form>";
} else if ($_POST['action'] == "Proceed" ||
           $_POST['action'] == "Skip This Step"  ||
           $_POST['action'] == "Back to .sql Import") {  

  print "<head>";
  // print "<meta http-equiv=REFRESH content=\"0;url=index.php?manage_db.php\">";
  print "<meta http-equiv=REFRESH content=\"0;url=index.php\">";
  print "</head>";
} else {
  print "Thanks for using AIStockBot Setup.<br>";
  print "Click <a href=index.php>here</a> to start using AIStockBot.";
}
?>
