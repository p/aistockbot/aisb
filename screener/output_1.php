<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	output_1.php
# Authors:	Nick Kuechler
#############################################################################
#
# 2004/05/22  Created File
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");


##################
# Output screened selections
#
if (isset($_POST['query'])) {

  $share_price_min = $_POST['share_price_min'];
  $share_price_max = $_POST['share_price_max'];

  $share_vol_min = $_POST['share_vol_min'];
  $share_vol_max = $_POST['share_vol_max'];

  $market_cap_min = $_POST['market_cap_min'];
  $market_cap_max = $_POST['market_cap_max'];

  $dividend_yield_min = $_POST['dividend_yield_min'];
  $dividend_yield_max = $_POST['dividend_yield_max'];

  $pe_min = $_POST['pe_min'];
  $pe_max = $_POST['pe_max'];
  $pb_min = $_POST['pb_min'];
  $pb_max = $_POST['pb_max'];
  $ps_min = $_POST['ps_min'];
  $ps_max = $_POST['ps_max'];

  print "QUERY RESULTS:<br>\n";

  # build query
  if (isset($_POST['dividend_yield_min']) && isset($_POST['dividend_yield_max'])) {
    if (empty($dividend_yield_min) || empty($dividend_yield_max)) {
    } else {
      $b_query .= " AND dividend_yield BETWEEN '". $dividend_yield_min ."' AND '". $dividend_yield_max ."'";
    }
  }
  if (isset($_POST['market_cap_min']) && isset($_POST['market_cap_max'])) {
    if (empty($market_cap_min) || empty($market_cap_max)) {
    } else {
      $b_query .= " AND market_cap BETWEEN '". $market_cap_min ."' AND '". $market_cap_max ."'";
    }
  }
  if (isset($_POST['pe_min']) && isset($_POST['pe_max'])) {
    if (empty($pe_min) || empty($pe_max)) {
    } else {
      $b_query .= " AND trailing_pe BETWEEN '". $pe_min ."' AND '". $pe_max ."'";
    }
  }
  if (isset($_POST['pb_min']) && isset($_POST['pb_max'])) {
    if (empty($pb_min) || empty($pb_max)) {
    } else {
      $b_query .= " AND price_to_book BETWEEN '". $pb_min ."' AND '". $pb_max ."'";
    }
  }
  if (isset($_POST['ps_min']) && isset($_POST['ps_max'])) {
    if (empty($ps_min) || empty($ps_max)) {
    } else {
      $b_query .= " AND price_to_sales BETWEEN '". $ps_min ."' AND '". $ps_max ."'";
    }
  }

  print "b_query: $b_query<br>\n";
  # get data for account we wish to edit
  $t_query="SELECT * FROM ai_fundamental WHERE id=0" . $b_query;
  print "t_query: $t_query<br>\n";
  $get_query=db_query($t_query);

#  $get_query=db_query("
#        SELECT          *
#        FROM            ai_fundamental
#        WHERE		dividend_yield BETWEEN '". $dividend_min ."' AND '". $dividend_max ."'
#        AND             market_cap BETWEEN '". $market_cap_min ."' AND '". $market_cap_max ."'
#        ");

  if (mysql_num_rows($get_query)==0) {
    print "<b>No companies found!</b><br>";
  } else {

    print "<table border=0>\n";

#  while ($line = mysql_fetch_array($get_query, MYSQL_ASSOC)) {
#    #print "\t<tr>\n";

#    foreach ($line as $col_value) {
#	print "\t<tr>\n";
#        print "\t\t<td>". $col_value ."</td>\n";
#	print "\t</tr>\n";
#    }
#  }

  $row=mysql_fetch_array($get_query, MYSQL_ASSOC);
           echo "<tr>";
           while(list($key,$val) = each($row) ) {
           echo "<td><font size=-2>$key</font></td>";
           }
       echo "</tr>";
      
  while($row=mysql_fetch_array($get_query, MYSQL_ASSOC)) {
           echo "<tr>";
           while(list($key,$val) = each($row) ) {
           echo "<td><font size=-2>$val</font></td>";
           }
       echo "</tr>";
  }

    mysql_free_result($get_query);

    print "</table>\n";
  }
} else {
  print " ";

}

print "<br><br><a href=\"{$path}screener/index.php\">Screener</a><br><br>";

include_once("{$path}include/footer.php");

?>
