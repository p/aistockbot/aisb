<?php
# screener/index.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/10  MS  Initial Release

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_GET);

print "<table width=100%>";
print "<tr>";
print "<td width=100% background={$path}images/hex.gif>";

print "<br>";
print "<br>";
print "<b>Stock Screener</b><br>";
print "While the Main Menu on the left will contain the bulk of the program, this area should contain an SQL Query Form for people wanting to do some simple queries.<br>";
print "<br>";
print "<form action={$path}screener/output_1.php method=post>";
print "<input type=\"hidden\" name=\"query\" value=\"1\">";
print "<table width=100%>";


print "<tr>";
print "<td><b>Share Data</b></td>";
print "<td align=\"center\">Min</td>";
print "<td align=\"center\">Max</td>";
print "</tr>";

print "<tr>";
print "<td>Share Price:</td>";
print "<td><input type=\"text\" name=\"share_price_min\"></td>";
print "<td><input type=\"text\" name=\"share_price_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Avg Share Volume</td>";
print "<td><input type=\"text\" name=\"share_vol_min\"></td>";
print "<td><input type=\"text\" name=\"share_vol_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Market Cap:</td>";
print "<td><input type=\"text\" name=\"market_cap_min\"></td>";
print "<td><input type=\"text\" name=\"market_cap_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Dividend Yield:</td>";
print "<td><input type=\"text\" name=\"dividend_yield_min\"></td>";
print "<td><input type=\"text\" name=\"dividend_yield_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Performance</b></td>";
print "</tr>";

print "<tr>";
print "<td>1 yr Stock Perf:</td>";
print "<td><input type=\"text\" name=\"one_year_min\"></td>";
print "<td><input type=\"text\" name=\"one_year_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Beta (Volatility):</td>";
print "<td><input type=\"text\" name=\"beta_min\"></td>";
print "<td><input type=\"text\" name=\"beta_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Sales / Profitability</b></td>";
print "</tr>";

print "<tr>";
print "<td>Sales Revenue:</td>";
print "<td><input type=\"text\" name=\"revenue_min\"></td>";
print "<td><input type=\"text\" name=\"revenue_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Profit Margin:</td>";
print "<td><input type=\"text\" name=\"profit_margin_min\"></td>";
print "<td><input type=\"text\" name=\"profit_margin_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Valuation Ratios</b></td>";
print "</tr>";

print "<tr>";
print "<td>Price/Earnings Ratio:</td>";
print "<td><input type=\"text\" name=\"pe_min\"></td>";
print "<td><input type=\"text\" name=\"pe_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Price/Book Ratio:</td>";
print "<td><input type=\"text\" name=\"pb_min\"></td>";
print "<td><input type=\"text\" name=\"pb_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Price/Sales Ratio:</td>";
print "<td><input type=\"text\" name=\"ps_min\"></td>";
print "<td><input type=\"text\" name=\"ps_max\"></td>";
print "</tr>";


print "</table>";
print "<center><input type=\"submit\" name=\"submit\" value=\"SUBMIT\"></center>";
print "</form><br>";
print "<br>";
print "<br>";
print "<br>";
print "<br>";

include_once("{$path}include/footer.php");

print "</td>";
print "</tr>";
print "</table>";
?>

