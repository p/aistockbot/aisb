<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	fund_output_1.php
# Authors:	Nick Kuechler
#############################################################################
#
# 2004/05/22  Created File
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");


##################
# Output screened selections
#
if (isset($_POST['query'])) {

  $share_price_min = $_POST['share_price_min'];
  $share_price_max = $_POST['share_price_max'];

  $share_vol_min = $_POST['share_vol_min'];
  $share_vol_max = $_POST['share_vol_max'];

  $market_cap_min = $_POST['market_cap_min'];
  $market_cap_max = $_POST['market_cap_max'];

  $enterprise_value_min = $_POST['enterprise_value_min'];
  $enterprise_value_max = $_POST['enterprise_value_max'];

  $dividend_yield_min = $_POST['dividend_yield_min'];
  $dividend_yield_max = $_POST['dividend_yield_max'];

  $one_year_min = $_POST['one_year_min'];
  $one_year_max = $_POST['one_year_max'];

  $beta_min = $_POST['beta_min'];
  $beta_max = $_POST['beta_max'];

  $pe_min = $_POST['pe_min'];
  $pe_max = $_POST['pe_max'];
  $pb_min = $_POST['pb_min'];
  $pb_max = $_POST['pb_max'];
  $ps_min = $_POST['ps_min'];
  $ps_max = $_POST['ps_max'];

  $revenue_ttm_min = $_POST['revenue_ttm_min'];
  $revenue_ttm_max = $_POST['revenue_ttm_max'];
  $revenue_per_share_ttm_min = $_POST['revenue_per_share_ttm_min'];
  $revenue_per_share_ttm_max = $_POST['revenue_per_share_ttm_max'];
  $revenue_growth_lfy_min = $_POST['revenue_growth_lfy_min'];
  $revenue_growth_lfy_max = $_POST['revenue_growth_lfy_max'];

  $profit_margin_min = $_POST['profit_margin_min'];
  $profit_margin_max = $_POST['profit_margin_max'];

  $avg_vol_3_month_min = $_POST['avg_vol_3_month_min'];
  $avg_vol_3_month_max = $_POST['avg_vol_3_month_max'];

  $avg_vol_10_day_min = $_POST['avg_vol_10_day_min'];
  $avg_vol_10_day_max = $_POST['avg_vol_10_day_max'];

  $shares_outstanding_min = $_POST['shares_outstanding_min'];
  $shares_outstanding_max = $_POST['shares_outstanding_max'];

  $float_outstanding_min = $_POST['float_outstanding_min'];
  $float_outstanding_max = $_POST['float_outstanding_max'];

  $held_by_insiders_min = $_POST['held_by_insiders_min'];
  $held_by_insiders_max = $_POST['held_by_insiders_max'];

  $held_by_institutions_min = $_POST['held_by_institutions_min'];
  $held_by_institutions_max = $_POST['held_by_institutions_max'];

  $shares_short_min = $_POST['shares_short_min'];
  $shares_short_max = $_POST['shares_short_max'];

  $short_ratio_min = $_POST['short_ratio_min'];
  $short_ratio_max = $_POST['short_ratio_max'];

  $short_percentage_of_float_min = $_POST['short_percentage_of_float_min'];
  $short_percentage_of_float_max = $_POST['short_percentage_of_float_max'];

  print "QUERY RESULTS:<br>\n";

  # build query
  if (isset($_POST['share_price_min']) && isset($_POST['share_price_max'])) {
    if (empty($share_price_min) || empty($share_price_max)) {
    } else {
      #$b_query .= " AND share_price BETWEEN '". $share_price_min ."' AND '". $share_price_max ."'";
    }
  }
  if (isset($_POST['share_vol_min']) && isset($_POST['share_vol_max'])) {
    if (empty($share_vol_min) || empty($share_vol_max)) {
    } else {
      #$b_query .= " AND share_volume BETWEEN '". $share_vol_min ."' AND '". $share_vol_max ."'";
    }
  }
  if (isset($_POST['market_cap_min']) && isset($_POST['market_cap_max'])) {
    if (empty($market_cap_min) || empty($market_cap_max)) {
    } else {
      $b_query .= " AND market_cap BETWEEN '". $market_cap_min ."' AND '". $market_cap_max ."'";
    }
  }
  if (isset($_POST['dividend_yield_min']) && isset($_POST['dividend_yield_max'])) {
    if (empty($dividend_yield_min) || empty($dividend_yield_max)) {
    } else {
      $b_query .= " AND dividend_yield BETWEEN '". $dividend_yield_min ."' AND '". $dividend_yield_max ."'";
    }
  }
  if (isset($_POST['pe_min']) && isset($_POST['pe_max'])) {
    if (empty($pe_min) || empty($pe_max)) {
    } else {
      $b_query .= " AND trailing_pe BETWEEN '". $pe_min ."' AND '". $pe_max ."'";
    }
  }
  if (isset($_POST['pb_min']) && isset($_POST['pb_max'])) {
    if (empty($pb_min) || empty($pb_max)) {
    } else {
      $b_query .= " AND price_to_book BETWEEN '". $pb_min ."' AND '". $pb_max ."'";
    }
  }
  if (isset($_POST['ps_min']) && isset($_POST['ps_max'])) {
    if (empty($ps_min) || empty($ps_max)) {
    } else {
      $b_query .= " AND price_to_sales BETWEEN '". $ps_min ."' AND '". $ps_max ."'";
    }
  }
  if (isset($_POST['one_year_min']) && isset($_POST['one_year_max'])) {
    if (empty($one_year_min) || empty($one_year_max)) {
    } else {
      #$b_query .= " AND one_year BETWEEN '". $one_year_min ."' AND '". $one_year_max ."'";
    }
  }
  if (isset($_POST['beta_min']) && isset($_POST['beta_max'])) {
    if (empty($beta_min) || empty($beta_max)) {
    } else {
      $b_query .= " AND beta BETWEEN '". $beta_min ."' AND '". $beta_max ."'";
    }
  }
  if (isset($_POST['enterprise_value_min']) && isset($_POST['enterprise_value_max'])) {
    if (empty($enterprise_value_min) || empty($enterprise_value_max)) {
    } else {
      $b_query .= " AND enterprise_value BETWEEN '". $enterprise_value_min ."' AND '". $enterprise_value_max ."'";
    }
  }
  if (isset($_POST['revenue_ttm_min']) && isset($_POST['revenue_ttm_max'])) {
    if (empty($revenue_ttm_min) || empty($revenue_ttm_max)) {
    } else {
      $b_query .= " AND revenue_ttm BETWEEN '". $revenue_ttm_min ."' AND '". $revenue_ttm_max ."'";
    }
  }
  if (isset($_POST['revenue_per_share_ttm_min']) && isset($_POST['revenue_per_share_ttm_max'])) {
    if (empty($revenue_per_share_ttm_min) || empty($revenue_per_share_ttm_max)) {
    } else {
      $b_query .= " AND revenue_per_share_ttm BETWEEN '". $revenue_per_share_ttm_min ."' AND '". $revenue_per_share_ttm_max ."'";
    }
  }
  if (isset($_POST['revenue_growth_lfy_min']) && isset($_POST['revenue_growth_lfy_max'])) {
    if (empty($revenue_growth_lfy_min) || empty($revenue_growth_lfy_max)) {
    } else {
      $b_query .= " AND revenue_growth_lfy BETWEEN '". $revenue_growth_lfy_min ."' AND '". $revenue_growth_lfy_max ."'";
    }
  }
  if (isset($_POST['profit_margin_min']) && isset($_POST['profit_margin_max'])) {
    if (empty($profit_margin_min) || empty($profit_margin_max)) {
    } else {
      $b_query .= " AND profit_margin BETWEEN '". $profit_margin_min ."' AND '". $profit_margin_max ."'";
    }
  }
  if (isset($_POST['avg_vol_3_month_min']) && isset($_POST['avg_vol_3_month_max'])) {
    if (empty($avg_vol_3_month_min) || empty($avg_vol_3_month_max)) {
    } else {
      $b_query .= " AND average_volume_3_month BETWEEN '". $avg_vol_3_month_min ."' AND '". $avg_vol_3_month_max ."'";
    }
  }
  if (isset($_POST['shares_outstanding_min']) && isset($_POST['shares_outstanding_max'])) {
    if (empty($shares_outstanding_min) || empty($shares_outstanding_max)) {
    } else {
      $b_query .= " AND shares_outstanding BETWEEN '". $shares_outstanding_min ."' AND '". $shares_outstanding_max ."'";
    }
  }
  if (isset($_POST['float_outstanding_min']) && isset($_POST['float_outstanding_max'])) {
    if (empty($float_outstanding_min) || empty($float_outstanding_max)) {
    } else {
      $b_query .= " AND float_outstanding BETWEEN '". $float_outstanding_min ."' AND '". $float_outstanding_max ."'";
    }
  }
  if (isset($_POST['held_by_insiders_min']) && isset($_POST['held_by_insiders_max'])) {
    if (empty($held_by_insiders_min) || empty($held_by_insiders_max)) {
    } else {
      $b_query .= " AND percent_held_by_insiders BETWEEN '". $held_by_insiders_min ."' AND '". $held_by_insiders_max ."'";
    }
  }
  if (isset($_POST['held_by_institutions_min']) && isset($_POST['held_by_institutions_max'])) {
    if (empty($held_by_institutions_min) || empty($held_by_institutions_max)) {
    } else {
      $b_query .= " AND percent_held_by_institutions BETWEEN '". $held_by_institutions_min ."' AND '". $held_by_institutions_max ."'";
    }
  }
  if (isset($_POST['shares_short_min']) && isset($_POST['shares_short_max'])) {
    if (empty($shares_short_min) || empty($shares_short_max)) {
    } else {
      $b_query .= " AND shares_short BETWEEN '". $shares_short_min ."' AND '". $shares_short_max ."'";
    }
  }
  if (isset($_POST['short_ratio_min']) && isset($_POST['short_ratio_max'])) {
    if (empty($short_ratio_min) || empty($short_ratio_max)) {
    } else {
      $b_query .= " AND short_ratio BETWEEN '". $short_ratio_min ."' AND '". $short_ratio_max ."'";
    }
  }
  if (isset($_POST['short_percentage_of_float_min']) && isset($_POST['short_percentage_of_float_max'])) {
    if (empty($short_percentage_of_float_min) || empty($short_percentage_of_float_max)) {
    } else {
      $b_query .= " AND short_percentage_of_float BETWEEN '". $short_percentage_of_float_min ."' AND '". $short_percentage_of_float_max ."'";
    }
  }

  #print "b_query: $b_query<br>\n";

  $t_query="SELECT * FROM ai_fundamental WHERE id=0" . $b_query;

  print "t_query: $t_query<br>\n";

  $get_query=db_query($t_query);

#  $get_query=db_query("
#        SELECT          *
#        FROM            ai_fundamental
#        WHERE		dividend_yield BETWEEN '". $dividend_min ."' AND '". $dividend_max ."'
#        AND             market_cap BETWEEN '". $market_cap_min ."' AND '". $market_cap_max ."'
#        ");

  if (mysql_num_rows($get_query)==0) {
    print "<b>No companies found!</b><br>";
  } else {

    $total_results = mysql_num_rows($get_query);
    print "$total_results total results:<br>";
    print "<table border=0>\n";

#  while ($line = mysql_fetch_array($get_query, MYSQL_ASSOC)) {
#    #print "\t<tr>\n";

#    foreach ($line as $col_value) {
#	print "\t<tr>\n";
#        print "\t\t<td>". $col_value ."</td>\n";
#	print "\t</tr>\n";
#    }
#  }

  $row=mysql_fetch_array($get_query, MYSQL_ASSOC);
           echo "<tr>";
           while(list($key,$val) = each($row) ) {
           echo "<td><font size=-2>$key</font></td>";
           }
       echo "</tr>";
      
  while($row=mysql_fetch_array($get_query, MYSQL_ASSOC)) {
           echo "<tr>";
           while(list($key,$val) = each($row) ) {
           echo "<td><font size=-2>$val</font></td>";
           }
       echo "</tr>";
  }

    mysql_free_result($get_query);

    print "</table>\n";
  }
} else {
  print " ";

}

print "<br><br><a href=\"{$path}screener/fundamental.php\">Fundamental Screener</a><br><br>";

include_once("{$path}include/footer.php");

?>
