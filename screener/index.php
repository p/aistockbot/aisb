<?php
# screener/index.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/10  MS  Initial Release

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_GET);

print "<table width=100%>";
print "<tr>";
print "<td width=100% background={$path}images/hex.gif>";

print "<br>";
print "<br>";
print "<b>Stock Screener</b><br>";
print "While the Main Menu on the left will contain the bulk of the program, this area should contain an SQL Query Form for people wanting to do some simple queries.<br>";
print "<br>";
print "<form action={$path}index.php method=post>";
print "<table width=100%>";
print "<tr>";
print "<td width=45%>Variable</td>";
print "<td width=5%>Operator</td>";
print "<td width=50%>Value</td>";
print "</tr>";

for ($c=1; $c<6; $c++) {
	print "<tr>";
	print "<td width=45%>";
	$sqlvariable=db_query("SELECT name FROM ai_variable");
	print "<SELECT NAME=variable$c>";
	print "<OPTION></OPTION>";
		while ($rowvariable=db_fetch_array($sqlvariable)) {
			$name=$rowvariable['name'];
	print "<OPTION>$name</OPTION>";
		}
	print "</SELECT>";
	print "</td>";
	
	print "<td width=5%>";
	
	$sqloperator=db_query("SELECT operator FROM ai_operator");
	print "<SELECT NAME=operator$c>";
	print "<OPTION></OPTION>";
		while ($rowoperator=db_fetch_array($sqloperator)) {
			$operator=$rowoperator['operator'];
	print "<OPTION>$operator</OPTION>";
		}
	print "</SELECT>";
	print "</td>";

	print "<td width=50%><input type=text size=30 name=value$c value=\"Not ready yet\"></td>";
	print "</tr>";
}
	print "<tr>";
	print "<td width=45%><input type=submit value=Submit></td>";
	print "<td width=5%>&nbsp;</td>";
	print "<td width=50%>&nbsp;</td>";
	print "</tr>";
print "</table>";
print "</form><br>";
print "<br>";
print "<br>";
print "<br>";
print "<br>";

include_once("{$path}include/footer.php");

print "</td>";
print "</tr>";
print "</table>";
?>

