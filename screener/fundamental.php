<?php
# screener/index.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/10  MS  Initial Release

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_GET);

print "<table width=100%>";
print "<tr>";
print "<td width=100% background={$path}images/hex.gif>";

print "<br>";
print "<br>";
print "<b>Stock Screener</b><br>";
print "While the Main Menu on the left will contain the bulk of the program, this area should contain an SQL Query Form for people wanting to do some simple queries.<br>";
print "<br>";
print "<form action={$path}screener/fund_output_1.php method=post>";
print "<input type=\"hidden\" name=\"query\" value=\"1\">";
print "<table width=100%>";


print "<tr>";
print "<td><b>Share Data</b></td>";
print "<td align=\"center\">Min</td>";
print "<td align=\"center\">Max</td>";
print "</tr>";

print "<tr>";
print "<td>Share Price:</td>";
print "<td><input type=\"text\" name=\"share_price_min\"></td>";
print "<td><input type=\"text\" name=\"share_price_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Avg Share Volume</td>";
print "<td><input type=\"text\" name=\"share_vol_min\"></td>";
print "<td><input type=\"text\" name=\"share_vol_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Market Cap:</td>";
print "<td><input type=\"text\" name=\"market_cap_min\"></td>";
print "<td><input type=\"text\" name=\"market_cap_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Enterprise Value:</td>";
print "<td><input type=\"text\" name=\"enterprise_value_min\"></td>";
print "<td><input type=\"text\" name=\"enterprise_value_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Dividend Yield (%):</td>";
print "<td><input type=\"text\" name=\"dividend_yield_min\"></td>";
print "<td><input type=\"text\" name=\"dividend_yield_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Performance</b></td>";
print "</tr>";

print "<tr>";
print "<td>1 yr Stock Perf:</td>";
print "<td><input type=\"text\" name=\"one_year_min\"></td>";
print "<td><input type=\"text\" name=\"one_year_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Beta (Volatility):</td>";
print "<td><input type=\"text\" name=\"beta_min\"></td>";
print "<td><input type=\"text\" name=\"beta_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Sales / Profitability</b></td>";
print "</tr>";

print "<tr>";
print "<td>Revenue (ttm):</td>";
print "<td><input type=\"text\" name=\"revenue_ttm_min\"></td>";
print "<td><input type=\"text\" name=\"revenue_ttm_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Revenue per share (ttm):</td>";
print "<td><input type=\"text\" name=\"revenue_per_share_ttm_min\"></td>";
print "<td><input type=\"text\" name=\"revenue_per_share_ttm_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Revenue growth (lfy):</td>";
print "<td><input type=\"text\" name=\"revenue_growth_lfy_min\"></td>";
print "<td><input type=\"text\" name=\"revenue_growth_lfy_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Profit Margin:</td>";
print "<td><input type=\"text\" name=\"profit_margin_min\"></td>";
print "<td><input type=\"text\" name=\"profit_margin_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Valuation Ratios</b></td>";
print "</tr>";

print "<tr>";
print "<td>Price/Earnings Ratio:</td>";
print "<td><input type=\"text\" name=\"pe_min\"></td>";
print "<td><input type=\"text\" name=\"pe_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Price/Book Ratio:</td>";
print "<td><input type=\"text\" name=\"pb_min\"></td>";
print "<td><input type=\"text\" name=\"pb_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Price/Sales Ratio:</td>";
print "<td><input type=\"text\" name=\"ps_min\"></td>";
print "<td><input type=\"text\" name=\"ps_max\"></td>";
print "</tr>";

print "<tr>";
print "<td><b>Share Statistics</b></td>";
print "</tr>";

print "<tr>";
print "<td>Average Volume (3 month):</td>";
print "<td><input type=\"text\" name=\"avg_vol_3_month_min\"></td>";
print "<td><input type=\"text\" name=\"avg_vol_3_month_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Average Volume (10 day):</td>";
print "<td><input type=\"text\" name=\"avg_vol_10_day_min\"></td>";
print "<td><input type=\"text\" name=\"avg_vol_10_day_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Shares Outstanding:</td>";
print "<td><input type=\"text\" name=\"shares_outstanding_min\"></td>";
print "<td><input type=\"text\" name=\"shares_outstanding_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Float Outstanding:</td>";
print "<td><input type=\"text\" name=\"float_outstanding_min\"></td>";
print "<td><input type=\"text\" name=\"float_outstandnig_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>% held by insiders:</td>";
print "<td><input type=\"text\" name=\"held_by_insiders_min\"></td>";
print "<td><input type=\"text\" name=\"held_by_insiders_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>% held by institutions:</td>";
print "<td><input type=\"text\" name=\"held_by_institutions_min\"></td>";
print "<td><input type=\"text\" name=\"held_by_institutions_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Shares Short:</td>";
print "<td><input type=\"text\" name=\"shares_short_min\"></td>";
print "<td><input type=\"text\" name=\"shares_short_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Short ratio:</td>";
print "<td><input type=\"text\" name=\"short_ratio_min\"></td>";
print "<td><input type=\"text\" name=\"short_ratio_max\"></td>";
print "</tr>";

print "<tr>";
print "<td>Short % of float:</td>";
print "<td><input type=\"text\" name=\"short_percentage_of_float_min\"></td>";
print "<td><input type=\"text\" name=\"short_percentage_of_float_max\"></td>";
print "</tr>";


print "</table>";
print "<center><input type=\"submit\" name=\"submit\" value=\"SUBMIT\"></center>";
print "</form><br>";
print "<br>";
print "<br>";
print "<br>";
print "<br>";

include_once("{$path}include/footer.php");

print "</td>";
print "</tr>";
print "</table>";
?>

