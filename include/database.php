<?php

/*
Summary of functions to open, query and close databases.

2004/05/24  FS  Added function db_fetch_row
2004/05/09  FS  Changed include mechanism and $path variable
2004/05/06  FS  Initial Release

*/

if (!isset($path)) {
        $path="../";
} // end if (!$path)

include_once("{$path}include/config.php");

function db_die($message = ""){
  echo $message."<br>\n";
  echo "MySQL error ".mysql_errno().": ".mysql_error()."<br>\n";
  
  if (mysql_errno() >= 2000){
    echo "Please ensure MySQL is installed and running<br>\n";
    if (strstr(PHP_OS, "WIN")){
      echo "For help installing MySQL on Windows click ";
      echo "<a href=\"http://dev.mysql.com/tech-resources/articles/ddws/3.html\">here</a>.<br>\n";
    } else {
      echo "For help installing MySQL on Linux click ";
      echo "<a href=\"http://dev.mysql.com/tech-resources/articles/ddws/4.html\">here</a>.<br>\n";
    }
    echo 'Click <a href="'.$path.'"setup.php>here</a> to configure AIStockBot<br>\n';
  }
  die("</body></html>");
}


function db_open () {
	
	global $dbcnx, $global_database, $global_hostname, $global_username, $global_password;
	
	$dbcnx=@mysql_connect("$global_hostname", "$global_username", "$global_password") or
	    db_die("Could not connect to database '$global_database'");

	@mysql_select_db("$global_database") or db_die (mysql_error());
}

function db_p_open (){ // open persistant connection
 	global $dbcnx, $global_database, $global_hostname, $global_username, $global_password;
	
	$dbcnx=@mysql_pconnect("$global_hostname", "$global_username", "$global_password") or
	    db_die("Could not connect to database '$global_database'");

	@mysql_select_db("$global_database") or db_die (mysql_error());
} 


function db_close () {
	
	global $dbcnx;
	
	mysql_close($dbcnx);
}

function db_query ($query) {
	
	global $dbcnx;
	
	db_open();
	$answer=@mysql_query($query) or db_die(mysql_error());
	//	db_close();
	return $answer;
}

function db_query_config ($param) {

	$answer=db_query("
		SELECT varname, value
		FROM ai_config
		WHERE username='default'
		  AND varname = '$param'");
	$result=mysql_result($answer,0,1);
	return $result;
}

function db_fetch_array ($query) {
	
	$result=mysql_fetch_array($query);
	return $result;
}

function db_fetch_row ($query) {
	
	$result=mysql_fetch_row($query);
	return $result;
}

function db_num_rows ($query) {

	$result=mysql_num_rows($query);
	return $result;
}

function db_result ($query, $dataset, $field) {

	$return=mysql_result($query, $dataset, $field);
	return $return;
}

function db_affected_rows(){
  return mysql_affected_rows();
}


?>
