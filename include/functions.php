<?php
/*

Miscellaneous functions for public use

2004/08/25  EW  Added naming_standards, date_to_mysql_date, csv_available, import_csv, import_yahoo_history_csv;
2004/08/24  EW  Added db_aggregate_insert
2004/08/23  EW  Added db_process_sql_file
2004/07/02  MS  Fixed to comply with php-4.3.7
2004/06/24  FS  Added extract_bot_name
2004/05/11  FS  Initial Release

*/

if (empty($path)) {
       $path="../";

} // end if (empty($path))

include_once("database.php");
include_once("finance_functions.php");


/*
With this function I find the menus in the $_SERVER["QUERY_STRING"],
so that I don't add this with the call for another script and 
to have the option to put more parameter to the called script
*/

function extract_menu_string ($input_string) {

	$input_string=ereg_replace("&&", "&", $input_string);
	if (substr($input_string, 0, 1)=="&") {
	        $work_string=substr($input_string, 1);
	} else {
	        $work_string=$input_string;
	} // end if (substr($input_string, 0, 1)=="&")

	# To comply with php-4.3.7, we need this isset() function:
	if(!isset($output_string)) {
		$output_string="";
	}

	if (preg_match('/&/', $work_string)) {
	        $work_array=@preg_split('/&/', $work_string, -1, PREG_SPLIT_NO_EMPTY);
	        foreach ($work_array as $key => $value) {
	                if (substr($value, 0, 5)=="menu_") {

	                        $output_string="{$output_string}&{$value}";
	                } // end if (substr($value, 1, 5)=="menu_")
	        } // end foreach ($work_array as $key => $value)
	} elseif (substr($work_string, 0, 5)=="menu_") {
	        $output_string=$work_string;
	} // end if (preg_match('/&/', $output_string))

	return $output_string;
}

/*
With this function I find the bot name in the $_SERVER["QUERY_STRING"],
so that I can use it in the script and give it to the next called
script 
*/

function extract_bot_name ($input_string) {

	$input_string=ereg_replace("&&", "&", $input_string);
	if (substr($input_string, 0, 1)=="&") {
	        $work_string=substr($input_string, 1);
	} else {
	        $work_string=$input_string;
	} // end if (substr($input_string, 0, 1)=="&")

	if (preg_match('/&/', $work_string)) {
	        $work_array=@preg_split('/&/', $work_string, -1, PREG_SPLIT_NO_EMPTY);
	        foreach ($work_array as $key => $value) {
	                if (substr($value, 0, 4)=="bot=") {
	                        $output_string=substr($value, 5);
	                } // end if (substr($value, 1, 4)=="bot=")
	        } // end foreach ($work_array as $key => $value)
	} elseif (substr($work_string, 0, 4)=="bot=") {
	        $output_string=substr($work_string, 5);
	} // end if (preg_match('/&/', $output_string))

	return $output_string;
}


//******************************************************
// This functions was named list_dir by whoever
// originally wrote it.
// if this is your code. Thanks for the start.
//*******************************************************



function scan_dir($dirname,$recurse=FALSE,$sort_flag=SORT_REGULAR)

// 08.30.2004  EW  Fixed major bug when called more than once. 

{
  if($dirname[strlen($dirname)-1]!='/')
    {$dirname.='/';}
  
  $file_array=array();
  $dir_array=array();
  $ret_array=array();
  $handle=opendir($dirname);
  
  while (false !== ($file = readdir($handle)))
    {
      if($file=='.'||$file=='..')
	continue;
      if(is_dir($dirname.$file))
	{
	  $dir_array[]=$dirname.$file;
	  if($recurse)
	    {
	      $tmp = scan_dir($dirname.$file.'/',$recurse);
	      $file_array = array_merge($file_array, $tmp['files']);
	      $dir_array = array_merge($dir_array, $tmp['directories']);
	    }
	}
      else
	$file_array[]=$dirname.$file;
    }
  closedir($handle);
  
  sort($file_array,$sort_flag);
  sort($dir_array,$sort_flag);
  
  reset($file_array);
  reset($dir_array);
  
  $ret_array['files']=$file_array;
  $ret_array['directories']=$dir_array;
  
  return $ret_array;
}



function db_process_sql_file($filename, $mode='n', $options=''){

  // 2004.08.24  EW  Now aggregates inserts for improved speed;
  // 2004.08.20  EW  Intial release

  // Expects the name of a file such as those created by phpMyAdmin:export
  // Processes said file and returns the number of affected rows

  // Possible values for $mode
  // 'n' - Normal Mode:      Sends each query as written.
  // 'd' - Drop Mode:        Precedes each CREATE TABLE x with DROP TABLE IF EXISTS x
  // 'r' - Replace Mode:     Changes CREATE TABLE x into CREATE TABLE IF NOT EXITS x
  //                         Changes INSERT x into REPLACE x
  // 'i' - Ignore Mode:      Changes CREATE TABLE x into CREATE TABLE IF NOT EXITS x
  //			     Changes INSERT x into INSERT IGNORE x
  // anything else is treated as $mode == 'n' 

  // Available Options
  // 'd' - Debug mode
  // 'nq' - No Queries, Process sql, but don't send queries to server

  $destructive = $mode == 'd';  
  $replace = $mode == 'r';
  $ignore = $mode == 'i';

  $debug = !(strpos($options, 'd') === false); 
  $no_queries = !(strpos($options, 'nq') === false);

  $affected_rows = 0;
  
  $handle = fopen($filename, 'rt'); 

  while(!feof($handle)){
    ##########
    // Get the next query from file into $query, stripped of comments and semicolon
    $query = "";
    $end_of_query = false;
    while(!$end_of_query){
      if(feof($handle)){
	$end_of_query = true;
      } else {
	$buffer = trim(fgets($handle));
	if(!(strpos($buffer, '#') === false)){ // line ends with an old style phpMyAdmin comment
	  $buffer = substr($buffer, 0, strpos($buffer, '#'));
	}	
	if(!(strpos($buffer, '--') === false)){ // line ends with a SQL comment
	  $buffer = substr($buffer, 0, strpos($buffer, '--'));
	}
	if(strpos($buffer, ';')){
	  $end_of_query = true;
	  $buffer = substr($buffer, 0, -1);
	}
	$query .= ' '.$buffer;
      }
    }
    $query = trim($query);
    
    //if($debug)
    //  echo "Raw Query: $query<br><br>\n";
    
    if(strpos($query, 'NSERT INTO')){ // not a typo
      // INSERT QUERY (aggregatable);
      $tmp = split(" ", $query, 5);  
      //echo "Table = $tmp[2]<br> Data = $tmp[4]<br><br>";
      $affected_rows += db_aggregate_insert($tmp[2], $tmp[4], $mode, $options);
    } else {
      // to ensure commands are executed in order, we must flush inserts
      $affected_rows += db_aggregate_insert('-flush-','',$mode,$options);
      
      // CREATE TABLE query
      if(strpos($query, 'CREATE TABLE') !== false && 
	 !strpos($query, 'IF NOT EXISTS')){
	if($destructive){
	  $tmp = explode(" ", $query, 4);  // having trouble with explode
	  $drop_query = "DROP TABLE IF EXISTS $tmp[2]";
	  //$tmp = trim(substr($query, 0, strpos($query, '(')));  
	  //$tmp = substr($tmp, 0, strpos($tmp, " "));
	  //$drop_query = str_replace("CREATE TABLE", "DROP TABLE IF EXISTS", $tmp);
	  if($debug)
	    echo "$drop_query<br><br>\n";
	  if(! $no_queries)
	    db_query($drop_query);
	} else if ($replace || $ignore){
	  $query = str_replace("CREATE TABLE", "CREATE TABLE IF NOT EXISTS", $query);
	}
      }
      // other types of queries are not altered
      
      $query = trim($query);
      if ($query != ""){
      
	if($debug)
	  echo "Finished Query: $query<br><br>\n";
	if(!$no_queries) {
	  db_query($query);
	  $tmp = db_affected_rows();
	  if($tmp == -1)
	    echo "Query Failed: '$query'<br><br>\N";
	  else
	    $affected_rows += $tmp;
	}
      }
    }
  }
  fclose($handle);

  $affected_rows += db_aggregate_insert('-flush-','',$mode,$options);
  return $affected_rows;  
}


function db_aggregate_insert($tablename, $data, $mode='n', $options=''){
  
  // 2004.08.24  EW Initial Release

  // collects data from multiple insert statements 
  // to send as a batch insert (which is much, much faster).
  // returns the number of records affected;

  // send is triggered when insert limit is reached, or 
  // $tablename or $mode are different from previous call.
  //
  // To trigger a send which resets the state, call
  // this function with "-flush-" as the tablename;   
  // Be sure to do this after the caller's last insert,
  // or remaining queued inserts may not be sent.
      
  // $tablename must be an existant table in the current database (or "-flush-");

  // $data should be a string containing a parenthasized list of 
  // comma seperated values, in legal SQL form
  // eg. "(5, 'b', 17)"
 
  // Possible values for $mode
  // 'n' - Normal Mode:      Creates INSERT INTO TABLE $table VALUES ($data1),($data2),...
  // 'r' - Replace Mode:     Creates REPLACE INTO TABLE $table VALUES ($data1),($data2),...
  // 'i' - Ignore Mode:      Creates INSERT IGNORE INTO TABLE $table VALUES ($data1),($data2),...
  // anything else is treated as $mode == 'n' 
 
  // Available Options
  // 'd' - Debug mode
  // 'nq' - No Queries, Process sql, but don't send queries to server
  // $options is not static. When send is triggered, current value of $options is used.
  
  $AGGREGATE_INSERT_LIMIT = 40; // number of inserts to aggregate before sending query.

  static $aggregate_insert_buffer = "";
  static $aggregate_insert_count = 0;
  static $aggregate_insert_table = "";
  static $aggregate_insert_mode = 'n';

  $query_done = false;
  $recurse = false;
  $count = 0;

  if($tablename == "-flush-"){
    $query_done = true;
  } else if ($aggregate_insert_count != 0 &&
	     ($tablename != $aggregate_insert_table ||
	      $mode != $aggregate_insert_mode ||
	      $aggregate_insert_count >= $AGGREGATE_INSERT_LIMIT)){
    // there are inserts which need to be sent
    $query_done = true;
    $recurse = true;
  } else if($aggregate_insert_count == 0){
    // begin a new insert
    $aggregate_insert_buffer = $data;
    $aggregate_insert_table = $tablename;       
    $aggregate_insert_count = 1;
    $aggregate_insert_mode = $mode;
  } else {
    // aggregate with previous
    $aggregate_insert_buffer .= ", $data";
    $aggregate_insert_count++;
  }


  if($query_done){
    if($aggregate_insert_count > 0){
      $insert_command = "INSERT INTO";    
      if($mode == 'r')
	$insert_command = "REPLACE INTO";
      if($mode == 'i')
	$insert_command = "INSERT IGNORE INTO";
      
      $query = "$insert_command $aggregate_insert_table VALUES $aggregate_insert_buffer";
      if(!(strpos($options, 'd') === false)) // debug mode;
	echo "Aggregate Query: $query<br><br>\n";
      if(strpos($options, 'nq') === false){ // !no queries
	db_query($query);
	$count = db_affected_rows();
	if($query == -1)
	  echo "Aggregate Insert Failed: '$query'<br><br>\n";
      }
      // reset static variables
      $aggregate_insert_buffer = "";
      $aggregate_insert_count = 0;
    } 
  }

  if ($recurse){
    // prepare variables for next time
    $aggregate_insert_count = 0;
    $aggregate_insert_buffer = "";
    
    // recurse
    return $count + db_aggregate_insert($tablename, $data, $mode, $options);
  }
  return $count;
}

function naming_standards($string){
  // 2004.08.25  EW
  // returns a lowercase string with
  // non-alphanumeric characters replaced by underscore

  $tmp = strtolower($string);
  for($i=0; $i<strlen($tmp); $i++){
    $ascii_code = ord($tmp{$i});
    if(! ctype_alnum($ascii_code)){
      $tmp{$i} = '_';		
    }		
  }	
  return $tmp;
}

function date_to_mysql_date($date_string){
  // 2004.08.25  EW
  $timestamp = strtotime($date_string);
  return date("'Y-m-d'", $timestamp); 
}

function csv_available(){
  // returns true if there are .csv files
  // in the $path/csv/ directory
  global $path;

  $csv_dir = scan_dir($path.'csv');
  $csv_files = $csv_dir['files'];
  $ret = false;
  foreach($csv_files as $file){
    if(substr($file, -4) == ".csv"){
      $ret = true;	
      break;	
    }
  }
  return $ret;
}


function import_csv($filename){
  // 2004.08.25  EW

  // Imports CSV file into database
  // Detects type of CSV & calls appropriate handler
  // Returns a count of affected records or -1 if csv is unsupported type;


  $fp = fopen ($filename, "r");
  $first_line = trim(fgets($fp)); // grab first line for testing
  fclose($fp);
  $count = 0;
  
  if($first_line == "Date,Open,High,Low,Close,Volume,Adj. Close*"){
    echo "Yahoo history data detected, importing...<br>\n";
    $count = import_yahoo_history_csv($filename);

 // } else if (matches another identifiable csv format){
 //   call_that_csv_handler();

  } else {
    echo "Unknown CSV format detected. Skipping file '$filename'<br\n";
    $count = -1;
  }
  return $count; 
}


function import_yahoo_history_csv($filename){
  // 2004.08.25  EW

  // Imports CSV file into table named after file
  // Intended for use with financial.yahoo.com history data
  // called from import_csv()
  // returns a count of affected records;

  $count = 0;
  $ticker = naming_standards(substr(basename($filename), 0,-4));
  $tablename  = "hist_$ticker";

  $fp = fopen ($filename, "r");
  $fields = fgetcsv($fp, 1000);
  if($fields == array('Date','Open','High','Low','Close','Volume','Adj. Close*')){
    
    $query = "CREATE TABLE IF NOT EXISTS $tablename (";
    $query .= "date date NOT NULL, ";
    $query .= "open decimal(8,6) NOT NULL default '0.000000', ";
    $query .= "high decimal(8,6) NOT NULL default '0.000000', ";
    $query .= "low decimal(8,6) NOT NULL default '0.000000', ";
    $query .= "close decimal(8,6) NOT NULL default '0.000000', ";
    $query .= "volume bigint(20) NOT NULL default '0', ";
    $query .= "adj_close decimal(8,6) NOT NULL default '0.000000', ";
    $query .= "PRIMARY KEY (date)";
    $query .= ") TYPE=MyISAM COMMENT='Historical price table for $ticker'";

    db_query($query);

    while ($line = fgets($fp)){
      if(strpos($line, '<!--') === false){
	$data = explode(',' , $line);
	$data[0] = date_to_mysql_date($data[0]);
	$data_str = '('. implode(', ', $data) .')';
	$count += db_aggregate_insert($tablename, $data_str, 'i');
      }
    }
    $count += db_aggregate_insert('-flush-', '', 'i');
  } else 
    echo "import_yahoo_history_csv() called on non-conformant file '$filename'<br>\n";
  fclose ($fp);
  return $count;
}


?>