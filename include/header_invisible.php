<?
#
# header_invisible.php
#
# 2004/07/12  MS  Improved functionality when someone is not logged in properly
# 2004/05/15  MS  A blank line appeared at the end of the "? >" (remove space) line
#                 The blank line threw an error on FreeBSD boxes.  To correct the error,
#                 I deleted the blank line at the end of the script.
# 2004/05/14  MS  Initial Release
#
# NOTE:  The purpose of this header file is to incorporate PEAR::Auth authentication
#        for scripts which do not use a header.php include (eg. dms/dms_insert_2.php).
#        Some scripts which use the header() function cannot display any text, and
#	 are unable to use header.php.  Therefore, we need this 'invisible' script to
#	 handle authentication, and generate the $username when needed.
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/config.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# For PEAR Authentication:
#ini_set("include_path", $pear_location . PATH_SEPARATOR . ini_get("include_path"));
#ini_set("include_path", $pear_location . ":" . ini_get("include_path"));


if (!isset($base_dir)){
  $base_dir=substr($_SERVER["PATH_TRANSLATED"],0,-17); //-17 due to length of "include/header.php";
 }

if(strstr(PHP_OS,'WIN')){ 
	$pear_location = $base_dir.'PEAR';
	ini_set("include_path", ini_get("include_path").';'.$pear_location); 
	// PHP on windows seems to use *last* pear dir listed.
} else {
	$pear_location = $base_dir.'/PEAR';
	ini_set("include_path", $pear_location . ":" . ini_get("include_path"));
}



require_once("{$path}PEAR/Auth/Auth.php");


$params = array(
        "dsn" => "mysql://$global_username:$global_password@$global_hostname/$global_database",
        "table" => "ai_auth",
        "usernamecol" => "username",
        "passwordcol" => "password"
        );
$a = new Auth("DB", $params, "loginFunction");
$a->start();
if ($a->getAuth()) {
	#
	# the output of your site goes here...
	# APPROVED! - DON'T SAY ANTHING BECAUSE THIS IS A SILENT SCRIPT.
	#
	if (isset($_GET['action'])) {
		if ($_GET['action'] == "logout") {
			# However, bomb out if we're logged out.
			$a->logout();
		    	echo "<html>";
		    	echo "</head>";
		    	echo "<body>";
		    	echo "You have been logged out!<br>";
		    	echo "<a href=\"$PHP_SELF?action=login\">Login</a><br>";
			exit();
		} else {
			# ???
		}
	}
} else {
	# Run this routine if someone is not logged in properly.
	# Sometimes the default login form appears which won't work, so I'm doing a redirection.
	# - Vooch
	print "<font size=+2>";
	print "<b>Disregard this login screen and click <a href={$path}index.php>here</a> to login.</b><br>";
	exit();
}

$username=$a->getUsername();

?>
