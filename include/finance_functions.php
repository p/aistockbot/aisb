<?php
/*

finance_functions.inc

This Include-File is used for the learn.php but not limited to.

2004/06/22  FS  Added function day_ago
2004/06/17  FS  Added function macd_histogram, highest and lowest
2004/06/07  FS  Added function macd_slow and clean some code
2004/06/04  FS  Added function macd_fast
2004/06/03  FS  Added function non
2004/05/14  FS  Added function moving_average and exponential_moving_average
2004/05/05  FS  Initial Release

*/

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

function non ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	
	DESCRIPTION:	Copy $options[3]-column data to ai_calculation because of no need
			to be calculated
	*/

	reset($history);
	while (list($key, $val) = each($history)) {
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$history[$key][$options[3]]}')
			");
	} // end while (list($key, $val) = each($history))
				
}

function moving_average ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	MOVING AVERAGE
	DESCRIPTION:	Calculate Moving Average
	*/

	$counter=1;
	while (list($key, $val) = each($history)) {
		if (($counter >= $options[4]) and $counter > 1) {
			$tmparray[]=$history[$key][$options[3]];
			$sum=array_sum($tmparray);
			$results=$sum/$options[4];
			array_shift($tmparray);
		} else {
			$results=$history[$key][$options[3]];
			$tmparray[]=$history[$key][$options[3]];
		} // end if (($counter >= $options[4]) and $counter > 1)
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$results}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function exponential_moving_average ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	EXPONENTIAL MOVING AVERAGE X-DAYS
	DESCRIPTION:	Exponential Moving Average:(EMA) = (Ptod * K) + (EMAyest * (1-K))
			where K = (2/(N+1))
			N = the number of days in the EMA
			Ptod = Today's Price
			EMAyest = the EMA of yesterday
	*/

	$counter=1;
	while (list($key, $val) = each($history)) {
		if ($counter > 1 and $counter >= $options[3]) {
			$result=($history[$key][$options[3]]*(2/($options[4]+1)))+($oldresult*(1-(2/($options[4]+1))));
			$oldresult=$result;
		} elseif ($counter > 1) {
			$result=$history[$key][$options[3]];
			$oldresult=($history[$key][$options[3]]*(2/($options[4]+1)))+($oldresult*(1-(2/($options[4]+1))));
		} else {
			$result=$history[$key][$options[3]];
			$oldresult=$result;
		}
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function macd_fast ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	MACD (MOVING AVERAGE CONVERGENCE-DIVERGENCE FAST) LINE
	DESCRIPTION:	X_day_Exponential_Moving_Average - Y_day_Exponential_Moving_Average
	*/

	$counter=1;
	$result=0;
	$ema_x=0;
	$ema_y=0;
	while (list($key, $val) = each($history)) {
		if ($counter > 1 and $counter >= $options[4] and $counter >= $options[5]) {
			$ema_x=($history[$key][$options[3]]*(2/($options[4]+1)))+($ema_x*(1-(2/($options[4]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_y*(1-(2/($options[5]+1))));
			$result=$ema_x-$ema_y;
		} elseif ($counter > 1) {
			$ema_x=($history[$key][$options[3]]*(2/($options[4]+1)))+($ema_x*(1-(2/($options[4]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_y*(1-(2/($options[5]+1))));
			$result=$history[$key][$options[3]];
		} else {
			$ema_x=$history[$key][$options[3]];
			$ema_y=$history[$key][$options[3]];
			$result=$history[$key][$options[3]];
		}
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function macd_slow ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	MACD (MOVING AVERAGE CONVERGENCE-DIVERGENCE) SLOW SIGNAL LINE
	DESCRIPTION:	X_day_Exponential_Moving_Average_of_MACD_Fast_Line
	*/

	$counter=1;
	$result=0;
	$ema_x=0;
	$ema_y=0;
	while (list($key, $val) = each($history)) {
		if ($counter > 1 and $counter > $options[5] and $counter > $options[6] and $counter > ($options[4]+$options[5]) and $counter > ($options[4]+$options[6])) {
			$ema_x=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_x*(1-(2/($options[5]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[6]+1)))+($ema_y*(1-(2/($options[6]+1))));
			$macd_fast=$ema_x-$ema_y;
			$result=($macd_fast*(2/($options[4]+1)))+($oldresult*(1-(2/($options[4]+1))));
			$oldresult=$result;
		} elseif ($counter > 1 and $counter > $options[5] and $counter > $options[6]) {
			$ema_x=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_x*(1-(2/($options[5]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[6]+1)))+($ema_y*(1-(2/($options[6]+1))));
			$macd_fast=$ema_x-$ema_y;
			$oldresult=($macd_fast*(2/($options[4]+1)))+($oldresult*(1-(2/($options[4]+1))));
			$result=$history[$key][$options[3]];
		} elseif ($counter > 1) {
			$ema_x=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_x*(1-(2/($options[5]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[6]+1)))+($ema_y*(1-(2/($options[6]+1))));
			$result=$history[$key][$options[3]];
		} else {
			$ema_x=$history[$key][$options[3]];
			$ema_y=$history[$key][$options[3]];
			$result=$history[$key][$options[3]];
		}
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function macd_histogram ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	MACD (MOVING AVERAGE CONVERGENCE-DIVERGENCE) HISTOGRAM
	DESCRIPTION:	macd_fast - macd_slow
	*/

	$counter=1;
	$result=0;
	$ema_x=0;
	$ema_y=0;
	while (list($key, $val) = each($history)) {
		if ($counter > 1 and $counter > $options[5] and $counter > $options[6] and $counter > ($options[4]+$options[5]) and $counter > ($options[4]+$options[6])) {
			$ema_x=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_x*(1-(2/($options[5]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[6]+1)))+($ema_y*(1-(2/($options[6]+1))));
			$macd_fast=$ema_x-$ema_y;
			$macd_slow=($macd_fast*(2/($options[4]+1)))+($oldmacd_slow*(1-(2/($options[4]+1))));
			$oldmacd_slow=$macd_slow;
			$result=$macd_fast-$macd_slow;
		} elseif ($counter > 1 and $counter > $options[5] and $counter > $options[6]) {
			$ema_x=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_x*(1-(2/($options[5]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[6]+1)))+($ema_y*(1-(2/($options[6]+1))));
			$macd_fast=$ema_x-$ema_y;
			$oldmacd_slow=($macd_fast*(2/($options[4]+1)))+($oldmacd_slow*(1-(2/($options[4]+1))));
			$result=$history[$key][$options[3]];
		} elseif ($counter > 1) {
			$ema_x=($history[$key][$options[3]]*(2/($options[5]+1)))+($ema_x*(1-(2/($options[5]+1))));
			$ema_y=($history[$key][$options[3]]*(2/($options[6]+1)))+($ema_y*(1-(2/($options[6]+1))));
			$result=$history[$key][$options[3]];
		} else {
			$ema_x=$history[$key][$options[3]];
			$ema_y=$history[$key][$options[3]];
			$result=$history[$key][$options[3]];
		}
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function highest ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	HIGH X DAYS
	DESCRIPTION:	highest price for the last X days
	*/

	$counter=1;
	$result=0;
	while (list($key, $val) = each($history)) {
		if (($counter >= $options[4]) and $counter > 1) {
			$tmparray[]=$history[$key][$options[3]];
			$result=max($tmparray);
			array_shift($tmparray);
		} else {
			$tmparray[]=$history[$key][$options[3]];
			$result=max($tmparray);
		} // end if (($counter >= $options[4]) and $counter > 1)
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function lowest ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	LOW X DAYS
	DESCRIPTION:	lowest price for the last X days
	*/

	$counter=1;
	$result=0;
	while (list($key, $val) = each($history)) {
		if (($counter >= $options[4]) and $counter > 1) {
			$tmparray[]=$history[$key][$options[3]];
			$result=min($tmparray);
			array_shift($tmparray);
		} else {
			$tmparray[]=$history[$key][$options[3]];
			$result=min($tmparray);
		} // end if (($counter >= $options[4]) and $counter > 1)
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

function day_ago ($ticker, $history, $variable, $options) {
	/*
	FORMULA:	PRICE X DAYS AGO
	DESCRIPTION:	price X days ago
	*/

	$counter=1;
	while (list($key, $val) = each($history)) {
		if (($counter >= $options[4]) and $counter > 1) {
			$tmparray[]=$history[$key][$options[3]];
			$tmpresult=array_shift($tmparray);
			print_r($tmpresult);
			print "<br>";
			$result=$tmpresult[$options[3]];
		} else {
			$tmparray[]=$history[$key][$options[3]];
			$result=$history[$key][$options[3]];
		} // end if (($counter >= $options[4]) and $counter > 1)
		db_query("
			INSERT INTO ai_calculation
			VALUES ({$variable},
				'{$ticker}',
				'{$key}',
				'{$result}')
			");
		$counter++;
	} // end while (list($key, $val) = each($history))
}

?>
