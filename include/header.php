<?php 
# include/header.php
#
####
# SAVE THIS NOTE:  Requests for Future versions of AIStockBot:
#		   (1) Calendar and Alarms - (eg. FOMC meeting today)
#		   (2) Alerts - Stock price alerts, news alerts, etc. - (eg. Hey!  INTC hit $20/share!)
#		   (3) Trade Analyzer - Analysis or Commentary of an individual's trades - (eg. In our
#		       opinion, your trading too much)
#		   (4) AIStockBot Researcher - (eg. individuals participate as a group for an experiment)
#		   (5) Automated Trading - (eg. computer able to place an order on Ameritrade automatically)
#
#		   We get all this stuff in there, and AIStockBot will get closer to becoming a fully-integrated trading platform - Vooch
#
####
# Changelog:
#
# 2014-11-28  FS  Created z-code-archive folder for archiving old code
#                 Moved VanGogh bot to archive for code clean-up
#                 Removed link to flite speech software and moved flite binary to archive
# 2004/08/18  EW  Fixed inclusion of PEAR on windows machines, no longer need to mess with php.ini
# 2004/07/15  MS  Fixed vangogh $artist_picture usage.  Fixed typo in monet $menu_sting when it should be $menu_string
# 2004/07/12  MS  Added &PHPSESSID=$PHPSESSID to $menu_string
# 2004/07/02  MS  Added the new $PHPSESSID=session_id(); because php-4.3.x and up
#		  require this
# 2004/07/02  MS  Fixed "Undefined index: QUERY_STRING in header.php on line 43"
#		  This new version of PHP is VERY sensitive, so it looks like we're going to have
# 		  to use the isset() function a lot ot test for a variable's existence.
#		  ...geez...I had to initialize a bunch of other variables too!
# 2004/04/15  MS  Fixed $path in LOGOUT feature.  The {$path} doesn't work during an <a> tag.
#                 hmmmm... still having problems with this when going to pms/signup_1.php
# 2004/04/15  MS  Fixed DMS:
#		  $menu_string requires include/functions.php (which is included above)
#	   	  I need to fix an error where when the PMS and DMS menus are open, and
#		  I click on the "View" button inside DMS, the PMS menu should not
#		  disappear.  Hopefully, this fixes that. 
# 2004/05/14  MS  Moved Activation Report stuff to modules/activation/*
# 2004/05/11  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed source for logo
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/09  MS  Added PEAR authentication.  Also, added $username to the bottom of this script
# 2004/05/09  MS  Fixed DMS drop-down box.  Took out default "My Diary"
# 2004/05/08  MS  Worked a bit on using submenus.  Do you like it?
#		  To choose my colors, I went to:  http://html-color-codes.com/ and chose colors which had black text
# 2004/05/06  FS  Changed to use settings from ai_config
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/05/06  FS  Added Configuration Page
# 2004/04/27  MS  Added Login and View Portfolio (for future use)
# 2004/04/23  FS  Changed HTML to PHP
# 2004/04/17  MS  Added Document Management
# 2004/04/09  MS  More cleanup
# 2003/05/26  MS  244 lines - Some cleanup
#
# TO DO:  Cleanup this script by getting rid of all HTML and going to 100% PHP
#

###
# I found these comments at:  http://us3.php.net/manual/en/function.session-id.php
# I'm not sure if we want to put this here (or even use it for that matter), but it might be of use in the
# future because of the recent PHPSESSID changes.
# if(!session_id()){
#   session_start();
# }
###




# Initialize variables
if(!isset($path)) {

  $path="../";
	
}

if (!isset($base_dir)){
  	if(phpversion() > '5.0.') {
  		$base_dir=substr($_SERVER["ORIG_PATH_TRANSLATED"],0,-17); //-17 due to length of "include/header.php";
	} else {
  		$base_dir=substr($_SERVER["PATH_TRANSLATED"],0,-17); //-17 due to length of "include/header.php";
	}
 }

if(!isset($menu_main)) 		{ $menu_main=0; }
if(!isset($menu_pms)) 		{ $menu_pms=0; }
if(!isset($menu_quantinetics))  { $menu_quantinetics=0; }
if(!isset($menu_screener)) 	{ $menu_screener=0; }
if(!isset($menu_dms)) 		{ $menu_dms=0; }
if(!isset($menu_picasso)) 	{ $menu_picasso=0; }
if(!isset($menu_monet)) 	{ $menu_monet=0; }
// if(!isset($menu_vangogh)) 	{ $menu_vangogh=0; }
if(!isset($menu_individual)) 	{ $menu_individual=0; }
if(!isset($menu_maintenance))	{ $menu_maintenance=0; }
if(!isset($menu_misc)) 		{ $menu_misc=0; }
if(!isset($menu_modules)) 	{ $menu_modules=0; }
if(!isset($csv_avail))	 	{ $csv_avail=0; }

include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
if(isset($_SERVER["QUERY_STRING"])) {
	$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));
} else {
	$menu_string=""; # PHP requires you to initialize a variable, so we'll do it here.
}

print "<head>";
print "<TITLE>AIStockBot - AIStockBot uses Artificial Intelligence to pick Stock with Bots</TITLE>";
print "</head>";
print "<table width=100% border=1>";
# print "<tr><td width=100% align=right colspan=2 bgcolor=ccffcc>";
print "<tr><td width=100% align=right colspan=2 background={$path}images/monopoly_money.jpg>";


# For PEAR Authentication:
#ini_set("include_path", $pear_location . PATH_SEPARATOR . ini_get("include_path"));

# this line did not work on unix, was giving errors.. base_dir is missing
# the trailing / and unix is very picky.. see mods below
#$pear_location = $base_dir.'PEAR';

if(strstr(PHP_OS,'WIN')){ 
	$pear_location = $base_dir.'PEAR';
	ini_set("include_path", ini_get("include_path").';'.$pear_location); 
	// PHP on windows seems to use *last* pear dir listed.
} else {
	$pear_location = $base_dir.'/PEAR';
	ini_set("include_path", $pear_location . ":" . ini_get("include_path"));
}


require_once("{$path}PEAR/Auth/Auth.php");



# WANT TO ADVERTISE?
# Contact vooch @  Lawrenceburg.com
# 
$number=rand(1,15);
if ($number==1) {
	print "<a href=http://cogniconference.com/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/cogniconf_banner.gif border=1></a>";
}
if ($number==2) {
	print "<a href=http://cogniconference.com/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/cogcfr001.gif border=1></a>";
}
if ($number==3) {
	print "<a href=http://cognigen.net/onestar/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/onestar.gif border=1></a>";
}
if ($number==4) {
	print "<a href=http://unitelagent.com/?salvucci>";
	print "<img align=left src=http://cognigen.net/banners/uni39.gif border=1></a>";
}
if ($number==5) {
	print "<a href=http://cognigen.net/capsule/?salvucci>";
	print "<img align=left src=http://cognigen.net/banners/capsule3.jpg border=1></a>";
}
if ($number==6) {
	print "<a href=http://cognisurf.com/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/cognisurf001.gif border=1></a>";
}
if ($number==7) {
	print "<a href=http://cognigen.net/bizop/main.cgi?salvucci>";
	print "<img align=left src=http://ld.net/images/fundraising.gif border=1></a>";
}
if ($number==8) {
	print "<a href=http://cognigen.net/bizop/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/clickhere2.gif border=1></a>";
}
if ($number==9) {
	print "<a href=http://cognigen.net/bizop/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/bizop4.gif border=1></a>";
}
if ($number==10) {
	print "<a href=http://cognigen.net/bizop/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/biz0001.gif border=1></a>";
}
if ($number==11) {
	print "<a href=http://cognigen.net/bizop/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/cogban1a.gif border=1></a>";
}
if ($number==12) {
	print "<a href=http://cognigen.net/bizop/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/probana109.gif border=1></a>";
}
if ($number==13) {
	print "<a href=http://cognibox.com/?salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/cogbox_001.gif border=1></a>";
}
if ($number==14) {
	print "<a href=http://cognidial.com/?salvucci>";
	print "<img align=left src=http://cognigen.net/banners/cogdl_ban2.gif border=1></a>";
}
if ($number==15) {
	print "<a href=http://cognigen-pc.com/salvucci>";
	print "<img align=left src=http://www.cognigen.net/banners/cogpc0002.gif border=1></a>";
}


print "<font face=arial size=+1><i>aistockbot-".db_query_config("version")."<br>";

# HILARIOUS (not really) QUOTE
$number=rand(1,7);
if ($number==1) {
	print "\"Rewriting the Rules of Investing Today\"<br>";
}
if ($number==2) {
	print "\"Increasing Wallet Weight Daily\"<br>";
}
if ($number==3) {
	print "\"Smarter Than Your Average Bear\"<br>";
}
if ($number==4) {
	print "\"Changes - Cha Cha Changes\"<br>";
}
if ($number==5) {
	print "\"AIStockBot Picks Stocks With Bots\"<br>";
}
if ($number==6) {
	print "\"Aiming to become The Greatest Stock Program Ever\"<br>";
}
if ($number==7) {
	print "\"Bull Market != Brains\"<br>";
}

print "</i></font>";
print "</td></tr>";

# THIS ENTIRE COLUMN
print "<tr>";
# print "<td width=20% bgcolor=eeeeff valign=top>";
print "<td width=20% background={$path}images/monopoly_money.jpg valign=top>";
print "<font size=-2>";

# LOGO
print "<center>";
# Show the logo if we're not logged out

if(!isset($action)) {
	$action="";
}
if ($action!="logout") {
	print "<a href=http://www.aistockbot.com>";
	print "<img width=185 border=2 src={$path}images/logo.jpg><br>";

	# VOOCH IS SAVING THIS STUFF FOR WHEN FEDORA CORE 2 READY - THE PLAN IS TO WRITE THE VERSION NUMBER ON THE .JPG WHEN WE'RE CAPABLE OF DOING IT

	# #1 TRY THIS LATER..  FROM WEBSITE (LINK BELOW)
	#function LoadJpeg($imgname)
	#{
	#   $im = @imagecreatefromjpeg($imgname); # Attempt to open
	#   if (!$im) { # See if it failed 
	#       $im  = imagecreate(271, 171); # Create a blank image 
	#       $bgc = imagecolorallocate($im, 255, 255, 255);
	#       $tc  = imagecolorallocate($im, 0, 0, 0);
	#       imagefilledrectangle($im, 0, 0, 271, 171, $bgc);
	#       # Output an errmsg
	#       imagestring($im, 1, 5, 5, "Error loading $imgname", $tc);
	#   }
	#   return $im;
	#}
	#$imgname="{$path}images/logo.jpg";
	#LoadJpeg("$imgname");


	# #2 TRY THIS LATER...pg. 578 red PHP4 book
	#$image = ImageCreate(271,171);
	#$black = ImageColorAllocate($image, 0, 0, 0);
	#ImageString($image, 5, 1, 1, "ERROR foo", $black);
	#ImagePNG($image);
	#ImageDestroy($image);

	# #3 TRY THIS LATER... red PHP4 book or somewhere else (before page 578)
	#$image = ImageCreateFromJPEG("images/logo.jpg");
	#ImagePNG($image);
	#ImageDestroy($image);

	# #4 TRY THIS LATER... 2003-Jul 08 comment at http://us4.php.net/manual/en/function.imagecreatefromjpeg.php
	#$img_src=imagecreatefromjpeg('images/logo.jpg');
	#$img_dst=imagecreatetruecolor(20,20);
	#imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, 20, 20, 20, 20);
	#imagejpeg($img_dst, $g_dstfile, 100);
	#imagedestroy($img_dst);

	print "</a>";
}
print "</center>";

# LOGIN - (Color: Red)
print "<table border=1 width=100%><tr><td bgcolor=ff6633><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_login=1","", $menu_string);
###
# Temporarily disabled...
#if($menu_login==1) {
	# print "<b><a href=?$tmp_menu_string>- LOGIN</a></b> - [<a href={$path}pms/signup_1.php>JOIN</a>]<br>";
###
	print "<b><a href=?$tmp_menu_string>- LOGIN</a></b> - [<a href={$path}pms/signup_1.php>JOIN</a>]<br>";

	function loginFunction()
	{
  		echo "<form method=\"post\" action=\"" . $_SERVER['PHP_SELF'] . "\">";
		print "&nbsp; &nbsp;Username:<br>";
		print "&nbsp; <input type=text name=username size=14><br>";
		print "&nbsp; &nbsp;Password:<br>";
		print "&nbsp; <input type=password name=password size=14>";
		print "&nbsp; <input type=submit value=Login><br>";
		print "This Login section allows you to have several people with several portfolios.";
		print "</form>";
		# 2004/05/14:  MS  Using {$path} does not work on this next line, so I fixed it:
		#              hmmmm... still having problems with this when going to pms/signup_1.php

		# Vooch says this fixes a Windows bug:
		if(!isset($path)) {
			$path="../";
		}

		print "<b>- <a href="."$path"."index.php?action=logout>LOGOUT</a></b><br>";
	}

	$params = array(
	        "dsn" => "mysql://$global_username:$global_password@$global_hostname/$global_database",
	        "table" => "ai_auth",
	        "usernamecol" => "username",
	        "passwordcol" => "password"
	        );

	$a = new Auth("DB", $params, "loginFunction");
	$a->start();

	if ($a->getAuth()) {
		# the output of your site goes here...
    		print "Welcome, " . $a->getUsername() ."!<br>";
		print "<b>- <a href={$path}index.php?action=logout>LOGOUT</a></b><br>";

		# To comply with php-4.3.7 and up:
		if(!isset($_GET['action'])) {
			$_GET['action']="";
		}

		if ($_GET['action'] == "logout") {
			$a->logout();
		    	echo "<html>";
		    	echo "<head>";
		    	echo "</head>";
		    	echo "<body>";
		    	echo "You have been logged out!<br>";
		    	echo "<a href=\"$_SERVER[PHP_SELF]?action=login\">Login</a><br>";
		} else {
			# ???
		}
	
	}


#} else {
#	print "<b><a href=?$tmp_menu_string&menu_login=1>+</a> LOGIN</b> - [<a href={$path}pms/signup_1.php>JOIN</a>]<br>";
#}
print "</font></td></tr></table>";


###
# 2004/07/02  This fixes the major problem with PHP Auth because of the new php-4.3.x version:
# This appears to be the appropriate place to put these 3 lines of code:
if(!isset($PHPSESSID)) { $PHPSESSID=""; }
$PHPSESSID=session_id();
# print "PHPSESSID=$PHPSESSID<br>";  # this debug line is here temporarily
$menu_string=$menu_string."&PHPSESSID=".$PHPSESSID;
###


#############################
# MAIN MENU - (Color: Orange)
# print "<hr width=50%>";
print "<table border=1 width=100%><tr><td bgcolor=ffcc00><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_main=1","", $menu_string);
if($menu_main==1) {
	print "<b><a href=?$tmp_menu_string>- MAIN MENU</a></b><br>";
	print "&nbsp; - <a href={$path}index.php?flag=1&$menu_string>Home</a><br>";
	print "&nbsp; - <a href={$path}administration/configuration_1.php?$menu_string>Configuration</a><br>";
	print "&nbsp; - <a href={$path}index.php>Reset Voice and Menus</a><br>";
	print "&nbsp; - <a href=http://www.aistockbot.com>AIStockBot.com</a><br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_main=1>+ MAIN MENU</a></b><br>";
}
print "</font></td></tr></table>";

#############################
# PMS - (Color: Yellow)
# print "<hr width=50%>";
print "<table border=1 width=100%><tr><td bgcolor=ffff00><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_pms=1","", $menu_string);
if($menu_pms==1) {
	print "<b><a href={$path}pms/portfolio_open_output_1.php?$tmp_menu_string>- PORTFOLIO MANAGEMENT</a></b><br>";
	print "View Portfolio:<br>";
	print "<form action={$path}index.php?$menu_string method=post>";
	print "<select name=account_id>";
	print "<option value=\"\" selected>ALL</option>";
	$username=$a->getUsername();
	$sql_portfolio=db_query("
		SELECT DISTINCT	account_id
		FROM		ai_portfolio
		WHERE		username='$username'
		");
	while ($row_portfolio=db_fetch_array($sql_portfolio)) {
		$frm_account_id	= $row_portfolio["account_id"];
		# If the index.php?account_id='' from the URL is equal to $frm_account_id, then set it as default
		if ($account_id==$frm_account_id) {
			print "<option value=\"$frm_account_id\" selected>$frm_account_id</option>";
		} else {
			print "<option value=\"$frm_account_id\">$frm_account_id</option>";
		}
	}
	print "</select>";
	print "<input type=submit value=View><br>";
	print "</form>";

	print "- <a href={$path}pms/transaction_add_1.php?$menu_string>New Transaction</a><br>";
	# print "- <a href={$path}pms/portfolio_output_1.php>View Portfolio</a><br>";
	print "- <a href={$path}pms/portfolio_analysis_1.php?$menu_string>Portfolio Analysis</a><br>";
	# $start_date and $end_date should appear in the URL, but if it doesn't use this:

	if ( !isset($start_date) OR !isset($end_date) ) {
		$start_date=date("Y");
		$start_date="$start_date"."-01-01";
		$end_date=date("Y-m-d");
	}
	print "- <a href={$path}pms/transaction_output_1.php?start_date=$start_date&end_date=$end_date&$menu_string>Transaction Report</a><br>";
	print "- <a href={$path}pms/transaction_output_balance_1.php?start_date=$start_date&end_date=$end_date&$menu_string>Cash Balance Report</a><br>";
	print "- <a href={$path}pms/portfolio_open_output_1.php?$menu_string>Current Holdings - Detail</a><br>";
	print "- <a href={$path}pms/export.php?$menu_string>Export</a><br>";	
	print "- <a href={$path}pms/import_1.php?$menu_string>Import (NOT WORKING YET)</a><br>";
	print "- <a href={$path}pms/manage_accounts_1.php?$menu_string>Manage Portfolios</a><br>";
	print "- <a href={$path}pms/portfolio_update_prices_1.php?$menu_string>Update Prices</a><br>";
	print "- <a href={$path}pms/winners.php?$menu_string>100% Winners</a><br>";
} else {
	print "<b><a href={$path}pms/portfolio_open_output_1.php?$tmp_menu_string&menu_pms=1>+ PORTFOLIO MANAGEMENT</a></b><br>";
}
print "</td></tr></table>";


#############################
# QUANTINETICS - LARGE-SCALE COMPUTING (Color:  Yellowish Brown)
#print "<table border=1 width=100%><tr><td bgcolor=ccff33><font face=arial size=-2>";
#$tmp_menu_string=str_replace("menu_quantinetics=1","", $menu_string);
#if($menu_quantinetics==1) {
#	print "<b><a href=?$tmp_menu_string>-</a> LARGE-SCALE COMPUTING</b><br>";
#	print "&nbsp; - <a href={$path}quantinetics/index.php?$menu_string>Login</a><br>";
#} else {
#	print "<b><a href=?$tmp_menu_string&menu_quantinetics=1>+</a> LARGE-SCALE COMPUTING</b><br>";
#}
#print "</font></td></tr></table>";


#############################
# MODULES
print "<table border=1 width=100%><tr><td bgcolor=ccff33><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_modules=1","", $menu_string);
if($menu_modules==1) {
	print "<b><a href=?$tmp_menu_string>- MODULES</a></b><br>";
	# populate list of modules from ../modules/ dir
	#print "&nbsp; - <a href={$path}modules/index.php?$menu_string>Login</a><br>";

	$scan  = array();
	#$files = array();
	$dirs  = array();

	$scan = scan_dir("{$path}modules/");
	#$files = $scan['files'];
	$dirs  = $scan['directories'];

	#echo "\n These are the files:\n\n";

	#foreach($files as $key => $val)
	#{echo"  $val\n";}
 
	#echo "\n These are the directories:\n\n";


	foreach($dirs as $key => $val)
	{
	  	$pieces = explode("/", $val);
		$result = count($pieces) - 1;
		if ($pieces[$result] == "CVS") {
		} else {
			$this_dir = "{$path}modules/" . $pieces[$result] . "/index.php";
			echo  "- <a href=\"$this_dir?$menu_string\">$pieces[$result]</a><br>\n";} 
		}

} else {
	print "<b><a href=?$tmp_menu_string&menu_modules=1>+ MODULES</a></b><br>";
}
print "</font></td></tr></table>";


#############################
# STOCK SCREENER (Color:  Yellowish Green)
print "<table border=1 width=100%><tr><td bgcolor=ccff66><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_screener=1","", $menu_string);
if($menu_screener==1) {
	print "<b><a href=?$tmp_menu_string>- STOCK SCREENERS</a></b><br>";
	print "(I imagine we will need more than one method)<br>";
	print "&nbsp; - <a href={$path}screener/index.php?$menu_string>Stock Screener</a><br>";
	print "&nbsp; - <a href={$path}screener/fundamental.php?$menu_string>Fundamental Stock Screener</a><br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_screener=1>+ STOCK SCREENERS</a></b><br>";
}
print "</font></td></tr></table>";

#############################
# DMS (Color: Light Green)
# print "<hr width=50%>";
print "<table border=1 width=100%><tr><td bgcolor=33ff33><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_dms=1","", $menu_string);
if($menu_dms==1) {
	print "<b><a href=?$tmp_menu_string>- DOCUMENT MANAGEMENT</a></b><br>";
	
	print "<form action={$path}dms/dms_output_1.php?$menu_string method=post>";
	print "&nbsp; &nbsp;View Folder:<br>";

	print "&nbsp; <input type=hidden name=menu_dms value=1>";
	print "<select name=folder>";
	# print "<option value=\"My Diary\" selected>My Diary</option>";
		$username=$a->getUsername();
		$sql_document=db_query("
			SELECT DISTINCT	folder
			FROM		ai_document
			WHERE		username='$username'
			ORDER BY	folder
			");

		while ($row_document=db_fetch_array($sql_document)) {
			$frm_folder	= $row_document["folder"];
			# If the index.php?folder='' from the URL is equal to $frm_folder, then set it as default
			if ($folder==$frm_folder) {
				print "<option value=\"$frm_folder\" selected>$frm_folder</option>";
			} else {
				print "<option value=\"$frm_folder\">$frm_folder</option>";
			}
		}
	print "</select>";
	print "&nbsp; <input type=submit value=View><br>";
	print "</form>";

	print "- <a href={$path}dms/document_manager_1.php?$menu_string>Document Manager</a><br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_dms=1>+ DOCUMENT MANAGEMENT</a></b><br>";
}
print "</td></tr></table>";

#############################
# PICASSO BOT (Color: Green)
print "<table border=1 width=100%><tr><td bgcolor=00cc66><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_picasso=1","", $menu_string);
$bot="bot=picasso";
if($menu_picasso==1) {
	print "<img border=1 src={$path}images/picasso.jpg width=75 align=right>";
	print "<b><a href=?$tmp_menu_string>- BOT:  PROJECT PICASSO</a></b><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/activation_report.php?$menu_string&$bot>STEP 0: Activation Report</a><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/stage_technical_data.php?$menu_string&$bot>STEP 1: Stage Technical Data</a><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/update_technical_data.php?$menu_string&$bot>STEP 2: Update Technical Data</a><br>";
	print "&nbsp; - STEP 3: Update Fundamental Data<br>";
	print "&nbsp; - <a href={$path}modules/allmodules/maintenance_table_results.php?$menu_string&$bot>STEP 3-1/2: Flush Results</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/picasso_4.php?$menu_string&$bot>STEP 4: Select Criteria</a><br>";
	# <!--- Step 4 form goes to Step 5 --->
	print "&nbsp; - STEP 5: Create records automatically<br>";
	print "&nbsp; - <a href={$path}modules/picasso/learn.php?$menu_string&$bot>STEP 6: Calculate Results</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/results.php?$menu_string&$bot>STEP 7: Results</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/update_signal.php?$menu_string&$bot>STEP 8: Update Signal</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/recommendation.php?$menu_string&$bot>STEP 9: Recommendations</a><br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_picasso=1>+ BOT:  PROJECT PICASSO</a></b><br>";
}
print "</font></td></tr></table>";

#############################
# MONET BOT (Color: Bluish Green)
# print "<hr width=50%>";
print "<table border=1 width=100%><tr><td bgcolor=33cccc><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_monet=1","", $menu_string);
$bot="bot=monet";
if($menu_monet==1) {
	print "<img border=1 src={$path}images/monet_1.jpg width=75 align=right>";
	print "<b><a href=?$tmp_menu_string>- BOT:  PROJECT MONET (UNDER CONSTRUCTION - NOT FOR USE)</a></b><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/activation_report.php?$menu_string&$bot>STEP 0: Activation Report</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/stage_technical_data.php?$menu_string&$bot>STEP 1: Stage Technical Data</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/update_technical_data.php?$menu_string&$bot>STEP 2: Update Technical Data</a><br>";
	print "&nbsp; - STEP 3: Update Fundamental Data<br>";
	print "&nbsp; - <a href={$path}modules/allmodules/maintenance_table_results.php?$menu_string&$bot>STEP 3-1/2: Flush Results</a><br>";
	print "&nbsp; - <a href={$path}modules/monet/formula_select_1.php?$menu_string&$bot>STEP 4: Select Formula</a><br>";
	print "&nbsp; - STEP 5: Not Applicable?<br>";
	print "&nbsp; - STEP 6: Not Applicable?<br>";
	print "&nbsp; - STEP 7: Not Applicable?<br>";
	print "&nbsp; - <a href={$path}modules/picasso/update_signal.php?$menu_string&$bot>STEP 8: Update Signal</a><br>";
	print "&nbsp; - <a href={$path}modules/picasso/recommendation.php?$menu_string&$bot>STEP 9: Recommendations</a><br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_monet=1>+ BOT:  PROJECT MONET (UNDER CONSTRUCTION - NOT FOR USE)</a></b><br>";
}
print "</font></td></tr></table>";

#############################
# VANGOGH BOT (Color: Violet Blue)
/* print "<table border=1 width=100%><tr><td bgcolor=00ccFF><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_vangogh=1","", $menu_string);
$bot="bot=vangogh";
if($menu_vangogh==1) {
	$num=rand(1,3);
	$artist_picture="van_gogh_".$num.".jpg";
	print "<img border=1 src={$path}images/$artist_picture width=75 align=right>";
	print "<b><a href=?$tmp_menu_string>- BOT:  PROJECT VAN GOGH (UNDER CONSTRUCTION - NOT FOR USE)</a></b><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/activation_report.php?$menu_string&$bot>STEP 0: Activation Report</a><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/stage_technical_data.php?$menu_string&$bot>STEP 1: Stage Technical Data</a><br>";
	print "&nbsp; - <a href={$path}modules/allmodules/update_technical_data.php?$menu_string&$bot>STEP 2: Update Technical Data</a><br>";
	print "&nbsp; - STEP 3: Update Fundamental Data (not used yet)<br>";
	print "&nbsp; - <a href={$path}modules/allmodules/maintenance_table_results.php?$menu_string&$bot>STEP 3: Flush Results</a><br>";
	print "&nbsp; - <a href={$path}modules/vangogh/create_rule_potential.php?$menu_string&$bot>STEP 4: Select Criteria</a><br>";
	print "&nbsp; - <a href={$path}modules/vangogh/create_combinations.php?$menu_string&$bot>STEP 5: Create Combinations</a><br>";
	print "&nbsp; - <a href={$path}modules/vangogh/learn.php?$menu_string&$bot>STEP 6: Calculate Results</a><br>";
	print "&nbsp; - <a href={$path}modules/vangogh/results.php?$menu_string&$bot>STEP 7: Results</a><br>";
	print "&nbsp; - <a href={$path}modules/vangogh/update_signal.php?$menu_string&$bot>STEP 8: Update Signal</a><br>";
	print "&nbsp; - <a href={$path}modules/vangogh/recommendation.php?$menu_string&$bot>STEP 9: Recommendations</a><br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_vangogh=1>+ BOT:  PROJECT VAN GOGH (UNDER CONSTRUCTION - NOT FOR USE)</a></b><br>";
}
print "</font></td></tr></table>";
*/
#############################
# INDIVIDUAL STOCKS (Color: Cyan)
print "<table border=1 width=100%><tr><td bgcolor=6699ff><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_individual=1","", $menu_string);
if($menu_individual==1) {
	# Vooch changed this from:
	# print "<b><a href=?$tmp_menu_string>- INDIVIDUAL STOCKS</a></b><br>";
	# to:
	print "<b><a href={$path}index.php?$tmp_menu_string>- INDIVIDUAL STOCKS</a></b><br>";
	print "<form action={$path}individual/individual.php?$menu_string method=post>";
	print "Ticker: <input type=text name=myvar size=7>";
	print "<input type=hidden name=days value=21>";
	print "<input type=submit value=Submit><br>";
	print "</form>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_individual=1>+ INDIVIDUAL STOCKS</a></b><br>";
}
print "</font></td></tr></table>";

#############################
# MAINTENANCE MENU (Color: Blue)
# print "<hr width=50%>";
print "<table border=1 width=100%><tr><td bgcolor=9999ff><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_maintenance=1","", $menu_string);

$cvs_avail = csv_available(); // it's a little costly, so we may as well not run it twice

if($csv_avail)
  $menu_maintenance=1;

if($menu_maintenance==1) {

?>
<b><a href=?$tmp_menu_string>- TABLE MAINTENANCE MENU</a></b><br>
<b>(under construction)</b><br><br>

<?php 
if($csv_avail)
  echo "<FONT COLOR='FF0000'>- There are CSV files ready for import -</FONT><br><br>\n";

?>


	
- <a href=index.php?pr=maintenance/manage_db.php>Database Management</a><br>
- <a href=index.php?pr=tools/import_csv.php>Import CSV Files</a><br>


<?php
	/*  // good lord is this dumb
	# EXAMPLE OF HOW IT SHOULD LOOK (USE THE SAME NAMING CONVENTION FOR THE REST):
	print "&nbsp; - account [<a href={$path}maintenance/account_insert_1.php>Insert</a>][<a href={$path}maintenance/account_update_1.php>Update</a>][<a href={$path}maintenance/account_delete_1.php>Delete</a>][<a href={$path}maintenance/account_output_1.php>Report</a>]<br>";
	###
	print "&nbsp; - combinations [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - combinations_sellside [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - company [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - config [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - document [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - exchange [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - formula [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - fundamental [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - history [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - me [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - operator [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - portfolio [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - results [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - rule_me [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - rule_parent [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - rule_potential [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - rule_potential_sellside [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - transaction [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	print "&nbsp; - variable [<a href=>Insert</a>][<a href=>Update</a>][<a href=>Delete</a>][<a href>Report</a>]<br>";
	*/


} else {
	print "<b><a href=?$tmp_menu_string&menu_maintenance=1>+ TABLE MAINTENANCE MENU</a></b><br>";
}
print "</font></td></tr></table>";


#############################
# MISCELLANEOUS (Color: Purple)
# print "<hr width=50%>";
print "<table border=1 width=100%><tr><td bgcolor=cc66cc><font face=arial size=-2>";
$tmp_menu_string=str_replace("menu_misc=1","", $menu_string);
if($menu_misc==1) {
	print "<b><a href=?$tmp_menu_string>- MISCELLANEOUS</a></b><br>";
	print "&nbsp; - <a href=http://sourceforge.net/projects/aistockbot>AIStockBot at SourceForge</a><br>";
	print "&nbsp; - <a href=http://html-color-codes.com/>216 Color Chart</a><br>";
	print "<br>";
	print "<b>SPONSORS</b><br>";
	print "&nbsp; - <a href=http://www.cognigenbestrates.com>CognigenBestRates.com</a><br>";
	print "&nbsp; - <a href=http://signup.cognigenbestrates.com>CBR Affiliate Program</a><br>";
	print "<i>Join the CBR Program and make money the way I do!</i><br>";
	print "<br>";
	print "<b>SPECIAL THANKS TO</b><br>";
	print "&nbsp; - <a href=http://www.aditus.nu/jpgraph>JpGraph</a><br>";
	print "&nbsp; - <a href=http://pear.php.net/package/Auth>PEAR::Auth</a><br>";
	print "&nbsp; - <a href=http://pear.php.net/package/DB>PEAR::DB</a><br>";
	print "<br>";
	print "<b>TECHNICAL SUPPORT</b><br>";
	print "&nbsp; - <a href={$path}support/phpinfo.php>phpInfo</a><br>";
	print "<br>";
	print "<b>IRC</b><br>";
	print " - Reach me at #aistockbot<br>";
	print "<font size=-2>";
	print "on fremont.ca.us.financialchat.com:7000<br>";
	print "or miami.fl.us.financialchat.com:7000<br>";
	print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Vooch</b><br>";
	print "<br>";
	print "</font>";
	print "<b>EMAIL</b><br>";
	print "Vooch@Lawrenceburg.com<br>";
} else {
	print "<b><a href=?$tmp_menu_string&menu_misc=1>+ MISCELLANEOUS</a></b><br>";
}
print "</font></td></tr></table>";


#  DONATION BUTTON
print "<table border=0 width=100%><tr><td align=center>";
print "<br>";
print "<form action=https://www.paypal.com/cgi-bin/webscr method=post>";
print "<input type=hidden name=cmd value=_s-xclick>";
print "<input type=image src=https://www.paypal.com/en_US/i/btn/x-click-but04.gif border=0 name=submit alt=\"Make payments with PayPal - it's fast, free and secure!\">";
print "<input type=hidden name=encrypted value=\"-----BEGIN PKCS7-----
MIIHDgYJKoZIhvcNAQcEoIIG/zCCBvsCAQExggEwMIIBLAIBADCBlDCBjjELMAkG
A1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQw
EgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UE
AxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJ
KoZIhvcNAQEBBQAEgYCYPOBLBvSHRdkrgWzBH0wrFvW2yClrnwauIlUGpAZyXpsJ
OwMTlBwm2i51VHWljGNnp/gNVMsy8dflahsZg9MoMSKeJmg5oAWN4lOgypQVoLgi
HwP+AVTANMCKCLN76gu/kG2tQpJ+bCHlM7BxYgUItLqnVisx/Y7gpQvnyuGQ2zEL
MAkGBSsOAwIaBQAwgYsGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIEZXbf6+22TGA
aLoU1OtDuUObnq7Hlo3j9NZ9DES5wzjBiZDMa6AUUI1YsOm2pTCUH8j6p6IBlimE
KzWq4IBMhbj3q7h72JCC+cwlZHS2ktwmUAR5wIkf1K4F/eT645mUavDuIw8ke9f+
3IbotCHksf+woIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4x
CzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmll
dzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAP
BgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4X
DTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQsw
CQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5
UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBp
MRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUA
A4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3s
JiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAAT
U8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQAB
o4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGz
MIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMx
CzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQ
YXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9h
cGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB
/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCD
jbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqX
a+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/c
CMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0Ex
FjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMw
EQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3
DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkD
MQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMDQwNTE1MDEzNzU5WjAjBgkq
hkiG9w0BCQQxFgQU7gahgfagO+nLH9GW/wDUWWvEIZgwDQYJKoZIhvcNAQEBBQAE
gYAUeD6cOWxc5YoKruUgfhy2ZU+bqmRzDjQIYiO2YchE/Ou7x3W9u3MsVP0ZcF9X
U3cspuO2RCodO1TohgDLBR0sJRTYt9C12gKpL2we5A+bsJlNgK5q9RPrlKzvfPJm
/bfbvTnlyNPvEvqCZJhYUM9NPFmdL1A/Nea45JC6XTDbbQ==
-----END PKCS7-----\">";
print "</form>";
print "<br></td></tr></table>";


# END OF HEADER - DO NOT ADD BELOW THIS LINE
print "</font>";
print "</td>";
print "<td width=80% valign=top>";
$username=$a->getUsername();


?>
