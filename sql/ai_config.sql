-- MySQL dump 9.09
--
-- Host: localhost    Database: aidev
---------------------------------------------------------
-- Server version	4.0.15

--
-- Table structure for table `ai_config`
--

CREATE TABLE ai_config (
  username varchar(255) NOT NULL default '',
  varname tinytext NOT NULL,
  value tinytext NOT NULL,
  possible_values mediumtext NOT NULL,
  configurable tinyint(1) NOT NULL default '0',
  description mediumtext,
  PRIMARY KEY  (username(63),varname(63)),
  UNIQUE KEY username_varname (username(63),varname(63))
) ENGINE=MyISAM;

--
-- Dumping data for table `ai_config`
--

INSERT INTO ai_config VALUES ('default','version','0.2-04','',0,'Version of actual program code');
INSERT INTO ai_config VALUES ('default','sound','0','0|1',1,'This specifies whether the flite speech syntesizer should be used.<br>0 (default) = off<br>1 = on');
INSERT INTO ai_config VALUES ('default','aistockbotpro','0','0|1',1,'This is used by recommendation.php.<br>This is in research and development and <b>should NOT be set to 1</b>.<br>0 (default) = off<br>1 = on');
INSERT INTO ai_config VALUES ('default','debug_mode','0','0|1',1,'This is used by learn.php.<br>Normally set it at 0, but during testing I have it at 1.<br>0 (default) = Don\'t show any data during debugs<br>1 = Show data during debugs');
INSERT INTO ai_config VALUES ('default','bypass_fundamental_data','1','0|1',1,'This is used in update_technical_data.php and learn.php<br>The reason for this is because sometimes people want to go to 100% technical analysis and not worrie about having a good fundamental table. Building the fundamental data table will take us a long time and we need your help getting the data!<br><b>Be forewarned!</b><br>By setting this value to 1, you will probably have poorer results.<br>0 = Fundamental data is required on every stock I analyze<br>1 (default) = Bypass fundamental data analysis and go 100% technical');
INSERT INTO ai_config VALUES ('default','currency','$','$',1,'This specifies the used currency.<br>$ (default)');
INSERT INTO ai_config VALUES ('default','delete_prefundamental','0','0|1',1,'This is used by update_technical_data.php.<br>It controls whether the quote data earlier than the earliest available fundamental data should be deleted.<br>This parameter is bypassed, when bypass_fundamental_data is set to 1.<br>0 (default) = no deletion of data<br>1 = deletion of data');
INSERT INTO ai_config VALUES ('default','proxy_host','','',2,'Set this, if you have to use a proxy server for getting the actual stock quotes.<br>192.168.0.1 (example)');
INSERT INTO ai_config VALUES ('default','proxy_port','3128','',2,'Set this, if you have to use a proxy server for getting the actual stock quotes. <b>It doesn\'t work without setting proxy_host</b>.<br>3128 (default)');
INSERT INTO ai_config VALUES ('default','keep_calculations','1','0|1',1,'This is used to keep the results of the calculations from technical formulas.<br>If you set this to 1, the scripts try to use the results from previous calculations, if they are actual. This will be checked first. It can make the calculations of the combinations faster.<br>0 = Don\'t use previous calculations<br>1 = Use previous calculations');

