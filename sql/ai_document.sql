-- phpMyAdmin MySQL-Dump
-- version 2.3.2
-- http://www.phpmyadmin.net/ (download page)
--
-- Host: localhost
-- Generation Time: May 08, 2004 at 09:57 PM
-- Server version: 3.23.54
-- PHP Version: 4.2.2
-- Database : `aistockbot`
-- --------------------------------------------------------

--
-- Table structure for table `ai_document`
--

CREATE TABLE ai_document (
  id bigint(20) NOT NULL auto_increment,
  username varchar(255) NOT NULL default '',
  folder varchar(255) NOT NULL default '',
  date date NOT NULL default '0000-00-00',
  subject varchar(255) NOT NULL default '',
  comment longtext NOT NULL,
  item_1_filename varchar(255) NOT NULL default '',
  ocr_item_1 longtext NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM;

--
-- Dumping data for table `ai_document`
--


