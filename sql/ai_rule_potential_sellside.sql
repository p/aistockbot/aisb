-- phpMyAdmin SQL Dump
-- version 2.5.7-pl1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2004 at 04:24 PM
-- Server version: 4.0.20
-- PHP Version: 4.3.7
-- 
-- Database : `aistockbot`
-- 

-- --------------------------------------------------------

--
-- Table structure for table `ai_rule_potential_sellside`
--

CREATE TABLE `ai_rule_potential_sellside` (
  `id` bigint(20) NOT NULL auto_increment,
  `bot` varchar(255) default '',
  `formula` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 COMMENT='Rule set which computer creates' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ai_rule_potential_sellside`
--

