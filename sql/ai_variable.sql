-- phpMyAdmin SQL Dump
-- version 2.5.7-pl1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2004 at 07:32 PM
-- Server version: 4.0.20
-- PHP Version: 4.3.7
-- 
-- Database : `aistockbot`
-- 

-- --------------------------------------------------------

--
-- Table structure for table `ai_variable`
--

CREATE TABLE `ai_variable` (
  `ID` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `active` tinyint(1) NOT NULL default '1',
  `options` mediumtext,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM COMMENT='Variable table' AUTO_INCREMENT=34 ;

--
-- Dumping data for table `ai_variable`
--

INSERT INTO `ai_variable` VALUES (1, '$close', 'Closing Price for a stock on a given day', 1, 'non|close');
INSERT INTO `ai_variable` VALUES (2, '$close_exponential_moving_average_12_days', 'Closing Price 12-day Exponential Moving Average', 0, 'exponential_moving_average|close|12');
INSERT INTO `ai_variable` VALUES (3, '$close_exponential_moving_average_26_days', 'Closing Price 26-day Exponential Moving Average', 0, 'exponential_moving_average|close|26');
INSERT INTO `ai_variable` VALUES (4, '$close_exponential_moving_average_5_days', 'Closing Price 5-day Exponential Moving Average', 0, 'exponential_moving_average|close|5');
INSERT INTO `ai_variable` VALUES (5, '$close_moving_average_10_days', 'Closing Price 10-day Moving Average', 1, 'moving_average|close|10');
INSERT INTO `ai_variable` VALUES (6, '$close_moving_average_135_days', 'Closing Price 135-day Moving Average', 0, 'moving_average|close|135');
INSERT INTO `ai_variable` VALUES (7, '$close_moving_average_20_days', 'Closing Price 20-day Moving Average', 0, 'moving_average|close|20');
INSERT INTO `ai_variable` VALUES (8, '$close_moving_average_21_days', 'Closing Price 21-day Moving Average', 0, 'moving_average|close|21');
INSERT INTO `ai_variable` VALUES (9, '$close_moving_average_22_days', 'Closing Price 22-day Moving Average', 0, 'moving_average|close|22');
INSERT INTO `ai_variable` VALUES (10, '$close_moving_average_23_days', 'Closing Price 23-day Moving Average', 0, 'moving_average|close|23');
INSERT INTO `ai_variable` VALUES (11, '$close_moving_average_260_days', 'Closing Price 260-day Moving Average', 0, 'moving_average|close|260');
INSERT INTO `ai_variable` VALUES (12, '$close_moving_average_30_days', 'Closing Price 30-day Moving Average', 1, 'moving_average|close|30');
INSERT INTO `ai_variable` VALUES (13, '$close_moving_average_45_days', 'Closing Price 45-day Moving Average', 0, 'moving_average|close|45');
INSERT INTO `ai_variable` VALUES (14, '$close_moving_average_5_days', 'Closing Price 5-day Moving Average', 0, 'moving_average|close|5');
INSERT INTO `ai_variable` VALUES (15, '$close_moving_average_60_days', 'Closing Price 60-day Moving Average', 0, 'moving_average|close|60');
INSERT INTO `ai_variable` VALUES (16, '$close_moving_average_70_days', 'Closing Price 70-day Moving Average', 0, 'moving_average|close|70');
INSERT INTO `ai_variable` VALUES (17, '$close_moving_average_90_days', 'Closing Price 90-day Moving Average', 0, 'moving_average|close|90');
INSERT INTO `ai_variable` VALUES (18, '$high', 'Highest price for a stock on a given day', 0, 'non|high');
INSERT INTO `ai_variable` VALUES (19, '$low', 'Lowest price for a stock on a given day', 0, 'non|low');
INSERT INTO `ai_variable` VALUES (20, '$close_macd_fast', 'Closing Price MACD fast line', 0, 'macd_fast|close|12|26');
INSERT INTO `ai_variable` VALUES (21, '$close_macd_histogram', 'Closing Price MACD Histogram', 0, 'macd_histogram|close|9|12|26');
INSERT INTO `ai_variable` VALUES (22, '$close_macd_slow', 'Closing Price MACD Slow Signal Line', 0, 'macd_slow|close|9|12|26');
INSERT INTO `ai_variable` VALUES (23, '$open', 'Opening Price for a stock on a given day', 0, 'non|close');
INSERT INTO `ai_variable` VALUES (24, '$volume', 'Volume for a stock on a given day', 0, 'non|volume');
INSERT INTO `ai_variable` VALUES (25, '$volume_moving_average_10_days', 'Volume 10-day Moving Average', 0, 'moving_average|volume|10');
INSERT INTO `ai_variable` VALUES (26, '$volume_moving_average_20_days', 'Volume 20-day Moving Average', 0, 'moving_average|volume|20');
INSERT INTO `ai_variable` VALUES (27, '$volume_moving_average_5_days', 'Volume 5-day Moving Average', 0, 'moving_average|volume|5');
INSERT INTO `ai_variable` VALUES (28, '$close_moving_average_200_days', 'Closing Price 200-day Moving Average', 0, 'moving_average|volume|200');
INSERT INTO `ai_variable` VALUES (29, '$close_high_254_days', 'Highest Closing Price in 254 Days', 0, 'highest|close|254');
INSERT INTO `ai_variable` VALUES (30, '$close_low_254_days', 'Lowest Closing Price in 254 Days', 0, 'lowest|close|254');
INSERT INTO `ai_variable` VALUES (31, '$close_high_10_days', 'Highest Closing Price in 10 Days', 0, 'highest|close|10');
INSERT INTO `ai_variable` VALUES (32, '$close_low_10_days', 'Lowest Closing Price in 10 Days', 0, 'lowest|close|10');
INSERT INTO `ai_variable` VALUES (33, '$close_1_days_ago', 'Closing Price 1 day ago', 0, 'day_ago|close|1');
