-- MySQL dump 9.09
--
-- Host: localhost    Database: aidev
---------------------------------------------------------
-- Server version	4.0.15

--
-- Table structure for table `ai_calculation`
--

CREATE TABLE ai_calculation (
  variable bigint(20) NOT NULL default '0',
  ticker varchar(20) NOT NULL default '',
  date date NOT NULL default '0000-00-00',
  value decimal(30,6) NOT NULL default '0.000000',
  PRIMARY KEY  (variable,ticker,date),
  KEY variable (variable),
  KEY ticker (ticker),
  KEY date (date)
) ENGINE=MyISAM COMMENT='Results from financial calculations';

--
-- Dumping data for table `ai_calculation`
--


