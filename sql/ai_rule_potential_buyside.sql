-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_rule_potential_buyside`
--

CREATE TABLE ai_rule_potential_buyside (
  id bigint(20) NOT NULL auto_increment,
  bot varchar(255) default '',
  formula varchar(255) NOT NULL default '',
  variable_left bigint(20) NOT NULL,
  operator bigint(20) NOT NULL,
  variable_right bigint(20) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM PACK_KEYS=1 COMMENT='Rule set which computer creates';

--
-- Dumping data for table `ai_rule_potential_buyside`
--



