-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_exchange`
--

CREATE TABLE ai_exchange (
  exchange char(3) NOT NULL default '',
  country varchar(100) NOT NULL default '',
  name varchar(255) NOT NULL default '',
  suffix varchar(4) NOT NULL default ''
) ENGINE=MyISAM COMMENT='World Stock Exchanges';

--
-- Dumping data for table `ai_exchange`
--


INSERT INTO ai_exchange VALUES ('ASE','United States','American Stock Exchange','');
INSERT INTO ai_exchange VALUES ('NAS','United States','Nasdaq Stock Exchange','');
INSERT INTO ai_exchange VALUES ('NYS','United States','New York Stock Exchange','');
INSERT INTO ai_exchange VALUES ('OBB','United States','OTC Bulletin Board Market','.OB');
INSERT INTO ai_exchange VALUES ('PNK','United States','Pink Sheets','.PK');
INSERT INTO ai_exchange VALUES ('BUE','Argentina','Buenos Aires Stock Exchange','.BA');
INSERT INTO ai_exchange VALUES ('VIE','Austria','Vienna Stock Exchange','.VA');
INSERT INTO ai_exchange VALUES ('ASX','Australia','Australian Stock Exchange','.AX');
INSERT INTO ai_exchange VALUES ('SAO','Brazil','Sao Paolo Stock Exchange','.SA');
INSERT INTO ai_exchange VALUES ('TOR','Canada','Toronto Stock Exchange','.TO');
INSERT INTO ai_exchange VALUES ('CVE','Canada','TSX Venture Exchange','.V');
INSERT INTO ai_exchange VALUES ('SGO','Chile','Santiago Stock Exchange','.SN');
INSERT INTO ai_exchange VALUES ('SHH','China','Shanghai Stock Exchange','.SS');
INSERT INTO ai_exchange VALUES ('SHZ','China','Shenzhen Stock Exchange','.SZ');
INSERT INTO ai_exchange VALUES ('CPH','Denmark','Copenhagen Stock Exchange','.CO');
INSERT INTO ai_exchange VALUES ('PAR','France','Paris Stock Exchange','.PA');
INSERT INTO ai_exchange VALUES ('BER','Germany','Berlin Stock Exchange','.BE');
INSERT INTO ai_exchange VALUES ('BRE','Germany','Bremen Stock Exchange','.BE');
INSERT INTO ai_exchange VALUES ('DUS','Germany','Dusseldorf Stock Exchange','.D');
INSERT INTO ai_exchange VALUES ('FRA','Germany','Frankfort Stock Exchange','.F');
INSERT INTO ai_exchange VALUES ('HAM','Germany','Hamburg Stock Exchange','.H');
INSERT INTO ai_exchange VALUES ('HAN','Germany','Hanover Stock Exchange','.HA');
INSERT INTO ai_exchange VALUES ('MUN','Germany','Munich Stock Exchange','.MU');
INSERT INTO ai_exchange VALUES ('STU','Germany','Stuttgart Stock Exchange','.SG');
INSERT INTO ai_exchange VALUES ('GER','Germany','XETRA Stock Exchange','.DE');
INSERT INTO ai_exchange VALUES ('HKG','Hong Kong','Hong Kong Stock Exchange','.HK');
INSERT INTO ai_exchange VALUES ('BSE','India','Bombay Stock Exchange','.BO');
INSERT INTO ai_exchange VALUES ('CAL','India','Calcutta Stock Exchange','.CL');
INSERT INTO ai_exchange VALUES ('NSI','India','National Stock Exchange of India','.NS');
INSERT INTO ai_exchange VALUES ('JKT','Indonesia','Jakarta Stock Exchange','.JK');
INSERT INTO ai_exchange VALUES ('TLV','Israel','Tel Aviv Stock Exchange','.TA');
INSERT INTO ai_exchange VALUES ('MIL','Italy','Milan Stock Exchange','.MI');
INSERT INTO ai_exchange VALUES ('KSC','Korea','Korean Stock Exchange','.KS');
INSERT INTO ai_exchange VALUES ('KOE','Korea','KOSDAQ','.KQ');
INSERT INTO ai_exchange VALUES ('KLS','Malaysia','Kuala Lumpur Stock Exchange','.KL');
INSERT INTO ai_exchange VALUES ('MEX','Mexico','Mexico Stock Exchange','.MX');
INSERT INTO ai_exchange VALUES ('NZE','New Zealand','New Zealand Stock Exchange','.NZ');
INSERT INTO ai_exchange VALUES ('AEX','Netherlands','Amsterdam Stock Exchange','.AS');
INSERT INTO ai_exchange VALUES ('OSL','Norway','Oslo Stock Exchange','.OL');
INSERT INTO ai_exchange VALUES ('LMA','Peru','Lima Stock Exchange','.LM');
INSERT INTO ai_exchange VALUES ('SES','Singapore','Singapore Stock Exchange','.SI');
INSERT INTO ai_exchange VALUES ('BAR','Spain','Barcelona Stock Exchange','.BC');
INSERT INTO ai_exchange VALUES ('BIL','Spain','Bilbao Stock Exchange','.BI');
INSERT INTO ai_exchange VALUES ('MCE','Spain','Madrid SE C.A.T.S.','.MC');
INSERT INTO ai_exchange VALUES ('MAD','Spain','Madrid Stock Exchange','.MA');
INSERT INTO ai_exchange VALUES ('STO','Sweden','Stockholm Stock Exchange','.ST');
INSERT INTO ai_exchange VALUES ('TWO','Taiwan','Taiwan OTC Exchange','.TWO');
INSERT INTO ai_exchange VALUES ('TAI','Taiwan','Taiwan Stock Exchange','.TW');
INSERT INTO ai_exchange VALUES ('SET','Thailand','Stock Exchange of Thailand','.BK');
INSERT INTO ai_exchange VALUES ('LSE','United Kingdom','London Stock Exchange','.L');
INSERT INTO ai_exchange VALUES ('CCS','Venezuela','Caracas Stock Exchange','.CR');

