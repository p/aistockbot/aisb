
-- Table structure for table `ai_transaction`
--
-- 2004/07/14  MS  Changed field action from varchar(10) to varchar(30)
--

CREATE TABLE `ai_transaction` (
  `username` varchar(50) NOT NULL default '',
  `account_id` varchar(255) NOT NULL default '',
  `date` date NOT NULL default '0000-00-00',
  `time` time NOT NULL default '00:00:00',
  `action` varchar(30) NOT NULL default '',
  `ticker` varchar(20) NOT NULL default '',
  `shares` decimal(15,6) NOT NULL default '0.000000',
  `price` decimal(15,6) NOT NULL default '0.000000',
  `commission` decimal(15,6) NOT NULL default '0.000000',
  `net_change` decimal(15,6) NOT NULL default '0.000000',
  `notes` longtext NOT NULL
) ENGINE=MyISAM COMMENT='Transaction summary of investor''s portfolio';
