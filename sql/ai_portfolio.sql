-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_portfolio`
--

CREATE TABLE ai_portfolio (
  id bigint(20) NOT NULL auto_increment,
  username varchar(255) NOT NULL default '',
  account_id varchar(255) NOT NULL default '',
  ticker varchar(20) NOT NULL default '',
  shares decimal(15,6) NOT NULL default '0.000000',
  value decimal(15,6) NOT NULL default '0.000000',
  buy_date datetime NOT NULL default '0000-00-00 00:00:00',
  buy_share_price decimal(15,6) NOT NULL default '0.000000',
  buy_cost_basis decimal(15,6) NOT NULL default '0.000000',
  buy_notes longtext NOT NULL,
  sell_date datetime NOT NULL default '0000-00-00 00:00:00',
  sell_share_price decimal(15,6) NOT NULL default '0.000000',
  sell_cost_basis decimal(15,6) NOT NULL default '0.000000',
  sell_notes longtext NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM COMMENT='This is the investor''s portfolio of securities';

--
-- Dumping data for table `ai_portfolio`
--


INSERT INTO ai_portfolio VALUES (1,'','123-456-7890','GZFX.OB',1000.000000,130.000000,'2004-01-29 00:00:00',0.055000,-56.099000,' ','2004-04-07 00:00:00',0.130000,130.000000,' ');
INSERT INTO ai_portfolio VALUES (2,'','123-456-7890','ACT_CASH',0.000000,19699.010000,'0000-00-00 00:00:00',0.000000,0.000000,'','0000-00-00 00:00:00',0.000000,0.000000,'');
INSERT INTO ai_portfolio VALUES (3,'','123-456-7890','GZFX.OB',9000.000000,1080.000000,'2004-01-29 00:00:00',0.055000,-504.891000,' ','0000-00-00 00:00:00',0.000000,0.000000,'');
INSERT INTO ai_portfolio VALUES (4,'','123-456-7890','GZFX.OB',9000.000000,1080.000000,'2004-01-29 00:00:00',0.055000,0.000000,' ','0000-00-00 00:00:00',0.000000,0.000000,'');

