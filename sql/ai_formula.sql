-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_formula`
--

CREATE TABLE ai_formula (
  ID bigint(20) NOT NULL auto_increment,
  description varchar(255) NOT NULL default '',
  buyside_formula longtext NOT NULL,
  sellside_formula longtext NOT NULL,
  UNIQUE KEY ID (ID)
) ENGINE=MyISAM;

--
-- Dumping data for table `ai_formula`
--



