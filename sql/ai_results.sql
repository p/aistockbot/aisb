-- phpMyAdmin SQL Dump
-- version 2.5.7-pl1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2004 at 05:29 PM
-- Server version: 4.0.20
-- PHP Version: 4.3.7
-- 
-- Database : `aistockbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `ai_results`
--

CREATE TABLE `ai_results` (
  `id` bigint(20) NOT NULL auto_increment,
  `bot` varchar(255) default '',
  `ticker` varchar(20) NOT NULL default '',
  `combination_id` bigint(20) NOT NULL default '0',
  `combinations_sellside_id` bigint(20) NOT NULL default '0',
  `score` decimal(14,6) NOT NULL default '0.000000',
  `percentage_of_days_score_above_zero` decimal(9,6) NOT NULL default '0.000000',
  `percentage_running_score_above_zero` decimal(9,6) NOT NULL default '0.000000',
  `last_run` date NOT NULL default '0000-00-00',
  `days_long` mediumint(9) NOT NULL default '0',
  `days_short` mediumint(9) NOT NULL default '0',
  `transactions` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM COMMENT='Each row is the result of running one combination for one st' AUTO_INCREMENT=1 ;
