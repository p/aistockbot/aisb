-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_operator`
--

CREATE TABLE ai_operator (
  ID bigint(20) NOT NULL auto_increment,
  operator char(2) NOT NULL default '',
  description varchar(255) NOT NULL default '',
  active tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (ID)
) ENGINE=MyISAM COMMENT='Operator Table';

--
-- Dumping data for table `ai_operator`
--


INSERT INTO ai_operator VALUES (1,'>','Greater than',0);
INSERT INTO ai_operator VALUES (2,'<','Less than',0);
INSERT INTO ai_operator VALUES (3,'>=','Greater than or equal to',1);
INSERT INTO ai_operator VALUES (4,'<=','Less than or equal to',1);
INSERT INTO ai_operator VALUES (5,'==','Equal to',0);

