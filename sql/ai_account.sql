CREATE TABLE `ai_account` (
  `username` varchar(50) NOT NULL default '',
  `account_id` varchar(255) NOT NULL default '',
  `account_name` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `contact_name` varchar(255) NOT NULL default '',
  `phone_number` varchar(255) NOT NULL default '',
  `broker_username` varchar(255) default NULL,
  `broker_password` varchar(255) default NULL,
  `notes` longtext NOT NULL,
  UNIQUE KEY `username` (`username`,`account_id`)
) ENGINE=MyISAM;

INSERT INTO `ai_account` VALUES ('demo', '123-456-7890', 'Foobar Brokerage Firm, Inc.', 'Roth IRA Retirement Account', 'John Doe', '555-555-5555', 'username', 'password', '');
INSERT INTO `ai_account` VALUES ('demo', 'Moving Averages', 'Moving Average Experimental Portfolio', 'Moving Average Experimental Portfolio', 'Me', '555-5555', 'username', 'password', '');
