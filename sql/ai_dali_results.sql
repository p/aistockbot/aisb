-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_dali_results`
--

CREATE TABLE ai_dali_results (
  ticker varchar(20) NOT NULL default '',
  dmi_result_up varchar(20) NOT NULL default '',
  support float default NULL,
  resistance float default NULL,
  period int(11) default NULL,
  date date default NULL,
  macd varchar(20) default NULL,
  vwma varchar(20) default NULL
) ENGINE=MyISAM;

--
-- Dumping data for table `ai_dali_results`
--



