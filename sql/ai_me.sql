-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_me`
--

CREATE TABLE ai_me (
  id tinyint(4) NOT NULL auto_increment,
  name varchar(255) default NULL,
  pronunciation varchar(255) default NULL,
  score bigint(20) default NULL,
  UNIQUE KEY id (id)
) ENGINE=MyISAM COMMENT='Information about the bot and how it is doing';

--
-- Dumping data for table `ai_me`
--


INSERT INTO ai_me VALUES (1,'Vooch','voo ooch',0);

