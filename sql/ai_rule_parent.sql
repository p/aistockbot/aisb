-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_rule_parent`
--

CREATE TABLE ai_rule_parent (
  id bigint(20) NOT NULL auto_increment,
  description varchar(255) default NULL,
  formula varchar(255) default NULL,
  variable_left bigint(20) NOT NULL,
  operator bigint(20) NOT NULL,
  variable_right bigint(20) NOT NULL,
  active tinyint(4) NOT NULL default '1',
  UNIQUE KEY id (id)
) ENGINE=MyISAM COMMENT='Rule set which humans create';

--
-- Dumping data for table `ai_rule_parent`
--


INSERT INTO ai_rule_parent VALUES (1,'BAD RULE BUT USED FOR TESTING:  If today\'s closing price is better than yesterday\'s closing price, then give a buy recommendation.','$close > $yesterday_close',1,1,33,0);
INSERT INTO ai_rule_parent VALUES (2,'DATA INTEGRITY CHECKER','$high >= $close',18,3,1,1);
INSERT INTO ai_rule_parent VALUES (4,'DATA INTEGRITY CHECKER','$high >= $low',18,3,19,1);

