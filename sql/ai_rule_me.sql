-- MySQL dump 8.23
--
-- Host: localhost    Database: enhanced
---------------------------------------------------------
-- Server version	4.0.16

--
-- Table structure for table `ai_rule_me`
--

CREATE TABLE ai_rule_me (
  id bigint(20) NOT NULL auto_increment,
  description varchar(255) NOT NULL default '',
  formula varchar(255) NOT NULL default '',
  active tinyint(4) NOT NULL default '1',
  UNIQUE KEY id (id)
) ENGINE=MyISAM COMMENT='Rule set which computer creates';

--
-- Dumping data for table `ai_rule_me`
--



