<?php
# index.php
#
# ATTENTION DEVELOPERS:  IF YOU MAKE CHANGES TO THIS SCRIPT, YOU NEED
#                        TO SPECIFY YOUR NAME WHEN MAKING CHANGES.
#
#
#
# 2014-11-28  FS  Deactivated speech output for code clean-up
# 2009/04/10  FS  Fixed redirect to setup.php, which under Linux did not work correctly
# 2006/11/23  JS  added check to see if we are configured, if not goto setup page
# 2004/12/14  MS  Corrected typo with PATH_TRANSLATED and fixed $pr to conform to php-4.3.x standards
# 2004/05/14  MS  Added a Welcome page if no $username is given
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/04/27  MS  Fixed $global_sound
# 2004/04/23  FS  Use $global_sound
# 2004/04/10  MS  Placed Portfolio Management System (PMS) on main page and moved Activation Report to menu link
# 2003/05/06  MS  Convert html to 100% php
# 2003/04/10  MS  Added extract($_GET) function
# 2002/10/27  MS  Added variable +/- to turn on/off
#

# 2009-04-10 FS Changed this:
## include_once('include/install_functions.php');
## if(! file_exists("include/config.php" ) )
##   redirect("./setup.php", 307);
# to this:
if(! file_exists("include/config.php" ) ) {
  require_once('include/install_functions.php');
  $redirect_destination = substr($_SERVER[PHP_SELF],0,-9);  // -9 due to length of index.php
  $redirect_destination .= "setup.php";
  redirect($redirect_destination, 307);
}

#####  Global Variables ######
// No need for if(!isset($x)) because this page should override
// the variable definitions on any page included by $pr
// This implies that no page should include index.php (so don't.)

$path="./";

# 2004-08-14 <vooch> This error:
# $base_dir=substr($_SERVER[PATH_TRANSLATED],0,-9);  // -9 due to length of "index.php"
# corrected to this:
if(phpversion() > '5.0.') {
	$base_dir=substr($_SERVER["ORIG_PATH_TRANSLATED"],0,-9);  // -9 due to length of "index.php"
} else {
	$base_dir=substr($_SERVER["PATH_TRANSLATED"],0,-9);  // -9 due to length of "index.php"
}



###
# 2004-04-18
# <vooch>  Message for ewinterhof: this conforms to php-4.3.x standards
if (!isset($pr)) { $pr=NULL; }
###



##### Header Inclusion #######
// I added this section incase any page would rather include a different header.
// I assume header_invisible.php was there for a reason. 
// If the included page wants the whole screen (*big if*) just send with 
// header=invisible. Most of the time it is fine to ignore this part.

if (isset($header) &&
    $header == 'invisible')
  include_once("{$path}include/header_invisible.php");
 else
   include_once("{$path}include/header.php");



##### Page Reference Expansion Table #####
// This is here so you can write links to index.php?pr=manage_db
// instead of index.php?pr=maintenance/manage_db.php
// Feel free to add shortcuts to your own pages.

$pr_expand=array('manage_db' => 'maintenance/manage_db.php',
		'import_csv' => 'tools/import_csv.php',
	       );

##########################################


if(isset($pr_expand[$pr])) {
  $pr = $pr_expand[$pr];
}


if (isset($pr)){ // $pr is shorthand for page reference;
  include("$path"."$pr");
 } else {


  extract($_GET);

  print "<table width=100%>";
  print "<tr>";
  print "<td width=100%>";
  print "<font face=arial size=-1>";

  print "<table width=100% background=images/monopoly_money.jpg>";
  print "<tr>";
  print "<td width=100% align=center>";
  
##### Default Welcome Page #####

  if ($username!="") {
# 2004/04/10: Let's switch from Activation Report to Portfolio Management System on the main page
# include_once("{$path}activation/activation_report.php");
    include_once("{$path}pms/portfolio_output_1.php");
  } else {
    
    print "<br><br><br><br>";
    print "<a href=http://www.aistockbot.com><img border=0 src=\"images/logo.jpg\"></a><br>";
    print "<b>Welcome to AIStockbot</b><br>";
    print "<b>Version ".db_query_config("version")."</b><br>";
    print "<b>Alpha Release</b><br>";
    print "<br>";
    print "The default login is:<br>";
    print "Username = <b>demo</b><br>";
    print "Password = <b>demo</b><br>";
    print "<br><br><br><br><br><br><br><br><br><br><br><br>";
    
  }
  
  print "</td>";
  print "</tr>";
  print "</table>";
  
# Enable this when I want sound and get php working again
// Does anyone still use this? Vooch?
/*  if(isset($flag)){
  if (db_query_config("sound")) {
    	exec ("./index.sh $myvar");
    
	# The <OBJECT> Tag is HTML4-compliant and used for Mozilla 1.1.  The other
	# tags will be deprecated.
	# The <EMBED> Tag works in Internet Explorer 4.0x, 5.0x, Navigator 3.0x, 4.0x 
	# The <BGSOUND> Tag works in Internet Explorer 3.0x (best used with the NOEMBED tag... see below)
	
	# THIS TAG IS NOT WORKING YET, BUT IT'S IN THE HTML 4 RECOMMENDATIONS
	# <OBJECT DATA="index.wav" TYPE="audio/x-wav" height=1 width=1>
	# <PARAM NAME=autostart VALUE=true>
	# <PARAM NAME=hidden VALUE=true>
	# </OBJECT>

	# <!--- Alternative method --->
	print "<EMBED SRC=index.wav AUTOSTART=TRUE LOOP=FALSE height=1 width=1 align=center>";
	print "<NOEMBED>";
	print "<BGSOUND SRC=index.wav LOOP=1>";
	print "</NOEMBED>";
	print "</EMBED>";



	
  } # end if (db_query_config("sound"))
     }# end in (isset($flag))
*/

  print "</td>";
  print "</tr>";
  print "</table>";
 }
include_once("{$path}include/footer.php");
?>
