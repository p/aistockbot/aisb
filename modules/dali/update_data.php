<?php

if (empty($path)) {
        $path="../../";
} // end if (!$path)
//include_once("{$path}include/header.php");
//include_once("{$path}include/database.php");


function update_data($symbol) {

  $path = "../../";
  $current_ticker = $symbol;

  $sqlcompany=db_query("
        SELECT  ticker
        FROM    ai_company
        WHERE   active=1
        AND     download=1
        ");

  while ($row=db_fetch_array($sqlcompany)) {
        $current_ticker=$row["ticker"];

        # when you set download=0, it means you are downloading the data and don't need to get it again.
        db_query("update ai_company set download=0 where ticker='$current_ticker'");

        $sqlhistory=db_query("
                SELECT          ticker, date, close
                FROM            ai_history
                WHERE           ticker='$current_ticker'
                ORDER BY        date DESC
        ");

        $numrows_history=db_num_rows($sqlhistory);

        if ($numrows_history) {
                #
                # Compare last 30-days of data in history against
                # the same 30-days at yahoo.com.  We probably can also
                # just get the last day and not 30-days.
                #
                $rowsqlhistory=db_fetch_array($sqlhistory);
                $last_date=$rowsqlhistory["date"];
                $last_price=$rowsqlhistory["close"];
                //print "Downloading data for $current_ticker.<br>";
                $f=substr($last_date,0,4);  # Year
                $d=substr($last_date,5,2);  # Month
                $d=$d-1;                    # finance.yahoo.com takes the month and subtracts one,
                                            # which means January is 0
                $e=substr($last_date,8,2);  # Day

                # Grab a month's worth of data
                $b=$e;   # Day
                $c=$f;   # Year
                $a=$d-1; # Month
                        if ($a == -1) {  # January is 0, but if in January and you roll back to -1, then:
                                $a=11; # 11=December
                                $c=$c-1; # Roll the year back to last year
                        }

                # Get data from yahoo.com
                $ticker_file="{$path}csv/$current_ticker.csv";
                $website="http://table.finance.yahoo.com/table.csv?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f&s=$current_ticker&y=0&ignore=.csv";
                $fcontents=@implode("",file($website));
                $tmpfile=fopen($ticker_file,"w");
                $fp=fwrite($tmpfile,$fcontents);
                fclose($tmpfile);

                # Initialize variable
                $flag1=0;

                $fp = fopen ("$ticker_file", "r");
                while ($data=fgetcsv($fp, 7000, ",")) {
                        if ($data[0] != "Date") {
                                if ($flag1!=1) {
                                        $new_close=$data[4];
                                        # print "$new_close<br>";
                                        $flag1=1; # Raise a flag so I do this subroutine only once.
                                }
                        }
                }
                fclose ($fp);

                if ($new_close == $last_price) {

                        # echo "prices match $new_close vs. $last_price";
                        # Since the data is the same, then grab the data
                        # from yahoo.com starting with the newest
                        # date that is in the history table for that stock.
                        # We are doing this because of stock splits and
                        # we want to get adjusted figures.
                        $a=$d; # Grab month from above as the starting month.
                        $b=$e+1; # Grab day from above as the starting day.
                                 # Add one to it so I don't get data I already
                                 # have.
                        $c=$f; # Grab the year above as the starting year.
                        $d=date('m');   #Today's month (two digits)
                        $d=$d-1;        # Subtract 1 (yahoo treats Jan as 0).
                        $e=date('d');   #Today's day (two digits)
                        $f=date('Y');   #Today's year (four digits)

#                       //print "aaaa=$a b=$b c=$c d=$d e=$e f=$f";

                        # Get data from yahoo.com
                        $ticker_file="{$path}csv/$current_ticker.csv";
                        $website="http://table.finance.yahoo.com/table.csv?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f&s=$current_ticker&y=0&ignore=.csv";
                                $fcontents=@implode("",file($website));
                                $tmpfile=fopen($ticker_file,"w");
                                $fp=fwrite($tmpfile,$fcontents);
                                fclose($tmpfile);

                        # Add contents to database
                        # REQUIRES:  $ticker_file and $current_ticker
                        $row=2;
                        $fp = fopen ("$ticker_file", "r");
                        while ($data=fgetcsv($fp, 7000, ",")) {
                                if ($data[0]!="Date" and !empty($data[1]) and !empty($data[2])) {
                                        # Convert date format from dd-mmm-yy or d-mmm-yy
                                        # to ISO 8601 international
                                        # format (yyyy/mm/dd)
                                        $day=substr($data[0],0,2);
                                        $day=str_replace("-","","$day");

                                        $month=substr($data[0],3,3);
                                        if ($month == "Jan" ) { $month="01"; }
                                        if ($month == "Feb" ) { $month="02"; }
                                        if ($month == "Mar" ) { $month="03"; }
                                        if ($month == "Apr" ) { $month="04"; }
                                        if ($month == "May" ) { $month="05"; }
                                        if ($month == "Jun" ) { $month="06"; }
                                        if ($month == "Jul" ) { $month="07"; }
                                        if ($month == "Aug" ) { $month="08"; }
                                        if ($month == "Sep" ) { $month="09"; }
                                        if ($month == "Oct" ) { $month="10"; }
                                        if ($month == "Nov" ) { $month="11"; }
                                        if ($month == "Dec" ) { $month="12"; }

                                        if ($month == "an-" ) { $month="01"; }
                                        if ($month == "eb-" ) { $month="02"; }
                                        if ($month == "ar-" ) { $month="03"; }
                                        if ($month == "pr-" ) { $month="04"; }
                                        if ($month == "ay-" ) { $month="05"; }
                                        if ($month == "un-" ) { $month="06"; }
                                        if ($month == "ul-" ) { $month="07"; }
                                        if ($month == "ug-" ) { $month="08"; }
                                        if ($month == "ep-" ) { $month="09"; }
                                        if ($month == "ct-" ) { $month="10"; }
                                        if ($month == "ov-" ) { $month="11"; }
                                        if ($month == "ec-" ) { $month="12"; }

                                        $year=substr($data[0],6,3);
                                        $year=str_replace("-","","$year");

                                        if ($year < 20) {
                                                $year="20$year";
                                        } else {
                                                $year="19$year";
                                        }
                                        $revised_date="{$year}-{$month}-{$day}";

                                        $sql="
                                        INSERT INTO ai_history
                                                (ticker
                                                , date
                                                , open
                                                , high
                                                , low
                                                , close
                                                , volume)
                                        VALUES
                                                ('$current_ticker'
                                                , '$revised_date'
                                                , $data[1]
                                                , $data[2]
                                                , $data[3]
                                                , $data[4]
                                                , $data[5])";

                                        $result=db_query("$sql");
                                        if (!$result) {
                                                //print "Error performing query - Data may already exist or ticker not found<br>";
                                        } else {
                                                # do nothing because record added successfully.
                                        }
                                } # End of (if $data[0] != "Date")
                        }
                        fclose ($fp);
#                       db_query("update ai_company set active=0 where ticker='$current_ticker'");

                } else {
                        # If the data is NOT the same, then delete
                        # all the records in the history table, and
                        # get all the data
                        //print "Resyncing data for $current_ticker<br>";
                        db_query("DELETE FROM ai_history WHERE ticker='$current_ticker'");
                        # When you set download=1, it means you want to get the data.
                        db_query("UPDATE ai_company SET download=1 WHERE ticker='$current_ticker'");
                }

                flush();


        } else {
                //print "You do not have data for $current_ticker ... Getting Data<br>";

                $a=0;           # Month:  0=January...11=December
                $b=1;           # Day
                $c=2004;        # Year
                $d=date('m');   # Today's month (two digits)
                $d=$d-1;        # Subtract 1 because yahoo treats Jan as 0.
                $e=date('d');   # Today's day (two digits)
                $f=date('Y');   # Today's year (four digits)

                # Get data from yahoo.com
                $ticker_file="{$path}csv/$current_ticker.csv";
                $website="http://table.finance.yahoo.com/table.csv?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f&s=$current_ticker&y=0&ignore=.csv";
                $fcontents=@implode("",file($website));
                $tmpfile=fopen($ticker_file,"w");
                $fp=fwrite($tmpfile,$fcontents);
                fclose($tmpfile);
                # Add contents to database
                # REQUIRES:  $ticker_file and $current_ticker
                $row=2;
                $fp = fopen ("$ticker_file", "r");
                while ($data=fgetcsv($fp, 7000, ",")) {
                        if ($data[0]!="Date" and !empty($data[1]) and !empty($data[2])) {
                                # Convert date format from dd-mmm-yy or d-mmm-yy
                                # to ISO 8601 international
                                # format (yyyy/mm/dd)
                                $day=substr($data[0],0,2);
                                $day=str_replace("-","","$day");

                                $month=substr($data[0],3,3);
                                if ($month == "Jan" ) { $month="01"; }
                                if ($month == "Feb" ) { $month="02"; }
                                if ($month == "Mar" ) { $month="03"; }
                                if ($month == "Apr" ) { $month="04"; }
                                if ($month == "May" ) { $month="05"; }
                                if ($month == "Jun" ) { $month="06"; }
                                if ($month == "Jul" ) { $month="07"; }
                                if ($month == "Aug" ) { $month="08"; }
                                if ($month == "Sep" ) { $month="09"; }
                                if ($month == "Oct" ) { $month="10"; }
                                if ($month == "Nov" ) { $month="11"; }
                                if ($month == "Dec" ) { $month="12"; }

                                if ($month == "an-" ) { $month="01"; }
                                if ($month == "eb-" ) { $month="02"; }
                                if ($month == "ar-" ) { $month="03"; }
                                if ($month == "pr-" ) { $month="04"; }
                                if ($month == "ay-" ) { $month="05"; }
                                if ($month == "un-" ) { $month="06"; }
                                if ($month == "ul-" ) { $month="07"; }
                                if ($month == "ug-" ) { $month="08"; }
                                if ($month == "ep-" ) { $month="09"; }
                                if ($month == "ct-" ) { $month="10"; }
                                if ($month == "ov-" ) { $month="11"; }
                                if ($month == "ec-" ) { $month="12"; }

                                $year=substr($data[0],6,3);
                                $year=str_replace("-","","$year");

                                if ($year < 20) {
                                        $year="20$year";
                                } else {
                                        $year="19$year";
                                }
                                $revised_date="$year/$month/$day";

                                $sql="
                                INSERT INTO ai_history
                                        (ticker
                                        , date
                                        , open
                                        , high
                                        , low
                                        , close
                                        , volume)
                                VALUES
                                        ('$current_ticker'
                                        , '$revised_date'
                                        , $data[1]
                                        , $data[2]
                                        , $data[3]
                                        , $data[4]
                                        , $data[5])";

                                $result=db_query($sql);
                                if (!$result) {
                                        # 2004/09/09 If there's no data in the table, it got it correctly, so don't
                                        # show this message:
                                        # echo "Error performing query - data may already exist or ticker not found";
                                } else {
                                        # do nothing because record added successfully.
                                }
                        } # End of (if $data[0] != "Date")
                }
                fclose ($fp);

#               db_query("update ai_company set download=0 where ticker='$current_ticker'");
	}
}
}

?>
