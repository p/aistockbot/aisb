<?php
# modules/dali/dali.php
#
# 2013/11/18  FS  Changed sql column signal to dmi_result_up because signal is reserved in MySQL
# 2004/05/30  NK  Created bot
# 

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");
include_once("equations.php");
include_once("update_data.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

if (isset($_POST['random'])) {
  // update technical data

  $a_query = "select ticker from ai_company ORDER BY RAND() LIMIT 100";
  //$a_query = "SELECT ticker from ai_company";
  $a_res = db_query($a_query);

  while ($line = mysql_fetch_array($a_res, MYSQL_ASSOC)) {
    db_query("update ai_company set active=0");
    $symbol = $line["ticker"];
    db_query("update ai_company set download=1,active=1 where ticker='$symbol'");
    update_data($symbol);

    $period = 14;
    $dmi_result_up = calculate_dmi($symbol, $period);


    $fibonacci_result = calculate_fibonacci($symbol, $period);
    $support = $fibonacci_result['support'];
    $resistance = $fibonacci_result['resistance'];

    $macd = calculate_macd($symbol);
    $vwma = calculate_vwma($symbol, $period);

    //$date = 

    $b_query = "INSERT INTO ai_dali_results (ticker, dmi_result_up, support, resistance, period, date, macd, vwma) values ('$symbol','$dmi_result_up','$support','$resistance','$period',NOW(), '$macd', '$vwma')";
    $b_res = db_query($b_query);
  }

  $c_query = "SELECT * from ai_dali_results ORDER BY dmi_result_up ASC";
  $c_res = db_query($c_query);

  // print ai_dali_results to html table
  print "<table border=1>";
  print "<tr>";
  print "<td align=\"center\">Symbol</td>";
  print "<td align=\"center\">Signal</td>";
  print "<td align=\"center\">Support</td>";
  print "<td align=\"center\">Resistance</td>";
  //print "<td align=\"center\">Period</td>";
  print "<td align=\"center\">MACD</td>";
  print "<td align=\"center\">VWMA</td>";
  print "</tr>";
  while ($line_res = mysql_fetch_array($c_res, MYSQL_ASSOC)) {
    print "<tr>";
    print "<td align=\"center\">". $line_res["ticker"] ."</td>";
    $signal = $line_res["dmi_result_up"];
    $support = $line_res["support"];
    $resistance = $line_res["resistance"];
    $period = $line_res["period"];
    $macd = $line_res["macd"];
    $vwma = $line_res["vwma"];
    //print "s: $signal s: $support r: $resistance p: $period";

    if ($line_res["dmi_result_up"] == 'long') {
      print "<td align=\"center\" bgcolor=\"#E0FFE3\">$signal</td>";
    } elseif ($signal == 'short') {
      print "<td align=\"center\" bgcolor=\"#FFF5F6\">$signal</td>";
    } else {
      print "<td align=\"center\">$signal</td>";
    }
    print "<td align=\"center\">$support</td>";
    print "<td align=\"center\">$resistance</td>";
    //print "<td align=\"center\">$period</td>";

    if ($macd == 'buy') {
      print "<td align=\"center\" bgcolor=\"#E0FFE3\">$macd</td>";
    } elseif ($macd == 'sell') {
      print "<td align=\"center\" bgcolor=\"#FFF5F6\">$macd</td>";
    } else {
      print "<td align=\"center\">$macd</td>";
    }
    if ($vwma == 'buy') {
      print "<td align=\"center\" bgcolor=\"#E0FFE3\">$vwma</td>";
    } elseif ($vwma == 'sell') {
      print "<td align=\"center\" bgcolor=\"#FFF5F6\">$vwma</td>";
    } else {
      print "<td align=\"center\">$vwma</td>";
    }

    print "</tr>";
  }
  print "</table>";

} elseif (isset($_POST['symbol'])) {

  // update technical data
  db_query("update ai_company set active=0");
  db_query("update ai_company set download=1,active=1 where ticker='$symbol'");

  update_data($symbol);


print "<table width=100% border=1>";
print "<tr>";
print "<td width=99% colspan=3 align=center>";
print "<center>Executing..</center><br>";
print "</td>";
print "</tr>";
print "\n";

#$symbol = "SIRI";
$period = 14;
$signal = calculate_dmi($symbol, $period);

$fibonacci_result = calculate_fibonacci($symbol, $period);
$support = $fibonacci_result['support'];
$resistance = $fibonacci_result['resistance'];

$vwma = calculate_vwma($symbol, $period);

$macd = calculate_macd($symbol);

print "<tr>";
print "<td>";
print "<center>Completed.</center>";
print "</td>";
print "</tr>";
print "</table>";

print "<br>";
print "<center>";
print "<table border=1>";
print "<tr>";
print "<td align=\"center\">Signal</td>";
print "<td align=\"center\">Support</td>";
print "<td align=\"center\">Resistance</td>";
print "<td align=\"center\">Period</td>";
print "</tr>";
print "<tr>";

if ($signal == 'long') {
  print "<td align=\"center\" bgcolor=\"#E0FFE3\">$signal</td>";
} elseif ($signal == 'short') {
  print "<td align=\"center\" bgcolor=\"#FFF5F6\">$signal</td>";
} else {
  print "<td align=\"center\">$signal</td>";
}
print "<td align=\"center\">$support</td>";
print "<td align=\"center\">$resistance</td>";
print "<td align=\"center\">$period days</td>";
print "</tr>";
print "</table></center><br><br>";

print "<center>";
print "<table border=1>";
print "<tr>";
print "<td>Indicator</td>";
print "<td>Signal</td>";
print "</tr>";
print "<tr>";
print "<td>VWMA</td>";
print "<td>$vwma</td>";
print "</tr>";
print "<tr>";
print "<td>MACD</td>";
print "<td>$macd</td>";
print "</tr>";
print "</table></center><br><br>";




} else {

  print "Enter a symbol!<br>";

}

include_once("{$path}include/footer.php");
?>
