<?php
#
# stage_technical_data.php
# This program gives the buy/sell signal based on the best result.
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/09/09  MS  Code cleanup
# 2002/11/06  MS  Fixed memory leaks
# 2002/10/28  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
set_time_limit (600);

print "<table width=100%>";
print "<tr>";
print "<td width=100% background=../images/hex.gif>";
print "<FONT face=Arial size=-1>";

# MYSQL QUERY STATEMENT DEFINITION SECTION:
# Define MySQL Query Statements

print "<b>STEP:  STAGE TECHNICAL DATA</b><br><br>";

db_query("
	UPDATE	ai_company
	SET	download=1
	WHERE	active = 1
");

print "Updating company table completed<br><br>";
print "You should run this program when you're ready to begin Update Technical Data.<br><br>";
print "This Step should be run no more than once per day.<br><br>";
print "If your Update Technical Data process breaks, then SKIP this Step so you don't redownload all the data again.  By skipping this step, you'll be saving alot of unnecessary downloads, but be sure to run this Step no more than once per day or you'll end up resetting the download process and begin downloading all the stocks again.<br><br>";
print "It's OK to run UPDATE Technical Data multiple times per day.<br><br>";

print "<b>END OF STEP:  STAGE TECHNICAL DATA</b><br>";

include_once("{$path}include/footer.php");
?>

