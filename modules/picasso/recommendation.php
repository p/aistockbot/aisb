<?php
#
# recommendation.php
# This program gives the buy/sell signal based on the best result.
#
# 2004/07/16  MS  Changed path to "../../" because we're in "modules/picasso/" now
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use settings from ai_config
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/09/09  MS  Added $path
# 2002/11/06  MS  Fixed memory leaks
# 2002/11/03  MS  Fixed bugs and shortened script by setting seek pointer to 0
# 2002/10/27  MS  Initial Release
#

# To comply with php-4.3.x:
if(!isset($ticker)) { $ticker=""; }

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");

print "<table width=100%>";
print "<tr>";
print "<td width=100% background={$path}images/hex.gif>";
print "<FONT size=-1>";

# Initialize variables - used for aistockbotpro
$signal = "MORNING\ OF\ ".date("M")."\ ".date("d")."\ ".date("Y").":";
#$signal = ereg_replace(" ", "X", $signal);

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
set_time_limit (60000);

# Start Flite Speech Synthesis wording
$learn="Here are my recommendations";

# MYSQL QUERY STATEMENT DEFINITION SECTION:
# Define MySQL Query Statements

#$sqlcompany=db_query("
#	SELECT 		ticker
#	FROM		ai_company
#	WHERE		active = 1
#	ORDER BY	ticker
#	");


#$sqldistinctcombinations=db_query("
#	SELECT 		combination_id AS id, SUM(score) AS grandtotal, AVG(percentage_of_days_score_above_zero) AS average_percentage_of_days_score_above_zero, AVG(percentage_running_score_above_zero) AS average_percentage_running_score_above_zero
#	FROM		ai_results
#	GROUP BY	combination_id
#	ORDER BY 	grandtotal DESC
#	LIMIT		1
#	");

# Grab the best combination...
$sqldistinctcombinations=db_query("
	SELECT 		combination_id, SUM(score) AS grandtotal
	FROM		ai_results
	GROUP BY	combination_id
	ORDER BY 	grandtotal DESC
	LIMIT		1
	");

$getsqldistinctcombinations=db_fetch_array($sqldistinctcombinations);
$winningid=$getsqldistinctcombinations["combination_id"];

# ...and recommend companies whose strategy worked for the best combination.
# This prevents showing a company who lost money based on the best strategy.
# Hmmm.... What happens if the person owns the stock already?  They won't get
# a recommendation based on the best combination if the results of that
# combination were negative for the company.
$sqlcompany=db_query("
	SELECT DISTINCT	ai_company.ticker AS ticker, ai_results.score
	FROM		ai_company
	INNER JOIN	ai_results
	ON		ai_company.ticker = ai_results.ticker
	WHERE		ai_company.active = 1
	AND		ai_results.score > 0
	AND		ai_results.combination_id = $winningid
	ORDER BY	ticker
	");

$sqlrecommendation=db_query("
	SELECT		ticker, date, recommendation
	FROM 		ai_history
	WHERE		ticker = '$ticker'
	ORDER BY 	date DESC
	LIMIT		2
	");

# Set SQL Queries For Fetching
$getsqlcompany=db_fetch_array($sqlcompany);
$getsqlrecommendation=db_fetch_array($sqlrecommendation);

print "<b>STEP:  RECOMMENDATIONS</b><br><br>";

# FIRST RECORD INITIALIZATION SECTION:
$ticker	= $getsqlcompany["ticker"];

# RESET POINTER
# This allows me to start with the first company
# This saved me having to repeat the recommendation process
mysql_data_seek($sqlcompany, 0);

if (db_query_config("aistockbotpro")==1) {
	# Create Stock List
	$ticker_file="/var/www/html/aistockbotPRO/netcat/stocks";
	$website="http://localhost/aistockbot-".db_query_config("version")."/echo_stocks.php";
	$fcontents=implode("", file($website));
	$tmpfile=fopen($ticker_file, "w");
	$fp=fwrite($tmpfile,$fcontents);
	fclose($tmpfile);
	### exec ("/var/www/html/aistockbotPRO/netcat/echo_stocks.sh");

	# Create Ticker List to listen for
	$ticker_file="/var/www/html/aistockbotPRO/netcat/wordonthestreet";
	$website="http://localhost/aistockbot-".db_query_config("version")."/echo_wordonthestreet.php";
	$fcontents=implode("", file($website));
	$tmpfile=fopen($ticker_file, "w");
	$fp=fwrite($tmpfile,$fcontents);
	fclose($tmpfile);
	### exec ("/var/www/html/aistockbotPRO/netcat/echo_wordonthestreet.sh");

	# Start Recommendations
	# exec ("/var/www/html/aistockbotPRO/netcat/echo.sh AISTOCKBOT RECOMMENDATIONS-Start-of-Report");
}

# LOOP THROUGH EVERY ID IN COMBINATION TABLE
while ($getsqlcompany=db_fetch_array($sqlcompany)) {
	$ticker = $getsqlcompany["ticker"];

#	print "ticker = $ticker<br>";

	$sqlrecommendation=db_query("
		SELECT		ticker, date, recommendation
		FROM 		ai_history
		WHERE		ticker = '$ticker'
		ORDER BY 	date DESC
		LIMIT		2
		");

	$cpt=0;

	# Process recommendations
	while ($getsqlrecommendation = db_fetch_array($sqlrecommendation)) {
		$rec[$cpt] = $getsqlrecommendation["recommendation"];
#		$tkr[$cpt] = $getsqlrecommendation["ticker"];
#		$dt[$cpt]  = $getsqlrecommendation["date"];
#		print "tkr=$tkr[$cpt] - Recommendation = rec[$cpt]=$rec[$cpt] and cpt=$cpt and dt=$dt[$cpt]<br>";
		$cpt=$cpt+1;
	}		
	# Make Recommendation
	# [0] is today
	# [1] is yesterday
	if ($rec[0] != $rec[1]) {
		if ($rec[0]==1) {
			print "BUY $ticker<br>";  # rec[0]=$rec[0] and rec[1]=$rec[1]<br>";
			if (db_query_config("aistockbotpro")==1) {
				# exec ("/var/www/html/aistockbotPRO/netcat/echo.sh BUY $ticker");
				$signal="$signal\ \ \ BUY\ $ticker";
			}
		} else {
			print "SELL $ticker<br>";
			if (db_query_config("aistockbotpro")==1) {
				# exec ("/var/www/html/aistockbotPRO/netcat/echo.sh SELL $ticker");
				$signal="$signal\ \ \ SELL\ $ticker";
			}
		}
	}
}

if (db_query_config("aistockbotpro")==1) {
	# exec ("/var/www/html/aistockbotPRO/netcat/echo.sh End-of-Report USE-AT-YOUR-OWN-RISK");

	exec ("/var/www/html/aistockbotPRO/netcat/echo.sh AISTOCKBOT RECOMMENDATIONS\ $signal\ \ \ :-USE-AT-YOUR-OWN-RISK");
}

print "<br><br><b>WARNING:  USE AT YOUR OWN RISK.  THIS PRODUCT DOES NOT WORK AS INTENDED!</b><br>";


print "<b>END OF STEP: RECOMMENDATIONS</b><br>";
print "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";

include_once("{$path}include/footer.php");
?>

