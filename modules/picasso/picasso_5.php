<?php
#
# picasso_5.php
#
# 2004/07/16  MS  Had to add include/config.php
# 2004/07/16  MS  New versions of MySQL do not reset to 1 when a table is empty, so we need to force it.
#                 WARNING:  This does not work on InnoDB Tables, so DO NOT USE InnoDB as a Table type for this table
# 2004/07/16  MS  Reverted back to version 1.5 to get this script working again
#		  Changed path to "../../" since we're in modules/picasso/ now
# 2004/05/17  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/01/22  MS  Changed from 10 to 50 on each side
# 2003/05/26  MS  Added Sellside
# 2003/05/07  MS  Initial Release
#

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
# 2004/07/16:  Had to add include/config.php for linux boxes because header.php is probably using the wrong path
#              because this one is ../../ instead of the customary ../
include_once("{$path}include/config.php");
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

print "<table width=100%>";
print "<tr><td width=100%>";
print "<font face=arial size=-1>";

print "<b>Flush Tables</b><br>";

print "Deleting all records in the <b>rule_potential</b> table.<br>" ;
# Wipe out rule_potential table
db_query("
	DELETE FROM ai_rule_potential
	");

# Reset auto_increment on table ai_rule_potential
# 2004/07/16  MS  New versions of MySQL do not reset to 1 when a table is empty, so we need to force it.
#                 WARNING:  This does not work on InnoDB Tables, so DO NOT USE InnoDB as a Table type for this table
db_query("
	ALTER TABLE ai_rule_potential AUTO_INCREMENT = 1
	");

print "Deleting all records in the <b>rule_potential_sellside</b> table.<br>" ;
# Wipe out rule_potential_sellside table
db_query("
	DELETE FROM ai_rule_potential_sellside
	");

# Reset auto_increment on table ai_rule_potential_sellside
# 2004/07/16  MS  New versions of MySQL do not reset to 1 when a table is empty, so we need to force it.
#                 WARNING:  This does not work on InnoDB Tables, so DO NOT USE InnoDB as a Table type for this table
db_query("
	ALTER TABLE ai_rule_potential_sellside AUTO_INCREMENT = 1
	");

print "Deleting all records in the <b>combinations</b> table.<br>";
# Wipe out combinations table
db_query("
	DELETE FROM ai_combinations
");

print "<br>";

# BUY SIDE
print "<b>Building Buy Side</b><br>";
# Initialize record counters
$y=0;
$z=0;
# Build formulas
for ($x=1; $x < $c; $x++) {
	$leftside=${'leftside'.$x};
	$operator=${'operator'.$x};
	$rightside=${'rightside'.$x};
	if (!$leftside) {
		# Don't do anything...continue on to the next row
		continue;
	} else {
		print "Creating <b>rule_potential</b> record(s)<br>" ;
		# We've got a formula, so let's create it!
		$formula = "$leftside $operator $rightside";
		# Write formula
		db_query("
		INSERT INTO ai_rule_potential
			(formula)
		VALUES
			('$formula')
		");
		print "$formula<br>";
		# Increment 'y' record count.
		$y++;
		print "Adding new record into the <b>combinations</b> table.<br>";
		# Build combination formula piece-by-piece
		db_query("
		INSERT INTO ai_combinations
			(id, rule_potential_id)
		VALUES
			(1, $y)
		");
		$z++;
	}
}
print "The <b>rule_potential</b> table build is complete. $y records added.<br>";
print "The <b>combinations</b> table build is complete. $z records added.<br>";

print "<br>";

# SELL SIDE
print "<b>Building Sell Side</b><br>";
# Initialize record counters
$y=0;
$z=0;
# Build formulas
# 2004/01/22:  Start out at 51 on the SellSide (1-50) is for BuySide  
for ($x=51; $x < $d; $x++) {
	$leftside=${'leftside'.$x};
	$operator=${'operator'.$x};
	$rightside=${'rightside'.$x};
	if (!$leftside) {
		# Don't do anything...continue on to the next row
		continue;
	} else {
		print "Creating <b>rule_potential_sellside</b> record(s)<br>" ;
		# We've got a formula, so let's create it!
		$formula = "$leftside $operator $rightside";

		# Write formula
		db_query("
		INSERT INTO ai_rule_potential_sellside
			(formula)
		VALUES
			('$formula')
		");

		print "$formula<br>";
		# Increment 'y' record count.
		$y++;
		print "Adding new record into the <b>combinations</b> table.<br>";
		# Build combinations_sellside formula piece-by-piece
		db_query("
		INSERT INTO ai_combinations
			(id, rule_potential_sellside_id)
		VALUES
			(1, $y)
		");
		$z++;
	}
}
print "The <b>rule_potential_sellside</b> table build is complete. $y records added.<br>";
print "The <b>combinations</b> table build is complete. $z records added.<br>";
print "<br>";
print "<a href=learn.php?{$menu_string}>Go To Next Step:  Calculate Results</a><br>";

print "</font>";
print "</td></tr>";
print "</table>";


include_once("{$path}include/footer.php");
?>
