<?php
#
# results.php
# Description:  This script gives the results
#
# 2004/07/16  MS  Temporarily disabled $menu_string
# 2004/07/16  MS  Changed path to "../../" since we're in modules/picasso now
# 2004/07/16  MS  Reverting back to earlier version
# 2004/05/17  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/09/09  MS  Added $path
# 2004/01/17  MS  Fixed $transactions divide by zero error
# 2003/10/28  MS  Fixed Top 10 error when there's less than 10 results
# 2003/09/25  MS  Initial Release
#

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);

####
# 2004/07/16  MS  Temporarily disabled:
# $menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));
####

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
set_time_limit (60000);

print "<font face=arial size=-1><b>STEP:  RESULTS</b></font><br>";

####################################
# GET THE AVERAGE STARTING PRICE

# Initialize variables
$sum_close=0;
$number_of_issues=0;
$average_starting_price=0;

$sql_history=db_query("
	SELECT 		DISTINCT ai_history.ticker AS ticker, min(ai_history.date) AS date
	FROM		ai_history
	INNER JOIN	ai_company
	ON		ai_history.ticker = ai_company.ticker
	WHERE		ai_company.active=1
	GROUP BY	ticker
	ORDER BY 	date
	");
while ($row_history=db_fetch_array($sql_history)) {
	# Select ticker and date
	# fetch close where ticker and date
	# sum all the first day closes
	# divide by number of issues
	# that'll give me average starting price
	$ticker		=$row_history["ticker"];
	$date		=$row_history["date"];
	$sql_history_2=db_query("
		SELECT	close
		FROM	ai_history
		WHERE	ticker	= '$ticker'
		AND	date	= '$date'
		");
	while ($row_history_2=db_fetch_array($sql_history_2)) {
		$close	=$row_history_2["close"];
		$sum_close = $sum_close+$close;
		$number_of_issues = $number_of_issues + 1;
		$average_starting_price=$sum_close/$number_of_issues;
	}
# print "d=$date t=$ticker c=$close s=$sum_close i=$number_of_issues a=$average_starting_price<br>";
}
#
########################################


# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlresults=db_query("
	SELECT 		combination_id, combinations_sellside_id, AVG(score) AS grandtotal, AVG(percentage_of_days_score_above_zero) AS average_percentage_of_days_score_above_zero, AVG(percentage_running_score_above_zero) AS average_percentage_running_score_above_zero, AVG(days_long) AS average_days_long, AVG(days_short) AS average_days_short, AVG(transactions) AS transactions
	FROM		ai_results
	GROUP BY	combination_id
	ORDER BY 	grandtotal DESC
	");

$numresults=db_num_rows($sqlresults);
# print "<b>$numresults combinations tested</b><br>";

# Fix Top 10 error when there's less than 10 results
$cmax=10;
if ($numresults < 10) {
	$cmax = (int)$numresults;
}

print "<table width=100% border=1>";

print "<tr>";
print "<td width=100% colspan=8 align=center>Top $cmax Combinations <i><font size=-2>(out of $numresults combinations tested)</font></i></td>";
print "</tr>";

print "<tr>";
print "<td width=30% align=center>Combination ID</td>";
print "<td width=10% align=center>Score (Avg. Profit & Cost)</td>";
print "<td width=10% align=center>Avg % Days Positive</td>";
print "<td width=10% align=center>Avg % Running Score Above 0</td>";
print "<td width=10% align=center>Number of Days Going Long</td>";
print "<td width=10% align=center>Number of Days Going Short</td>";
print "<td width=10% align=center>Trading Days per Roundtrip</td>";
print "<td width=10% align=center>Average Annual Profit Percentage</td>";
print "</tr>";

for ($c=0; $c < $cmax; $c++) {
	$row=db_fetch_array($sqlresults);
	$combination_id			=$row["combination_id"];
	$combinations_sellside_id	=$row["combinations_sellside_id"];
	$grandtotal			=$row["grandtotal"];
	$average_percentage_of_days_score_above_zero=$row["average_percentage_of_days_score_above_zero"];
	$average_percentage_running_score_above_zero=$row["average_percentage_running_score_above_zero"];
	$average_days_long		=$row["average_days_long"];
	$average_days_short		=$row["average_days_short"];
	$transactions			=$row["transactions"];

	# Get the average total days divided by number of transactions, then multiply by two because you buy and sell.
	if ($transactions!=0) {
		$days_per_roundtrip	= (($average_days_long+$average_days_short)/$transactions)*2;
	} else {
		$days_per_roundtrip	=0;
	}
	
	print "<tr>";
	# GET rule_parent FORMULAS
	$sqlrule_parent=db_query("SELECT formula FROM ai_rule_parent WHERE active=1");
	$formula="";
	$formula_sellside="";
	while ($rowrule_parent=db_fetch_array($sqlrule_parent)) {
		$newformula=$rowrule_parent["formula"];
		$formula="$formula <br> $newformula";
		$formula_sellside="$formula_sellside <br> $newformula";
	}
	# GET rule_me FORMULAS
	$sqlrule_me=db_query("SELECT formula FROM ai_rule_me");
	while ($rowrule_me=db_fetch_array($sqlrule_me)) {
		$newformula=$rowrule_me["formula"];
		$formula="$formula <br> $newformula";
		$formula_sellside="$formula_sellside <br> $newformula";
	}
	# GET combinations
	$sqlcombinations=db_query("SELECT formula FROM ai_rule_potential INNER JOIN ai_combinations ON ai_rule_potential.id=ai_combinations.rule_potential_id WHERE ai_combinations.id=$combination_id");
	while ($rowcombinations=db_fetch_array($sqlcombinations)) {
		$newformula=$rowcombinations["formula"];
		$formula="$formula <br> $newformula";
	}
	# GET combinations_sellside from rule_potential_sellside
	$sqle=db_query("SELECT formula FROM ai_rule_potential_sellside INNER JOIN ai_combinations ON ai_rule_potential_sellside.id=ai_combinations.rule_potential_sellside_id WHERE ai_combinations.id=$combinations_sellside_id");
	while ($rowe=db_fetch_array($sqle)) {
		$newformula=$rowe["formula"];
		$formula_sellside="$formula_sellside <br> $newformula";
	}

	# Calculate Average Annual Profit
	$years=(($average_days_long+$average_days_short)/254);  # For some reason, the number_format fucks up the
								# calculations, so I better get these values now
								# before I use them.  Also, there's 254 business
								# days in a year.
	#$average_annual_profit_percentage=((($grandtotal/$average_starting_price)/(($average_days_long+$average_days_short)/365))*100);
	$average_annual_profit_percentage=(($grandtotal/$average_starting_price)/$years);
	$average_annual_profit_percentage=$average_annual_profit_percentage*100;

	# NOTE:  Combination ID:  $combination_id IS THE SAME AS $combinations_sellside_id
	print "<td width=40% align=center><font face=arial color=000099 size=-2>Combination:  $combination_id</font><br><font face=arial color=000000 size=-2>BuySide Formula:</font><font color=009900 size=-2>$formula</font><br><font color=000000 size=-2>SellSide Formula:</font><font color=ff0000 size=-2>$formula_sellside</font></font></td>";
	$grandtotal=number_format($grandtotal,2,".",",");
	$average_starting_price=number_format($average_starting_price,2,".",",");
	print "<td width=10% align=center><font size=-1>$$grandtotal<br>&<br>$$average_starting_price</font></td>";
	$average_percentage_of_days_score_above_zero=number_format($average_percentage_of_days_score_above_zero,2,".",",");
	print "<td width=10% align=center><font size=-1>$average_percentage_of_days_score_above_zero %</font></td>";
	$average_percentage_running_score_above_zero=number_format($average_percentage_running_score_above_zero,2,".",",");
	print "<td width=10% align=center><font size=-1>$average_percentage_running_score_above_zero %</font></td>";
	$average_days_long=number_format($average_days_long,2,".",",");
	print "<td width=10% align=center><font size=-1>$average_days_long</font></td>";
	$average_days_short=number_format($average_days_short,2,".",",");
	print "<td width=10% align=center><font size=-1>$average_days_short</font></td>";
	$days_per_roundtrip=number_format($days_per_roundtrip,2,".",",");
	print "<td width=10% align=center><font size=-1>$days_per_roundtrip</font></td>";
	$average_annual_profit_percentage=number_format($average_annual_profit_percentage,2,".",",");
	print "<td width=10% align=center><font size=-1>$average_annual_profit_percentage%</font></td>";
	print "</tr>";
}
#print "</tr>"; 05/26/2003: i don't think i need this
print "</table>";
print "</b></font>";
print "<font face=arial size=-1><b>END OF STEP:  RESULTS</b></font><br>";

include_once("{$path}include/footer.php");
?>