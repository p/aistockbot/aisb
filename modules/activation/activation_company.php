<?php
#
# activation_company.php
#
# This program updates the company status on the Activation Report
#
# 2004/05/14  MS  Added PHPSESSID.  Changed path to "../../" because we're at modules/activation/*
# 2004/05/11  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/13  MS  Fixed paths and filenames while going to CVS
# 2003/04/11  MS  Added '?flag=1' to keep bot from repeating speech
# 2003/04/04  MS  Extract data from form or link
# 2002/10/28  MS  Added download status
# 2002/10/26  MS  Initial Release
#

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/config.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# Update Database
if ($active==1) {
	$sql=db_query("
		UPDATE	ai_company
		SET	active 	= $active, download=1
		WHERE	ticker	= '$ticker'
		");
	} else {
	$sql=db_query("
		UPDATE	ai_company
		SET	active 	= $active, download=0
		WHERE	ticker	= '$ticker'
		");
}

$sql_company=db_query("
	SELECT	ticker
	FROM	ai_company
	WHERE	ticker='$ticker'
	");

if (db_num_rows($sql_company)==0) {
	print "<b>Company Insertion Form for AIStockBot</b><br>";
	print "You do not have this ticker in the company database.  You need to create a record for this company in the database.<br>";
	print "Please complete this form:<br>";
	print "<form action={$path}modules/activation_company.php?$menu_string method=post>";
	print "Company Name: <input type=text name=name size=30> &nbsp; ";
	# print "FIELDS FOR COMPANY TABLE:  ticker     exchange     name     description     active ";
	$ticker=strtoupper($ticker);
	print "Ticker: <input type=text name=ticker value=$ticker size=5> &nbsp; ";
	print "Exchange:  ";
	# If someone would be so kind to write a SQL query that gets all the exchanges out of the 'exchange' table, I would appreciate it.  Thanks -Vooch
	print "<select name=exchange>";
	print "<option>AMEX</option>";
	print "<option selected>NASDAQ</option>";
	print "<option>NYSE</option>";
	print "</select><br>";
	print "Description:<br>";
	print "<textarea name=description rows=7 cols=80></textarea><br>";
	print "<input type=hidden name=active value=1>";
	print "<input type=submit value=Submit><br>";
	print "</form>";
	print "We've provided this Profile to help you get the information you need.  Simply copy-n-paste the Company Description and anything else you desire.<br>";
	print "<hr width=50%>";
	print "<hr width=50%>";

	# Get data from yahoo.com
	# Start at position 0 and grab the first letter
	$alphabetical=substr($ticker, 0, 1);
	# http://biz.yahoo.com/p/s/scox.html
	$website="http://biz.yahoo.com/p/$alphabetical/$ticker.html";
	$fcontents=implode("", file($website));
	print "$fcontents";
	exit();
}

HEADER("Location: {$path}modules/activation/activation_report.php?flag=1&PHPSESSID=$PHPSESSID&$menu_string");
?>
