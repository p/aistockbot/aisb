<?php
# activation_report.php
#
# 2004/06/03  FS  Changed the order for the variable
# 2004/05/14  MS  Changed path to ../../ since we're two levels deep (ie. modules/activation/* )
# 2004/05/14  MS  Converted remaining html to php and/or javascript
# 2004/05/11  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/13  MS  Fixed link errors
# 2002/11/06  MS  Split away from index.php
# 
print "<head>";
print "<script LANGUAGE=JavaScript>";
?>
<!-- Begin
	function toForm() {
			document.myform.ticker.focus();
			// Replace ticker with the field name of which you want to place the focus.
	}
	// End -->
<?php
print "</SCRIPT>";
print "</head>";
print "<body onLoad=toForm()>";

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

print "<table width=100% border=1>";
print "<tr>";
print "<td width=99% colspan=3 align=center>";
print "<font face=Arial size=+0>";
print "<b>A C T I V A T I O N &nbsp; &nbsp; &nbsp; R E P O R T</b><br>";
print "<i>Select the companies, operators, and variables you wish to analyze, then click STEP 1 on one of the bots.</i><br>";
print "</font>";
print "<font face=Arial size=-1>";
print "<font color=006600>Green=Activated</font> and <font color=FF0000>Red=Inactive</font><br>";
print "</font>";
print "</td>";
print "</tr>";
print "<tr>";
print "<td width=35%><font size=+0><b>Companies</b></font></td>";
print "<td width=21%><font size=+0><b>Operators</b> - <i>(used for Van Gogh Bot only)</i></font></td>";
print "<td width=43%><font size=+0><b>Variables</b> - <i>(used for Van Gogh Bot only)</i></font></td>";
print "</tr>";

print "<tr>";
print "<td width=35% valign=top>";
print "<font size=-1>";
print "<form name=myform action={$path}modules/activation/activation_company.php?$menu_string method=post>";
print "Ticker:&nbsp;&nbsp";
	print "<input type=text name=ticker size=6>";
	print "<input type=hidden name=active value=1>";
	print "<input type=submit name=submit value=Activate><br>";
print "</form>";
#print "<br>";
#print "<form action=update_company_active.php method=post>";
#print "Ticker:&nbsp;&nbsp;";
#	print "<input type=text name=ticker size=6>";
#	print "<input type=hidden name=active value=0>";
#	print "<input type=submit name=submit value=Deactivate>";
#print "</form>";
print "</font>";


$sqlcompany=db_query("
	SELECT ticker, name, active 
	FROM ai_company
	WHERE active=1 
	ORDER BY ticker");

while ($rowsqlcompany=db_fetch_array($sqlcompany)) {
	$ticker=$rowsqlcompany['ticker'];
	$name=$rowsqlcompany['name'];
	$active=$rowsqlcompany['active'];
	if ($active == 1) {
		print "<font color=006600 face=arial size=-2>";
		# reactivated
		$alphabetical=substr($ticker, 0, 1);
		$website="http://biz.yahoo.com/p/$alphabetical/$ticker.html";
		print "<form name={$ticker} action={$path}modules/activation/activation_company.php?ticker=$ticker&active=0&$menu_string method=post>";
		print "<input type=submit name=submit value=-->";
		print "&nbsp; $ticker - $name [<a href=$website>Profile</a>]<br>";
		print "</form>";
	} else {
		# Do nothing because there's too many companies
		print "<font color=FF0000 face=Arial size=-2>";
	}
	print "</font>";
}

print "</td>";
print "<td width=21% valign=top>";

$sqloperator=db_query("
	SELECT ID, operator, description, active
	FROM ai_operator");
while ($rowsqloperator=db_fetch_array($sqloperator)) {
	$ID	= $rowsqloperator['ID'];
	$operator=$rowsqloperator['operator'];
	$description=$rowsqloperator['description'];
	$active=$rowsqloperator['active'];
	if ($active == 1) {
		# Active operator is on (1), so ask to turn it off (0)
		print "<font color=006600 face=arial size=-2>";
		print "<form name={$ID} action={$path}modules/activation/activation_operator.php?ID=$ID&active=0&$menu_string method=post>";
		print "<input type=submit name=submit value=-->";
	} else {
		# Active operator is off (0), so ask to turn it on (1)
		print "<font color=FF0000 face=Arial size=-2>";
		print "<form name={$ID} action={$path}modules/activation/activation_operator.php?ID=$ID&active=1&$menu_string method=post>";
		print "<input type=submit name=submit value=+>";
	}
	print "&nbsp; $operator $description<br>";
	print "</form>";
	print "</font>";
}
print "</td>";
print "<td width=43% valign=top>";
$sqlvariable=db_query("
	SELECT id, description, active
	FROM ai_variable
	ORDER BY name");
while ($rowsqlvariable=db_fetch_array($sqlvariable)) {
	$name=$rowsqlvariable['description'];
	$active=$rowsqlvariable['active'];
	$id=$rowsqlvariable['id'];
	if ($active == 1) {
		# Active variable is on (1), so ask to turn it off (0)
		print "<font color=006600 face=arial size=-2>";
		print "<form name={$ID} action={$path}modules/activation/activation_variable.php?ID=$id&active=0&$menu_string method=post>";
		print "<input type=submit name=submit value=-->";
	} else {
		# Active variable is off (0), so ask to turn it on (1)
		print "<font color=FF0000 face=Arial size=-2>";
		print "<form name={$ID} action={$path}modules/activation/activation_variable.php?ID=$id&active=1&$menu_string method=post>";
		print "<input type=submit name=submit value=+>";
	}
	print "$name<br>";
	print "</form>";
	print "</font>";
}


print "</td></tr></table><br><br>";

include_once("{$path}include/footer.php");
?>
