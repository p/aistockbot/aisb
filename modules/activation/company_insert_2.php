<?php
###########################################################################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	company_insert_2.php
# Authors:	Michael Salvucci
#
# Principally used by "activation_company.php", but could be used by a company insertion form (eg. company_insert_1.php)
#
###########################################################################################################################
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2003/05/23  MS  Initial Release
###########################################################################################################################

if (empty($path)) {
        $path="./";
} // end if (!$path)
include_once("{$path}include/database.php");

# Get form field data
extract($_POST);

# Force $ticker to UPPERCASE
$ticker=strtoupper($ticker);

# print "FIELDS FOR COMPANY TABLE:  ticker     exchange     name     description     active ";

$sql_company=db_query("
	INSERT INTO	ai_company
		(ticker
			, exchange
			, name
			, description
			, active)
	VALUES 	('$ticker'
			, '$exchange'
			, '$name'
			, '$description'
			, 1)
	");

# Error checking
# $result = db_query($sql_company);


# Go!
HEADER("Location: {$path}index.php?flag=1");
?>
