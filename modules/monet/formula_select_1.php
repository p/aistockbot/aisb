<?php

#
# formula_select_1.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/14  MS  Initial Release
#

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/header.php");

$sql_formula=db_query("
	SELECT		ID, description, buyside_formula, sellside_formula
	FROM		ai_formula
	");

print "<table width=100%>";

# Print Headers
print "<tr>";
print "<td width=100% colspan=3>Select a Formula</td>";
print "</tr>";

print "<tr>";
print "<td width=33% bgcolor=ccccff>&nbsp;</td>";
print "<td width=33% bgcolor=ccccff>Description</td>";
print "<td width=33% bgcolor=ccccff>Formulas</td>";
print "</tr>";

$i=1;

while ($row_formula = db_fetch_array($sql_formula)) {
	$ID			= $row_formula["ID"];
	$description		= $row_formula["description"];
	$buyside_formula	= $row_formula["buyside_formula"];
	$sellside_formula	= $row_formula["sellside_formula"];

	# Print Detail
	if ($i % 2) { 
		echo "<tr bgcolor=ffffff>"; 
	} else { 
      		echo "<tr bgcolor=eeeeee>"; 
	} 
	$i++;

	print "<td width=33% align=center>";
	print "<form action={$path}picasso/learn.php>";
	print "<input type=hidden name=monet_bot value=$ID>";
	print "<input type=submit value=Select>";
	print "</form>";
	print "</td>";

	print "<td width=33% valign=top>";
	print "<font face=arial size=-1>";
	print "$description";
	print "</font>";
	print "</td>";

	print "<td width=33% valign=top>";
	print "<font face=arial color=000000 size=-2>";
	print "Buy Side Formula:<br>";
	print "</font>";
	print "<font face=arial color=009900 size=-1>";
	print "$buyside_formula<br>";
	print "</font>";
	print "<font face=arial color=000000 size=-2>";
	print "Sell Side Formula:<br>";
	print "</font>";
	print "<font face=arial color=ff0000 size=-1>";
	print "$sellside_formula<br>";
	print "</font>";
	print "</td>";

	print "</tr>";
}
print "</table>";

include_once("{$path}include/footer.php");

?>
