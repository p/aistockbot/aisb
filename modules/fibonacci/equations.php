<?php
# equations.php
#
# 05/30/2004  NK  Created file
#


/* this function will calculate the dmi (directional movement index)
   for specified symbol over specified averaging period
   - this will use historical tick data from aistockbot db
*/
function calculate_dmi ($symbol, $period) {
  /* First the positive and the negative directional movement is 
     calculated - +DMj and -DMj
  */

  //$period = 14;
  $tr = $period;
  //$tr = 14;

  // should limit get_data by $tr, the number of periods
  $get_data_num = "SELECT date,high,low,open,close,volume from ai_history WHERE ticker='$symbol' order by date ASC";
  $get_num_rows = db_query($get_data_num);

  $num_rows = mysql_num_rows($get_num_rows);

  $offset = $num_rows - $tr;
  //print "num_rows: $num_rows offset: $offset<br>";

  $get_data = "SELECT date,high,low,open,close,volume from ai_history WHERE ticker='$symbol' order by date ASC LIMIT $offset,$tr";
  $get_query = db_query($get_data);

  $x = 0;
  $dm_p = 0;
  $dm_m = 0;
  $dm_p_prev = 0;
  $dm_m_prev = 0;
  $di_p = 0;
  $di_m = 0;
  $di_p_prev = 0;
  $di_m_prev = 0;
  $tr_calc = 0;
  $tr_calc_prev = 0;

  $high = 0;
  $low = 0;
  $high_prev = 0;
  $low_prev = 0;
  $open = 0;
  $open_prev = 0;
  $close = 0;
  $close_prev = 0;
  $volume = 0;
  $volume_prev = 0;
  $adx = 0;
  $adx_prev = 0;

  $extreme_high = 0;
  $extreme_low = 0;

  while ($line = mysql_fetch_array($get_query, MYSQL_ASSOC)) {
    //$line["high"] $line["low"]
    $high = $line["high"];
    $low = $line["low"];
    $open = $line["open"];
    $close = $line["close"];
    $volume = $line["volume"];
    $date = $line["date"];

    // calculate extreme high / extreme low
    if ($high > $extreme_high) {
      $extreme_high = $high;
    }
    if ($extreme_low == 0) {
      $extreme_low = $low;
    } elseif ($low < $extreme_low) {
      $extreme_low = $low;
    }

    // calculate +dm
    if ( ($high > $high_prev) && ( ($high-$high_prev) > ($low_prev-$low) ) ) {
      $blah = $high - $high_prev;
    } else {
      $blah = 0;
    }
    $dm_p = ( ($tr - 1) * $dm_p_prev + $blah ) / $tr;

    //print "dm_p: $dm_p<br>";

    // calculate -dm
    if ( ($low < $low_prev) && ( ($low_prev-$low) > ($high-$high_prev) ) ) {
      $blah = $low_prev - $low;
    } else {
      $blah = 0;
    }
    $dm_m = ( ($tr - 1) * $dm_m_prev + $blah ) / $tr;

    //print "dm_m: $dm_m<br>";

    // calculate TR
    $tr_calc = ( ($tr - 1) * $tr_calc_prev + max( ($high-$high_prev), ($high - $close_prev), ($close_prev - $low) ) ) / $tr;

    // calculate +di
    $di_p = $dm_p / $tr_calc;

    // calculate -di
    $di_m = $dm_m / $tr_calc;

    // calculate adx
    $adx = ( ($tr - 1) * $adx_prev + abs($di_p - $di_m)/($di_p+$di_m) ) / $tr;

    //print "1: $dm_p 2: $dm_m 3: $tr_calc 4: $di_p 5: $di_m 6: $adx<br>";
    //print "ex_high: $extreme_high ex_low: $extreme_low<br>";

    // rotate the variables
    $tr_calc_prev = $tr_calc;
    $open_prev = $open;
    $close_prev = $close;
    $high_prev = $high;
    $low_prev = $low;
    $dm_p_prev = $dm_p;
    $dm_m_prev = $dm_m;
    $di_p_prev = $di_p;
    $di_m_prev = $di_m;
    $volume_prev = $volume;
    $adx_prev = $adx;

    $x++;
  }

  //return $dm_p;
  if ($di_p > $di_m) {
    $position = "long";
  } elseif ($di_p < $di_m) {
    $position = "short";
  } else {
    $position = "neutral";
  }

  return $position;
}



/* This function calculates support and resistance points based on
   Fibonacci retracements.
*/
function calculate_fibonacci ($symbol, $period) {
  //$period = 14;
  $tr = $period;
  //$tr = 14;

  // should limit get_data by $tr, the number of periods
  $get_data_num = "SELECT date,high,low,open,close,volume from ai_history WHERE ticker='$symbol' order by date ASC";
  $get_num_rows = db_query($get_data_num);

  $num_rows = mysql_num_rows($get_num_rows);

  $offset = $num_rows - $tr;
  //print "num_rows: $num_rows offset: $offset<br>";

  $get_data = "SELECT date,high,low,open,close,volume from ai_history WHERE ticker='$symbol' order by date ASC LIMIT $offset,$tr";
  $get_query = db_query($get_data);

  $x = 0;
  $dm_p = 0;
  $dm_m = 0;
  $dm_p_prev = 0;
  $dm_m_prev = 0;
  $di_p = 0;
  $di_m = 0;
  $di_p_prev = 0;
  $di_m_prev = 0;
  $tr_calc = 0;
  $tr_calc_prev = 0;

  $high = 0;
  $low = 0;
  $high_prev = 0;
  $low_prev = 0;
  $open = 0;
  $open_prev = 0;
  $close = 0;
  $close_prev = 0;
  $volume = 0;
  $volume_prev = 0;
  $adx = 0;
  $adx_prev = 0;

  $extreme_high = 0;
  $extreme_low = 0;

  while ($line = mysql_fetch_array($get_query, MYSQL_ASSOC)) {
    //$line["high"] $line["low"]
    $high = $line["high"];
    $low = $line["low"];
    $open = $line["open"];
    $close = $line["close"];
    $volume = $line["volume"];
    $date = $line["date"];

    // calculate extreme high / extreme low
    if ($high > $extreme_high) {
      $extreme_high = $high;
    }
    if ($extreme_low == 0) {
      $extreme_low = $low;
    } elseif ($low < $extreme_low) {
      $extreme_low = $low;
    }

    //print "ex_high: $extreme_high ex_low: $extreme_low<br>";

    // rotate the variables
    $tr_calc_prev = $tr_calc;
    $open_prev = $open;
    $close_prev = $close;
    $high_prev = $high;
    $low_prev = $low;
    $dm_p_prev = $dm_p;
    $dm_m_prev = $dm_m;
    $di_p_prev = $di_p;
    $di_m_prev = $di_m;
    $volume_prev = $volume;
    $adx_prev = $adx;

    $x++;
  }

  // $extreme_high $extreme_low
  print "ex_high: $extreme_high ex_low: $extreme_low";
  $diff = $extreme_high - $extreme_low;

  $fib62 = $diff * .6180;
  $fib38 = $diff * .382;
  $fib50 = $diff * .5;
  $fib79 = $diff * .786;

  $support = $extreme_high - $fib62;
  $resistance = $extreme_high - $fib38;


  $fibtest1 = $extreme_high * .6180;
  $fibtest2 = $extreme_high * .6180;
  $fibtest3 = $extreme_high * .6180;
  $fibtest4 = $extreme_high * .6180;

  //return $position;
  //return "$support $resistance";
  $ret = array($support, $resistance);
  return $ret;
}

?>
