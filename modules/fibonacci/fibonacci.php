<?php
# modules/dali/dali.php
#
# 2004/05/30  NK  Created bot
# 

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");
include_once("equations.php");
include_once("update_data.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

if (isset($_POST['symbol'])) {

  // update technical data
  db_query("update ai_company set active=0");
  db_query("update ai_company set download=1,active=1 where ticker='$symbol'");

  update_data($symbol);


print "<table width=100% border=1>";

print "<tr>";
print "<td>";
print "<center>Executing..</center><br>";
print "</td>";
print "</tr>";
print "\n";

#$symbol = "SIRI";
$period = 14;
$fibonacci_result = calculate_fibonacci($symbol, $period);

print "<tr>";
print "<td>";
print "<center>Completed.</center>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td>";
//print "<center>Result for $symbol: $fibonacci_result</center>";
print "<center>Result for $symbol:</center>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td align=\"right\">";
print "Support:";
print "</td>";
print "<td align=\"left\">";
print "$fibonacci_result[0]";
print "</td>";
print "</tr>";

print "<tr>";
print "<td align=\"right\">";
print "Resistance:";
print "</td>";
print "<td align=\"left\">";
print "$fibonacci_result[1]";
print "</td>";
print "</tr>";


print "</table><br><br>";

} else {

  print "Enter a symbol!<br>";

}

include_once("{$path}include/footer.php");

?>
