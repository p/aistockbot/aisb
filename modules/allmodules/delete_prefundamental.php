<?php

# delete_prefundamental.php
#
#  THIS SCRIPT DELETES HISTORY 'TECHNICAL DATA' (EG. HIGH, LOW, CLOSE, OPEN, ETC.) WHERE THE
#  DATE IS EARLIER THAN THE EARLIEST FUNDAMENTAL DATA FOUND.
#
#  THIS SCRIPT IS PART OF AN INCLUDE STATEMENT IN update_technical_data.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2003/03/11  MS  Initial Release
#

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include("{$path}include/database.php");

###################
#
# GET LIST OF ALL ACTIVE COMPANIES
$sqlticker=db_query("
	SELECT DISTINCT	ticker, name
	FROM		ai_company
	WHERE		active = 1
");

while ($rowticker=db_fetch_array($sqlticker)) {
	$ticker		= $rowticker["ticker"];
	$name		= $rowticker["name"];
	# DEBUG:
	# print "ticker=$ticker<br>";

	# Initialize Variable
	$report_date = "";

	# Get Earliest Fundamental Data DATE (eg. sales, earnings, etc.)
	# for each active company
	$sqlfundamental=db_query("
		SELECT 		report_date
		FROM		ai_fundamental
		WHERE		ticker = '$ticker'
		ORDER BY	report_date
		LIMIT		1
	");
	while ($rowfundamental=db_fetch_array($sqlfundamental)) {
		$report_date 	= $rowfundamental["report_date"];
		# DEBUG:
		# print "ticker=$ticker:::report_date=$report_date<br>";
	}
	# print "ticker=$ticker:::report_date=$report_date ";


	if (!$report_date) {
		# If no fundamental report dates, wipe out technical data
		db_query("
			DELETE FROM 	ai_history
			WHERE		ticker = '$ticker'
		");
	}

	# Delete from history where the data in the history table
	# is older than the data in the fundamental table
	db_query("
		DELETE FROM 	ai_history
		WHERE		ticker = '$ticker'
		AND		date < '$report_date'
	");

	# print "$ticker ";
	
	# If 0 records are left in the history table (ie. no fundamental data so everything go deleted), then
	# deactivate the company from analysis
	$sqlhistory2=db_query("
		SELECT 		date
		FROM	 	ai_history
		WHERE		ticker = '$ticker'
	");

	$num_rows_history2=db_num_rows($sqlhistory2);

	# print "Number of records:  $num_rows_history2<br>";

	if ($num_rows_history2 == 0) {
		# Deactivate Company
		db_query("
			UPDATE	ai_company
			SET	active = 0
			WHERE	ticker = '$ticker'
		");

		# Say it
		print "<br><b>WARNING: DEACTIVATED [$ticker] [$name] BECAUSE NO FUNDAMENTAL DATA AVAILABLE.  You must add ";
		print "fundamental data (eg. sales, earnings, etc.) before analysis can begin.  After you ";
		print "add fundamental data for this stock, rerun this step.</b><br>";
	}
}

?>
