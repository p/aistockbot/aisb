<?php
#
# activation_variable.php
#
# This program updates the variable in the Activation Report
#
# 2007/07/15  MS  Fixed typo
# 2007/07/15  MS  Removed PHPSESSID from header() function because $bot_string already includes it
# 2004/05/14  MS  Changed $path to "../../" since we're at modules/allmodules/*
#		  Added PHPSESSID and added 'activation' folder within header()
# 2004/05/11  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.php
# 2004/04/13  MS  Fixed paths and header() function, redid database connection, and added db close
# 2003/04/11  MS  Added '?flag=1' so bot doesn't keep repeating speech
# 2003/04/04  MS  Extract data from a link
# 2003/10/26  MS  Initial Release
#

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string($_SERVER["QUERY_STRING"]);
$bot=extract_bot_name($_SERVER["QUERY_STRING"]);
$bot_string="bot=".$bot;

# Update Database
$sql=db_query("
	UPDATE	ai_variable
	SET	active 	= $active
	WHERE	ID	= $ID
	");

HEADER("Location: {$path}modules/allmodules/activation_report.php?flag=1&$menu_string&$bot_string");
?>