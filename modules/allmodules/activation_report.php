<?php
# activation_report.php
#
# 2004/07/12  MS  Fixed typo
# 2004/06/03  FS  Changed the order for the variable
# 2004/05/14  MS  Changed path to ../../ since we're two levels deep (ie. modules/allmodules/* )
# 2004/05/14  MS  Converted remaining html to php and/or javascript
# 2004/05/11  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/13  MS  Fixed link errors
# 2002/11/06  MS  Split away from index.php
# 
print "<head>";
print "<script LANGUAGE=JavaScript>";
?>
<!-- Begin
	function toForm() {
			document.myform.ticker.focus();
			// Replace ticker with the field name of which you want to place the focus.
	}
	// End -->
<?php
print "</SCRIPT>";
print "</head>";
print "<body onLoad=toForm()>";

if (empty($path)) {
        $path="../../";
} // end if (!$path)
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string($_SERVER["QUERY_STRING"]);
$bot=extract_bot_name($_SERVER["QUERY_STRING"]);
$bot_string="bot=".$bot;

print "<table width=100% border=1>";
print "<tr>";
print "<td width=99% colspan=3 align=center>";
print "<font face=Arial size=+0>";
print "<b>A C T I V A T I O N &nbsp; &nbsp; &nbsp; R E P O R T</b><br>";
print "<i>Select the companies, operators, and variables you wish to analyze, then click STEP 1 on one of the bots.</i><br>";
print "</font>";
print "<font face=Arial size=-1>";
print "<font color=006600>Green=Activated</font> and <font color=FF0000>Red=Inactive</font><br>";
print "</font>";
print "</td>";
print "</tr>";
print "<tr>";
print "<td width=30%><font size=+0><b>Companies</b></font></td>";
print "<td width=20%><font size=+0><b>Operators</b> - <i>(used for Van Gogh Bot only)</i></font></td>";
print "<td width=50%><font size=+0><b>Variables</b> - <i>(used for Van Gogh Bot only)</i></font></td>";
print "</tr>";

print "<tr>";
print "<td width=30% valign=top>";
print "<table width=100% border=0>";
print "<tr><td valign=top>";
print "<font size=-1>";
print "<form name=myform action={$path}modules/allmodules/activation_company.php?$menu_string&$bot_string method=post>";
print "Ticker:";
print "</font></td>";
print "<td valign=top>";
print "<font size=-1>";
print "<center><input type=text name=ticker size=15></center>";
print "<input type=hidden name=active value=1>";
print "<br><center><input type=submit name=submit value=Activate></center>";
print "</font></td></tr>";
print "</form>";
print "</table>";

$sqlcompany=db_query("
	SELECT ticker, name, active 
	FROM ai_company
	WHERE active=1 
	ORDER BY ticker");

print "<table width=100% border=0 cellspacing=2%>";
print "<tr><td>&nbsp; </td></tr>";
while ($rowsqlcompany=db_fetch_array($sqlcompany)) {
	$ticker=$rowsqlcompany['ticker'];
	$name=$rowsqlcompany['name'];
	$active=$rowsqlcompany['active'];
	if ($active == 1) {
		# reactivated
		$alphabetical=substr($ticker, 0, 1);
		$website="http://biz.yahoo.com/p/$alphabetical/$ticker.html";
		print "<tr>";
		print "<td valign=center>";
		print "<font color=006600 face=arial size=-1>";
		print "<form name={$ticker} action={$path}modules/allmodules/activation_company.php?ticker=$ticker&active=0&$menu_string&$bot_string method=post>";
		print "<input type=submit name=submit value=-->";
		print "</font></td>";
		print "<td valign=center>";
		print "<font color=006600 face=arial size=-1>";
		print "<b>$ticker</b><br>$name<br>[<a href=$website>Profile</a>]<br>";
		print "</font></td>";
		print "</tr>";
		print "</form>";
	}
}

print "</table>";
print "</td>";

print "<td width=20% valign=top>";

$sqloperator=db_query("
	SELECT ID, operator, description, active
	FROM ai_operator");

print "<table width=100% border=0 cellspacing=2%>";
while ($rowsqloperator=db_fetch_array($sqloperator)) {
	$ID	= $rowsqloperator['ID'];
	$operator=$rowsqloperator['operator'];
	$description=$rowsqloperator['description'];
	$active=$rowsqloperator['active'];
	print "<tr>";
	if ($active == 1) {
		print "<td valign=center>";
		# Active operator is on (1), so ask to turn it off (0)
		print "<font color=006600 face=arial size=-1>";
		print "<form name={$ID} action={$path}modules/allmodules/activation_operator.php?ID=$ID&active=0&$menu_string&$bot_string method=post>";
		print "<input type=submit name=submit value=-->";
		print "</font></td>";
		print "<td valign=center>";
		print "<font color=006600 face=arial size=-1>";
		print "<b>$operator</b><br>$description<br>";
		print "</font></td>";
	} else {
		print "<td valign=center>";
		# Active operator is off (0), so ask to turn it on (1)
		print "<font color=FF0000 face=Arial size=-1>";
		print "<form name={$ID} action={$path}modules/allmodules/activation_operator.php?ID=$ID&active=1&$menu_string&$bot_string method=post>";
		print "<input type=submit name=submit value=+>";
		print "</font></td>";
		print "<td valign=center>";
		print "<font color=FF0000 face=Arial size=-1>";
		print "<b>$operator</b><br>$description<br>";
		print "</font></td>";
	}
	print "</form>";
	print "</tr>";
}
print "</table>";
print "</td>";

print "<td width=50% valign=top>";
$sqlvariable=db_query("
	SELECT id, description, active
	FROM ai_variable
	ORDER BY name");

print "<table width=100% border=0 cellspacing=2%>";
while ($rowsqlvariable=db_fetch_array($sqlvariable)) {
	$name=$rowsqlvariable['description'];
	$active=$rowsqlvariable['active'];
	$id=$rowsqlvariable['id'];
	print "<tr>";
	if ($active == 1) {
		print "<td valign=center>";
		# Active variable is on (1), so ask to turn it off (0)
		print "<font color=006600 face=arial size=-1>";
		print "<form name={$ID} action={$path}modules/allmodules/activation_variable.php?ID=$id&active=0&$menu_string&$bot_string method=post>";
		print "<input type=submit name=submit value=-->";
		print "</font></td>";
		print "<td valign=center>";
		print "<font color=006600 face=arial size=-1>";
		print "$name<br>";
		print "</font></td>";
	} else {
		print "<td valign=center>";
		# Active variable is off (0), so ask to turn it on (1)
		print "<font color=FF0000 face=Arial size=-1>";
		print "<form name={$ID} action={$path}modules/allmodules/activation_variable.php?ID=$id&active=1&$menu_string&$bot_string method=post>";
		print "<input type=submit name=submit value=+>";
		print "</font></td>";
		print "<td valign=center>";
		print "<font color=FF0000 face=Arial size=-1>";
		print "$name<br>";
		print "</font></td>";
	}
	print "</form>";
	print "</tr>";
}
print "</table>";

# Flush Variables
unset ($sqlcompany);
unset ($rowsqlcompany);
unset ($ticker);
unset ($active);
unset ($sqloperator);
unset ($rowsqloperator);
unset ($operator);
unset ($description);
unset ($sqlvariable);
unset ($rowsqlvariable);
unset ($name);
unset ($id);

print "</td></tr></table><br><br>";

include_once("{$path}include/footer.php");
?>
