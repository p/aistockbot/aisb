<?php
#
# quotes/yahoo_delayed_quote_2.php
#
# 2004/07/16  MS  Added new update portfolio values feature.  This is great for when
#		  you use the Portfolio Management System's "Current Holdings - Detail", but only
#		  if there's no data for the stock in ai_history - I'll have to improve that later.
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.php
# 2004/05/06  FS  CHanged use of PHP instead of HTML

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");

extract($_POST);

$fp = fopen ("http://finance.yahoo.com/d/quotes.csv?s=$ticker&f=sl1d1t1c1ohgv&e=.csv","r");

$data = fgetcsv ($fp, 1000, ",");

#this is our table which displays the stock info
#we access the individual items by using $data[0]
print "<br>";
print "<center>";
print "<table border=1 width=50%>";
print "<tr><td>description</td><td>latest figure</td><tr>";
print "<tr><td>symbol</td><td>$data[0]</td></tr>";
print "<tr><td>last price</td><td>$data[1]</td></tr>";
print "<tr><td>date</td><td>$data[2]</td></tr>";
print "<tr><td>time</td><td>$data[3]</td></tr>";
print "<tr><td>change</td><td>$data[4]</td></tr>";
print "<tr><td>open</td><td>$data[5]</td></tr>";
print "<tr><td>high</td><td>$data[6]</td></tr>";
print "<tr><td>low</td><td>$data[7]</td></tr>";
print "<tr><td>volume</td><td>$data[8]</td></tr>";
print "</table>";
print "</center>";

# 2004/07/16:  New Feature:  Update Portfolio values.  Useful for Portfolio Management System's "Current Holdings - Detail", but only if there's no data for the stock in ai_history - I'll have to improve that later.
if(!isset($username)) { $username=""; }
if($username<>"") {
	$sql_portfolio3=db_query("
		SELECT		ticker, shares
		FROM		ai_portfolio
		WHERE		sell_cost_basis = 0
		AND		ticker 		<> 'ACT_CASH'
		AND		username 	= '$username'

		AND		sell_date 	= '0000-00-00 00:00:00'
		");
#let's just get the whole damn portfolio updated...hehe
#		AND		ticker 		= '$ticker'
	while ($row_portfolio3 = db_fetch_array($sql_portfolio3)) {
		$ticker		= $row_portfolio3["ticker"];
		$shares		= $row_portfolio3["shares"];

		$fp = fopen ("http://finance.yahoo.com/d/quotes.csv?s=$ticker&f=sl1d1t1c1ohgv&e=.csv","r");
		$data = fgetcsv ($fp, 1000, ",");

		$value 		= $shares * $data[1];
		# print "value=$value ::: ticker=$ticker ::: shares=$shares ::: price=$data[1] ::: username=$username<br>";
		$sql=db_query("
			UPDATE 	ai_portfolio 
			SET 	value 		= $value
			WHERE 	ticker		= '$ticker' 
			AND 	shares		= $shares
			AND	username 	= '$username'
			AND	sell_date 	= '0000-00-00 00:00:00'
			");
	}
}

include_once("{$path}include/footer.php");
?>