<?php
/*
configuration_2.php

This script updates the data on configuration page

2004/05/09  FS  Changed include mechanism and $path variable
2004/05/08  FS  Changed to use different types of input fields
2004/05/06  FS  Changed to use functions from include/database.inc
2004/04/28  FS  Initial Release
*/

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");

extract ($_POST);
extract ($_GET);
$query_string=$_SERVER["QUERY_STRING"];
$query_string=ereg_replace("&&","&",$query_string);

$sqlvariable=db_query("
	SELECT varname, value
	FROM ai_config
	WHERE username='default'
	  AND configurable>'0'
	ORDER BY varname
	");

while ($rowsqlvariable=db_fetch_array($sqlvariable)) {
	$varname=$rowsqlvariable['varname'];
	$value=$rowsqlvariable['value'];
	$varnamevar=$$rowsqlvariable['varname'];
	if ($value != $varnamevar) {
		$sql=db_query("
			UPDATE	ai_config
			SET	value	= '$varnamevar'
			WHERE	varname	= '$varname'
			 AND	username= 'default'
			");
	} # end if ($value != $varnamevar)
} # end while ($rowsqlvariable=db_fetch_array($sqlvariable))

HEADER("Location: {$path}administration/configuration_1.php?$query_string");
?>
