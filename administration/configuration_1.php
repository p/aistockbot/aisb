<?php
/*
configuration_1.php

Configuration script for all Variables in ai_config

2004/05/09  FS  Changed include mechanism and $path variable
2004/05/08  FS  Changed to use different types of input fields
2004/05/06  FS  Changed to use functions from include/database.inc
2004/04/28  FS  Initial Release
*/

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_POST);
extract($_GET);
$query_string=$_SERVER["QUERY_STRING"];
$query_string=ereg_replace("&&","&",$query_string);

$sqlvariable=db_query("
	SELECT varname, value, possible_values, configurable, description
	FROM ai_config
	WHERE username='default'
	  AND configurable>'0'
	ORDER BY varname");

print "<table width=100% border=1>";
print "<tr>";
print "<td width=99% colspan=3 align=center>";
print "<font face=Arial size=+1>";
print "<b>C O N F I G U R A T I O N &nbsp; &nbsp; &nbsp; P A G E</b><br>";
print "</font>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td><font size=+0><b>Variable</b></font></td>";
print "<td><font size=+0><b>Value</b></font></td>";
print "<td><font size=+0><b>Description</b></font></td>";
print "</tr>";

print "<form action={$path}administration/configuration_2.php?$query_string method=post>";
while ($rowsqlvariable=db_fetch_array($sqlvariable)) {
	print "<tr>";
	print "<td valign=top>";
	print "<font size=-1>";
	print $rowsqlvariable['varname'];
	print "</font>";
	print "</td>";
	print "<td valign=top>";
	print "<font size=-1>";
	if ($rowsqlvariable['configurable']=='1') {
		print "<select name={$rowsqlvariable['varname']}>";
		$possible_values=split("\|", $rowsqlvariable['possible_values']);
		$countvalue=count($possible_values);
		while ($countvalue) {
			if ($possible_values[count($possible_values)-$countvalue]==$rowsqlvariable['value']) {
				print "<option selected>".$possible_values[count($possible_values)-$countvalue]."</option>";
			} else {
				print "<option>".$possible_values[count($possible_values)-$countvalue]."</option>";
			} // end if ($possible_values[count($possible_values)==$rowsqlvariable['value'])
			$countvalue--;
		} // end while ($possible_values)
		print "</select>";
	} elseif ($rowsqlvariable['configurable']=='2') {
		print "<input type=text size=16 name={$rowsqlvariable['varname']} value=\"{$rowsqlvariable['value']}\">";
	} // end if ($rowsqlvariable['configurable']=='1')
	print "<br>";
	print "<input type=submit value=Submit>";
	print "</font>";
	print "</td>";
	print "<td valign=top>";
	print "<font size=-1>";
	print $rowsqlvariable['description'];
	print "</font>";
	print "</td>";
	print "</tr>";
} // end while ($rowsqlvariable=db_fetch_array($sqlvariable))
print "</form>";
print "</td></tr></table><br><br>";

include_once("{$path}include/footer.php");
?>
