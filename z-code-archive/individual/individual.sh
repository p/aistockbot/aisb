# index.sh

# These are the things that have to be done in advance because the
# bash shell is unable to run these when the .php script executes the .sh:
# touch /var/www/html/test.wav
# chown nobody:nobody /var/www/html/test.wav
# chmod 777 /var/www/html/test.wav

myvar="$1"

myvar=`echo $1 | sed -e 's/foobarspacer/ /g'` 

# WARNING:  The phrase to be spoken may have to be at least 2 words long!
# echo "$myvar" | /var/www/html/aistockbot-0.0.2/flite -o /var/www/html/aistockbot-0.0.2/individual.wav
echo "$myvar" | ./flite -o ./individual.wav
