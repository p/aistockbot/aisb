<html>
<?php

#
# individual.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/08  MS  Added sound check
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2003/04/13  MS  Some code cleanup
# 2003/05/21  MS  Forgot to close database connection at end
#
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/header.php");

print "<table width=100%>";
print "<tr>";
print "<td width=100% background=images/hex.gif>";

# php 4.2.0 or later fix:
if (!empty($_POST)) {
	extract($_POST);
	} else if (!empty($HTTP_POST_VARS)) {
	extract($HTTP_POST_VARS);
}


if (!isset ($myvar)) {
	echo "";
} else {
	# Queries
	$result=db_query("
		SELECT ticker, name, description
		FROM ai_company
		WHERE ticker = '$myvar'");

	$getprices=db_query("
		SELECT ai_company.ticker, ai_company.name, ai_history.close AS historyclose, ai_history.date AS historydate, ai_history.recommendation AS historyrecommendation
		FROM ai_company
		INNER JOIN ai_history
		ON ai_company.ticker = ai_history.ticker
		WHERE ai_company.ticker='$myvar'
		ORDER BY ai_history.date DESC
		LIMIT $days
		");
	
	$numrows=db_num_rows($result);
	while ($row = db_fetch_array($result) ) {
		$ticker=$row["ticker"];
		$company_name=$row["name"];
		$description=$row["description"];

		print "<font face=arial size=+0>";
		print "<table width=100% border=1>";
		print "<tr>";
		print "<td colspan=2 width=100%>";

print "<table width=100%>";

print "<tr><td width=100% colspan=8 align=center>&nbsp;";
if (file_exists("{$path}images/logos/$ticker.GIF")) {
	print "<img border=0 height=30 src={$path}images/logos/$ticker.GIF><br>";
} else {
	print "$ticker";
}
print "</td></tr>";

?>


<tr>

<td width="10%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="21">
<input type="submit" value="21 Days">
</form>
</td>

<td width="10%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="42">
<input type="submit" value="42 Days">
</form>
</td>

<td width="10%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="63">
<input type="submit" value="63 Days">
</form>
</td>

<td width="11%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="126">
<input type="submit" value="126 Days">
</form>
</td>

<td width="11%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="252">
<input type="submit" value="252 Days">
</form>
</td>

<td width="11%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="500">
<input type="submit" value="500 Days">
</form>
</td>

<td width="12%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="1000">
<input type="submit" value="1000 Days">
</form>
</td>

<td width="12%">
<form action="individual.php" method="post">
<input type="hidden" name="myvar" value="<?php echo "$ticker"; ?>">
<input type="hidden" name="days" value="4000">
<input type="submit" value="4000 Days">
</form>

</td>

</tr>
</table>

<?php
# 04/18/2003  Dominic Porreco says this was a security problem:
# $img_src="individual_2.php?global_hostname=$global_hostname&global_username=$global_username&global_password=$global_password&ticker=$ticker&days=$days";
# Here's the fix:
$img_src="{$path}individual/individual_2.php?ticker=$ticker&days=$days";
?>
<center><img src="<?php echo"$img_src";?>">
<br>
<br>
<font size="-2" face="Arial">
<b>
<i>Note:</i><br>
<i>When the Signal goes from 0 to 1, you buy.</i><br>
<i>When the Signal goes from 1 to 0, you sell or go short</i><br>
</b>
</font>
<br>
</center>  
		</tr>

<?php
/* This section is not used right now
print "<tr>";
print "<td width=100% colspan=2>";
print "<CENTER><B>FINANCIAL REPORTS ALREADY IN OUR DATABASE</B></CENTER>";
PRINT "</td>";
print "</tr>";
$sqlfundamental=db_query("
	SELECT 		id, ticker, book_value, report_date, edgar_accession_number, edgar_central_index_key, qtr_sales, qtr_earnings, shares_outstanding, equity, source 
	FROM 		ai_fundamental
	WHERE		ticker = '$myvar'
	ORDER BY	report_date DESC
	");
	while ($row_sqlfundamental = db_fetch_array($sqlfundamental) ) {
		$id			=	$row_sqlfundamental["id"];
		$ticker			=	$row_sqlfundamental["ticker"];
		$book_value		=	$row_sqlfundamental["book_value"];
		$report_date		=	$row_sqlfundamental["report_date"];
		$edgar_accession_number	=	$row_sqlfundamental["edgar_accession_number"];
		$edgar_central_index_key =	$row_sqlfundamental["edgar_central_index_key"];
		$qtr_sales		=	$row_sqlfundamental["qtr_sales"];
		$qtr_earnings		=	$row_sqlfundamental["qtr_earnings"];
		$shares_outstanding	=	$row_sqlfundamental["shares_outstanding"];
		$equity			=	$row_sqlfundamental["equity"];
		$source			=	$row_sqlfundamental["source"];
	print "<tr>";
		print "<td width=100% colspan=2><font face=arial size=-1><center>";
		$edgar_central_index_key = (int)$edgar_central_index_key;
		$e_an = ereg_replace ("-", "", $edgar_accession_number);
		print "<b>$report_date</b> - <a href=http://edgar.sec.gov/Archives/edgar/data/$edgar_central_index_key/$e_an/$edgar_accession_number-index.htm>http://edgar.sec.gov/Archives/edgar/data/$edgar_central_index_key/$e_an/$edgar_accession_number-index.htm</a>";
		print "</center></font></td>";
	print "</tr>";
	}
*/

?>

		<tr>
		<td width="30%" align="right" bgcolor="ff66ff">Company Name:</td>
		<td width="70%" align="left"><?php echo "$company_name";?></td>
			<tr>
			<td width="30%" align="right" bgcolor="ff66ff">Ticker:</td>
		      <td width="70%" align="left"><?php echo "$ticker";?></td>
			</tr>
		</tr>
		<tr>
		<td width="30%" align="right" bgcolor="66ffff">Date</td>
		<td width="70%" align="left" bgcolor="66ffff">Closing Price</td>
		</tr>
		<tr>
		<?php
		while ($row=db_fetch_array($getprices)) {
			$date=$row["historydate"];
			$close=$row["historyclose"];
			$recommendation=$row["historyrecommendation"];
			?>
			<td width="30%" align="right"><?php echo "$date";?></td>
			<td width="70%" align="left"><?php echo "$close - $recommendation";?></td>
			</tr>
			<?php
		}

		?>
		</table>
		<br>
		<?php
	}

	if ($description == "Not Available") {
		$description = "";
	}

	if ($ticker) {
		$myvar="The company you selected is $company_name with ticker symbol $ticker , $company_name $description";
	} else {
		$myvar="The ticker symbol you used cannot be found  Please try again";
	}
}

	if (db_query_config("sound")) {
		# The <EMBED> Tag works in Internet Explorer 4.0x, 5.0x, Navigator 3.0x, 4.0x 
		# The <BGSOUND> Tag works in Internet Explorer 3.0x (best used with the NOEMBED tag... see below) 
		$myvar=str_replace(" ","foobarspacer",$myvar);
		exec ("./individual.sh $myvar");

		print "<EMBED SRC=individual.wav AUTOSTART=TRUE LOOP=FALSE height=1 width=1 align=center>";
		print "<NOEMBED>";
		print "<BGSOUND SRC=individual.wav LOOP=1>";
		print "</NOEMBED>";
		print "</EMBED>";
	}

print "<br>";
print "<br>";

# FLUSH VARIABLES
unset ($myvar);

unset ($result);
unset ($getprices);

unset ($rows);
unset ($numrows);

unset ($ticker);
unset ($company_name);
unset ($description);

unset ($date);
unset ($close);
unset ($recommendation);

unset ($img_src);

include_once("{$path}include/footer.php");

?>

</td>
</tr>
</table>
</html>

