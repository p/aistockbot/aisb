<?php

# individual_2.php
#
# 2005/03/15  NK  Removed Flush Buffer code
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 04/18/2003  MS  Added include/config.php to fix security issue 
#                 which Dominic Porreco pointed out.
# 04/04/2003  MS  Added extract() function
# 10/27/2002  MS  Added second y-scale to graph for recommendation
#
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/database.php");
include_once("{$path}jpgraph-1.12.1/src/jpgraph.php");
include_once("{$path}jpgraph-1.12.1/src/jpgraph_line.php");
include_once("{$path}jpgraph-1.12.1/src/jpgraph_log.php");

# Get data from form or link
extract($_GET);
extract($_POST);


# In order to get the last 1000 records, I have to sort by date DESC
# Then, I need to reverse the process and count downwards by changing
# $cpt to be numrows, and subtract $cpt instead of $cpt++;
# NOTE: JpGraph can only handle 4,000 data points, so I need to make
# provisions for old companies.

if (!isset($days)) {
	$days = 270;
}

$result=db_query("
	SELECT close AS historyclose, date AS dateclose, recommendation 
	FROM ai_history 
	WHERE ticker='$ticker' 
	ORDER BY date DESC LIMIT $days");

# $cpt=0; # Normal method
$cpt=db_num_rows($result);

if ($cpt > 4000) { $cpt=3999; }
while($row = db_fetch_array($result))
{
	$xdata[$cpt]=$row['dateclose'];
	$ydata[$cpt]=$row['historyclose']; # y-axis (closing price)
	$y2data[$cpt]=(int)$row['recommendation']; # 2nd y-axis
	$cpt=$cpt-1;
#	$cpt++; # Normal method
}

// Some data
#$datax=array("2001-04-01", "2001-04-02", "2001-04-03");
#$ydata = array(11,3,8,12,5,1,9,13,5,7); #$ydata is defined above already

// Create the graph. These two calls are always required
$graph = new Graph(600,350,"auto");	
$graph->SetScale("lin");
$graph->SetY2Scale("lin",0,1);


// Show x-axis grids
$graph->xgrid->Show(true);

// Show y-axis grids
$graph->ygrid->Show(true,true);

// Show Secondary y-axis grids
#$graph->y2grid->Show(true,true);

// Set margin
#(LEFT, RIGHT, TOP, BOTTOM);
$graph->img->SetMargin(50,50,40,70);

# Set shadow background
#$graph->SetShadow();

// Set titles
$graph->title->Set("$ticker");
$graph->subtitle->Set("($days trading days shown)");
#$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->title->SetFont(FF_ARIAL,FS_BOLD,12);
$graph->yaxis->title->Set("Share Price");

$graph->y2axis->title->Set("Signal BUY=1 SELL=0");

// Setup X-scale
$graph->xaxis->SetTickLabels($xdata);
$graph->xaxis->SetFont(FF_FONT1);
$graph->xaxis->SetLabelAngle(90);
#$graph->xaxis->title->Set("Date ($days days shown)");

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot->SetColor("blue");
$lineplot->SetWeight(1);
#$lineplot->SetFillColor("red"); # Set Fill Color

$lineplot2=new LinePlot($y2data); # Secondary y-axis
$lineplot2->SetColor("orange"); # Secondary y-axis
$lineplot2->SetWeight(2); # Secondary y-axis
#$lineplot2->SetFillColor("yellow"); # Set Fill Color

// Set Legend
$graph->legend->Pos(0.05,0.05,"right","center");
$lineplot->SetLegend("Stock Price");
$lineplot2->SetLegend("Signal");

$graph->y2scale->ticks->Set(1,0.1);

// Add the plot to the graph
$graph->Add($lineplot);

$graph->AddY2($lineplot2); # Seconday y-axis


// Display the graph
$graph->Stroke();


?>
