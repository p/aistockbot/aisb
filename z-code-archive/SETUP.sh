#!/bin/sh

# setup.sh
#
# NOTE:  I'd like to keep the permission settings here to the bare minimum.
# 	 If you see something that should be improved, let me know. - Vooch
#
# 
# 2004/11/14  --  Adding check to see if PEAR is installed & working. -- blame ransombot 4 bugs ;)
# 2004/05/09  NK  a lot of improvements, now sets jpgraph settings, 
#                 base_dir, and location of PEAR
# 2004/05/09  FS  Changed to use the new include/config.php
# 2004/04/02  NK  Added a lot of things to SETUP.sh
# 2002/09/16  MS  Added comments for MySQL setup for users other than myself
# 2002/09/09  MS  Initial Release

echo "Checking PEAR.. "
#lets do the check first to save the user troubles
if [ pear ]; then
echo "PEAR: INSTALLED"
clear
else
echo "PEAR: NOT INSTALLE!!!!"
echo "Please goto http://pear.php.net/ & get PEAR, then run SETUP.sc again."
exit
fi

CONFIG_FILE="include/config.php";
#database_name="";
#database_hostname="";
#DBCHOICE=0
PWD=`pwd`;
#chown nobody:nobody index.wav
# didn't work -> chmod 755 index.wav
# didn't work -> chmod 766 index.wav

# USERS SHOULD UNCOMMENT THESE LINES IF YOUR MYSQL DATA IS IN /var/lib/mysql
# service mysqld stop
# cp mysql/aistockbot/* /var/lib/mysql/aistockbot/
# service mysqld start



# have the SETUP program generate config.php values

echo "pwd: $PWD"
echo " "

echo "Database Setup:"

database_name="aistockbot"
echo "Enter name: [aistockbot]";
read database_name
if [ -z "$database_name" ]; then
  database_name="aistockbot"
fi
echo "database name is $database_name"

echo " "

database_hostname="localhost"
echo "Enter hostname: [localhost]";
read database_hostname
if [ -z "$database_hostname" ]; then
  database_hostname="localhost"
fi
echo "database hostname is $database_hostname"

echo " "

database_username=""
while ! [ $database_username ]; do
  echo "Enter username: ";
  read database_username
done

echo " "

database_password=""
while ! [ $database_password ]; do
  echo "Enter password: ";
  read database_password
done

echo " "

DBCHOICE=0

while ! [ $DBCHOICE -eq "1" -o $DBCHOICE -eq "2" ]; do
  echo "Database choices:"
  echo "1. Use current aistockbot tables"
  echo "2. Install new aistockbot tables"
  read DBCHOICE
done

if [ $DBCHOICE -eq "1" ]; then
  echo "You chose to use your current AIStockBot database"
  # don't do anything
elif [ $DBCHOICE -eq "2" ]; then
  echo "You chose to install a new AIStockBot database"
  # run mysql command
  echo "Installing database now.."
  #loop through all the *.sql files in sql/ directory
  cd sql
  for table in `ls ai*.sql`
  do
    echo "importing table: $table"
    export fix_table=`echo $table | sed s/.sql//g`
    echo "fix_table: $fix_table"
    mysql -u $database_username --password=$database_password --exec="DROP TABLE $fix_table" $database_name
    mysql -u $database_username --password=$database_password $database_name < $table
  done
  cd ..
  echo "Done."
fi

echo " "

# for setting up the database in /var/lib/mysql
#echo "Is your MySQL data in /var/lib/mysql? [Y/n]"
#read mysql_location
#echo "you entered $mysql_location"

#####
# Create include/config.php with specified values
#####

cat > $CONFIG_FILE << END
<?php
#  include/config.php
#
# 2004/05/09  FS  Moved most globals to table ai_config
# 2004/05/09  MS/NK  Added \$base_dir (Base Directory) and \$pear_location (Location of PEAR)
# 2004/04/15  MS  Added \$global_sound
# 2003/04/29  MS  Added \$global_bypass_fundamental_data
# 2003/04/27  MS  Added \$global_aistockbotpro
# 2003/04/18  MS  Added \$global_database per Dominic Porreco
# 2002/09/20  MS  Added \$global_debug_mode
# 2002/09/16  MS  Initial Release
#

# COMMENT THESE 3 LINES OUT AFTER VERIFYING THE PATHS STATED IN JPGRAPH.PHP
#print "include/config.php says you need to edit the first DEFINE() functions ";
#print "of jpgraph-1.8/src/jpgraph.php to contain the file location absolute paths.  ";
#print "A working example is shown there.";

# Used by individual.php, learn.php, and stuff in the tools directory
\$global_database="$database_name";
\$global_hostname="$database_hostname";
\$global_username="$database_username";
\$global_password="$database_password";

# Base Directory of AIStockBot
# You will most likely need to change this depending on where you store aistockbot
\$base_dir = "$PWD";

# Location of PEAR
# This is the folder location of the PEAR modules
\$pear_location = "$PWD/PEAR/";
?>
END

echo " "

#####
# Set file permissions
#####
echo "Setting permissions.."

# So these files can get created on the fly:

touch index.wav
touch individual.sh
touch individual.wav
touch learn.wav

chmod 767 index.wav
chmod 777 index.sh
chmod 777 individual.sh
chmod 777 individual.wav
chmod 777 learn.wav
chmod 777 tools
chmod 777 csv

# Make jpgraph directory and set its permissions
DIR_TTF="$PWD/ttf/"
DIR_CACHE="/tmp/jpgraph_cache/"
mkdir /tmp/jpgraph_cache
chmod 777 /tmp/jpgraph_cache
chmod 777 /tmp/jpgraph_cache
#/tmp/jpgraph_cache/

# need to fix dir_ttf and dir_cache replacing / with \/
# fix jpgraph.php


#!/bin/sh
FILENAME="$PWD/jpgraph-1.12.1/src/jpgraph.php"

OLDLINE=`cat $FILENAME | grep "CHANGETTF"`
OLDLINE=`echo $OLDLINE | sed 's/\//\\\\\//g'`  
OLDLINE=`echo $OLDLINE | sed 's/'\ '/\\\'\ '/g'`
NEWLINE="DEFINE('TTF_DIR','$PWD/ttf/');"
NEWLINE=`echo $NEWLINE | sed 's/\//\\\\\//g' | sed 's/(/\\\(/g' | sed 's/)/\\\)/g'`  
cat $FILENAME | sed s/$OLDLINE/$NEWLINE/g > jpgraph-1.12.1/src/new.txt

cp jpgraph-1.12.1/src/new.txt jpgraph-1.12.1/src/jpgraph.php
rm jpgraph-1.12.1/src/new.txt

OLDLINE=`cat $FILENAME | grep "CHANGECACHE"`
OLDLINE=`echo $OLDLINE | sed 's/\//\\\\\//g'`
OLDLINE=`echo $OLDLINE | sed 's/'\ '/\\\'\ '/g'`
NEWLINE="DEFINE('CACHE_DIR','/tmp/jpgraph_cache/');"
NEWLINE=`echo $NEWLINE | sed 's/\//\\\\\//g' | sed 's/(/\\\(/g' | sed 's/)/\\\)/g'`  
cat $FILENAME | sed s/$OLDLINE/$NEWLINE/g > jpgraph-1.12.1/src/new.txt

cp jpgraph-1.12.1/src/new.txt jpgraph-1.12.1/src/jpgraph.php

# exit cleanly

exit 0
