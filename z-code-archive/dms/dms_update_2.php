<?php
#
# dms_update_2.php
#
# 2005/07/18  MS  Cleanup
# 2004/05/14  MS  Added extract($_GET) to build PHPSESSID as hidden form variable
# 2004/05/08  MS  Initial Release
#
$path="../";
include("$path"."include/header.php");

extract($_POST);  # Get $folder if selected from the menu
extract($_GET);   # Get $PHPSESSID

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

$sql_document=db_query("
	SELECT		id, folder, date, subject, comment
	FROM		ai_document
	WHERE		folder = '$folder'
	AND		id = '$id'
	ORDER BY	date DESC, id DESC
	");

print "<table width=100%>";

print "<tr>";
print "<td colspan=5 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Update Comment</b>";
print "</td>";
print "</tr>";

while ($row_document=db_fetch_array($sql_document)) {
	$id		= $row_document["id"];
	$folder		= $row_document["folder"];
	$date		= $row_document["date"];
	$subject	= $row_document["subject"];
	$comment	= $row_document["comment"];

	# Add New Item
	$folder=urlencode($folder); # Wipe out folder from URL before recreating
	if(!isset($query_string)) { $query_string=""; }
	$query_string=ereg_replace("folder=".urlencode($folder)."","",$query_string); # Wipe out folder from URL before recreating
	$folder=urldecode($folder); # Readjust $folder
	print "<form action=dms_update_3.php?$menu_string&$query_string method=post ENCTYPE=multipart/form-data>";
	print "<input type=hidden name=id value=$id>";
	print "<input type=hidden name=folder value=\"$folder\">";
	print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";

	print "<tr>";
	print "<td width=10% align=right>";
	print "<font face=arial size=-1>";
	print "Date:<br>";
	print "</td>";
	print "<td colspan=4 width=90%>";
	print "<input type=text name=date size=10 value=$date><br>";
	print "</td>";
	print "</tr>";

	print "<tr>";
	print "<td width=10% align=right>";
	print "<font face=arial size=-1>";
	print "Subject:<br>";
	print "</td>";
	print "<td colspan=4 width=90%>";
	print "<input type=text size=70 name=subject value=\"$subject\"><br>";
	print "</td>";
	print "</tr>";

	print "<tr>";
	print "<td width=10% align=right valign=top>";
	print "<font face=arial size=-1>";
	print "Comment:<br>";
	print "</td>";
	print "<td colspan=4 width=90%>";
	print "<textarea name=comment cols=70 rows=15>$comment</textarea><br>";
	print "</td>";
	print "</tr>";

#print "<tr>";
#print "<td width=10% align=right>";
#print "<font face=arial size=-1>";
#print "File:<br>";
#print "</td>";
#print "<td colspan=4 width=90%>";
#print "(update files not available - please delete and reinsert if you wish)";
#print "</td>";
#print "</tr>";

print "<tr>";
print "<td width=10% align=right>";
print "<br>";
print "</td>";
print "<td colspan=4 width=90%>";
print "<input type=submit value=Update><br>";
print "</td>";
print "</tr>";

print "</form>";

}

print "</table>";

include("$path"."include/footer.php");

?>

