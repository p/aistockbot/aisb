<?php

#
# dms_search_2.php
#
# 2004/07/12  MS  Added folder column.  Complies with php-4.3.7 and up now.
# 2004/05/15  MS  OCR functionality is not implemented yet, but I'm including
#		  it in the search right now (for later use)
# 2004/05/15  MS  Initial Release
#
#
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/header.php");

$default_date=date("Y-m-d");

extract($_POST); 
extract($_GET);  

# Get $username after doing extract()
#include ("$path"."include/header_invisible.php");
#$username=$a->getUsername();

if(!isset($serch_terms)) { $search_terms=""; }

$sql_document=db_query("
	SELECT		id, folder, date, subject, comment, item_1_filename
	FROM		ai_document
	WHERE		username = '$username'
	AND		(
			subject 	LIKE '%$search_terms%'
	OR		comment 	LIKE '%$search_terms%'
	OR		item_1_filename	LIKE '%$search_terms%'
	OR		ocr_item_1	LIKE '%$search_terms%'
			)
	ORDER BY	date DESC, id DESC
	");

print "<table width=100%>";

# Search Results
print "<tr>";
print "<td colspan=6 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Search Results</b>";
print "</td>";
print "</tr>";

# Print Header for Existing Records
print "<tr bgcolor=fedcba>";

print "<td width=16%>";
print "<br>";
print "</td>";

print "<td width=16%>";
print "<font face=arial size=-1>";
print "<b>Date</b>";
print "</td>";

print "<td width=16%>";
print "<font face=arial size=-1>";
print "<b>Folder</b><br>";
print "</td>";

print "<td width=16%>";
print "<font face=arial size=-1>";
print "<b>Subject</b><br>";
print "</td>";

print "<td width=16%>";
print "<font face=arial size=-1>";
print "<b>Comment</b><br>";
print "</td>";

print "<td width=16%>";
print "<font face=arial size=-1>";
print "<b> </b><br>";
print "</td>";

print "</tr>";

while ($row_document=db_fetch_array($sql_document)) {
	$id		= $row_document["id"];
	$folder		= $row_document["folder"];
	$date		= $row_document["date"];
	$subject	= $row_document["subject"];
	$comment	= $row_document["comment"];
	$item_1_filename = $row_document["item_1_filename"];

	# Print Detail
	print "<tr>";

	print "<td width=16% valign=top>";
		print "<table width=100%>";
		print "<tr>";

		print "<td width=50%>";
		print "<form action=dms_update_2.php?$menu_string method=post>";
		print "<input type=hidden name=id value=$id>";
		print "<input type=hidden name=folder value=\"$folder\">";
		print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";
		print "<input type=submit value=Update>";
		print "</form>";
		print "</td>";
	
		print "<td width=50%>";
		print "<form action=dms_delete_2.php?$menu_string method=post>";
		print "<input type=hidden name=id value=$id>";
		print "<input type=hidden name=folder value=\"$folder\">";
		print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";
		print "<input type=submit value=Delete>";
		print "</form>";
		print "</td>";

		print "</tr>";
		print "</table>";

	print "</td>";

	print "<td width=16% valign=top>";
	print "<font face=arial size=-1>";
	print "$date";
	print "</td>";

	print "<td width=16% valign=top>";
	print "<font face=arial size=-1>";
	print "$folder<br>";
	print "</td>";

	print "<td width=16% valign=top>";
	print "<font face=arial size=-1>";
	print "$subject<br>";
	print "</td>";

	print "<td width=16% valign=top>";
	print "<font face=arial size=-1>";
	print "$comment<br>";
	print "</td>";

	print "<td width=16% valign=top>";
	print "<font face=arial size=-1>";
	$test=substr($item_1_filename, -4);
	# .GIF or .JPG 
	if ($test==".jpg" OR $test==".JPG" OR $test==".gif" OR $test==".GIF") {
		print "<a href=../$item_1_filename><img border=0 height=40 src=../$item_1_filename></a><br>";
	}
	# .PDF 
	if ($test==".pdf" OR $test==".PDF") {
		print "<a href=../$item_1_filename><img border=0 height=40 src=../images/pdf.gif></a><br>";
	}
	print "</td>";

	print "</tr>";

}

print "</table>";

include_once("{$path}include/footer.php");

?>
