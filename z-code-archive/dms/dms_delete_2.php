<?php
#
# dms_delete_2.php
#
# 2004/05/14  MS  Added $username
# 2004/05/08  MS  Initial Release
#
#
$default_date=date("Y-m-d");

$path="../";
include("$path"."include/database.php");
include("$path"."include/config.php");

extract($_POST);
extract($_GET); # Grab URL variables to build submenus

# Get $username after doing extract()
include ("$path"."include/header_invisible.php");
$username=$a->getUsername();

#$query_string = $_SERVER["QUERY_STRING"];
#$query_string=ereg_replace("&&","&",$query_string);

$sql_document=db_query("
	DELETE FROM	ai_document
	WHERE		id = $id
	AND		username = '$username'
	");

# header("Location:  dms_output_1.php?folder=$folder");
#header("Location:  dms_output_1.php?$query_string&PHPSESSID=$PHPSESSID&folder=$folder");

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

header("Location:  dms_output_1.php?$menu_string&PHPSESSID=$PHPSESSID&folder=$folder");
?>
