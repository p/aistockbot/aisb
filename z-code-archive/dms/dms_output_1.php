<?php

#
# dms_output_1.php
#
# 2004/07/18  MS  Adjusted $comment so it shows <br>.  Changed column widths.
# 2004/07/02  MS  Fixed to comply with php-4.3.7 and up
# 2004/05/14  MS  Added $username to SQL query
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/30  MS  Initial Release
#
#
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/header.php");

$default_date=date("Y-m-d");

extract($_POST);  # Get $folder if selected from the menu
extract($_GET);  # Get $folder if from script (eg. dms_insert_2.php)

# To comply with php-4.3.7 and up:
if(!isset($folder)) { $folder=""; }

# Get $username after doing extract()
#include ("$path"."include/header_invisible.php");
#$username=$a->getUsername();

$sql_document=db_query("
	SELECT		id, folder, date, subject, comment, item_1_filename
	FROM		ai_document
	WHERE		folder = '$folder'
	AND		username = '$username'
	ORDER BY	date DESC, id DESC
	");

print "<table width=100%>";

# Add New Information Into Folder Name
print "<tr>";
print "<td colspan=5 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Add new information into folder: $folder</b>";
print "</td>";
print "</tr>";

# Add New Item
print "<form action=dms_insert_2.php?$menu_string method=post ENCTYPE=multipart/form-data>";
print "<input type=hidden name=folder value=\"$folder\">";

print "<tr>";
print "<td width=20% align=right>";
print "<font face=arial size=-1>";
print "Date:<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=text name=date size=10 value=$default_date><br>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td width=20% align=right>";
print "<font face=arial size=-1>";
print "Subject:<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=text size=70 name=subject><br>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td width=20% align=right valign=top>";
print "<font face=arial size=-1>";
print "Comment:<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<textarea name=comment cols=70 rows=5></textarea><br>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td width=20% align=right>";
print "<font face=arial size=-1>";
print "File:<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=file name=item_1 size=60>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td width=20% align=right>";
print "<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=submit value=\"Add New Entry\"><br>";
print "</td>";
print "</tr>";

print "</form>";

# Print Folder Name
print "<tr>";
print "<td colspan=5 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Contents of folder: $folder</b>";
print "</td>";
print "</tr>";




# PRINT HEADER INFORMATION
print "<tr bgcolor=fedcba>";

print "<td width=15%>";
print "<br>";
print "</td>";

print "<td width=5%>";
print "<font face=arial size=-1>";
print "<b>Date</b>";
print "</td>";

print "<td width=20%>";
print "<font face=arial size=-1>";
print "<b>Subject</b><br>";
print "</td>";

print "<td width=55%>";
print "<font face=arial size=-1>";
print "<b>Comment</b><br>";
print "</td>";

print "<td width=5%>";
print "<br>";
print "</td>";

print "</tr>";


# PRINT DETAIL RECORDS
while ($row_document=db_fetch_array($sql_document)) {
	$id		= $row_document["id"];
	$folder		= $row_document["folder"];
	$date		= $row_document["date"];
	$subject	= $row_document["subject"];
	$comment	= $row_document["comment"];
	$item_1_filename = $row_document["item_1_filename"];


	print "<tr>";

	print "<td width=15% valign=top>";
		print "<table width=100%>";
		print "<tr>";

		print "<td width=50%>";
		print "<form action=dms_update_2.php?$menu_string method=post>";
		print "<input type=hidden name=id value=$id>";
		print "<input type=hidden name=folder value=\"$folder\">";
		print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";
		print "<input type=submit value=Update>";
		print "</form>";
		print "</td>";
	
		print "<td width=50%>";
		print "<form action=dms_delete_2.php?$menu_string method=post>";
		print "<input type=hidden name=id value=$id>";
		print "<input type=hidden name=folder value=\"$folder\">";
		print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";
		print "<input type=submit value=Delete>";
		print "</form>";
		print "</td>";

		print "</tr>";
		print "</table>";

	print "</td>";

	print "<td width=5% valign=top>";
	print "<font face=arial size=-2>";
	print "$date";
	print "</td>";

	print "<td width=20% valign=top>";
	print "<font face=arial size=-2>";
	print "$subject<br>";
	print "</td>";

	print "<td width=55% valign=top>";
	print "<font face=arial size=-2>";
	$comment=ereg_replace("\015","<br>",$comment);
	#$comment=ereg_replace("<br><br>","<br>",$comment);
	print "$comment<br>";
	print "</td>";

	print "<td width=5% valign=top>";
	print "<font face=arial size=-2>";
	$test=substr($item_1_filename, -4);
	# .GIF or .JPG 
	if ($test==".jpg" OR $test==".JPG" OR $test==".gif" OR $test==".GIF") {
		print "<a href=../$item_1_filename><img border=0 height=40 src=../$item_1_filename></a><br>";
	}
	# .PDF 
	if ($test==".pdf" OR $test==".PDF") {
		print "<a href=../$item_1_filename><img border=0 height=40 src=../images/pdf.gif></a><br>";
	}
	print "</td>";

	print "</tr>";

}

print "</table>";

include_once("{$path}include/footer.php");

?>
