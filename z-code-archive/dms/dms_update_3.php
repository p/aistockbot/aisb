<?php
#
# dms_update_3.php
#
# 2004/05/14  MS  Added 'and username=$username' and PHPSESSID to header()
# 2004/05/08  MS  Initial Release
#
#

$default_date=date("Y-m-d");

$path="../";
include("$path"."include/database.php");
include("$path"."include/config.php");

extract($_POST);

extract($_GET); # Grab URL variables to build submenus
#$query_string = $_SERVER["QUERY_STRING"];
#$query_string=ereg_replace("&&","&",$query_string);

# Get $username after doing extract()
include ("$path"."include/header_invisible.php");
$username=$a->getUsername();

$sql_document=db_query("
	UPDATE	ai_document
	SET	folder		= '$folder'
		, date		= '$date'
		, subject	= '$subject'
		, comment	= '$comment'
	WHERE	id 		= $id
	AND	username	= '$username'
	");

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

header("Location:  dms_output_1.php?$menu_string&PHPSESSID=$PHPSESSID&folder=$folder");
?>
