<?php
#
# dms_folder_delete_2.php
#
# 2004/05/14  MS  Initial Release
#

$default_date=date("Y-m-d");

$path="../";
include("$path"."include/database.php");
include("$path"."include/config.php");

extract($_GET); # Grab URL variables to build submenus
extract($_POST); # Get $_POST after $_GET just in case there's a folder=something in the URL

# Get $username after doing extract()
include ("$path"."include/header_invisible.php");
$username=$a->getUsername();

$folder=$_POST['folder'];

$sql_document=db_query("
	DELETE FROM 	ai_document
	WHERE		username	='$username'
	AND		folder		='$folder'
	");

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# Wipe out $folder
$folder="";

# Grab the last folder so we can open up something
$sql_document2=db_query("
	SELECT		folder
	FROM 		ai_document
	WHERE		username	='$username'
	");

while ($row_document2=db_fetch_array($sql_document2)) {
	$folder		= $row_document2["folder"];
}

header("Location:  dms_output_1.php?$menu_string&PHPSESSID=$PHPSESSID&folder=$folder");
?>
