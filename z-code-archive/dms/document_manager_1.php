<?php
#
# document_manager_1.php
#
# 2004/07/13  MS  Checks to see if user is logged in first
# 2004/07/12  MS  Fixed to comply with php-4.3.7 and up.
#		  Added tests for username
# 2004/05/08  MS  Initial Release
#
#
extract($_POST);  # Get $folder if selected from the menu
extract($_GET);  # Get $folder if from script (eg. dms_insert_2.php)

$path="../";
include("$path"."include/header.php");

if($username=="") {
	print "You must be logged in before you can view the Document Manager.<br>";
	exit();
}

# Initialize variables to comply with php-4.3.7 and up:
if(!isset($folder)) { $folder = ""; }
if(!isset($PHPSESSID)) { $PHPSESSID=""; }
if(!isset($_SERVER["QUERY_STRING"])) { $_SERVER["QUERY_STRING"]=""; }

$sql_document=db_query("
	SELECT		id, folder, date, subject, comment, item_1_filename
	FROM		ai_document
	WHERE		username='$username'
	AND		folder = '$folder'
	ORDER BY	date DESC, id DESC
	");

print "<table width=100%>";

# Add Folder Name
print "<tr>";
print "<td colspan=5 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Add new folder</b>";
print "</td>";
print "</tr>";

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# Add New Item
print "<form action=dms_folder_insert_2.php?$menu_string method=post>";
print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";

print "<tr>";
print "<td width=20% align=right>";
print "<font face=arial size=-1>";
print "Folder:<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=text name=folder size=10><br>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td width=20% align=right>";
print "<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=submit value=\"Insert\"><br>";
print "</td>";
print "</tr>";

print "</form>";



# Search Term
print "<tr>";
print "<td colspan=5 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Search for items in a folder</b>";
print "</td>";
print "</tr>";

# Search For Item
print "<form action=dms_search_2.php?$menu_string method=post>";
print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";

print "<tr>";
print "<td width=20% align=right>";
print "<font face=arial size=-1>";
print "Search For:<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=text name=search_terms size=20><br>";
print "</td>";
print "</tr>";

print "<tr>";
print "<td width=20% align=right>";
print "<br>";
print "</td>";
print "<td colspan=4 width=80%>";
print "<input type=submit value=\"Search\"><br>";
print "</td>";
print "</tr>";

print "</form>";



# Edit - Delete Buttons
print "<tr>";
print "<td colspan=5 width=100% bgcolor=edcba9>";
print "<font face=arial size=-1>";
print "<b>Available folders</b>";
print "</td>";
print "</tr>";

# Edit - Delete Buttons

$sql_document=db_query("
	SELECT DISTINCT folder
	FROM		ai_document
	WHERE		username='$username'
	ORDER BY	folder
	");

while ($row_document=db_fetch_array($sql_document)) {
	$folder		= $row_document["folder"];

	print "<tr>";
	print "<td width=20% align=right>";
	# Delete button
	print "<form action=dms_folder_delete_2.php?$menu_string method=post>";
	print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";
	print "<input type=hidden name=folder value=\"$folder\">";
	print "<input type=submit value=Delete>";
	print "</form>";
	print "</td>";
	print "<td colspan=4 width=80%>";
	print "<font face=arial size=-1>";
	# Rename button
	print "<form action=dms_folder_update_2.php?$menu_string method=post>";
	print "<input type=hidden name=PHPSESSID value=\"$PHPSESSID\">";
	print "<input type=hidden name=folder_name_old value=\"$folder\">";
	print "<input type=text   name=folder_name_new value=\"$folder\" size=12>";
	print "<input type=submit value=Rename>";
	print "</form>";
	print "</td>";
	print "</tr>";
}

print "</table>";

include("$path"."include/footer.php");

?>
