<?php
#
# dms_insert_2.php
#
# 2004/07/12  MS  Conforms to php-4.3.7 and up
# 2004/05/14  MS  Added $username so it gets added to the database
#		  Added PHPSESSID to header()
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/05/02  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include("{$path}include/database.php");

$default_date=date("Y-m-d-h:i:s");

extract($_POST);
extract($_GET); # Grab URL variables to build submenus

# Get $username after doing extract()
include ("$path"."include/header_invisible.php");
$username=$a->getUsername();

# File Upload
$item_1_temporary_name = $_FILES['item_1']['tmp_name']; # temporary filename
$item_1_name = $_FILES['item_1']['name'];  # real filename
# Now, I need to VERIFY that this is either a .gif or .jpg
$item_1_length = strlen($item_1_name);
$item_1_starting_position = $item_1_length-4;
$item_1_test = substr("$item_1_name", $item_1_starting_position, 4);

if (($item_1_test==".jpg") OR ($item_1_test==".gif") OR ($item_1_test=="") OR ($item_1_test==".JPG") OR ($item_1_test==".GIF") OR ($item_1_test=="jpeg") OR ($item_1_test=="JPEG") OR ($item_1_test==".pdf") OR ($item_1_test==".PDF")) {
        # OK
} else {
        print "<font size=+0><b>File Upload Status = <font color=ff0000>FAILED</font></b></font><br><br>";
        print "Only <b>.gif</b> and <b>.jpg</b> and <b>.pdf</b> files can be uploaded at this time.<br>";
        print "Also, the max filesize limit is set to 524,288 bytes<br><br></center>"; # per php.ini?
        # Check /var/log/httpd/error_log for filesize errors if this page does not show on the browser correctly.
        # Example:  Requested content-length of 888683 is larger than the configured limit of 524288
        exit();
}
if (is_uploaded_file($_FILES['item_1']['tmp_name'])) {
        # If I wanted to copy, I would use this:
        # copy($_FILES['item_1']['tmp_name'], "/var/www/html/foobar/images");
        # But I don't want to copy, I want to move so I use this:
        # move_uploaded_file($_FILES['item_1']['tmp_name'], "/var/www/html/foobar/images/tmp.jpg");
        $item_1_mysql_name = "dms/files/$default_date-$item_1_name";
        $item_1_name = "/var/www/html/cvs_aistockbot/aistockbot/dms/files/$default_date-$item_1_name";
        move_uploaded_file($item_1_temporary_name, "$item_1_name");
        chmod("$item_1_name", 0444);
} else {
        # This shows if it's blank, so I took it out.
        # echo "Possible file upload attack. Filename: " . $_FILES['item_1']['name'];
}

if(!isset($item_1_mysql_name)) { $item_1_mysql_name=""; }

$sql_document=db_query("
	INSERT INTO 	ai_document
			(username, folder, date, subject, comment, item_1_filename)
	VALUES		(
			'$username'
			, '$folder'
			, '$date'
			, '$subject'
			, '$comment'
			, '$item_1_mysql_name'
			)
	");

# $query_string = $_SERVER["QUERY_STRING"];
# $query_string=ereg_replace("&&","&",$query_string);

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# header("Location:  dms_output_1.php?folder=$folder");
header("Location:  {$path}dms/dms_output_1.php?$menu_string&PHPSESSID=$PHPSESSID&folder=$folder");
?>
