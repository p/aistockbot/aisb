<?php
#
# dms_folder_insert_2.php
#
# 2004/07/02  MS  Fixed to comply with php-4.3.7 and up
# 2004/05/14  MS  Added $username
# 2004/05/08  MS  Initial Release
#

$default_date=date("Y-m-d");

$path="../";
include("$path"."include/database.php");
include("$path"."include/config.php");

# Added this to comply with php-4.3.7 and up:
if(!isset($_SERVER["QUERY_STRING"])) {
	$_SERVER["QUERY_STRING"]="";
}

extract($_GET); # Grab URL variables to build submenus
extract($_POST); # Get $_POST after $_GET just in case there's a folder=something in the URL

# DEBUG STUFF:
# print "PHPSESSID=$_SERVER[PHPSESSID]<br>";
# print "PHPSESSID=$_GET[PHPSESSID]<br>";
# print "PHPSESSID=$_POST[PHPSESSID]<br>";
# print "ABORTED!<br>";
# exit();

# Get $username after doing extract()
include ("$path"."include/header_invisible.php");

$username=$a->getUsername();

$sql_document=db_query("
	INSERT INTO 	ai_document
			(username, folder, date, subject, comment)
	VALUES		(
			'$username'
			, '$folder'
			, '$default_date'
			, 'Folder Added'
			, 'Folder Added'
			)
	");


include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

header("Location:  dms_output_1.php?$menu_string&PHPSESSID=$PHPSESSID&folder=$folder");
?>
