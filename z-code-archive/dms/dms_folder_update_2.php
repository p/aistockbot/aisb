<?php
#
# dms_folder_update_2.php
#
# 2004/07/12  MS  Conforms to php-4.3.7 and up
# 2004/05/15  MS  Initial Release
#

$default_date=date("Y-m-d");

$path="../";
include("$path"."include/database.php");
include("$path"."include/config.php");

extract($_GET); # Grab URL variables to build submenus
extract($_POST); # Get $_POST after $_GET just in case there's a folder=something in the URL

# Get $username after doing extract()
include ("$path"."include/header_invisible.php");
$username=$a->getUsername();

$folder_name_old=$_POST['folder_name_old'];
$folder_name_new=$_POST['folder_name_new'];

$sql_document=db_query("
	UPDATE	 	ai_document
	SET		folder		= '$folder_name_new'
	WHERE		username	= '$username'
	AND		folder		= '$folder_name_old'
	");

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

if(!isset($folder)) { $folder=""; }

header("Location:  dms_output_1.php?$menu_string&PHPSESSID=$PHPSESSID&folder=$folder_name_new");
?>
