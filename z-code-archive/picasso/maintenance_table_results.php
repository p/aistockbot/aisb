<?php
#  maintenance_table_results.php
#
#  Since several calculations are placed into the "results" table on a regular
#  basis, it might be a good idea to flush it out from time-to-time.
#  You also may want to keep the data and reflect upon how changes occur over
#  time.
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/09/09  MS  Code cleanup
# 2002/11/06  MS  Close database and flush variables
# 2002/09/16  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include("{$path}include/header.php");

print "<font face=arial size=-1><b>STEP: FLUSH RESULTS</b><br><br>";

# Perform maintenance
db_query("
	DELETE 
	FROM ai_result 
	WHERE ticker IS NOT NULL");

print "Deletion of all records in the RESULTS table is complete.<br><br>";

print "<b>END OF STEP:  FLUSH RESULTS</b><br></font>";

include_once("{$path}include/footer.php");

?>
