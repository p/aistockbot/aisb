<?php
#
# update_technical_data.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use settings from ai_config
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/09  MS  Code cleanup
# 2002/10/28  MS  Started using download variable instead of active to maintain download progress.
# 2002/10/03  MS  Delete records when stock splits and reactivate stock
# 2002/09/28  MS  Got it working with New York Stock Exchange
# 2002/09/25  MS  Initial Release 
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

print "<table width=100%>";
print "<tr>";
print "<td width=100% background=$path"."images/hex.gif>";
print "<FONT face=Arial size=2>";

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
#set_time_limit (60000);
set_time_limit (25000);

# MYSQL QUERY STATEMENT DEFINITION SECTION:
# Define MySQL Query Statements

$sqlcompany=db_query("
	SELECT 	ticker
	FROM 	ai_company
	WHERE	active=1
	AND	download=1
	");

# GET ACTIVE COMPANIES
print "<b>STEP: UPDATE TECHNICAL DATA</b> <i>(updates where active=1 and download=1)</i>:<br>";

$numrows=db_num_rows($sqlcompany);
print "Downloading data for $numrows companies<br>";

while ($row=db_fetch_array($sqlcompany)) {
	$current_ticker=$row["ticker"];

	# when you set download=0, it means you are downloading the data and don't need to get it again.
	db_query("update ai_company set download=0 where ticker='$current_ticker'");

	$sqlhistory=db_query("
		SELECT		ticker, date, close
		FROM		ai_history
		WHERE		ticker = '$current_ticker'
		ORDER BY	date DESC
	");
	$numrows_history=db_num_rows($sqlhistory);
	if ($numrows_history) {
		#
		# Compare last 30-days of data in history against
		# the same 30-days at yahoo.com.  We probably can also
		# just get the last day and not 30-days.
		#
		$rowsqlhistory=db_fetch_array($sqlhistory);
		$last_date=$rowsqlhistory["date"];
		$last_price=$rowsqlhistory["close"];
		print "Downloading data for $current_ticker.<br>";

		# Since I want to look at the last date, I make $d, $e, $f the last date.
                $f=substr($last_date,0,4);  # Year
		$d=substr($last_date,5,2);  # Month
		$d=$d-1;		    # finance.yahoo.com takes the month and subtracts one,
					    # which means January is 0
		$e=substr($last_date,8,2);  # Day

		# Grab a month's worth of data
		$b=$e;   # Day
		$c=$f;   # Year
		$a=$d-1; # Month
			if ($a == -1) {  # January is 0, but if in January and you roll back to -1, then:
				$a=11; # 11=December
				$c=$c-1; # Roll the year back to last year
			}

		# Get data from yahoo.com
		$ticker_file="../csv/$current_ticker.csv";
		$website="http://table.finance.yahoo.com/table.csv?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f&s=$current_ticker&y=0&ignore=.csv";
		$fcontents=implode("", file($website));
		$tmpfile=fopen($ticker_file, "w");
		$fp=fwrite($tmpfile,$fcontents);
		fclose($tmpfile);

		# Initialize variable
		$flag1=0;

		$fp = fopen ("$ticker_file", "r");
		while ($data=fgetcsv($fp, 7000, ",")) {
		        $num = count ($data);
		        if ($data[0] != "Date") {
				if ($flag1!=1) {
					$new_close=$data[4];
					# print "$new_close<br>";
					$flag1=1; # Raise a flag so I do this subroutine only once.
				}
			}
		}
		fclose ($fp);

		if ($new_close == $last_price) {
			# echo "prices match $new_close vs. $last_price";
			# Since the data is the same, then grab the data
			# from yahoo.com starting with the newest
			# date that is in the history table for that stock.
			# We are doing this because of stock splits and 
			# we want to get adjusted figures.
			$a=$d; # Grab month from above as the starting month.
			$b=$e+1; # Grab day from above as the starting day.
				 # Add one to it so I don't get data I already
				 # have.
			$c=$f; # Grab the year above as the starting year.
			$d=date('m'); 	#Today's month (two digits)
			$d=$d-1;	# Subtract 1 (yahoo treats Jan as 0).
			$e=date('d'); 	#Today's day (two digits)
			$f=date('Y'); 	#Today's year (four digits)

#			print "aaaa=$a b=$b c=$c d=$d e=$e f=$f";

			# Get data from yahoo.com
			$ticker_file="../csv/$current_ticker.csv";
			$website="http://table.finance.yahoo.com/table.csv?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f&s=$current_ticker&y=0&ignore=.csv";
			# IF YOU GET ERROR MESSAGES AT THIS POINT... IT MIGHT BE A HOLIDAY OR WEEKEND
			$fcontents=implode("", file($website));
			$tmpfile=fopen($ticker_file, "w");
			$fp=fwrite($tmpfile,$fcontents);
			fclose($tmpfile);

			# Add contents to database
			# REQUIRES:  $ticker_file and $current_ticker		
			$row=2;
			$fp = fopen ("$ticker_file", "r");
			while ($data=fgetcsv($fp, 7000, ",")) {
			        $row++;
			        $num = count ($data);
			        if ($data[0] != "Date") {
			                # Convert date format from dd-mmm-yy or d-mmm-yy
			                # to ISO 8601 international
			                # format (yyyy/mm/dd)
			                $day=substr($data[0],0,2);
			                $day=str_replace("-","","$day");
			
			                $month=substr($data[0],3,3);
			                if ($month == "Jan" ) { $month="01"; }
			                if ($month == "Feb" ) { $month="02"; }
			                if ($month == "Mar" ) { $month="03"; }
			                if ($month == "Apr" ) { $month="04"; }
			                if ($month == "May" ) { $month="05"; }
			                if ($month == "Jun" ) { $month="06"; }
			                if ($month == "Jul" ) { $month="07"; }
			                if ($month == "Aug" ) { $month="08"; }
			                if ($month == "Sep" ) { $month="09"; }
			                if ($month == "Oct" ) { $month="10"; }
			                if ($month == "Nov" ) { $month="11"; }
			                if ($month == "Dec" ) { $month="12"; }
	
			                if ($month == "an-" ) { $month="01"; }
			                if ($month == "eb-" ) { $month="02"; }
			                if ($month == "ar-" ) { $month="03"; }
			                if ($month == "pr-" ) { $month="04"; }
			                if ($month == "ay-" ) { $month="05"; }
			                if ($month == "un-" ) { $month="06"; }
			                if ($month == "ul-" ) { $month="07"; }
			                if ($month == "ug-" ) { $month="08"; }
			                if ($month == "ep-" ) { $month="09"; }
			                if ($month == "ct-" ) { $month="10"; }
			                if ($month == "ov-" ) { $month="11"; }
			                if ($month == "ec-" ) { $month="12"; }
			
			                $year=substr($data[0],6,3);
			                $year=str_replace("-","","$year");
		
			                if ($year < 20) {
			                        $year="20$year";
			                } else {
			                        $year="19$year";
			                }
			                $revised_date="$year/$month/$day";
		
			                $sql="
			                INSERT INTO ai_history
			                        (ticker
			                        , date
			                        , open
			                        , high
			                        , low
			                        , close
			                        , volume)
			                VALUES
			                        ('$current_ticker'
			                        , '$revised_date'
			                        , $data[1]
			                        , $data[2]
			                        , $data[3]
			                        , $data[4]
			                        , $data[5])";
	
			        $result=db_query("$sql");
			        if (!$result) {
			                print "Error performing query - Data may already exist or ticker not found<br>";
			        } else {
			                # do nothing because record added successfully.
			        }
			        } # End of (if $data[0] != "Date")
			}
			fclose ($fp);
#			db_query("update ai_company set active=0 where ticker='$current_ticker'");
		
		} else {
			# If the data is NOT the same, then delete
			# all the records in the history table, and 
			# get all the data
			print "Resyncing data for $current_ticker<br>";
			db_query("DELETE FROM ai_history WHERE ticker='$current_ticker'");
			# When you set download=1, it means you want to get the data.
			db_query("UPDATE ai_company SET download=1 WHERE ticker='$current_ticker'");
		}

flush();


	} else {
		print "You do not have data for $current_ticker ... Getting Data<br>";
		#
		# Since we have no data, we need to grab all the data for $current_ticker
		# 
		$a=0; 		# Month:  0=January...11=December
		$b=1; 		# Day
		$c=2004; 	# Year
		$d=date('m');	# Today's month (two digits)
		$d=$d-1;	# Subtract 1 because yahoo treats Jan as 0.
		$e=date('d'); 	# Today's day (two digits)
		$f=date('Y'); 	# Today's year (four digits)

		# Get data from yahoo.com
		$ticker_file="../csv/$current_ticker.csv";
		$website="http://table.finance.yahoo.com/table.csv?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f&s=$current_ticker&y=0&ignore=.csv";
		$fcontents=implode("", file($website));
		$tmpfile=fopen($ticker_file, "w");
		$fp=fwrite($tmpfile,$fcontents);
		fclose($tmpfile);

		# Add contents to database
		# REQUIRES:  $ticker_file and $current_ticker		
		$row=2;
		$fp = fopen ("$ticker_file", "r");
		while ($data=fgetcsv($fp, 7000, ",")) {
		        $row++;
		        $num = count ($data);
		        if ($data[0] != "Date") {
		                # Convert date format from dd-mmm-yy or d-mmm-yy
		                # to ISO 8601 international
		                # format (yyyy/mm/dd)
		                $day=substr($data[0],0,2);
		                $day=str_replace("-","","$day");
		
		                $month=substr($data[0],3,3);
		                if ($month == "Jan" ) { $month="01"; }
		                if ($month == "Feb" ) { $month="02"; }
		                if ($month == "Mar" ) { $month="03"; }
		                if ($month == "Apr" ) { $month="04"; }
		                if ($month == "May" ) { $month="05"; }
		                if ($month == "Jun" ) { $month="06"; }
		                if ($month == "Jul" ) { $month="07"; }
		                if ($month == "Aug" ) { $month="08"; }
		                if ($month == "Sep" ) { $month="09"; }
		                if ($month == "Oct" ) { $month="10"; }
		                if ($month == "Nov" ) { $month="11"; }
		                if ($month == "Dec" ) { $month="12"; }

		                if ($month == "an-" ) { $month="01"; }
		                if ($month == "eb-" ) { $month="02"; }
		                if ($month == "ar-" ) { $month="03"; }
		                if ($month == "pr-" ) { $month="04"; }
		                if ($month == "ay-" ) { $month="05"; }
		                if ($month == "un-" ) { $month="06"; }
		                if ($month == "ul-" ) { $month="07"; }
		                if ($month == "ug-" ) { $month="08"; }
		                if ($month == "ep-" ) { $month="09"; }
		                if ($month == "ct-" ) { $month="10"; }
		                if ($month == "ov-" ) { $month="11"; }
		                if ($month == "ec-" ) { $month="12"; }
		
		                $year=substr($data[0],6,3);
		                $year=str_replace("-","","$year");
	
		                if ($year < 20) {
		                        $year="20$year";
		                } else {
		                        $year="19$year";
		                }
		                $revised_date="$year/$month/$day";
	
		                $sql="
		                INSERT INTO ai_history
		                        (ticker
		                        , date
		                        , open
		                        , high
		                        , low
		                        , close
		                        , volume)
		                VALUES
		                        ('$current_ticker'
		                        , '$revised_date'
		                        , $data[1]
		                        , $data[2]
		                        , $data[3]
		                        , $data[4]
		                        , $data[5])";

		        $result=db_query($sql);
		        if (!$result) {
				# 2004/09/09 If there's no data in the table, it got it correctly, so don't
				# show this message:
		                # echo "Error performing query - data may already exist or ticker not found";
		        } else {
		                # do nothing because record added successfully.
		        }
		        } # End of (if $data[0] != "Date")
		}
		fclose ($fp);

#		db_query("update ai_company set download=0 where ticker='$current_ticker'");

	}
}


if (db_query_config("bypass_fundamental_data")==1) {
	print "<br><b>NOTE:  CURRENT CONFIGURATION BYPASSES FUNDAMENTAL DATA USAGE.<BR>";
	print "THIS MEANS THAT I BETTER NOT HAVE ANY FUNDAMENTAL DATA 'VARIABLES'<BR>";
	print "IN MY ANALYSIS.  IF I DO HAVE FUNDAMENTAL DATA FORMULA VARIABLES,<BR>";
	print "THEN MY ANALYSIS WILL BE VERY WRONG.  YOU HAVE THREE OPTIONS:<BR>";
	print "(1) DO NOTHING AND CONTINUE AS IS<BR>";
	print "(2) SET 'bypass_fundamental_data' TO 0<BR>";
	print "(3) REMOVE ANY FUNDAMENTAL FORMULAS AND CONTINUE<BR>";
} else {
	print "<b>NOW I'M DELETING HISTORICAL TECHNICAL DATA THAT IS EARLIER THAN MY EARLIEST FUNDAMENTAL DATA FOR EACH TICKER ";
	print "SYMBOL.  THE REASON I'M DOING THIS IS BECAUSE I MUST HAVE FUNDAMENTAL DATA IN ORDER TO GET THE WHOLE PICTURE.</b><BR>";
	include("{$path}picasso/delete_prefundamental.php");
}

print "<br><b>END OF STEP:  UPDATE TECHNICAL DATA</b>";
print "</font>";
print "<br><br><br>";
print "</td></tr></table>";

include_once("{$path}include/footer.php"); 
?>
