<?php
# picasso_4.php
#
# 2004/05/17  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/01/22  MS  Going from 10 to 50 formulas per side - gotta jack it up, baby!
# 2004/01/17  MS  Going from 5 to 10 formulas per side
# 2003/05/26  MS  Added Sellside
# 2003/05/07  MS  Initial Release
# 

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

print "<form action={$path}picasso/picasso_5.php?{$menu_string} method=post>";
print "<table width=100%>";

print "<tr>";
print "<td colspan=3 width=100%>";
print "<b>Picasso Bot</b><br>";
print "Select the combination of formulas you want and click the Submit button to run your analysis.  If all of the formulas are true for a side, you will invoke the action (BUY or SELL), depending on its turn:";
print "</td>";
print "</tr>";

# BUY SIDE
print "<tr>";
print "<td width=45%><b>BUY SIDE</b></td>";
print "<td width=5%> &nbsp;</td>";
print "<td width=50%> &nbsp;</td>";
print "</tr>";

print "<tr>";
print "<td width=45%>Variable</td>";
print "<td width=5%>Operator</td>";
print "<td width=50%>Variable</td>";
print "</tr>";

for ($c=1; $c<51; $c++) {
	print "<tr>";

	print "<td width=45%>";
	$sqlvariable=db_query("SELECT name FROM ai_variable");
	print "<SELECT NAME=leftside$c>";
	print "<OPTION></OPTION>";
		while ($rowvariable=db_fetch_array($sqlvariable)) {
			$name=$rowvariable['name'];
			print "<OPTION>$name</OPTION>";
		}
	print "</SELECT>";
	print "</td>";
	
	print "<td width=5%>";
	$sqloperator=db_query("SELECT operator FROM ai_operator");
	print "<SELECT NAME=operator$c>";
	print "<OPTION></OPTION>";
		while ($rowoperator=db_fetch_array($sqloperator)) {
			$operator=$rowoperator['operator'];
			print "<OPTION>$operator</OPTION>";
		}
	print "</SELECT>";
	print "</td>";

	print "<td width=45%>";
	$sqlvariable=db_query("SELECT name FROM ai_variable");
	print "<SELECT NAME=rightside$c>";
	print "<OPTION></OPTION>";
		while ($rowvariable=db_fetch_array($sqlvariable)) {
			$name=$rowvariable['name'];
			print "<OPTION>$name</OPTION>";
		}
	print "</SELECT>";
	print "</td>";

	print "</tr>";
}


print "<tr>";
print "<td width=45%> &nbsp;</td>";
print "<td width=5%> &nbsp;</td>";
print "<td width=50%> &nbsp</td>";
print "</tr>";


# SELL SIDE
print "<tr>";
print "<td width=45%><b>SELL SIDE</b></td>";
print "<td width=5%> &nbsp;</td>";
print "<td width=50%> &nbsp;</td>";
print "</tr>";

print "<tr>";
print "<td width=45%>Variable</td>";
print "<td width=5%>Operator</td>";
print "<td width=50%>Variable</td>";
print "</tr>";

# $d (51-100) has to be a different number sequence from $c (1-50)
for ($d=51; $d<101; $d++) {
	print "<tr>";

	print "<td width=45%>";
	$sqlvariable=db_query("SELECT name FROM ai_variable");
	print "<SELECT NAME=leftside$d>";
	print "<OPTION></OPTION>";
		while ($rowvariable=db_fetch_array($sqlvariable)) {
			$name=$rowvariable['name'];
			print "<OPTION>$name</OPTION>";
		}
	print "</SELECT>";
	print "</td>";
	
	print "<td width=5%>";
	$sqloperator=db_query("SELECT operator FROM ai_operator");
	print "<SELECT NAME=operator$d>";
	print "<OPTION></OPTION>";
		while ($rowoperator=db_fetch_array($sqloperator)) {
			$operator=$rowoperator['operator'];
			print "<OPTION>$operator</OPTION>";
		}
	print "</SELECT>";
	print "</td>";

	print "<td width=45%>";
	$sqlvariable=db_query("SELECT name FROM ai_variable");
	print "<SELECT NAME=rightside$d>";
	print "<OPTION></OPTION>";
		while ($rowvariable=db_fetch_array($sqlvariable)) {
			$name=$rowvariable['name'];
			print "<OPTION>$name</OPTION>";
		}
	print "</SELECT>";
	print "</td>";

	print "</tr>";
}

print "<tr>";
	print "<input type=hidden name=c value=$c>";
	print "<input type=hidden name=d value=$d>";
	print "<td width=45%><input type=submit value=Submit></td>";
	print "<td width=5%>&nbsp;</td>";
	print "<td width=50%>&nbsp;</td>";
print "</tr>";

print "</table>";
print "</form><br><br><br>";


include_once("{$path}include/footer.php");
?>
