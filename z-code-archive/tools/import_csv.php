<?php
# import_csv.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2002/09/16  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");

# SQL SELECT FOR EXISTENCE OF RECORD

$row=2;
$fp = fopen ("data.csv", "r");
while ($data=fgetcsv($fp, 1000, ",")) {
	$num = count ($data);
#	print "<p> $num fields in line $row: <br>";
	$row++;
	$sql="
	INSERT INTO history 
		(ticker
		, date
		, open
		, high
		, low
		, close
		, volume) 
	VALUES
		('$data[0]'
		, '$data[1]'
		, $data[2]
		, $data[3]
		, $data[4]
		, $data[5]
		, $data[6])";

	$result=db_query("$sql");
	if (!result) { 
		print "Error performing query";
	} else {
		print "Record added successfully";
	}

#	for ($c=0; $c < $num; $c++) {
#		print $data[$c] . "<br>";
#	}
}
fclose ($fp);
?>
