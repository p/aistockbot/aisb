sequence thedate
thedate = {}
thedate = prompt()
global procedure part5(atom icompany)
atom loopy
atom loopy2
sequence furapplies
sequence indicatorreturns
--atom output
sequence output
atom holder
atom todo
loopy = 1
loopy2 = 1

--output = open("Predictions.txt","a")
--puts(output,repeat('-',20)&"\n")
--puts(output,"Predictions for " & sprint(thedate[1]) & " / " & sprint(thedate[2]) & " / " & sprint(thedate[3])&"\n")
--if edbseq_length("Fur",{}) = 0 then
--	puts(output,"There are no rules. This occasionally happens when brute forcing.\nPlease run the indicator self eval, and this should fix this.")
--	return
--end if

-- I guess this is now the responsibility of the main program :(
-- The main program must check to see if there are any rules.

    setstatus("Part 5 - Predicting Rises - Checking company : "&edbseq_get("Company_Inf_Var",{icompany}))
    furapplies = {}
    for i = 1 to edbseq_length("Fur",{}) do
    furapplies &= i
    end for
    indicatorreturns = repeat(-1,edbseq_length("Indicators",{}))
    while loopy <= edbseq_length("Indicators",{}) and length(furapplies) > 0 do 
        indicatorreturns[loopy] = evaluate(edbseq_get("Indicators",{loopy}),thedate,edbseq_get("Company_Inf_Var",{icompany}))
    if indicatorreturns[loopy] = -1 then
       furapplies = {}
       exit
    else indicatorreturns[loopy] = round(indicatorreturns[loopy],roundto)
    end if
        while loopy2 <= length(furapplies) do
            if edbseq_get("Fur",{loopy2,3,loopy,2}) != indicatorreturns[loopy] then
                furapplies = delete_element(furapplies,loopy2)
            else
                loopy2 = loopy2 + 1
            end if
    handlekeys()
        end while
        loopy +=1
    loopy2 = 1
    end while
        if length(furapplies) > 0 then
            for j = 1 to length(furapplies) do
                output = {}
            if edbseq_length("Fur",{furapplies[j],2}) >= 1 then -- length(furapplies[j][2]) -- this is part of the changover from 
                output &= repeat('*',10)&"\n"               -- furapplies being a copy of fur to a list of indexes.
                output &= "Prediction : " & edbseq_get("Company_Inf_Var",{icompany}) & "\n"
                output &= "Because it conforms to rule "&furapplies[j]&"\n"
            for i = 1 to edbseq_length("Fur",{furapplies[j],3}) do
                holder = edbseq_get("Fur",{furapplies[j],3,i,1})
                output &= edbseq_get("Indicators",{holder,1}) & " = " & sprint(edbseq_get("Fur",{furapplies[j],3,i,2})) & "\n"
            end for
                holder = edbseq_length("Fur",{furapplies[j],1})+edbseq_length("Fur",{furapplies[j],2})
                holder = edbseq_length("Fur",{furapplies[j],2})/holder
                holder *= 100
                holder = round(holder,0.01)
                output &= "Mr Fuzzy Says : "&sprint(holder)&"% Chance\n"
                edbseq_append("Predictions",output)
            end if
        end for
    end if
end procedure