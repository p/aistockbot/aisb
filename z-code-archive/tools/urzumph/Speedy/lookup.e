include b_Urzinclude.e
include globaldefs.e

global constant Metastock = {{"Code","Date","Open","High","Low","Close","Volume"},{1,2,3,4,5,6,7}}

global function startdate2meta(sequence start)
atom index
index = edbseq_search("sdate",start)
if not( index = -1 ) then
    return edbseq_get("Date_Inf_Var",{index})
else
    return -1
end if
end function

global function meta2startdate(sequence meta)
atom year
atom month
atom day
year = makenumber(meta[1..4])
month = makenumber(meta[5..6])
day = makenumber(meta[7..8])
return {day,month,year}
end function

atom holesaccepted
    holesaccepted = 1/3 -- so we can change l8r

global function check_requirements(sequence startdate,sequence company,sequence requirements)
object price
object newprice
sequence lookups
sequence dayaddreturn
atom holes
object check

    holes = 0

    if dump = on then
        send2dump("Checking Requirements : \n")
        send2dump("Share : " & company & "\n")
        send2dump("Startdate : " & sprint(startdate) & "\n")
        send2dump("Requirements : " & sprint(requirements) & "\n")
    end if

    lookups = repeat(-1,3)
    lookups[3] = 6 -- closing price
    lookups[2] = edbseq_search("Company_Inf_Var",company)
    check = startdate2meta(startdate)
    if sequence(check) then
        lookups[1] = edbseq_search("Date_Inf_Var",check)
    else
        lookups[1] = -1
    end if
    if lookups[1] = -1 or lookups[2] = -1 or lookups[3] = -1 then
        if dump = on then
            send2dump(
                 "Check requirements failed because one of the lookups failed. (price)\n")
        end if
        return notok
    end if

    price = read_share_data(lookups[1],lookups[2],lookups[3])
    if compare(price,notok) = 0 then
        if dump = on then
            send2dump(
                 "Check requirements failed because read share data failed. (price) \n")
        end if
        return notok
    end if

    for i = 1 to length(requirements) do
        if dump = on then
            send2dump("Starting on " & sprint(i) & " / " & sprint(length(requirements
                                                                                 )) & 
                 " checks required\n")
        end if
        if holes > (holesaccepted * length(requirements)) then
            if dump = on then
                send2dump("Terminated inside for loop because too many holes\n")
            end if
            return notok
        end if
        if dump = on then
            send2dump("adding days onto start date.... \n")

        end if

        dayaddreturn = add_days(startdate[1],startdate[2],startdate[3],requirements[i][1])
        lookups[3] = 6 -- closing price
        lookups[2] = edbseq_search("Company_Inf_Var",company)
        check = startdate2meta(dayaddreturn)
        if sequence(check) then
            lookups[1] = edbseq_search("Date_Inf_Var",check)
        else
            lookups[1] = -1
        end if
        if lookups[1] = -1 or lookups[2] = -1 or lookups[3] = -1 then
            if dump = on then
                send2dump(
                     "Check requirements failed because one of the lookups failed. (newprice)\n"
                    )
            end if
            return notok
        end if
        if dump = on then
            send2dump("Dayadd returned successfully. " &sprint(startdate) &" + "& 
                 sprint(requirements[i][1])& " days = " &sprint(dayaddreturn)& "\n")
        end if
        newprice = read_share_data(lookups[1],lookups[2],lookups[3])

        if compare(newprice,notok) = 0 then
            holes+=1
            if dump = on then
                send2dump("read share data failed. (newprice) \n")
            end if
        elsif dump = on then
            send2dump("read share data (newprice) returned successfully.\n")
--return notok
        end if

        if (price * (requirements[i][2])) < newprice -- the min is less than the price
        and (price * (requirements[i][3])) > newprice then -- the max is greater than the price
-- do nothing
        else
            if compare(newprice,notok) != 0 then
                if dump = on then
                    if price * (requirements[i][2]) > newprice then
                        send2dump("Check requirements failed because price * ")
                        send2dump(sprint(requirements[i][2]))
                        send2dump(" > ")
                        send2dump(sprint(newprice)) 
                    elsif (price * (requirements[i][3])) < newprice then
                        send2dump("Check requirements failed because price * ")
                        send2dump(sprint(requirements[i][3]))
                        send2dump(" < ")
                        send2dump(sprint(newprice))
                    end if
                    send2dump("\n")
                end if
                return notok
            end if
        end if
    end for

    if holes < (holesaccepted * length(requirements)) then
        if dump = on then
            send2dump("Check returned OK \n")
        end if
        return ok -- it all worked, we made it out of the for loop
    else
        if dump = on then
            send2dump("Check failed because too many holes")
        end if
        return notok
    end if
end function

constant not_cached = -2
constant not_found = -1
sequence collected_cache
collected_cache = {}

function collect_information(sequence toeval3,sequence company, sequence zerodaydate)
sequence dateholder
atom counter
object temp
sequence collected
atom companylooked
atom datelooked
atom daysbroken
atom shatterlimmit
atom manual_lookup
atom cache_found

    daysbroken = 0
    --puts(1,"Collecting data.\n")
    companylooked = edbseq_search("Company_Inf_Var",company)
    if companylooked = notok then
        return statnotok
    end if
    collected = {}
--{days to max,6}
    if length(toeval3) != 2 then
        errorandabort(
        "toeval[3], which has been passed to collect_information, is too short or long."
                     )
    end if

if toeval3[1] = 1 then
shatterlimmit = 3
else
shatterlimmit = 2*toeval3[1]
end if
    counter = 0
    while length(collected) < toeval3[1] do
        manual_lookup = on
        dateholder = minus_days(zerodaydate[1],zerodaydate[2],zerodaydate[3],counter)
        cache_found = 0
        for i = 1 to length(collected_cache) do
            cache_found = 0
            if equal(collected_cache[i][1],dateholder) then
                cache_found = i
                if collected_cache[i][toeval3[2]] != not_cached then
                    if collected_cache[i][toeval3[2]] != not_found then
                        collected = append(collected,collected_cache[i][toeval3[2]])
                    else
                        daysbroken +=1
                    end if
                    manual_lookup = off
                    exit
                else
                    exit
                end if
            end if
        end for
        if manual_lookup = on then
            datelooked = edbseq_search("sdate",dateholder)
            if datelooked != notok then
                temp = read_share_data(datelooked,companylooked,toeval3[2])
                if cache_found then
                    collected_cache[cache_found][toeval3[2]] = temp
                else
                    collected_cache = append(collected_cache,{dateholder}&repeat(not_cached,6)) -- 6 = number of data pieces in metastock format
                    collected_cache[length(collected_cache)][toeval3[2]] = temp
                end if
                if not(sequence(temp)) and temp != notok then
                    collected = append(collected,temp)
                else
                    if dump = on then 
                        send2dump("INFOCOLLECTERROR: "&dateholder)
                if sequence(temp) then
                send2dump(" (return was a sequence...)")
                else
                send2dump(" (read_share_data returned fail.)")
                end if
                        send2dump("\n")
                    end if
                    daysbroken +=1
                end if
            else
                daysbroken +=1
            end if
        end if
        counter += 1 --Q : What does this do? why is it here? A : this keeps us moving back days
        if daysbroken > shatterlimmit then
            --puts(1,"returning notok, to many broken days\n")
            return notok
        end if
    end while
    if length(collected) > 0 then
    return collected
    else 
        return notok
    end if
end function


constant average = 1
constant raverage = 2
constant cmax = 3
constant rmax = 4
constant cmin = 5
constant rmin = 6
constant standdev = 7
constant rstanddev = 8
constant number_eval_types = 8
sequence cpucache
cpucache = repeat({},number_eval_types)

function check_cache(atom evalnum,sequence params)
object finder
finder = SiM(cpucache[evalnum],0,1,params)
if sequence(finder) then
    return cpucache[evalnum][finder[1]][2]
else
    return -2
end if
end function

procedure addcpucache(atom evalnum,sequence params,atom toreturn)
cpucache[evalnum] = append(cpucache[evalnum],{params,toreturn})
end procedure

function safe_devide(atom one,atom two)
if two = 0 then
    return -1
else
    return one / two
end if
end function

global function evaluate(sequence toeval,sequence zerodaydate,sequence company)
atom toreturn
object data
sequence foobar

    if dump = on then
        send2dump(repeat('*',15))
        send2dump("Evaluating " & toeval[1] & "\n")
        send2dump("Which is an " & toeval[2] & "\n")
        if compare(toeval[2],"calculation") != 0 then
            send2dump("on " & Rev_Inf_Lookup(Metastock,toeval[3][2]) & " over " & 
                 sprint(toeval[3][1]) & " days.\n")
        end if
    end if

    if length(toeval) < 3 or atom(toeval[1]) or atom(toeval[2]) or atom(toeval[3]) then
        errorandabort("The sequence passed to the function evaluate was not valid!")
    end if

    if compare(toeval[2],"average") = 0 then
        toreturn = check_cache(average,toeval[3])
        if toreturn = -2 then
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = safe_devide(find_average(data),data[1])
            if dump = on then
                send2dump("Average returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(average,toeval[3],toreturn)
        end if
        return toreturn
    end if

    if compare(toeval[2],"raverage") = 0 then
        toreturn = check_cache(raverage,toeval[3])
        if toreturn = -2 then
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = find_average(data)
            if dump = on then
                send2dump("rAverage returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(raverage,toeval[3],toreturn)
        end if
        return toreturn
    end if

    if compare(toeval[2],"max") = 0 then
        toreturn = check_cache(cmax,toeval[3])
        if toreturn = -2 then
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = safe_devide(max(data),data[1])
            if dump = on then
                send2dump("Max returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(cmax,toeval[3],toreturn)
        end if
        return toreturn
    end if

    if compare(toeval[2],"rmax") = 0 then
        toreturn = check_cache(rmax,toeval[3])
        if toreturn = -2 then
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = max(data)
            if dump = on then
                send2dump("rMax returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(rmax,toeval[3],toreturn)
        end if
        return toreturn
    end if

    if compare(toeval[2],"min") = 0 then
        toreturn = check_cache(cmin,toeval[3])
        if toreturn = -2 then
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = safe_devide(min(data),data[1])
            if dump = on then
                send2dump("Min returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(cmin,toeval[3],toreturn)
        end if
        return toreturn
    end if

    if compare(toeval[2],"rmin") = 0 then
        toreturn = check_cache(rmin,toeval[3])
        if toreturn = -2 then       
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = min(data)
            if dump = on then
                send2dump("rMin returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(rmin,toeval[3],toreturn)
        end if  
        return toreturn     
    end if

    if compare(toeval[2],"standdev") = 0 then
        toreturn = check_cache(standdev,toeval[3])
        if toreturn = -2 then
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = safe_devide(stand_dev(data,notok),data[1])
            if dump = on then
                send2dump("Standdev returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(standdev,toeval[3],toreturn)
        end if  
        return toreturn         
    end if

    if compare(toeval[2],"rstanddev") = 0 then
        toreturn = check_cache(rstanddev,toeval[3])
        if toreturn = -2 then       
            data = collect_information(toeval[3],company,zerodaydate)
            if dump = on then
                send2dump("Collect information returned : ")
                send2dump(sprint(data))
                send2dump("\n")
            end if
            if equal(data,statnotok) then
                errorandabort("Information could not properly be collected...")
            elsif equal(data,notok) then
                return notok
            end if
            toreturn = stand_dev(data,notok)
            if dump = on then
                send2dump("rStanddev returned : ")
                send2dump(sprint(toreturn))
                send2dump("\n")
            end if
            addcpucache(rstanddev,toeval[3],toreturn)
        end if  
        return toreturn         
    end if

    if compare(toeval[2],"calculation") = 0 then
        data = {{},{}}
        foobar = {{},{}}
        if atom(toeval[3][1]) then
            foobar[1] = minus_days(zerodaydate[1],zerodaydate[2],zerodaydate[3],toeval[3][4])
            --foobar[1] = startdate2meta(foobar[1])
            --if search(DatesInfVar[1],foobar[1]) = -1 then
            --    if dump = on then
            --        puts(dump_location,
            --             "Calculation failed because the first date was not found in Datesinfvar\n"
            --            )
            --	end if
            --  return notok
            --end if
            if dump = on then
                send2dump("Modifying Contents of data[1] (atom)\n")
            end if
            data[1] = collect_information({1,toeval[3][1]},company, foobar[1])
            if sequence(data[1]) then
                data[1] = data[1][1]
            end if
            --data[1] = read_share_data(Inf_Lookup(DatesInfVar,foobar[1]),Inf_Lookup(
            --CompanyInfVar,
            --company),toeval [3][1])
            if dump = on then
                send2dump("toeval[3][1] was an atom. data[1] is now " & sprint(data[1]) & "\n")
            end if
        end if
        if atom(toeval[3][3]) then
            foobar[2] = minus_days(zerodaydate[1],zerodaydate[2],zerodaydate[3],toeval[3][5])
            --foobar[2] = startdate2meta(foobar[2])
            --if search(DatesInfVar[1],foobar[2]) = -1 then
            --    if dump = on then
            --        puts(dump_location,
            --             "Calculation failed because the first date was not found in Datesinfvar\n"
            --            )
            --    end if
            --    return notok
            --end if
            if dump = on then
                send2dump("Modifying Contents of data[2] (atom)\n")
            end if
            data[2] = collect_information({1,toeval[3][3]},company, foobar[2])
            if sequence(data[2]) then
                data[2] = data[2][1]
            end if
            --data[2] = read_share_data(Inf_Lookup(DatesInfVar,foobar[2]),Inf_Lookup(
            --CompanyInfVar,
            --company),toeval[3][3])
            if dump = on then
                send2dump("toeval[3][3] was an atom. data[2] is now " & sprint(data[2]) & "\n")
            end if
        end if

        if sequence(toeval[3][1]) then
            if dump = on then
                send2dump("Modifying the contents of data[1] (sequence)\n")
            end if
            data[1] = evaluate(toeval[3][1],zerodaydate,company)
            if dump = on then
                send2dump("toeval[3][1] was a sequence. data[1] is now " & sprint(data[1]) & "\n")
            end if
        end if
        if sequence(toeval[3][3]) then
            if dump = on then
                send2dump("Modifying contents of data[2] (sequence)\n")
            end if
            data[2] = evaluate(toeval[3][3],zerodaydate,company)
            if dump = on then
                send2dump("toeval[3][3] was a sequence. data[2] is now " & sprint(data[2]) & "\n")
            end if
        end if

        if equal(data[1],-1) or equal(data[2],-1) or equal(data[1],{}) or equal(data[2],{}) then
            if equal(data[1],{}) or equal(data[2],{}) then
                if dump = on then
                    send2dump(
                         "ERROR! Data[1] or [2] is {}. This should NEVER happen!\n")
                    send2dump("occoured on : " & toeval[1] & "\n")
                end if
            end if
            return notok
        end if

        if toeval[3][2] = '/' then
            if atom(data[2]) and data[2] = 0 then
                puts(1,"\n\n\n")
                puts(1,"WARNING WARNING! Attempted devide by 0! \nThere is something wrong with your indicators!")
                puts(1,"\nIgnoring.....\n\n\n")
                if dump = on then
                    send2dump("Attempted devide by 0 on indicator ")
                    send2dump(toeval[1])
                    send2dump("\n")
                end if
                return notok
            end if
            toreturn = data[1] / data[2]
        elsif toeval[3][2] = '-' then
            toreturn = data[1] - data[2]
        elsif toeval[3][2] = '+' then
            toreturn = data[1] + data[2]
        elsif toeval[3][2] = '*' then
            toreturn = data[1] * data[2]
        else
            errorandabort("the sign for a calculation, \"" & toeval[3][2] & 
                          "\" is not a recognised sign.")
        end if
        if dump = on then
            send2dump("Calculation returned : ")
            send2dump(sprint(toreturn))
            send2dump("\n")
        end if
        return toreturn
    end if

end function

----------------------------------------------------------------------------------------------------------------------------
global function lookup_indicators(sequence startdate,sequence company)
sequence toreturn
--sequence adate
object toevalreturn
atom lengthind
sequence currindicator
collected_cache = {}
cpucache = repeat({},number_eval_types)
lengthind = edbseq_length("Indicators",{})


    toreturn = {}
    for i = 1 to lengthind do
        handlekeys() -- makes more reponsive
        currindicator = edbseq_get("Indicators",{i})
        toevalreturn = evaluate(currindicator,startdate,company)
        if not(equal(toevalreturn,notok)) then
            toreturn = append(toreturn,{i,round(toevalreturn,roundto)})
        else
            toreturn = append(toreturn,-1)
        end if -- else, silently ignore
    end for

    return toreturn
end function 