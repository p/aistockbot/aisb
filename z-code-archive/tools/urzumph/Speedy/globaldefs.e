include b_Urzinclude.e
include databasemod.e
include edb_as_seq.e
include misc.e
--include Win32lib.ew
object foobar
global sequence sharedir
without warning
global constant edbdebug = on
atom searchreturn
atom code
atom remote_share_data
global atom roundto
global sequence server_IP
remote_share_data = off

global procedure FATAL_DATA_ERROR(sequence error)
atom filehandle
filehandle = open("Error.txt","a")
puts(filehandle,repeat('-',40)&"\n")
puts(filehandle,"There has been a fatal error which has forced the program to close. The error was :\n")
puts(filehandle,error)
puts(filehandle,"\n")
abort(0)
end procedure
------------------------------------------------------------------------------
global function makeriselosses()
atom maxriseloss
atom temp
sequence goted
sequence days
maxriseloss = 0
for i = 1 to edbseq_length("RiseChecks",{}) do
goted = edbseq_get("RiseChecks",{i})
days = {}
for j = 1 to length(goted) do
    days &= goted[j][1]
end for
temp = max(days)
if temp > maxriseloss then
    maxriseloss= temp
end if
end for

for i = 1 to edbseq_length("LossChecks",{}) do
goted = edbseq_get("LossChecks",{i})
days = {}
for j = 1 to length(goted) do
    days &= goted[j][1]
end for
temp = max(days)
if temp > maxriseloss then
    maxriseloss= temp
end if
end for
return maxriseloss
end function
--------------------------------------------------------------------------------
global procedure getoptions()
if platform() < 3 then
sharedir = edbseq_get("Options",{1})
else 
sharedir = edbseq_get("Options",{2})
end if
roundto = edbseq_get("Options",{3})
end procedure
-----------------
function remote_rsd(sequence filename)
atom file
sequence netret
atom loop
loop = 1
if edbseq_search("RSD_BAD_CACHE",filename) = -1 then -- if we have looked for it before and it was not found
    netret = expect_response(netsock,"RSD>"&filename)-- then it is not magically going to appear
    while loop < 5 and equal(netret,"TIMEOUT") do
    netret = expect_response(netsock,"RSD>"&filename)
    end while
    if equal(netret,"SENDING") then
        file = open(sharedir&filename,"w")
        netret = ""
        while not(equal(netret,"RSD>DONE")) do
            if not(equal(netret,"TIMEOUT")) then
                puts(file,netret)
            else
                --FATAL_DATA_ERROR("Request for share data timed out!")
                close(file)
                return remote_rsd(filename) -- IE: delete and try again
            end if
            netret = expect_response(netsock,"RSD-ACK")
        end while
        close(file)
        return open(sharedir&filename,"r")
    else
        send2dump("GlobDefs> Failed to open "&filename&" & server did not have it either. Returning -1 \n")
        edbseq_append("RSD_BAD_CACHE",filename)
        return notok
    end if
else
    return -1
end if
end function


global function read_share_data(atom date,atom company,atom line)
atom mydump
sequence filename
sequence companyalpha
atom file
atom mnret
atom found
object geter
object toreturn
sequence netret
mydump = dump
dump = off
    if date = -1 or company = -1 then 
        return -1
    end if
    companyalpha = edbseq_get("Company_Inf_Var",{company})
    filename = edbseq_get("Date_Inf_Var",{date}) & ".txt"
    file = open(sharedir&filename,"r")
    if file = -1 then
        file = remote_rsd(filename)
    end if
    if file = -1 then
        return -1
    end if
    geter = 0
    found = notok
    while compare(geter,-1) != 0 and found = notok do
        geter = gets(file)
        if sequence(geter) and length(geter) > length(companyalpha) and equal(geter[1..length(companyalpha)],
                                                           companyalpha) then
            if geter[length(geter)] = '\n' then
                geter = delete_element(geter,length(geter))
            end if
            toreturn = text_to_sequence(geter)
            if length(toreturn) < line then
                dump = mydump
                return notok
            end if
            if check_seq_is_number(toreturn[line]) = ok then
                close(file)
                mnret = makenumber(toreturn[line]) 
                dump = mydump
                return mnret
            else
                close(file)
                dump = mydump
                return toreturn[line]
            end if
            found = ok
        end if
    end while
    close(file)
    dump=mydump
    send2dump("GlobDefs> Company not found, returning -1\n")
    return notok
end function
-----------------------------------------------------------------
object dumpsize
global procedure handlekeys()
-- I don't like this much, but again short of conditional inculdes, it must be done.
handleevents()
--call_proc(eventhandler, {}) 
---------------------------------------
dumpsize = dir(mydir() & "dump.txt")
if sequence(dumpsize) and dumpsize[1][D_SIZE] > 200*1024*1024 then
dump = off
end if
end procedure

global procedure log_comm(sequence program,sequence comm)
atom filehandle
atom finder
sequence datime
object dumpsize
filehandle = -1
dumpsize = dir(mydir() & "commlog.txt")
if atom(dumpsize) or (sequence(dumpsize) and dumpsize[1][D_SIZE] <= 200*1024*1024) then
datime = date()
while filehandle = -1 do
filehandle = open("commlog.txt","a")
end while
puts(filehandle,sprintf("Recieved by %s | %d : %d : %d | %s |\n",{program,datime[4],datime[5],datime[6],comm}))
close(filehandle)
end if
end procedure

global procedure getoptions_ini()
sequence filepaths
filepaths = INI_to_Inf("paths.ini")
if not(equal(filepaths,{{"Error"},{"paths.ini could not be opened"}})) then
if platform() < 3 then
    sharedir = Inf_Lookup(filepaths,"Windows Directory")
else
    sharedir = Inf_Lookup(filepaths,"Linux Directory")  
end if
else
    FATAL_DATA_ERROR("Paths.ini could not be found. Please create it.")
end if
roundto = edbseq_get("Options",{3})
end procedure
sequence temp
temp = INI_to_Inf("paths.ini")
if not(equal(temp,{{"Error"},{"paths.ini could not be opened"}})) then
    server_IP = IPString_to_IP(Inf_Lookup(temp,"Server"))
end if
-----------------------------------------------------------------Command line switches.
--	puts(1,"Parsing Command line switches.\n")
sequence commandswitch
    commandswitch = command_line()
    if length(commandswitch) < 3 then 
        --puts(1,"There are no command-line switches to parse.\n")
    else
        for i = 3 to length(commandswitch) do
--if equal(commandswitch[i],"-c") then
--puts(1,"Crashsafe mode command line switch detected. Crashsafe mode is now ON\n")
--crashsafemode = on
-- crashsafemode is obsolete, as we are using edb
            if equal(commandswitch[i],"-w") then
                --puts(1,"Waiting 30 seconds for OS to finish booting....\n")
                for j = 1 to 30 do
                    sleep(1)
                    --doEvents(0)
                end for
            end if
        end for
    end if