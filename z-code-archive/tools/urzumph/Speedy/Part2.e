function get_largest_data_req()
atom big
atom thisbig
big = 0
for i = 1 to edbseq_length("RiseChecks",{}) do
    for j = 1 to edbseq_length("RiseChecks",{i}) do
        thisbig = edbseq_get("RiseChecks",{i,j,1})
        if thisbig > big then
            big = thisbig
        end if
    end for
end for
for i = 1 to edbseq_length("LossChecks",{}) do
    for j = 1 to edbseq_length("LossChecks",{i}) do
        thisbig = edbseq_get("LossChecks",{i,j,1})
        if thisbig > big then
            big = thisbig
        end if
    end for
end for
return big
end function

global procedure part2(atom icompany,sequence start,sequence theend)
sequence data
sequence temp_date
atom longest_data
atom date
sequence sdate
sequence check
sequence company
company = edbseq_get("Company_Inf_Var",{icompany})
setstatus("Part 2 - Finding Rises and Losses in data for "&company)
data = {}
longest_data = get_largest_data_req()
temp_date = start
for i = 0 to longest_data do
    date = edbseq_search("sdate",temp_date)
    if date != -1 then
        data = append(data,read_share_data(date,icompany,6))
    else
        data = append(data,-1)
    end if
    temp_date = add_days(temp_date[1],temp_date[2],temp_date[3],1)
end for
while not(equal(Date_Compare(temp_date,theend),"AFTER")) do
    date = edbseq_search("sdate",temp_date)
    if date != -1 then
        data = append(data,read_share_data(date,icompany,6))
    else
        data = append(data,-1)
    end if
    data = delete_element(data,1)
    for i = 1 to edbseq_length("RiseChecks",{}) do
        for j = 1 to edbseq_length("RiseChecks",{i}) do
            handlekeys()
            check = edbseq_get("RiseChecks",{i,j})
            if not(data[check[1]] = -1 or data[1] = -1) then
            if data[check[1]] > (data[1]*check[2]) and data[check[i]] < (data[1]*check[3]) then
                -- data[checkday] > min and < max
                edbseq_append("Todo",{3,'w',{date,icompany}})
            end if
            end if
        end for
    end for
    for i = 1 to edbseq_length("LossChecks",{}) do
        for j = 1 to edbseq_length("LossChecks",{i}) do
            handlekeys()
            check = edbseq_get("LossChecks",{i,j})
            if not(data[check[1]] = -1 or data[1] = -1) then
            if data[check[1]] > (data[1]*check[2]) and data[check[i]] < (data[1]*check[3]) then
                -- data[checkday] > min and < max
                --send2dump(sprintf("Adding loss, %g is > than the required min %g, and < than the max %g\n",
                --{data[check[1]],data[1]*check[2],data[1]*check[3]}))
                edbseq_append("Todo",{4,'w',{date,icompany}})
            end if
            end if
        end for
    end for
temp_date = add_days(temp_date[1],temp_date[2],temp_date[3],1)  
end while   
end procedure   













































--atom searchreturn
--atom checkok
--sequence ndate
--atom loopstodo
--sequence multiRLcheck
--atom temp
--atom idate
--atom icompany
--atom lastRLwrite
--sequence tempseq
--sequence macompany
--
--	if edbseq_length("RiseChecks",{}) = 0 or edbseq_length("LossChecks",{}) = 0 then
--		puts(1,
--			 "Your config has not been properly configured. Please run the config program to configure it."
--			)
--		sleep(5)
--		abort(0)
--	end if
--
--	idate = edbseq_search("Date_Inf_Var",madate)
--	ndate = edbseq_get("sdate",{idate})
--	icompany = 1
--	edbseq_assign("New",{},{})
--	setstatus("Part 2 - Finding Rises and Losses in data on "&madate)
--	while icompany <= edbseq_length("Company_Inf_Var",{}) do
--		macompany = edbseq_get("Company_Inf_Var",{icompany})
--
--		for icheckslist = 1 to edbseq_length("RiseChecks",{}) do
--			-- only 1 computer does each date & company pair. Therefore, we can save the check in ram.
--			handlekeys()
--			--temp = edbseq_length("RisesFound",{})
----			if temp > 0 then
----				multiRLcheck = edbseq_get("RisesFound",{temp})
----				multiRLcheck = multiRLcheck[1..2]
----			else
----				multiRLcheck = {-1,-1}
----			end if -- if it is the first check for rises, then it CAN'T have been done before
---- this is new
--			if icheckslist = 1 then
--				multiRLcheck = {-1,-1}
--			end if
---- this is mostly old
--			checkok = check_requirements(ndate,macompany,edbseq_get("RiseChecks",{icheckslist}
--																   ))
--
--			if checkok = ok then
--				if not(equal(multiRLcheck,{idate,icompany})) then --searchreturn = -1 then				
--					-- date index, company index, applicable rises/losses
--					lastRLwrite = edbseq_length("RisesFound",{}) + 1
--					edbseq_append("RisesFound",{idate,icompany,{icheckslist}})
--					edbseq_append("New",{'r',idate,icompany})
--					multiRLcheck = {idate,icompany}
--				else
--					edbseq_assign("RisesFound",{lastRLwrite,3},
--								  edbseq_get("RisesFound",{lastRLwrite,3})&
--								  icheckslist)
--				end if
--			end if
--
--		end for
--
--		for lcheckslist = 1 to edbseq_length("LossChecks",{}) do
--			handlekeys()
----			temp = edbseq_length("LossesFound",{})
----			if temp > 0 then
----				multiRLcheck = edbseq_get("LossesFound",{temp})
----				multiRLcheck = multiRLcheck[1..2]
----			else
----				multiRLcheck = {-1,-1}
----			end if -- if it is the first check for rises, then it CAN'T have been done before
---- this is new
--			if lcheckslist = 1 then
--				multiRLcheck = {-1,-1}
--			end if
---- this is mostly old
--			checkok = check_requirements(ndate,macompany,edbseq_get("LossChecks",{lcheckslist}
--																   ))
--
--			if checkok = ok then
--				if not(equal(multiRLcheck,{idate,icompany})) then --searchreturn = -1 then
--					lastRLwrite = edbseq_length("LossesFound",{}) + 1				
--					edbseq_append("LossesFound",{idate,icompany,{lcheckslist}})
--					edbseq_append("New",{'l',idate,icompany})
--					multiRLcheck = {idate,icompany}
--				else
--					edbseq_assign("LossesFound",{lastRLwrite,3}, 
--					-- Assign x to LossesFound[lastRLwrite][3]
--								  edbseq_get("LossesFound",{lastRLwrite,3}
--											)&lcheckslist) 
--					-- x = LossesFound[lastRLwrite][3]&icheckslist
--				end if
--			end if
--
--		end for
--
--		icompany +=1
--
--		handlekeys()
--	end while
--
--	for i = 1 to edbseq_length("New",{}) do
--		tempseq = edbseq_get("New",{i})
--		if tempseq[1] = 'r' then
--			edbseq_append("Todo",{3,'w',tempseq[2],tempseq[3]})
--		elsif tempseq[1] = 'l' then
--			edbseq_append("Todo",{4,'w',tempseq[2],tempseq[3]}) 
--		end if
--	end for
--end procedure