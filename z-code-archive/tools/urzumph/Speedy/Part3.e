global procedure part3(atom idate,atom icompany)
object temp
object foobar
sequence DateComp
sequence date
sequence company
atom searchreturn
atom todo

    date = edbseq_get("sdate",{idate})
    company = edbseq_get("Company_Inf_Var",{icompany})
    setstatus("Part 3 - Collecting information on Rises - "&company&" : "&sprint(date[1])&"/"&sprint(date[2])&"/"&sprint(date[3]))
    temp = lookup_indicators(date,company)
    for i = 1 to length(temp) do
        if not(equal(temp[i],-1)) then
        searchreturn = edbseq_wildsearch("SelfEval",{'*','*',temp[i]})
        DateComp = {idate,icompany}
        if searchreturn = -1 then
            edbseq_append("SelfEval",{{},{DateComp},temp[i]})
        else
            foobar = edbseq_get("SelfEval",{searchreturn,2})
            foobar = append(foobar,DateComp)
            edbseq_assign("SelfEval",{searchreturn,2},foobar)
        end if
        end if
    end for
    if not find(-1,temp) then
        searchreturn = edbseq_wildsearch("Fur",{'*','*',temp})
        DateComp = {idate,icompany}
        if searchreturn = -1 then
            edbseq_append("Fur",{{},{DateComp},temp})
        else
            foobar = edbseq_get("Fur",{searchreturn,2})
            foobar = append(foobar,DateComp)
            edbseq_assign("Fur",{searchreturn,2},foobar)
        end if
    end if

handlekeys()
end procedure