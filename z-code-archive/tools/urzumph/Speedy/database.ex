without warning
global atom log_ID
include get.e
include bounce!.ew
global atom netsock
include b_Urzinclude.e
include databasemod.e
include edb_as_seq.e

edbseq_init("MYCrashlog",off)
constant FALSE = (1!=1)
object foobar
sequence command
sequence index
sequence ret
sequence seq
atom exitme
exitme = FALSE

function unprint(sequence data)
atom filehandle
sequence ret
filehandle = open("temp.pnt","w")
puts(filehandle,data)
close(filehandle)
filehandle = open("temp.pnt","r")
ret = get(filehandle)
if ret[1] = GET_SUCCESS then
    return ret[2]
else 
    return -1
end if
end function

while exitme = FALSE do
    command = prompt_string("Your Command Master? ")
    if length(command) >= 3 and equal(command[1..3],"get") then
        seq = command[searchr(command,' ')+1..length(command)]
        index = {}
        foobar = -1
        while foobar != 0 do
        foobar = prompt_number("Next Index? (0 exits) ",{0,999})
        if foobar != 0 then 
            index = append(index,foobar)
        end if
        end while
        ret = edbseq_get(seq,index)
        if equal(index,{}) then
        puts(1,"Listing "&seq&"\n")
        puts(1,seq&" has "&sprint(edbseq_length(seq,index))&" records\n")
            for i = 1 to length(ret) do
                puts(1," Index : "&sprint(i)&" Value : "&sprint(ret[i])&"\n")
            end for
        else
            puts(1,sprint(ret)&"\n")
        end if
    elsif length(command) > length("length") and equal(command[1..7],"length ") then
	ret = command[8..length(command)]
	? edbseq_length(ret,{})
    elsif length(command) >= 6 and equal(command[1..6],"assign") then
        seq = command[searchr(command,' ')+1..length(command)]
        index = {}
        foobar = -1
        while foobar != 0 do
        foobar = prompt_number("Next Index? (0 exits) ",{0,999})
        if foobar != 0 then 
            index = append(index,foobar)
        end if
        end while
        foobar = prompt_string("Please Print what you wish to assign ")
        edbseq_assign(seq,index,foobar)
    elsif length(command) = length("list_tables") and equal(command,"list_tables") then
        ret = db_table_list() 
        for i = 1 to length(ret) do
            puts(1,"Table "&sprint(i)&" "&ret[i]&"\n")
        end for
    elsif equal(command,"exit") then
    abort(1)
    end if
end while
        
                
        