sequence datereturn
global function prompt()
    sequence thedate
    thedate = {-1,-1,-1}
    datereturn = date()
    thedate[1] = datereturn[3]
    thedate[2] = datereturn[2]
    thedate[3] = datereturn[1] + 1900
return thedate
end function