global procedure part4(atom idate,atom icompany)
object temp
object foobar
sequence DateComp
sequence date
sequence company
atom searchreturn

    date = edbseq_get("sdate",{idate})
    company = edbseq_get("Company_Inf_Var",{icompany})
    temp = lookup_indicators(date,company)
    setstatus("Part 4 - Collecting information on Losses - "&company&" : "&sprint(date[1])&"/"&sprint(date[2])&"/"&sprint(date[3]))
    for i = 1 to length(temp) do
        if not(equal(temp[i],-1)) then
        searchreturn = edbseq_wildsearch("SelfEval",{'*','*',temp[i]})
        DateComp = {idate,icompany}
        if searchreturn = -1 then
            edbseq_append("SelfEval",{{DateComp},{},temp[i]})
        else
            foobar = edbseq_get("SelfEval",{searchreturn,1})
            foobar = append(foobar,DateComp)
            edbseq_assign("SelfEval",{searchreturn,1},foobar)
        end if
        end if
    end for
    if not find(-1,temp) then
        searchreturn = edbseq_wildsearch("Fur",{'*','*',temp})
        DateComp = {idate,icompany}
        if searchreturn = -1 then
            edbseq_append("Fur",{{DateComp},{},temp}) 
            -- This line cut because we only want it added if the loss conforms to the rule.
            -- This line should not be cut because A) predictions takes this into account, and B)
            -- It is possible that a rise could fit it near the end, and lots of losses fit it at the start.
        else
            foobar = edbseq_get("Fur",{searchreturn,1})
            foobar = append(foobar,DateComp)
            edbseq_assign("Fur",{searchreturn,1},foobar)
        end if
    end if

handlekeys()
end procedure