-- note, globaldefs, lookup, edb_as_seq , win32inc, Urzinclude, Urzwin32inc and others are already included VIA the main app.
-- This was format.
without warning

global procedure part1()
atom searchreturn
sequence dirret
sequence donedates
sequence loadcsv
sequence dates2do
sequence mydate
sequence sdate
sequence part2

    dates2do = {}

    dirret = dir(sharedir)
    handlekeys()
    dirret = crop_folders(dirret)
    handlekeys()
    dirret = strip_dir_info(dirret)
    handlekeys()
    dirret = checkextension(dirret, {".txt"})
    dirret = sortA(dirret)
    handlekeys()

    donedates = edbseq_get("Date_Inf_Var",{})
    setstatus("Part 1 - Finding Needed Dates")
    edbseq_assign("New",{},{})
    for i = 1 to length(dirret) do
        handlekeys()
        searchreturn = edbseq_search("Date_Inf_Var",dirret[i][1..length(dirret[i])-4])
        if searchreturn = -1 then
            dates2do = append(dates2do,dirret[i])
--			edbseq_append("New",dirret[i][1..length(dirret[i])-4])
        end if
    end for

    handlekeys()
    for i = 1 to length(dates2do) do
        setstatus("Part 1 - Finding Companies and Converting Date Formats - "&sprint(i)&" / "&
                  sprint(length(dates2do)))
        loadcsv = opencsv(sharedir & dates2do[i] ) 
        for j = 1 to length(loadcsv) do
            if length(loadcsv[j]) > 0 and edbseq_search("Company_Inf_Var",loadcsv[j][1]) = -1 then
                edbseq_append("Company_Inf_Var",loadcsv[j][1])
            end if
            handlekeys()
        end for
        edbseq_append("Date_Inf_Var",dates2do[i][1..length(dates2do[i])-4])
        edbseq_append("sdate",meta2startdate(dates2do[i]))
        handlekeys()

    end for

--	mydate = edbseq_get("sdate",{edbseq_length("sdate",{})})
--	mydate = minus_days(mydate[1], mydate[2],mydate[3],maxriseloss)
--	for i = 1 to edbseq_length("New",{}) do
--		sdate = meta2startdate(edbseq_get("New",{i}))
--		compared = Date_Compare(sdate,mydate)
--		if equal(compared,"BEFORE") or equal(compared,"SAME") then
--		edbseq_append("Todo",{2,'w',edbseq_get("New",{i})})
--		end if
--	end for
if length(dates2do) > 0 then
    mydate = sortA(dates2do)
    mydate = {mydate[1]}&{mydate[length(mydate)]}
    part2 = {meta2startdate(mydate[1]),meta2startdate(mydate[2])}
    for i = 1 to edbseq_length("Company_Inf_Var",{}) do
        edbseq_append("Todo",{2,'w',{i,part2[1],part2[2]}})
    end for
end if      
send2dump("Part 1 Complete.\n")
end procedure