---------------------------------------------------------------------------------
-- Clib.e
-- version 1.3
-- written by Chris Bensler
---------------------------------------------------------------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- BITWISE MANIPULATORS --

------------------------------------------------
global function lo_byte( integer i1 )
------------------------------------------------
-- syntax: i2 = lo_byte( i1 )
-- description:
--    returns the low byte i2, from the word(2 bytes) i1

    -- return low byte
    return and_bits( i1, #FF )
end function

------------------------------------------------
global function hi_byte( integer i1 )
------------------------------------------------
-- syntax: i2 = hi_byte( i1 )
-- description:
--    returns the high byte i2, from the word(2 bytes) i1

    -- return high byte
    return floor( and_bits(i1,#FF00) / #100 )
end function

------------------------------------------------
global function lo_word( atom a )
------------------------------------------------
-- syntax: i = lo_word( a )
-- description:
--    returns the low word(2 bytes) i, from the long(4 bytes) a

    -- return low word
    return and_bits( a, #FFFF )
end function

------------------------------------------------
global function hi_word( atom a )
------------------------------------------------
-- syntax: i =  hi_word( a )
-- description:
--    returns the high word(2 bytes) i, from the long(4 bytes) a

    -- return high word
    return and_bits(floor( a / #10000 ), #FFFF)
end function

------------------------------------------------
global function make_word( integer lo, integer hi)
------------------------------------------------
-- syntax: i = make_word( lo, hi )
-- description:
--    make 2 bytes (8bit) into a word (16bit)

   -- mask values ( copied from Derek Parnell's tk_math.e )
   lo  = and_bits( lo,  #FF )
   hi = and_bits( hi, #FF )
   
   return hi*#100 + lo
end function

------------------------------------------------
global function make_long( integer lo, integer hi)
------------------------------------------------
-- syntax: a = make_long( lo, hi )
-- description:
--    make 2 words (16bit) into a long (32bit)

   -- mask values ( copied from Derek Parnell's tk_math.e )
   lo  = and_bits( lo,  #FFFF )
   hi = and_bits( hi, #FFFF )

   return hi*#10000 + lo
end function

------------------------------------------------
global function or_all(sequence s)
------------------------------------------------
-- syntax: a = or_all( s )
-- description:
--    combine a sequence of atoms using or_bits(), and return the value

 atom a
   a = 0

   for i = 1 to length(s) do
      a = or_bits(a,s[i])
   end for
   return a
end function

-----------------------------------------------------------------------------
-- MEMORY DATA HANDLERS --
------------------------------------------------
global procedure poke_string(atom a, sequence s)
------------------------------------------------
-- syntax: poke_string( a, s )
-- description:
--    pokes the string sequence s, to address a, as a null-terminated string

   poke(a, s)
   poke(a+length(s), 0)
end procedure

------------------------------------------------
global function peek_string(atom x)
------------------------------------------------
-- syntax: s = peek_string( x )
-- description:
--    retrieves a null-terminated string from address x.
--    if x is a sequence, it is assumed to be an array of addresses,
--       an array of strings will be returned respectively
 atom offset

   offset = x
   while peek(offset) do offset +=1 end while
   return peek({x,offset-x})
end function

------------------------------------------------
global procedure poke2(atom a, object x)
------------------------------------------------
-- syntax: poke2( a, x )
-- description:
--    pokes x as a word, to address a
--    if x is a sequence, it will be poked as an array of words

   if atom(x) then x = {x} end if

   for i = 1 to length(x) do
      poke(a,{and_bits( x[i], #FF ), floor( x[i] / #100 )})
      a += 2
   end for
end procedure

------------------------------------------------
global function peek2u(object x)
------------------------------------------------
-- syntax: i = peek2u( x )
-- description:
--    returns a 2 byte unsigned word from address x
--    if x is a sequence, it is expected to be of the form: {addr, len}
--       where addr is the base address of the array, and len is the number of words to retrieve
 sequence ret
 
   if atom(x) then
      return peek(x+1)*#100 + peek(x)
   else
      ret = repeat(0,x[2])
      x = x[1]
      for i = 1 to length(ret) do
         ret[i] = peek(x+1)*#100 + peek(x)
         x += 2
      end for
      return ret
   end if
end function

------------------------------------------------
global function peek2s(object x)
------------------------------------------------
-- syntax: i = peek2s( x )
-- description:
--    returns a 2 byte signed word from address x
--    if x is a sequence, it is expected to be of the form: {addr, len}
--       where addr is the base address of the array, and len is the number of words to retrieve
 object ret
   if atom(x) then
      ret = peek(x+1)*#100 + peek(x)
      if ret >= #8000 then ret -= #10000 end if      
      return ret
   else
      ret = repeat(0,x[2])
      x = x[1]
      for i = 1 to length(ret) do
         ret[i] = peek(x+1)*#100 + peek(x)
         if ret[i] >= #8000 then ret[i] -= #10000 end if
         x += 2
      end for
      return ret
   end if
end function

------------------------------------------------
global procedure poke_float32(atom a, object x)
------------------------------------------------
-- syntax: poke_float32( a, x )
-- description:
--    pokes x as a 32bit float, to address a
--    if x is a sequence, it will be poked as an array of 32bit floats

   if atom(x) then x = {x} end if

   for i = 1 to length(x) do
      poke(a, machine_func(48, x[i]))
      a += 4
   end for   
end procedure

------------------------------------------------
global function peek_float32(object x)
------------------------------------------------
-- syntax: a = peek_float32( x )
-- description:
--    returns a 32bit float value from address x
--    if x is a sequence, it is expected to be of the form: {addr, len}
--       where addr is the base address of the array, and len is the number of 32bit floats to retrieve
 object ret
 sequence bytes
 integer a
   if atom(x) then
      return machine_func(49,peek({x, 4}))
   else
      ret = repeat(0,x[2])
      bytes = peek({x[1], x[2]*4})
      a = 1
      for i = 1 to length(ret) do
         ret[i] = machine_func(49,bytes[a..a+3])
         a += 4
      end for
      return ret
   end if
end function

------------------------------------------------
global procedure poke_float64(atom a, object x)
------------------------------------------------
-- syntax: poke_float64( a, x )
-- description:
--    pokes x as a 64bit float, to address a
--    if x is a sequence, it will be poked as an array of 64bit float's

   if atom(x) then x = {x} end if
   
   for i = 1 to length(x) do
      poke(a, machine_func(46, x[i]))
      a += 8
   end for
end procedure

------------------------------------------------
global function peek_float64(object x)
------------------------------------------------
-- syntax: a = peek_float64( x )
-- description:
--    returns a 64bit float value from address x
--    if x is a sequence, it is expected to be of the form: {addr, len}
--       where addr is the base address of the array, and len is the number of 64bit floats to retrieve
 object ret
 sequence bytes
 integer a
   if atom(x) then      
      return machine_func(47,peek({x, 8}))
   else
      ret = repeat(0,x[2])
      bytes = peek({x[1], x[2]*8})
      a = 1
      for i = 1 to length(ret) do
         ret[i] = machine_func(47,bytes[a..a+7])
         a += 8
      end for
      return ret
   end if
end function

-----------------------------------------------------------------------------
-- HISTORY --

-------------------------------------------------
-- v1.3 --
-- bug fix in peek routines: always returned sequences.  Thanks again Elliott Sales de Andrade
-- bug fix in peek2s(): x not being incremented.  Yet again, Thank You Elliott Sales de Andrade

-------------------------------------------------
-- v1.2 --
-- bug fix in peek_string(): offset defined as integer, should be atom. Thank you Jason Mirwald
-- bug fix in poke2(): incorrectly handled x value. Thanks to Elliott Sales de Andrade
-- bug fix in poke_floatXX(): incorrectly handled floats. Thanks to Jordah Ferguson and Derek Parnell

-------------------------------------------------
-- v1.1 --
-- fixed a couple of bugs that derek and myself created :/
-- removed dead variable from peek2s()
-- changed peek_string() to accept only 1 string at a time, conforms to poke_string() now

-------------------------------------------------
-- v1.0 --

-- these changes were done by Derek Parnell:
--    hi_byte(): ensure output is 8-bits
--    hi_word(): ensure output is 16-bits
--    peek_string(sequence): changed routine to comply with the documentation
--    poke2(): Faster by removing excess address computations
--    peek2u(): Faster by removing excess address computations
--    peeks2(): Faster by removing excess address computations
--    poke_float32(): Faster by removing excess address computations
--    peek_float32(): Faster by removing excess address computations
--    poke_float64(): Faster by removing excess address computations
--    peek_float64(): Faster by removing excess address computations

-------------------------------------------------
-- v0.7a --
-- bug fix:
--    make_long(): incorrect mask values. Thanks to Elliott Sales de Andrade

-------------------------------------------------
-- v0.6a --
-- added:
--    documented the routines
-- modified:
--    or_all(): will now accept a sequence of any length

-------------------------------------------------
-- v0.5a --
-- modified:
--    or_all(): removed nested sequence ability, and optimized
-- fixed:
--    peek_float32(): incorrectly peeked 8 bytes, instead of 4. Thanks to Jordah Ferguson

-------------------------------------------------
-- v0.4a --
-- modified:
--    removed machine.e dependencies
--    removed dll.e dependencies
--    removed link routines
--    removed C_TYPE constants
--    added poke_string()
-- fixed:
--    lo_byte(): incorrect values returned
--    hi_byte(): "         "      "
--    lo_word(): "         "      "
--    hi_word(): "         "      "

-------------------------------------------------
-- v0.3a --
-- fixed:
--    Memory Data Handlers:
--       poke8(): incorrectly poked 8 byte atoms
--       peek8(): incorrectly peeked 8 byte atoms
-- renamed:
--    Memory Data Handlers:
--       poke8() renamed to poke_float64()
--       peek8() renamed to peek_float64()
-- added:
--       global procedure poke_float64()
--       global function peek_float64()

-------------------------------------------------
-- v0.2a --
-- added:
--    Memory Data Handlers:
--       global procedure poke8(object a)
--       global function peek8(object a)

-------------------------------------------------
-- v0.1a --
-- added:
--       global constant C_BOOL
--       global constant C_WORD
--       global constant C_UWORD
--       global constant C_PTR
--       global constant C_LPSZ
--       global constant C_FLOAT32
--       global constant C_FLOAT64
--       gloabl object VOID
--    Linking Routines:
--       global integer LINK_COMPLAIN
--       global function link_dll(sequence dll_name)
--       global function link_func(atom dll_id, sequence func_name, sequence args, atom result)
--       global function link_proc(atom dll_id, sequence proc_name, sequence args)
--       global function link_var(atom dll_id, sequence var_name)
--    Bitwise Manipulators:
--       global function lo_byte(integer word)
--       global function hi_byte(integer word)
--       global function lo_word(atom long)
--       global function hi_word(atom long)
--       global function make_word(integer lo, integer hi)
--       global function make_long(integer lo, integer hi)
--       global function or_all(sequence vals)
--    Memory Data Handlers:
--       global function peek_string(object a)
--       global procedure poke2(atom a, object x)
--       global function peek2u(object a)
--       global function peek2s(object a)
