---------------------------------------------------------------------------------
-- STRUCTS.E
-- version 1.1a
-- written by Chris Bensler and Jason Mirwald
-- LAST MODIFIED : 12/11/02
-------------------------------------------------

include heaps.e

-------------------------------------------------

global constant 
   CHAR     = #01000001,
   UCHAR    = #02000001,
   SHORT    = #01000002,
   USHORT   = #02000002,
   INT      = #01000004,
   UINT     = #02000004,
   FLOAT    = #03000004,
   DOUBLE   = #03000008,
   LPSZ     = #04000004,
   LONG     = INT,
   ULONG    = UINT,
   POINTER  = ULONG,
   BOOL     = CHAR,
   BYTE     = UCHAR,
   WORD     = USHORT,
   DWORD    = ULONG,
   PTR      = POINTER,
   FLOAT32  = FLOAT,
   FLOAT64  = DOUBLE

constant
   STRUCT   = #10000000,
   ARRAY    = #20000000,
   UNION    = #30000000

-------------------------------------------------

sequence structs structs = {}

constant
 stsz = 1,
 sids = 2,
 soff = 3,
 sszs = 4,
 snbs = 5

-------------------------------------------------

global function sizeOf( object i, sequence ids )
 atom numb, idx, dtype
 idx = 0
 numb = 1
 if sequence(i) then
  if length(i) = 2 then
   numb = i[2]
   i = i[1]
  end if
 end if
 dtype = and_bits(i,#F0000000)
 i = and_bits(i,#0FFFFFFF)
 if length(ids) then
  if dtype = ARRAY then
   idx = ids[1]
   ids = ids[2..length(ids)]
  else
   if sequence(ids[1]) then
    idx = find(ids[1],structs[i][sids])
    ids = ids[2..length(ids)]
   else
    idx = find(ids,structs[i][sids])
    ids = {}
   end if
  end if
 end if
 if idx > 0 then
  return sizeOf( {structs[i][sszs][idx],structs[i][snbs][idx]}, ids )
 elsif dtype > #0FFFFFFF then
  return structs[i][stsz]
 end if
 return and_bits(i,#FF)*numb
end function

-------------------------------------------------

global function addressOf( object i, sequence ids )
 atom addr, idx, dtype
 idx = 0
 if atom(i) then
  addr = i
  i = peek4u(i-4)
 elsif length(i) > 1 then
  addr = i[1]
  i = i[2]
 end if
 dtype = and_bits(i,#F0000000)
 i = and_bits(i,#0FFFFFFF)
 if length(ids) then
  if dtype = ARRAY then
   idx = ids[1]
   ids = ids[2..length(ids)]
  else
   if sequence(ids[1]) then
    idx = find(ids[1],structs[i][sids])
    ids = ids[2..length(ids)]
   else
    idx = find(ids,structs[i][sids])
    ids = {}
   end if
  end if
 end if
 if idx > 0 then
  return addressOf( {addr+structs[i][soff][idx],structs[i][sszs][idx]}, ids )
 elsif dtype > #0FFFFFFF then
  return addr + structs[i][soff][1]
 end if
 return addr
end function

-------------------------------------------------

global function array( integer i, integer l )
 atom offset, sz
 sequence ids, ofs, szs, nbs
 offset = 0
 sz = sizeOf(i,{})
 ids = repeat(0,l)
 ofs = ids
 szs = repeat(i,l)
 nbs = repeat(1,l)
 for n = 1 to l do
  ofs[n] = offset
  offset += sz
 end for
 structs = append(structs,{offset,ids,ofs,szs,nbs})
 return length(structs)+ARRAY
end function

-------------------------------------------------

global function union( sequence s )
 atom sz, offset
 sequence ids, ofs, szs, nbs
 ids = repeat(0,length(s))
 ofs = ids
 szs = ids
 nbs = ids
 sz = 0
 for n = 1 to length(s) do
  ids[n] = s[n][1]
  szs[n] = s[n][2]
  nbs[n] = s[n][3]
  offset = sizeOf(szs[n],{})
  if offset > sz then
   sz = sizeOf(szs[n],{})
  end if
 end for
 if length(ids) then
  structs = append(structs,{sz,ids,ofs,szs,nbs})
  return length(structs)+UNION
 end if
 return 0
end function

-------------------------------------------------

global function define_c_struct( sequence s )
 atom offset
 sequence ids, ofs, szs, nbs
 offset = 0
 ids = repeat(0,length(s))
 ofs = ids
 szs = ids
 nbs = ids
 for n = 1 to length(s) do
  if s[n][2] < #01000001 and s[n][3] > 1 then
   -- add a new struct defining the array first?
   s[n][2] = array(s[n][2],s[n][3])
   s[n][3] = 1
  end if
  ids[n] = s[n][1]
  ofs[n] = offset
  szs[n] = s[n][2]
  nbs[n] = s[n][3]
  offset += sizeOf(szs[n],{})*nbs[n]
 end for
 if length(ids) then
  structs = append(structs,{offset,ids,ofs,szs,nbs})
  return length(structs)+STRUCT
 end if
 return 0
end function

-------------------------------------------------

global function alloc_struct( atom i )
 atom addr
 addr = alloc( sizeOf(i,{}) )
 mem_set(addr,0,sizeOf(i,{}))
 poke4(addr-4,i)
 return addr
end function

-------------------------------------------------

function peek_data( atom addr, atom sz, atom numb )
 sequence ret
 if sz = CHAR then
  ret = peek({addr,numb})
  ret = ret-(256*(and_bits(ret,#80)!=0))
 elsif sz = UCHAR then
  ret = peek({addr,numb})
 elsif sz = SHORT then
  ret = repeat(0,numb)
  for i = 0 to numb-1 do
   ret[i+1] = peek(addr+(i*2)) + peek(addr+(i*2)+1)*#100
   ret[i+1] -= #10000*(ret[i+1] >= #8000)
  end for
 elsif sz = USHORT then
  ret = repeat(0,numb)
  for i = 0 to numb-1 do
   ret[i+1] = peek(addr+(i*2)) + peek(addr+(i*2)+1)*#100
  end for
 elsif sz = INT then
  ret = peek4s({addr,numb})
 elsif sz = UINT then
  ret = peek4u({addr,numb})
 elsif sz = FLOAT then
  ret = repeat(0,numb)
  for i = 0 to numb-1 do
   ret[i+1] = machine_func(49,peek({addr+(i*4), 4}))
  end for
 elsif sz = DOUBLE then
  ret = repeat(0,numb)
  for i = 0 to numb-1 do
   ret[i+1] = machine_func(47,peek({addr+(i*8),8}))
  end for
 elsif sz = LPSZ then    -- string stuff
  ret = peek4u({addr,numb})
 end if
 if numb = 1 then
  return ret[1]
 end if
 return ret
end function

-------------------------------------------------

global function peek_struct( object i, sequence ids )
 atom addr, numb, idx, dtype
 sequence r
 r = {}
 idx = 0
 if atom(i) then
  numb = 1
  addr = i
  i = peek4u(i-4)
 elsif length(i) = 2 then
  numb = 1
  addr = i[1]
  i = i[2]
 elsif length(i) = 3 then
  numb = i[3]
  addr = i[1]
  i = i[2]
 end if
 dtype = and_bits(i,#F0000000)
 i = and_bits(i,#0FFFFFFF)
 if length(ids) then
  if dtype = ARRAY then
   idx = ids[1]
   ids = ids[2..length(ids)]
  else
   if sequence(ids[1]) then
    idx = find(ids[1],structs[i][sids])
    ids = ids[2..length(ids)]
   else
    idx = find(ids,structs[i][sids])
    ids = {}
   end if
  end if
 elsif dtype = UNION then
  -- if ids = {} then update idx for unions
  idx = 1
 end if
 if idx > 0 then
  return peek_struct( {addr+structs[i][soff][idx],structs[i][sszs][idx],structs[i][snbs][idx]}, ids )
 elsif dtype >= STRUCT then
  for n = 1 to length( structs[i][sids] ) do
   r &= {peek_struct( {addr+structs[i][soff][n],structs[i][sszs][n],structs[i][snbs][n]}, {} )}
  end for
  return r
 end if
 return peek_data( addr, i, numb )
end function

-------------------------------------------------

global procedure poke_data( object addr, atom sz, atom numb, object data )
 if atom(data) then
  data = {data}
 elsif sz = #04000004 and numb = 1 then
  -- this code does NOT handle a {""} that may be passed
  -- in case the user wants to alloc a 0-length() string
  if length(data) then
   if atom(data[1]) then
    data = {data}
   end if
  else
   data = {0}
  end if
 end if
 if numb > 1 then
  if length(data) < numb then
   data &= repeat(0,numb-length(data))
  elsif length(data) > numb then
   data = data[1..numb]
  end if
 end if
 if sz = CHAR or sz = UCHAR then
  poke(addr,data)
 elsif sz = SHORT or sz = USHORT then
  for i = 1 to numb do
   poke(addr + ((i-1)*2),{remainder(data[i],#100),floor(data[i]/#100)})
  end for
 elsif sz = INT or sz = UINT then
  poke4(addr,data)
 elsif sz = FLOAT then
  for i = 0 to numb-1 do
   poke(addr+(i*4), machine_func(48,data[i+1]))
  end for
 elsif sz = DOUBLE then
  for i = 0 to numb-1 do
   poke(addr+(i*8), machine_func(46,data[i+1]))
  end for
 elsif sz = LPSZ then
  poke4(addr,data)
 end if
end procedure

-------------------------------------------------

global procedure poke_struct( object i, sequence ids, object data )
 atom addr, numb, idx, dtype
 sequence r
 r = {}
 idx = 0
 if atom(i) then
  numb = 1
  addr = i
  i = peek4u(i-4)
 elsif length(i) = 2 then
  numb = 1
  addr = i[1]
  i = i[2]
 elsif length(i) = 3 then
  numb = i[3]
  addr = i[1]
  i = i[2]
 end if
 dtype = and_bits(i,#F0000000)
 i = and_bits(i,#0FFFFFFF)
 if length(ids) then
  if dtype = ARRAY then
   idx = ids[1]
   ids = ids[2..length(ids)]
  else
   if sequence(ids[1]) then
    idx = find(ids[1],structs[i][sids])
    ids = ids[2..length(ids)]
   else
    idx = find(ids,structs[i][sids])
    ids = {}
   end if
  end if
 elsif dtype = UNION then
  -- if ids = {} then update idx for unions
  idx = 1
 end if
 if idx > 0 then
  poke_struct( {addr+structs[i][soff][idx],structs[i][sszs][idx],structs[i][snbs][idx]}, ids, data )
 elsif dtype >= STRUCT then
  for n = 1 to length( structs[i][sids] ) do
   poke_struct( {addr+structs[i][soff][n],structs[i][sszs][n],structs[i][snbs][n]}, {}, data[n] )
  end for
  return
 end if
 poke_data( addr, i, numb, data )
end procedure

-------------------------------------------------------------------------------
-- HISTORY --

-------------------------------------------------
-- v1.1a --
-- modified:
--    added mem_set to alloc_struct() to zero memory

-------------------------------------------------
-- v1.0a --
-- officially released structs.e

-------------------------------------------------
-- v0.12b --
-- fixed a bug in the type-definitions
-- removed:
--    UWORD -- repetitive constant, WORD is unsigned
-- modified:
--    WORD  -- now defined as a 16-bit unsigned

-------------------------------------------------
-- v0.12a --
-- reformatted library to accomodate the following changes;
-- added:
--    array()  -- support for array of structures
--             -- returns a structure-id
--    union()  -- support for unions
--             -- returns a structure-id
-- removed:
--    peek_aquired() -- supported by arguments
--    poke_aquired() -- supported by arguments

-------------------------------------------------
-- v0.11a --
-- reverted need for manually allocating lpsz strings

-------------------------------------------------
-- v0.10a --
-- renamed C_type constants w/o C_, for compatability with dll.e
-- sizeOf() now works for struct indices instead of struct addresses
-- struct, in peek/poke_aquired, now an object, to handle struct references

-------------------------------------------------
-- v0.9a --
-- completely revamped the library
--  now much faster
--  lpsz strings must now be manually allocated and poked
--  removed *_ex routines
-- removed clib.e dependency
--   added type constants
-- added peek_aquired(), and poke_aquired()

-------------------------------------------------
-- v0.8a --
-- removed handshake system, alloc_struct() and dealloc_struct()
--  are now resident in structs.e
-- modified:
--    updated to conform with heaps.e v0.16a
--     ( alloc_lpsz() replaced with alloc_sz(), and intuitive references removed )

-------------------------------------------------
-- v0.7a --
-- modified:
--    updated to conform with heaps.e v0.15a ( alloc_sz() replaced with alloc_lpsz() )

-------------------------------------------------
-- v0.6a --
-- added:
--    handshake system for memlib v0.2a coordination
--    dealloc_struct(): local, called only by memlib.e via the handshake
-- modified:
--    alloc_struct(): now local, called only by memlib.e via the handshake
-- fixed:
--    poke_struct(): incorrectly poked nested struct elements to the parent struct

-------------------------------------------------
-- v0.5a --
-- fixed:
--    routines incorrectly appended -addr to the structs array, but passed +addr for recursion
--    poke_struct() routines: incorrectly allocated string arrays
--    peek_struct(): strings were being peeked incorrectly
--    peek_struct(): incorrectly 
-- modified:
--   all routines to conform with Clib v0.3a
-- added:
--   support for FLOAT

-------------------------------------------------
-- v0.4a --
-- bug fix:
--    2 typos in peek_struct_ex()
--    peek_struct(): incorrectly peeked nested structs, when the entire nested struct was requested
-- modified:
--    peek_struct_ex(): cosmetic change, made routine consistent with others (using c_type, etc)
--    poke_struct_ex() and poke_struct(): now zeros element's memory before poking

-------------------------------------------------
-- v0.3a --
-- modified:
--    sizeOf(): now takes 2 args
--    poke_struct() to poke_struct_ex() (multi element version)
--    peek_struct() to peek_struct_ex() (multi element version)
--    created new, single element versions of poke_struct() and peek_struct()
-- added:
--    addressOf()

-------------------------------------------------
-- v0.2a --
-- modified:
--    poke_struct() and peek_struct() to handle structs, nesting included

-------------------------------------------------
-- v0.1a --
-- basic library conceptualized
-- support for basic TYPES, no struct support
-- added:
--    define_c_struct()
--    alloc_struct()
--    sizeOf()
--    poke_stuct()
--    peek_struct()

-------------------------------------------------
-- Thanks to:
--    Jason Mirwald for testing and suggestions
--    Lucius Hilley III for testing and suggestions