--edbseq_init(sequence database)
--edbseq_assign(sequence seq,sequence index,object toassign)
--edbseq_append(sequence seq,object toappend)
--edbseq_length(sequence seq,sequence index)
--edbseq_get(sequence seq,sequence index)
--
include databasemod.e
include file.e
include b_Urzinclude.e
include get.e
atom subscript_checking
subscript_checking = on
-- remember to disable this...
sequence dumps
dumps = {{},{}}
sequence mydatabases
sequence mytables
mydatabases = {}
mytables = {}

procedure edbseqdump(sequence table,sequence message)
atom mydump
object ret
sequence database
mydump = dump
dump = off
ret = SiM(mytables,0,0,table)
if atom(ret) then
    dump = mydump
    return
end if
database = mydatabases[ret[1]]
if Inf_Lookup(dumps,database) = on then
    dump = mydump
    send2dump(message)
else
    dump = mydump
end if

end procedure

global sequence caches
caches = {{},{}}
-- caches[1][x] = a Cache'd sequence
-- caches[2][x] = the cache in *DB-Seq form
sequence length_cache_allowed
length_cache_allowed = {}
sequence bad_search_cache
bad_search_cache = {{},{}}
sequence length_cache_table
length_cache_table = ""
sequence wsearch_cache
wsearch_cache = {{},{}}

constant local = 1
constant remote = 2

function index_print(sequence index)
sequence output
output = {}
for i = 1 to length(index) do
output &= "["&sprint(index[i])&"]"
end for
return output
end function

procedure edbseq_ERROR(sequence error)
atom filehandle
filehandle = open("Error.txt","a")
puts(filehandle,repeat('-',40)&"\n")
puts(filehandle,"There has been a fatal error which has forced the program to close. The error was :\n")
puts(filehandle,error)
puts(filehandle,"\n")
abort(0)
end procedure

global procedure chk_make_empty(sequence databasename,sequence tables)
object dirret
atom DBret
sequence existing_tables
dirret = dir(mydir()&databasename)
if atom(dirret) then
    DBret = db_create(mydir()&databasename, DB_LOCK_EXCLUSIVE) 
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to create "&databasename)
    end if
else
    DBret = db_open(mydir()&databasename, DB_LOCK_EXCLUSIVE)
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to open "&databasename)
    end if
end if
existing_tables = db_table_list()
tables = remove_matches(tables,existing_tables)
for i = 1 to length(tables) do
    DBret = db_create_table(tables[i]) 
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to make "&tables[i]&" in database "&databasename)
    end if  
end for
for h = 1 to length(existing_tables) do
    DBret = db_select_table(existing_tables[h])
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to make switch to "&existing_tables[h]&" in database "&databasename)
    end if  
    for i = 1 to db_table_size() do
        db_delete_record(1) -- because when you delete record 1, record 2 becomes record 1....
    end for 
end for
db_close() 
end procedure

global procedure chk_make(sequence databasename,sequence tables)
object dirret
atom DBret
sequence existing_tables
dirret = dir(mydir()&databasename)
if atom(dirret) then
    DBret = db_create(mydir()&databasename, DB_LOCK_EXCLUSIVE) 
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to create "&databasename)
    end if
else
    DBret = db_open(mydir()&databasename, DB_LOCK_EXCLUSIVE)
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to open "&databasename)
    end if
end if
existing_tables = db_table_list()
tables = remove_matches(tables,existing_tables)
for i = 1 to length(tables) do
    DBret = db_create_table(tables[i]) 
    if DBret != DB_OK then
        edbseq_ERROR("Check & Make failed to make "&tables[i]&" in database "&databasename)
    end if  
end for
db_close() 
end procedure

procedure subscript_check(sequence index,sequence var,sequence caller)
atom key
object recorddata
sequence temp
if subscript_checking = on then
    if find(var,caches[2]) != 0 then
        return -- cannot properly subscript these because not all entries are meant to be there.
    end if
    if length(index) > 0 then
        if index[1] > db_table_size() then
            edbseq_ERROR("EDBSEQ subscript error! "&caller&" Attempted to retrieve "&var&index_print(index)&"\n"&
            "when "&var&" only has "&sprint(db_table_size())&" entries.")
        end if
        for i = 1 to length(index) do
            if index[i] <= 0 then
                edbseq_ERROR("EDBSEQ subscript error! "&caller&" Attempted to retrieve "&var&index_print(index)&"\n"&
                "Where the index is invalid.")              
            end if
        end for
        key = index[1]
        recorddata = db_record_data(index[1]) 
        if length(index) > 1 and not(atom(recorddata)) then
            -- This badly needs explanation....
        index = index[2..length(index)]
        -- this next line is the start...
        -- basically, assume you have a sequence like :
        --{{{'a','b'},'c'},'d'}
        -- temp would be {{{{'a','b'},'c'},'d'}}
        temp = {recorddata}
        for i = 1 to length(index)-1 do
        -- now assume we have an index of {key,1,1,2}
        -- temp would equal :
        -- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'}}	
        -- then
        -- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'},{'a','b'}}
        -- then
        -- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'},{'a','b'},'b'}
            if sequence(temp[i]) and index[i] > length(temp[i]) then
                edbseq_ERROR("EDBSEQ subscript error! "&caller&" Attempted to retrive "&var&index_print(key&index)&"\n"&
                " when "&var&index_print(key&index[1..i])&" only has "&sprint(length(temp[i]))&" entries.\n")
            elsif atom(temp[i]) then
                edbseq_ERROR(sprintf("EDBSEQ subscript error! %s Attempted to retrieve %s,\n when %s is an atom",
                {caller,var&index_print(key&index),var&index_print(key&index[1..length(index)])}))
            end if
            temp = append(temp,temp[i][index[i]])
        end for
        if sequence(temp[length(index)]) and index[length(index)] > length(temp[length(index)]) then
            edbseq_ERROR("EDBSEQ subscript error! "&caller&" Attempted to retrive "&var&index_print(key&index)&"\n"&
            " when "&var&index_print(key&index[1..length(index)])&" only has "&sprint(length(temp[length(index)]))&" entries.\n")
        end if
        elsif length(index) > 1 and atom(recorddata) then
        edbseq_ERROR("EDBSEQ subscript error! "&caller&" Attempted to retrive "&var&index_print(index)&"\n"&
        " when "&var&index_print({key})&" is an atom.\n")       
        end if
    end if
end if
end procedure


global function edbseq_gettable(sequence table)
    sequence currentDB
    atom finder
    atom found
--------------Special exemptions for *DB-Seq
    finder = find('*',table)
    found = find('-',table)
    if finder != 0 and found != 0 then
        currentDB = table[finder+1..found-1]
        table = table[found+1..length(table)]
        if db_select(currentDB) != DB_OK then
            edbseq_ERROR("Could not switch to database :"&currentDB&" Note : Forced DB -> passed *"&currentDB&"-"&table)
        else
            if db_select_table(table) != DB_OK then
                edbseq_ERROR("Could not open table "&table&" in database "&currentDB&"!\n")
            end if
        end if
        return local
    end if
    found = 0   
    for i = 1 to length(mytables) do
    finder = find(table,mytables[i])
    if finder != 0 then 
        found = i
    end if
    end for
    if found != 0 then
        currentDB = mydatabases[found]
        if length(currentDB) >= 1 and currentDB[1] = '+' then
            return remote
        end if
    else
    edbseq_ERROR("Could not find table "&table&" in any database!\n(Databases : "&strprint(mydatabases)&"\nTables : "&strprint(mytables))
    end if

    if db_select(currentDB) != DB_OK then
        edbseq_ERROR("Could not switch to database :"&currentDB)
    end if
    if db_select_table(table) != DB_OK then
        edbseq_ERROR("Could not open table "&table&" in database "&currentDB&"!\n")
    end if
    return local
end function

global procedure edbseq_init(sequence database,atom dumpp)
sequence tables
    mydatabases = append(mydatabases,database)
    if db_open(database, DB_LOCK_EXCLUSIVE) != DB_OK then
        edbseq_ERROR("The Database : "&database&" could not be opened or locked.")
    end if
    if db_select(database) != DB_OK then
    edbseq_ERROR("Could not switch to database : "&database)
    end if
    tables = db_table_list()
    mytables = append(mytables,tables)
    dumps = write_Inf_Var(dumps,database,dumpp,off)
    edbseqdump(tables[1],"EDBSEQ>EDB-SEQ successfully Initiaiated.\n")
end procedure

global procedure remote_edbseq_init(sequence Name,atom dumpp,atom mysock)
atom file
sequence tables
mydatabases = append(mydatabases,'+'&Name)
netsock = mysock
dumps = write_Inf_Var(dumps,'+'&Name,dumpp,off)
    tables = expect_response(mysock,"EDBSEQ>LISTTABLES>")
    if fbequal(tables,"EDBSEQ>LISTTABRET>") then 
    tables = trim(tables,"EDBSEQ>LISTTABRET>")
    tables = value(tables) 
    else
        edbseq_ERROR("Invalid response to ListTables request (returned : "&strprint(tables)&")")
    end if
    if tables[1] != GET_SUCCESS then
        edbseq_ERROR("Getting table listing from remote server returned bad value")
    end if
    mytables = append(mytables,tables[2])
    edbseqdump('+'&Name,"EDBSEQ>Current Databases :\n")
    edbseqdump('+'&Name,strprint(mydatabases)&'\n')
    edbseqdump('+'&Name,strprint(mytables)&'\n')
--	file = open("edbseq_error.txt","w") -- overwrite last error
--	puts(file,"No error here!\n")
--	close(file)
    edbseqdump('+'&Name,"EDBSEQ>EDB-SEQ successfully Initiaiated.\n")
end procedure

------------------------------------------------------
--
-- Note : some functions should never be cached, like append
--
------------------------------------------------------
global procedure remote_edbseq_cache_init(sequence seq,sequence cache_database,sequence cache_seq)
caches[1] = append(caches[1],seq)
caches[2] = append(caches[2],"*"&cache_database&"-"&cache_seq)
end procedure

global procedure allow_length_caching(sequence table,sequence cache_database,sequence allowed)
length_cache_allowed &= allowed
length_cache_table = "*"&cache_database&"-"&table
end procedure

global procedure bad_search_cache_init(sequence seq,sequence cache_database,sequence cache_seq)
bad_search_cache[1] = append(bad_search_cache[1],seq)
bad_search_cache[2] = append(bad_search_cache[2],"*"&cache_database&"-"&cache_seq)
end procedure

global procedure wild_search_cache_init(sequence seq,sequence cache_database,sequence cache_seq)
wsearch_cache[1] = append(wsearch_cache[1],seq)
wsearch_cache[2] = append(wsearch_cache[2],"*"&cache_database&"-"&cache_seq)
end procedure

-- Return codes
constant EDBSEQ_No_Cache = -1
constant EDBSEQ_Not_in_Cache = -2
constant EDBSEQ_In_Cache = 0

function check_cache(sequence seq,sequence index,sequence caller)
sequence cached_table
atom finder
atom foobar

---- length cacheing
if equal(caller,"length") and equal(index,{}) then
    finder = find(seq,length_cache_allowed)
    if finder != 0 then
        foobar = edbseq_gettable(length_cache_table)
        foobar = db_find_key(seq) 
        if foobar > 0 then
            return {EDBSEQ_In_Cache,length_cache_table}
        else
            return {EDBSEQ_Not_in_Cache,length_cache_table}
        end if
    else
        return {EDBSEQ_No_Cache,0}
    end if
end if

if equal(caller,"bsearch") then
    finder = find(seq,bad_search_cache[1])
    if finder != 0 then
        return {EDBSEQ_In_Cache,bad_search_cache[2][finder]}
    else
        return {EDBSEQ_No_Cache,0}
    end if
end if

if equal(caller,"wsearch") then -- index is foobar for wsearch because it doesn't know the index
    finder = find(seq,wsearch_cache[1])
    if finder != 0 then
        return {EDBSEQ_In_Cache,wsearch_cache[2][finder]}
    else
        return {EDBSEQ_No_Cache,0}
    end if
end if

finder = find(seq,caches[1])
if finder = 0 then
    return {EDBSEQ_No_Cache,0}
end if
if equal(caller,"get") and equal(index,{}) then
    return {EDBSEQ_Not_in_Cache,0}
end if
cached_table = caches[2][finder]
if equal(caller,"search") then -- index is foobar for search because it doesn't know the index
    return {EDBSEQ_In_Cache,cached_table}
end if
foobar = edbseq_gettable(cached_table)
finder = db_find_key(index[1])
if finder > 0 then
    return {EDBSEQ_In_Cache,cached_table}
else
    return {EDBSEQ_Not_in_Cache,cached_table}
end if
end function

procedure add_to_length_cache(sequence seq,atom thelength)
atom findret
atom dbret
findret = db_find_key(seq) 
if findret < 0 then
    dbret = db_insert(seq, thelength) 
    if dbret != DB_OK then
        edbseq_ERROR("Add to length Cache failed to add "&seq&" to the cache")
    end if
else
    db_replace_data(findret, thelength) 
end if
end procedure

global procedure uncache_length(sequence seq)
atom findret
findret = db_find_key(seq) 
if findret > 0 then -- if it isn't found, then it's not in the cache, hence I can't un-cache it
    db_delete_record(findret) 
end if
end procedure


global procedure edbseq_shutdown()
atom foobar
for i = 1 to length(mydatabases) do
foobar = db_select(mydatabases[i])
db_close()
end for
dumps = {{},{}}
mydatabases = {}
mytables = {}
end procedure

procedure local_edbseq_assign(sequence seq,sequence index,object toassign)
object recorddata
sequence temp
atom key
subscript_check(index,seq,"Edbseq_assign")
    if equal(index,{}) then
        edbseqdump(seq,"EDBSEQ>Overwriting Entire Sequence.\n")
        edbseqdump(seq,"EDBSEQ>Sequence has "&sprint(db_table_size())&" entries\n")
        for i = 1 to db_table_size() do
            edbseqdump(seq,"EDBSEQ>Deleting Record "&sprint(i)&"\n")
            db_delete_record(1)
        end for 
        for i = 1 to length(toassign) do
            edbseqdump(seq,"EDBSEQ>Assigning Record "&sprint(i)&"\n")
            if db_insert(i, toassign[i]) != DB_OK then
                edbseq_ERROR("Could not insert value into table "&seq)
            end if
        end for
        return
    end if
    key = index[1]
    edbseqdump(seq,"EDBSEQ>Key = "&sprint(key)&"\n")
    recorddata = db_record_data(index[1]) 
    if length(index) > 1 and not(atom(recorddata)) then
-- This badly needs explanation....
        index = index[2..length(index)]
-- this next line is the start...
-- basically, assume you have a sequence like :
--{{{'a','b'},'c'},'d'}
-- temp would be {{{{'a','b'},'c'},'d'}}
        temp = {recorddata}
        for i = 1 to length(index) do
-- now assume we have an index of {key,1,1,2}
-- temp would equal :
-- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'}}
-- then
-- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'},{'a','b'}}
-- then
-- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'},{'a','b'},'b'}
--			edbseqdump("EDBSEQ>temp = "&sprint(temp)&"\n")
--			flush(dump_location)
            temp = append(temp,temp[i][index[i]])
        end for
--		if length(temp) >= toassign then
        temp[length(temp)] = toassign
--		elsif length(temp) = (toassinggn-1)
--			temp[length(temp)] = append(temp[length(temp)],toassign)              Fix Part 2, is broken I think
--		else
--			edbseq_ERROR("Attemped to subscript past end of sequence
-- in this example, we will assume toassign = 'f'
-- we now have {{{{'a','b'},'c'},'d'},{{'a','b'},'c'},{'a','b'},'f'}
        for i = 0 to length(index)-1 do
            temp[length(temp)-1][index[length(index)-i]] = temp[length(temp)]
-- this re-compresses the sequence....
-- {{{{'a','b'},'c'},'d'},{{'a','b'},'c'},{'a','f'}}
-- {{{{'a','b'},'c'},'d'},{{'a','f'},'c'}}
-- {{{{'a','f'},'c'},'d'}}
-- temp[1] gives us
-- {{{'a','f'},'c'},'d'} which is
-- {{{'a','b'},'c'},'d'} with b changed to f
        end for
        db_replace_data(key, temp[1]) 
    elsif length(index) = 1 then
        db_replace_data(key, toassign)
    elsif length(index) > 1 and atom(recorddata) then
        edbseq_ERROR("Attempt to subscript atom in sequence "&seq&".\nIndex was "&sprint(index
                                                                                        )&
                     " where the old value was "&sprint(recorddata))
    end if
end procedure

procedure remote_edbseq_assign(sequence seq,sequence index,object toassign)
sequence ret
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>ASSIGN>"&sprint({seq,index,toassign}))
    if equal(ret,"EDBSEQ>OK>") then
        return
    end if
end for
end procedure

global procedure edbseq_assign(sequence seq,sequence index,object toassign)
sequence temp
temp = sprint(toassign)
if length(temp) > 20 then
    edbseqdump(seq,"EDBSEQ>EDB-SEQ Assign Assigning (sequence too long) to "&seq&sprint(index)&"\n")
else
    edbseqdump(seq,"EDBSEQ>EDB-SEQ Assign Assigning "&temp&" to "&seq&sprint(index)&"\n")
end if
    
if edbseq_gettable(seq) = remote then
    remote_edbseq_assign(seq,index,toassign)
else
    local_edbseq_assign(seq,index,toassign)
end if
end procedure

procedure local_edbseq_append(sequence seq,object toappend)
atom length_table
    length_table = db_table_size()
    if db_insert(length_table+1, toappend) != DB_OK then
        edbseq_ERROR("Failed to write to table!")
    end if
end procedure

procedure remote_edbseq_append(sequence seq,object toappend)
sequence ret
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>APPEND>"&strprint({seq,toappend}))
    if equal(ret,"EDBSEQ>OK>") then
        return
    end if
end for
end procedure

global procedure edbseq_append(sequence seq,object toappend)
edbseqdump(seq,"EDBSEQ>Appending "&sprint(toappend)&"to "&seq&"\n")
if edbseq_gettable(seq) = remote then
    remote_edbseq_append(seq,toappend)
else
    local_edbseq_append(seq,toappend)
end if
end procedure

function local_edbseq_length(sequence seq,sequence index)
object temp

subscript_check(index,seq,"edbseq_length")
    if equal(index,{}) then
        edbseqdump(seq,"EDBSEQ>Returning "&sprint(db_table_size())&"\n")
        return db_table_size()
    else
        temp = db_record_data(db_find_key(index[1])) -- should this be find_key(dindex[1]) ?
        for i = 2 to length(index) do
            temp = temp[index[i]]
        end for
            edbseqdump(seq,"EDBSEQ>Returning "&sprint(length(temp))&"\n")
        return length(temp)
    end if
end function

function length_cache_get(sequence seq,sequence index)
atom temp
temp = edbseq_gettable(seq)
return db_record_data(db_find_key(seq)) 
end function

function remote_edbseq_length(sequence seq,sequence index)
sequence ret
sequence cacheret 
atom mnret
cacheret = check_cache(seq,index,"length")
if cacheret[1] = EDBSEQ_In_Cache and not(equal(index,{})) then
    return local_edbseq_length(cacheret[2],index)
elsif cacheret[1] = EDBSEQ_In_Cache and equal(index,{}) then
    return length_cache_get(seq,index)
end if
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>LENGTH>"&strprint({seq,index}))
    if not(equal(ret,"TIMEOUT")) then
        exit
    end if
end for
mnret = makenumber(ret)
if cacheret[1] = EDBSEQ_Not_in_Cache then
add_to_length_cache(seq,mnret)
end if
return mnret
end function

global function edbseq_length(sequence seq,sequence index)
edbseqdump(seq,"EDBSEQ>Getting length of "&seq&sprint(index)&"\n")
if edbseq_gettable(seq) = remote then
    return remote_edbseq_length(seq,index)
else
    return local_edbseq_length(seq,index)
end if
end function

function local_edbseq_get(sequence seq,sequence index)
object temp
subscript_check(index,seq,"edbseq_get")
    if equal(index,{}) then
        temp = {}
        for i = 1 to edbseq_length(seq,{}) do
            temp = append(temp,db_record_data(db_find_key(i)))
        end for
        return temp
    end if
    temp = db_record_data(db_find_key(index[1])) 
    if length(index) > 1 then
        for i = 2 to length(index) do
            temp = temp[index[i]]
        end for
    end if
    return temp
end function

function remote_edbseq_get(sequence seq,sequence index)
sequence ret
sequence valret
sequence cacheret
atom dbret 
cacheret = check_cache(seq,index,"get")
if cacheret[1] = EDBSEQ_In_Cache then
    return local_edbseq_get(cacheret[2],index)
end if
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>GET>"&strprint({seq,index}))
    if not(equal(ret,"TIMEOUT")) then
        exit
    end if
end for
valret = value(ret)
if valret[1] = GET_SUCCESS then
    if cacheret[1] = EDBSEQ_Not_in_Cache and not(equal(cacheret[2],0)) and length(index) = 1 then
        dbret = db_insert(index[1], valret[2])
    end if
    return valret[2]
else
    edbseq_ERROR("Value returned Get Fail. Ret is "&ret)
end if
end function

global function edbseq_get(sequence seq,sequence index)
edbseqdump(seq,"EDBSEQ>Getting "&seq&index_print(index)&"\n")
if edbseq_gettable(seq) = remote then
    return remote_edbseq_get(seq,index)
else
    return local_edbseq_get(seq,index)
end if
end function

function local_edbseq_search(sequence seq,object searchfor)
edbseqdump(seq,"EDBSEQ>Searching for "&sprint(searchfor)&" in "&seq&"\n")
for i = 1 to db_table_size() do
    if compare(searchfor,db_record_data(i)) = 0 then
        return db_record_key(i) 
    end if
end for
return -1
end function

function remote_edbseq_search(sequence seq,object searchfor)
sequence ret
sequence cacheret
sequence goodcache
sequence badcache
sequence cacherets
atom localret
atom mnret
atom dbret
cacherets = {1,1}
cacheret = check_cache(seq,{},"search")
cacherets[1] = cacheret[1]
if cacheret[1] = EDBSEQ_In_Cache then
    goodcache = cacheret[2]
    localret = edbseq_gettable(cacheret[2])
    localret = local_edbseq_search(cacheret[2],searchfor)
    if localret != -1 then -- otherwise just get it from the server
        return localret
    end if
end if
cacheret = check_cache(seq,{},"bsearch")
cacherets[2] = cacheret[1]
if cacheret[1] = EDBSEQ_In_Cache then
    badcache = cacheret[2]
    localret = edbseq_gettable(cacheret[2])
    localret = local_edbseq_search(cacheret[2],searchfor)
    if localret != -1 then
        return -1 -- this is because bsearch is a list of items we have tried that DID NOT WORK
    end if        -- the caller expects -1 if it did not work, so therefore, if it is in that table, we should return -1
end if
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>SEARCH>"&strprint({seq,searchfor}))
    if not(equal(ret,"TIMEOUT")) then
        exit
    end if
end for
mnret = makenumber(ret)
if cacherets[1] = EDBSEQ_In_Cache and mnret != -1 then  -- it will only ever reach this point if it was not in the db to start with
    localret = edbseq_gettable(goodcache)
    dbret = db_insert(mnret,searchfor)
end if
if cacherets[2] = EDBSEQ_In_Cache and mnret = -1 then -- we have a bad cache and our return from server is bad
    localret = edbseq_gettable(badcache)
    local_edbseq_append(badcache,searchfor)
end if
return mnret 
end function

global function edbseq_search(sequence seq,object searchfor)
edbseqdump(seq,"EDBSEQ>Searching for "&sprint(searchfor)&" in "&seq&"\n")
if edbseq_gettable(seq) = remote then
    return remote_edbseq_search(seq,searchfor)
else
    return local_edbseq_search(seq,searchfor)
end if
end function

global function local_edbseq_wildsearch(sequence seq,sequence match)
object temp
atom yes

for i = 1 to db_table_size() do
    yes = ok
    temp = db_record_data(db_find_key(i))
    for j = 1 to length(match) do
        if not(equal(match[j],'*') or equal(match[j],temp[j])) then
            yes = notok
        end if
    end for
    if yes = ok then
            return i
        end if
    yes = ok
end for
return -1
end function

function remote_edbseq_wildsearch(sequence seq,sequence match)
sequence ret
sequence cacheret
sequence goodcache
sequence cacherets
atom localret
atom mnret
atom dbret
cacherets = {1,1}
cacheret = check_cache(seq,{},"wsearch")
cacherets[1] = cacheret[1]
if cacheret[1] = EDBSEQ_In_Cache then
    goodcache = cacheret[2]
    localret = edbseq_gettable(cacheret[2])
    localret = local_edbseq_search(cacheret[2],match)
    if localret != -1 then -- otherwise just get it from the server
        return localret
    end if
end if
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>WSEARCH>"&strprint({seq,match}))
    if not(equal(ret,"TIMEOUT")) then
        exit
    end if
end for
mnret = makenumber(ret)
if cacherets[1] = EDBSEQ_In_Cache and mnret != -1 then  -- it will only ever reach this point if it was not in the db to start with
    localret = edbseq_gettable(goodcache)
    dbret = db_insert(mnret,match)
end if
return mnret 
end function

global function edbseq_wildsearch(sequence seq,sequence match)
edbseqdump(seq,"EDBSEQ>Wildsearching "&seq&" for "&sprint(match)&"\n")
if edbseq_gettable(seq) = remote then
    return remote_edbseq_wildsearch(seq,match)
else
    return local_edbseq_wildsearch(seq,match)
end if
end function

global function local_edbseq_wildsearch_a_to_b(sequence seq,sequence match,atom start,atom finish)
object temp
atom yes

for i = start to finish do
    yes = ok
    temp = db_record_data(db_find_key(i))
    for j = 1 to length(match) do
        if not(equal(match[j],'*') or equal(match[j],temp[j])) then
            yes = notok
        end if
    end for
    if yes = ok then
            return i
        end if
    yes = ok
end for
return -1
end function

function remote_edbseq_wildsearch_a_to_b(sequence seq,sequence match,atom start,atom finish)
sequence ret
for i = 1 to 5 do
    ret = expect_response(netsock,"EDBSEQ>WABSEARCH>"&strprint({seq,match,start,finish}))
    if not(equal(ret,"TIMEOUT")) then
        exit
    end if
end for
return makenumber(ret)
end function

global function edbseq_wildsearch_a_to_b(sequence seq,sequence match,atom start,atom finish)
edbseqdump(seq,"EDBSEQ>Wildsearching "&seq&" for "&sprint(match)&"\n")
if edbseq_gettable(seq) = remote then
    return remote_edbseq_wildsearch_a_to_b(seq,match,start,finish)
else
    return local_edbseq_wildsearch_a_to_b(seq,match,start,finish)
end if
end function