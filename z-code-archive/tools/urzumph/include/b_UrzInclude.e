include file.e
include binary.e 
-- not mine, thanks goes to G.Boehme for that one, used to save TRIDDS's to file
include Compress.e -- compresso compression library, for use with Solidstate
include wildcard.e
global constant notok = -1
global constant statnotok = "notok"
global constant on = 0
global constant off = -1
global constant ok = 0

type string(object str)
atom yn
    if atom(str) then
        return 1 = 0
    end if
    yn = ok
    for i = 1 to length(str) do
        if not integer(str[i]) or (str[i] < 0 or str[i] > 255) then
            yn = notok
        end if
    end for
    if yn = ok then
        return (1=1)
    else
        return (1!=1)
    end if
end type

function str_correct(sequence to_correct)
atom i
    i = 1
    while i <= length(to_correct) do
        if to_correct[i] = '\\' then
            if i < length(to_correct) then
                to_correct = to_correct[1..i] & '\\' & to_correct[i+1..length(to_correct)]
            else
                to_correct = to_correct & '\\'
            end if
            i += 1
        elsif to_correct[i] = '\n' then
            to_correct[i] = '\\'
            if i < length(to_correct) then
                to_correct = to_correct[1..i] & "n" & to_correct[i+1..length(to_correct)]
            else
                to_correct = to_correct & 'n'
            end if
            i += 1
        elsif to_correct[i] = '\t' then
            to_correct[i] = '\\'
            if i < length(to_correct) then
                to_correct = to_correct[1..i] & "t" & to_correct[i+1..length(to_correct)]
            else
                to_correct = to_correct & 't'
            end if
            i += 1
        elsif to_correct[i] = '\"' then
            to_correct[i] = '\\'
            if i < length(to_correct) then
                to_correct = to_correct[1..i] & "\"" & to_correct[i+1..length(to_correct)]
            else
                to_correct = to_correct & '\"'
            end if
            i += 1
        elsif to_correct[i] = '\'' then
            to_correct[i] = '\\'
            if i < length(to_correct) then
                to_correct = to_correct[1..i] & "\'" & to_correct[i+1..length(to_correct)]
            else
                to_correct = to_correct & '\''
            end if
            i += 1
        end if
        i+=1
    end while
    return to_correct
end function

global function strprint(object toprint)
sequence toret
    if atom(toprint) then
        return sprint(toprint)
    end if
    if string(toprint) then
        return "\""&str_correct(toprint)&"\""
    end if
    toret = "{"
    for i = 1 to length(toprint) do
        if string(toprint[i]) then
            toret &= strprint(toprint[i])
        elsif sequence(toprint) then
            toret &= strprint(toprint[i])
        elsif atom(toprint) then
            toret &= strprint(toprint[i])
        end if
        if i != length(toprint) then
            toret &= ','
        end if
    end for
    toret &= '}'
    return toret
end function

object foobar
global atom dump
global atom dump_location
global atom searchseqdumplength
    searchseqdumplength = 10
    dump = off
    dump_location = 0
global sequence dumproutines
    dumproutines = {}

--without warning

-- EZ DUMP :
-- for windows applications, initialize with 
--EZdumpinit(routine_id("do_w32dump"),routine_id("Windump"),{%MainWindowHandle%})
-- for DOS / Console Linux
--EZdumpinit(routine_id("do_dump"),routine_id("dosdump"),{%Filehandle ( 1 for screen)%})

-----------------------------------------------------------------------------------
global procedure dosdump(sequence data)
    if dump = on then
        puts(dump_location,data)
        flush(dump_location)
    end if
end procedure
-----------------------------------------------------------------------------------
global procedure do_dump(atom filehandle) -- for dump to screen, use filehandle = 1
    dump_location = filehandle
    puts(dump_location,"Starting Dump :\n")
end procedure
----------------------------------------------------------------------------------
global procedure EZdumpinit(atom routineidinit,atom dumpID,sequence params)
    dumproutines &= dumpID
    call_proc(routineidinit, params)
    dump = on -- so we can't dump before we are done initialising
end procedure
---------------------------------------------------------------------------------------
global procedure send2dump(sequence dumpdata)
for i = 1 to length(dumproutines) do
        call_proc(dumproutines[i],{dumpdata})
end for
end procedure
---------------
global procedure printftodump(sequence text,sequence data)
send2dump(sprintf(text,data))
end procedure
----------------------------------------------------------------------------------------
global function SMMP(sequence TPP) -- Sequence MultiPutsPrint
sequence toret
    toret = {}
    for i = 1 to length(TPP) do
        if compare(TPP[i][1],"u") = 0 then
            toret &= TPP[i][2]
        elsif compare(TPP[i][1],"r") = 0 then
            toret &= strprint(TPP[i][2])
        end if
    end for
    return toret
end function
----------------------------------------------------------------------------------------
global procedure Multiputsprint(atom handle,sequence toputsprint) 
    -- EG Multiputsprint(1,{{"u","hello "}{"u",username}})
    for i = 1 to length(toputsprint) do
        if compare(toputsprint[i][1],"u") = 0 then
            puts(handle,toputsprint[i][2])
        elsif compare(toputsprint[i][1],"r") = 0 then
            print(handle,toputsprint[i][2])
        end if
    end for
end procedure
-----------------------------------------------------------------------------------
global function is_even(atom number)
atom remainderreturn

    if dump = on then
        send2dump("is_even> Checking if " & sprint(number) & " is even.\n") 
    end if

    remainderreturn = remainder(number,2)
    if remainderreturn = 0 then
        return ok
    else
        return notok
    end if
end function
-----------------------------------------------------------------------------------
global function insert_element(sequence SEQ,object to_insert,atom elementnumber,atom ba) 
    -- 'b' or 'a'
    if dump = on then
        send2dump(SMMP({{"u","Insert Element> Called. Inserting "},{"i",to_insert},{"u","\n"}}
                      ))
    end if

    if ba = 'a' then
        if elementnumber < length(SEQ) then
            SEQ = SEQ[1..elementnumber] & to_insert & SEQ[elementnumber+1..length(SEQ)]
        else
            SEQ = SEQ & to_insert
        end if
    end if

    if ba = 'b' then
        if elementnumber > 1 then
            SEQ = SEQ[1..elementnumber-1] & to_insert & SEQ[elementnumber..length(SEQ)]
        else
            SEQ = to_insert & SEQ
        end if
    end if

    return SEQ
end function
-----------------------------------------------------------------------------------
global function delete_element(sequence SEQ,atom elementnumber)
    if dump = on then
        send2dump(SMMP({{"u","Delete Element> Called. Deleting "},{"r",elementnumber},{"u",
                                                                                       " ( "},
                        {"r",SEQ[elementnumber]},{"u"," )"},{'u'," from "},{'r',SEQ},{"u","\n"
                                                                                     }}))
    end if
    if elementnumber > length(SEQ) then
        return SEQ
    end if
----
    if length(SEQ) = 1 and elementnumber = 1 then
        return {}
    end if
---
    if elementnumber = 1 then
        SEQ = SEQ[2..length(SEQ)]
    elsif elementnumber = length(SEQ) then
        SEQ = SEQ[1..length(SEQ)-1]
    else
        SEQ = SEQ[1..elementnumber-1] & SEQ[elementnumber+1..length(SEQ)]
    end if
    return SEQ
end function
-----------------------------------------------------------------------------------
global function search(sequence tosearch,object tofind)
--atom tosearchdepthchecker -- dump use only
--tosearchdepthchecker = 0
--if dump = on then
--for i = 1 to length(tosearch) do
--if sequence(tosearch[i]) then
--tosearchdepthchecker = 1
--end if
--end for
--if tosearchdepthchecker = 1 then
--if length(tosearch) < searchseqdumplength then
--send2dump(SMMP({{"u","Search> Called. Searching "},{"r",tosearch},{"u"," for "},{"u",tofind},{"u","\n"}}))
--else
--send2dump("Search> Called. Sequence to long to be stated, increase length if nessisary\n")
--end if
--else
    if dump = on then
        if length(tosearch) < searchseqdumplength then
            send2dump(SMMP({{"u","Search> Called. Searching "},{"r",tosearch},{"u"," for "},{
                                                                                             "r",
                                                                                             tofind
                                                                                            },
                            {"u","\n"}}))
        else
            send2dump(
                      "Search> Called. Sequence to long to be stated, increase length if nessisary\n"
                     )
        end if
    end if
--end if
--end if


    if length(tosearch) > 0 then
        for i = 1 to length(tosearch) do
            if compare(tosearch[i],tofind) = 0 then
                if dump = on then
                    send2dump(SMMP({{"u","Search> returned "},{"r",i},{"u","\n"}}))
                end if
                return i
            end if
        end for

        if dump = on then
            send2dump(SMMP({{"u","Search> failed. (not found)"},{"u","\n"}}))
        end if
        return notok
    else
        if dump = on then
            send2dump(SMMP({{"u","Search> failed. (length toseaerch <= 0)"},{"u","\n"}}))
        end if
        return notok
    end if
end function
-----------------------------------------------------------------------------------
global function searchall(sequence tosearch,object tofind)
sequence toreturn
atom tosearchdepthchecker -- dump use only
    toreturn = {}
    tosearchdepthchecker = 0
    if dump = on then
        for i = 1 to length(tosearch) do
            if sequence(tosearch[i]) then
                tosearchdepthchecker = 1
            end if
        end for
        if tosearchdepthchecker = 1 then
--send2dump(SMMP({{"u","Searchall> Called. Searching "},{"r",tosearch},{"u"," for "},{"u",tofind},{"u","\n"}}))
        else
            send2dump(SMMP({{"u","Searchall> Called. Searching "},{"r",tosearch},{"u"," for "},
                            {"r",tofind},{"u","\n"}}))
        end if
    end if
    if length(tosearch) > 0 then
        for i = 1 to length(tosearch) do
            if compare(tosearch[i],tofind) = 0 then
                if dump = on then
                    send2dump(SMMP({{"u","Searchall> returned "},{"r",i},{"u","\n"}}))
                end if
                toreturn = append(toreturn,i)
            end if
        end for
        if length(toreturn) = 0 then
            return notok
        else
            return toreturn
        end if
    else
        return notok
    end if
end function
-----------------------------------------------------------------------------------
global function searchr(sequence tosearch,object tofind)
atom tosearchdepthchecker -- dump use only
    tosearchdepthchecker = 0
    if dump = on then
        for i = 1 to length(tosearch) do
            if sequence(tosearch[i]) then
                tosearchdepthchecker = 1
            end if
        end for
        if tosearchdepthchecker = 1 then
--send2dump(SMMP({{"u","Search (reverse)> Called. Searching "},{"r",tosearch},{"u"," for "},{"u",tofind},{"u","\n"}}))
        else
            send2dump(SMMP({{"u","Search (reverse)> Called. Searching "},{"u",tosearch},{"u",
                                                                                         " for "
                                                                                        },{"u",
                                                                                           tofind
                                                                                          },{
                                                                                             "u",
                                                                                             "\n"
                                                                                            }}
                          ))
        end if
    end if
    if length(tosearch) > 0 then
        for i = 0 to length(tosearch)-1 do
            if compare(tosearch[length(tosearch)-i],tofind) = 0 then
                if dump = on then
                    send2dump(SMMP({{"u","Search (reverse)> returned "},{"r",length(tosearch)-
                                                                         i},{"u","\n"}}))
                end if
                return length(tosearch)-i
            end if
        end for
        return notok
    else
        return notok
    end if
end function
-----------------------------------------------------------------------------------------------
global function makenumber(sequence input)
atom mydump
atom decsearch
sequence multiplytable
atom output
atom neg
    output = 0
    multiplytable = {}

    send2dump("Makenumber> making \"" & input & "\" into a number \n")
    mydump = dump
    dump = off
    decsearch = search(input,'.')
    if decsearch != notok then
--if decsearch != 1 and decsearch != length(input) then -- remove decimal place.
--input = input[1..decsearch-1]&input[decsearch+1..length(input)]
--elsif decsearch = 1  then
--input = input[2..length(input)]
--elsif decsearch = length(input) then
--input = input[1..length(input)-1]
--end if
        input = delete_element(input,decsearch)
    end if

    if length(input) > 0 and input[1] = '-' then
        send2dump("Makenumber> "&strprint(input)&" is negative. Taking action.\n")
        neg = ok
        input = input[2..length(input)]
    else
        neg = notok
    end if

    if decsearch != notok then
        decsearch = decsearch - 1 --(because 1 = 1*10^0, but it would be 1 otherwise) 5032.45 
    end if

--print(1,input)
--puts(1,"\n")
--puts(1,input)
--puts(1,"\n")

    if decsearch = notok then
        for i = 1 to length(input) do
--multiplytable = prepend(multiplytable,length(input)-i)
            multiplytable = append(multiplytable,length(input)-i)
        end for
    end if

    if decsearch != notok then
        for i = 1 to length(input) do
--multiplytable = prepend(multiplytable,decsearch - i)
            multiplytable = append(multiplytable,decsearch - i)
        end for
    end if

--print(1,multiplytable)
--puts(1,"\n")

    for i = 1 to length(input) do
--Multiputsprint(1,{{"u","output = "},{"r",output},{"u"," input - 48 = "},{"r",input[i]-48},{"u"," * "},{"r",power(10,multiplytable[i])},{"u","\n\n"}})
        output = output + (input[i]-48)*power(10,multiplytable[i]) 
        --  50 = 2, ergo 50 - 48 = 2
    end for

    if neg = ok then
        output = 0 - output
    end if

    dump = mydump
    send2dump("Makenumber> returned :" & sprint(output) & "\n")

    return output
end function
---------------------------------------------------------------------------------------------------------------
global function check_seq_is_number(sequence input)
atom decimalused
    decimalused = off

    if dump = on then
        send2dump("sequence(number)> Checking to see if " & input & " is a number\n")
    end if

    if length(input) > 0 and input[1] = '-' then
        input = delete_element(input,1)
    end if

    for i = 1 to length(input) do
        if atom(input[i]) and input[i] >= '0' and input[i] <= '9' then

        elsif atom(input[i]) and decimalused = off and input[i] = '.' then
            decimalused = on

        else
            if dump = on then
                send2dump("sequence(number)> returned notok\n")
            end if
            return notok
        end if
    end for
    if dump = on then
        send2dump("sequence(number)> returned ok\n")
    end if
    return ok
end function
-----------------------------------------------------------------------------------\
global function SiS(sequence searchin,sequence searchfor)
atom oksofar
atom searchreturn
object searchallret
sequence display1
sequence display2
    searchreturn = 0
    oksofar = ok

    if dump = on then
        send2dump("SiS> Starting.\n")
    end if

    if length(searchfor) > length(searchin) then
        return notok
    end if

    if length(searchfor) < 1 then 
        return notok
    end if

-- fix me. see know bug. investigate using search all then cycling through results. this should be fixed
    searchallret = searchall(searchin,searchfor[1])
    if atom (searchallret) then
        return notok
    end if
    for h = 1 to length(searchallret) do
        oksofar = ok
        searchreturn = searchallret[h]
        if length(searchfor) > 2 and searchreturn+1 <= length(searchin) and searchreturn != -1 
        and length(searchin)-(length(searchfor)-1 + searchreturn) >= 0 then
--length(searchfor) > 2 to stop the constant, i = 2 being out of bounds.
--searchreturn+1 <= length(searchin) so that searchin[searchreturn+i-1]  is not out of bounds
--searchreturn != -1 so that the searchreturn returns a value, not "not found"
--length(searchin)-(length(searchfor)+ searchreturn -1) >= 0 then so that what we are 
--searching for is at least the length it HAS to be for it to be success
-- also avoids subscript errors
            for i = 2 to length(searchfor) do
                if searchin[searchreturn+i-1] != searchfor[i] then 
-- if searchin = {1,2,3,4,5,6}
--and searchfor = {3,4,5}
-- then search will return 3.
-- to get 4, it will be searchin[searchreturn+1]
--but in searchfor, 4 is in the second place.
                    if dump = on then
                        display1 = searchin
                        display1 = insert_element(display1,']',searchreturn+i-1,'a') 
                        -- must do it this way or the indexes change
                        display1 = insert_element(display1,'[',searchreturn+i-1,'b')
                        display2 = searchfor
                        display2 = insert_element(display2,']',i,'a') 
                        -- must do it this way or the indexes change
                        display2 = insert_element(display2,'[',i,'b')
                        send2dump(sprintf("SiS> failed. \"%s\" != \"%s\" \n",{display1,
                                                                              display2}))
                    end if
                    oksofar = notok
                    exit -- exit this loop to save time
                end if
            end for
        else
            oksofar = notok
        end if
        if oksofar = ok then
            return {searchreturn,searchreturn+length(searchfor)}
        end if
    end for
    return notok
end function
-----------------------------------------------------------------------------------
global function SiSiS(sequence searchin,sequence searchfor)
object SiSreturn
    SiSreturn = notok

    if dump = on then
        send2dump("SiSiS> called, searching for : ")
        send2dump(searchfor)
        send2dump("\n")
    end if

    for i = 1 to length(searchin) do
        SiSreturn = SiS(searchin[i],searchfor)
        if sequence(SiSreturn) then
            return {i,SiSreturn[1],SiSreturn[2]}
        end if
    end for
    return notok
end function
-----------------------------------------------------------------------------------
sequence madir
    madir = {}
global function mydir()
object cmdlne
atom num
    if compare(madir,{}) != 0 then
        return madir
    end if
    cmdlne = command_line()
    if platform() < 3 then
        num = searchr(cmdlne[2],'\\')
    else
        num = searchr(cmdlne[2],'/')
    end if
    if dump = on then
        send2dump(SMMP({{"u","My Dir> Called. My Dir is: "},{"u",cmdlne[2][1..num]},{"u","\n"}
                       }))
    end if
    madir = cmdlne[2][1..num]
    return cmdlne[2][1..num]
end function
-----------------------------------------------------------------------------------
global function get_contents(sequence filepath,atom NL)
atom Handle
object getter
sequence returndata
    getter = {}
    returndata = {}

    if dump = on then
        send2dump(SMMP({{"u","Get_Contents_Text> Called. Filename: "},{"u",filepath},{"u","\n"
                                                                                     }}))
    end if

    Handle = open(filepath,"r")
    if Handle = -1 then

        if dump = on then
            send2dump(SMMP({{"u",
                             "Get_Contents_Text> DID NOT Return Succesfully! File could not be opened!"
                            },{"u","\n"}}))
        end if

        return notok
    end if

    while compare(getter,-1) != 0 do
        getter = gets(Handle)
        if compare(getter,-1) != 0 then
            if sequence(getter) then
                if getter[length(getter)] = '\n' and NL = on then
                    getter = getter[1..length(getter)-1]
                end if
            end if
            returndata = append(returndata,getter)
        end if
    end while

    if dump = on then
        send2dump(SMMP({{"u","Get_Contents_Text> Returned Succesfully"},{"u","\n"}}))
    end if
    close(Handle)
    return returndata
end function

global function get_contents_b(sequence filepath,atom NL) ----------------------------------
atom Handle
object getter
sequence returndata
    getter = {}
    returndata = {}

    if dump = on then
        send2dump(SMMP({{"u","Get_Contents_Binary> Called. Filename: "},{"u",filepath},{"u",
                                                                                        "\n"}}
                      ))
    end if

    Handle = open(filepath,"rb")
    if Handle = -1 then

        if dump = on then
            send2dump(SMMP({{"u",
                             "Get_Contents_Binary> DID NOT Return Succesfully! File could not be opened!"
                            },{"u","\n"}}))
        end if

        return notok
    end if

    while compare(getter,-1) != 0 do
        getter = getc(Handle)
        if compare(getter,-1) != 0 then
            if sequence(getter) then
                if getter[length(getter)] = '\n' and NL = on then
                    getter = getter[1..length(getter)-1]
                end if
            end if
            returndata = append(returndata,getter)
        end if
    end while

    if dump = on then
        send2dump(SMMP({{"u","Get_Contents_Binary> Returned Successfully!"},{"u","\n"}}))
    end if

    return returndata
end function
-----------------------------------------------------------------------------------
global function copy_binary(sequence from,sequence too,atom overwrite)
atom from_handle
atom to_handle
atom getter
    getter = 0

    if dump = on then
        send2dump(SMMP({{"u","Copy Binary> Called. Filename: "},{"u",from},{"u"," to "},{"u",
                                                                                         too},
                        {"u","\n"}}))
    end if

    from_handle = open(from,"rb")

    if dump = on then
        send2dump(SMMP({{"u","Copy Binary> The handle for "},{"u",from},{"u"," is "},{"r",
                                                                                      from_handle
                                                                                     },{"u",
                                                                                        "\n"}}
                      ))
    end if

    if overwrite = off then
        if dump = on then
            send2dump(SMMP({{"u",
                             "Copy Binary> Overwrite Off, Proceeding to check to_location "},{
                                                                                              "u",
                                                                                              "\n"
                                                                                             }
                           }))
        end if
        to_handle = open(too,"rb")
    else
        to_handle = -1
    end if
    if from_handle = -1 or to_handle != -1 then
        if dump = on then
            send2dump(SMMP({{"u",
                             "Copy Binary> From file does not exist, or Overwrite = off and file exists. "
                            },{"u","From handle returned: "},{"r",from_handle}}))
            send2dump(SMMP({{"u"," To handle returned (if overwrite = on, this should be -1) "
                            },{"r",to_handle},{"u","\n"}}))
        end if
        if from_handle != -1 then
            close(from_handle)
        end if
        if to_handle != -1 then
            close(to_handle)
        end if
        return notok
    end if
    if dump = on then
        send2dump(SMMP({{"u",
                         "Copy Binary> Overwrite On or to_location check cleared file for writing"
                        },{"u","\n"}}))
    end if
    to_handle = open(too,"wb")
    while getter != -1 do
        getter = getc(from_handle)
        if getter != -1 then
            puts(to_handle,getter)
        end if
    end while
    close(from_handle)
    close(to_handle)
    if dump = on then
        send2dump(SMMP({{"u","Copy Binary> File Written Successfully"},{"u","\n"}}))
    end if
    return ok
end function
----------------------------------------------------------------------------------------
--CSV / Matrix handling
----------------------------------------------------------------------------------------
atom CSVhandle
    CSVhandle = -2
object getter
    getter = 0
sequence readCSV
    readCSV = {}
sequence line
    line = {}
atom searcher
    searcher = 0
atom tosearch
    tosearch = ','

global function opencsv(sequence path)
atom upto
object searchreturn
sequence rawCSV
    upto = 0
    readCSV = {}
--puts(1,"opening CSV\n")

    rawCSV = get_contents(path,on)

    for i = 1 to length(rawCSV) do
        searchreturn = searchall(rawCSV[i],tosearch)
        if sequence(searchreturn) then --ERGO != notok
            line = {}
            line = append(line,rawCSV[i][1..searchreturn[1]-1])
            for j = 1 to length(searchreturn)-1 do
                if searchreturn[j] != length(rawCSV[i]) then
                    line = append(line,rawCSV[i][searchreturn[j]+1..searchreturn[j+1]-1])
                end if
            end for


        end if
        if sequence(searchreturn) then
            line = append(line,rawCSV[i][searchreturn[length(searchreturn)]+1..length(rawCSV[i
                                                                                            ])
                                        ])
            readCSV = append(readCSV,line)
        else
            readCSV = append(readCSV,rawCSV[i])
        end if

--print(1,readCSV)
    end for
    return readCSV
end function
----------------------------------------------------------

global function getcell(atom row,atom column,sequence CSV)

    return CSV[row][column]

end function

-------------------------------------------------------------
global function writecell(atom row,atom column,object towrite,sequence CSV)
    while row > length(CSV) do
        CSV = append(CSV,{})
    end while
    while column > length(CSV[row]) do
        CSV[row] = append(CSV[row],{})
    end while

    CSV[row][column] = towrite
    return CSV
end function
-----------------------------------------------------------------
global procedure appendline(sequence towrite,sequence CSV)

    CSV = append(CSV,towrite)

end procedure
----------------------------------------------------------------
global procedure writeCSV(sequence path,atom overwrite,sequence CSV)

    if CSVhandle != -2 then
        close(CSVhandle)
    end if

    if compare(dir(path),-1) != 0 or overwrite = on then
        CSVhandle = open(path,"w")
        for one = 1 to length(CSV) do
            for two = 1 to length(CSV[one]) do
                puts(CSVhandle,CSV[one][two])
                puts(CSVhandle,",")
            end for
            puts(CSVhandle,"\n")
        end for
    end if

end procedure
----------------------------------------------------------------
global procedure display_CSV(sequence CSV,atom place)
    for i = 1 to length(CSV) do
        for j = 1 to length(CSV[i]) do
            if atom(CSV[i][j]) then
                print(place,CSV[i][j])
            else
                puts(place,CSV[i][j])
            end if
            puts(place,9) -- TAB
        end for
        puts(place,"\n")
    end for
end procedure
-----------------------------------------------------------------
-- Recursive Subdirectories

global function checkextension(sequence files, sequence extenslist)
atom searchreturn
sequence outlist

    searchreturn = 0

    if dump = on then
        send2dump(SMMP({{"u","CheckExtension> Checking the Extensions of : "},{"u","\n"}}))
        for i = 1 to length(files) do
            send2dump(SMMP({{"u","CheckExtension> "},{"u",files[i]},{"u","\n"}}))
        end for
        send2dump(SMMP({{"u","CheckExtension> "},{"u","Against : "},{"u","\n"}}))
        for i = 1 to length(extenslist) do
            send2dump(SMMP({{"u","CheckExtension> "},{"u",extenslist[i]},{"u","\n"}}))
        end for
    end if

    for i = 1 to length(files) do
        files[i] = lower(files[i])
    end for
    if not(equal(extenslist,"*.*")) then
        for i = 1 to length(extenslist) do
            extenslist[i] = lower(extenslist[i])
        end for
    end if

    if compare(extenslist,"*.*") != 0 then
        outlist = {}
        for i = 1 to length(extenslist) do
            for j = 1 to length(files) do
                send2dump("CheckExtension> Checking to see if "&files[j]&" has the extension "
                          &extenslist[i]&"\n")
                if length(files[j]) > length(extenslist[i]) then
                    send2dump("CheckExtension> Checking "&files[j][length(files[j])-length(
                                                                                           extenslist
                                                                                           [i]
                                                                                          )+1
                                                                   ..length(files[j])]&
                              " against "&extenslist[i]&"\n")
                    if compare(files[j][length(files[j])-length(extenslist[i])+1..length(files
                                                                                         [j])],
                               extenslist[i]) = 0 then
                        send2dump("CheckExtension> "&files[j]&" has the extension "&extenslist
                                  [i]&"\n")
                        outlist = append(outlist,files[j])
                    else
                        send2dump("CheckExtension> It does not.\n")
                    end if
                else
                    send2dump("File is shorter than extension...\n")
                end if
            end for
        end for
    else
        outlist = files
    end if
    if dump = on then
        send2dump(SMMP({{"u","CheckExtension> Returned : "},{"u","\n"}}))
        for i = 1 to length(outlist) do
            send2dump(SMMP({{"u","CheckExtension>"},{"u",outlist[i]},{"u","\n"}}))
        end for
    end if
    return outlist
end function
-----------------------------------------------------------------------------------
global function recursefilefinder(sequence placestolook,sequence valid_extensions_list)
sequence directorylist
object direc
atom handle
object foobar
atom searchresults
sequence DirectoryINUSE
sequence List

    List = {}
    directorylist = ""

    if dump = on then
        send2dump(SMMP({{"u","RFF> Called : "},{"u","\n"},{"u"," Looking in :"},{"u","\n"}}))
        for i = 1 to length(placestolook) do
            send2dump(SMMP({{"u",placestolook[i]},{"u","\n"}}))
        end for
        send2dump(SMMP({{"u","For Files with the Exension : "},{"u","\n"}}))
        for i = 1 to length(valid_extensions_list) do
            send2dump(SMMP({{"u",valid_extensions_list[i]},{"u","\n"}}))
        end for
    end if

    for i = 1 to length(placestolook) do
        directorylist = append(directorylist,placestolook[i])
    end for

    while length(directorylist) > 0 do

        DirectoryINUSE = directorylist[1]

        if dump = on then
            send2dump(SMMP({{"u","RFF> Currently Searching : "},{"u",DirectoryINUSE},{"u","\n"
                                                                                     }}))
        end if

        direc = dir(DirectoryINUSE)
        if sequence(direc) then

            for i = 1 to length(direc) do
                if compare(direc[i][1],".") = 0 or compare(direc[i][1],"..") = 0 then

                    --direc = direc[1..i-1]&direc[i+1..length(direc)]

                else --so it restricts the loop to one, otherwise it might do BOTH on 1 loop

                    searchresults = search(direc[i][2],'d')

                    if searchresults != notok then
                        directorylist = append(directorylist,DirectoryINUSE&direc[i][1]&'\\')
                        if dump = on then
                            send2dump(SMMP({{"u","RFF> Found Directory! : "},{"u",
                                                                              DirectoryINUSE&
                                                                              direc[i][1]&"\\"
                                                                             },{"u","\n"}}))
                        end if
                    else -- not . or .., and not a folder

                        if compare(checkextension({direc[i][1]},valid_extensions_list),{direc[
                                                                                              i
                                                                                             ]
                                                                                        [1]}) 
                            = 0 then
                            if dump = on then
                                send2dump(SMMP({{"u","RFF> Found File! : "},{"u",
                                                                             DirectoryINUSE & 
                                                                             direc[i][1]},{"u",
                                                                                           "\n"
                                                                                          }}))
                            end if
                            List = append(List,DirectoryINUSE & direc[i][1])
                        end if

                    end if

                end if
            end for
        else
            if dump = on then
                send2dump(SMMP({{"u","RFF> Error: Dir did not return Successfully"},{"u","\n"}
                               }))
            end if
        end if
        directorylist = directorylist[2..length(directorylist)]
    end while

    if dump = on then
        send2dump(SMMP({{"u","RFF> Found: "},{"u","\n"}}))

        for i = 1 to length(List) do
            send2dump(List[i])
            send2dump("\n")
        end for

    end if
    return List
end function
----------------------------------------------------------------------------------------------
global function direc_structure_only_recursive_dir(sequence placestolook,sequence 
                                                   valid_extensions_list,sequence 
                                                   forced_location)
sequence dir_ret
    dir_ret = recursefilefinder(placestolook,valid_extensions_list)
    for i = 1 to length(dir_ret) do
        dir_ret[i] = dir_ret[i][length(forced_location)+1..length(dir_ret[i])]
    end for
    return dir_ret
end function
------------------------------------------------------------------------------------------
global function recursefilefinderWblacklist(sequence placestolook,sequence 
                                            valid_extensions_list,sequence blacklistfolders,
                                            sequence blacklistfiles)
sequence directorylist
object direc
atom handle
object foobar
atom searchresults
sequence DirectoryINUSE
sequence List
atom search2

    List = {}
    directorylist = ""

    for i = 1 to length(placestolook) do
        directorylist = append(directorylist,placestolook[i])
    end for

    while length(directorylist) > 0 do

        DirectoryINUSE = directorylist[1]

        direc = dir(DirectoryINUSE)
        if sequence(direc) then

            for i = 1 to length(direc) do
                if compare(direc[i][1],".") = 0 or compare(direc[i][1],"..") = 0 then
--direc = direc[1..i-1]&direc[i+1..length(direc)]

                else --so it restricts the loop to one, otherwise it might do BOTH on 1 loop

                    searchresults = searchr(direc[i][2],'d')

                    if searchresults != notok then
                        search2 = search(blacklistfolders,direc[i][1])
                        if compare(blacklistfolders,{}) = 0 or search2 = -1 then
                            directorylist = append(directorylist,DirectoryINUSE&direc[i][1]&
                                                   "\\")
                        end if
                    else -- not . or .., and not a folder
-- Shouldn't this be != (so that blacklist files has things in it, as opposed to being empty, and shouldn't it be AND, not or?
                        if compare(blacklistfiles,{}) = 0 or compare(checkextension({direc[i][
                                                                                              1
                                                                                             ]
                                                                                    },
                                                                                    valid_extensions_list
                                                                                   ),{direc[i]
                                                                                      [1]}) = 
                            0 then
                            search2 = search(blacklistfiles,direc[i][1])
                            if search2 = -1 then
                                List = append(List,DirectoryINUSE & direc[i][1])
                            end if
                        end if

                    end if

                end if
            end for
        end if
        directorylist = directorylist[2..length(directorylist)]
    end while

    if dump = on then
        send2dump(SMMP({{"u","Recurse Directories (with Blacklist) Found: "},{"u","\n"}}))

        for i = 1 to length(List) do
            send2dump(List[i])
            send2dump("\n")
        end for

    end if

    return List
end function
-----------------------------------------------------------------------------------
global function crop_folders(sequence dir)
sequence outputlist
    outputlist = {}
    for i = 1 to length(dir) do
        if search(dir[i][2],'d') = -1 then
            outputlist = append(outputlist,dir[i])
        end if
    end for
    return outputlist
end function
-----------------------------------------------------------------------------------
global function crop_files(sequence dir)
sequence outputlist
    outputlist = {}
    for i = 1 to length(dir) do
        if search(dir[i][2],'d') != -1 then
            outputlist = append(outputlist,dir[i])
        end if
    end for
    return outputlist
end function
-----------------------------------------------------------------------------------
global function strip_dir_info(sequence dir)
    for i = 1 to length(dir) do
        dir[i] = dir[i][1]
    end for
    return dir
end function
-----------------------------------------------------------------------------------
global procedure check_n_make_dir(sequence pathtomake)
sequence sys_command
    if pathtomake[length(pathtomake)] != '\\' then
        pathtomake = pathtomake[1..searchr(pathtomake,'\\')]
    end if
    if compare(dir(pathtomake),-1) = 0 then
        sys_command = "mkdir "&pathtomake
        system(sys_command,2)
    end if
end procedure
------------------------------------------------------------
global function text_to_sequence(sequence text)
sequence SEQ
object searcher
    SEQ = {}

    searcher = searchall(text,',')
    if sequence(searcher) then
        SEQ = append(SEQ,text[1..searcher[1]-1])
        for i = 1 to length(searcher)-1 do
            SEQ = append(SEQ,text[searcher[i]+1..searcher[i+1]-1])
        end for
        SEQ = append(SEQ,text[searcher[length(searcher)]+1..length(text)])
    else
        SEQ = {text}
    end if

    return SEQ
end function
--------------------------------------------------------------------
global function remove_matches(sequence main, sequence badlist)
atom searcher
    for i = 1 to length(badlist) do
        searcher = search(main,badlist[i])
        while searcher != notok do
            main = delete_element(main,searcher)
            searcher = search(main,badlist[i])
        end while
    end for
    return main
end function
--------------------------------------------------------------------
global function check_atom_vertex(sequence vertex)
    for i = 1 to length(vertex) do
        if sequence(vertex[i])then
            return notok
        end if
    end for
    return ok
end function
---------------------------------------------------------------------
global function CSV2Matrix(sequence CSV)
atom okornotok
    for i = 1 to length(CSV) do
        for j = 1 to length(CSV[i]) do
            okornotok = check_seq_is_number(CSV[i][j])
            if okornotok = ok then
                CSV[i][j] = makenumber(CSV[i][j])
            end if
        end for
    end for
    return CSV
end function
--------------------------------------------------------------------------------------------
--Matricies
----------------------------------------------------------------------------------Matracies----
global function matrix_multiply(sequence matrix1,sequence matrix2)
sequence outputmatrix
atom total
    outputmatrix = {}

    for rowsm1 = 1 to length(matrix1) do

        for columnsm2 = 1 to length(matrix2[rowsm1]) do

            for rowncolumnupto = 1 to length(matrix2) do
                total = total + matrix1[rowsm1][rowncolumnupto] * matrix2[rowncolumnupto][
                                                                                          columnsm2
                                                                                         ]
            end for
            outputmatrix = writecell(rowsm1,columnsm2,total,outputmatrix)
        end for
    end for

    return outputmatrix
end function

global function matrix_addition(sequence matrix1,sequence matrix2)
sequence output
    output = {}
    for i = 1 to length(matrix1) do
        for j = 1 to length(matrix1[i]) do
            output = writecell(i,j,matrix1[i][j] + matrix2[i][j],output)
        end for
    end for
    return output
end function

global function matrix_subtraction(sequence matrix1,sequence matrix2)
sequence output
    output = {}
    for i = 1 to length(matrix1) do
        for j = 1 to length(matrix1[i]) do
            output = writecell(i,j,matrix1[i][j] - matrix2[i][j],output)
        end for
    end for
    return output
end function

global function matrix_scalar_multiplication(sequence matrix,atom scalar)
    for i = 1 to length(matrix) do
        for j = 1 to length(matrix[i]) do
            matrix[i][j] = matrix[i][j] * scalar
        end for
    end for
    return matrix
end function

global function twoxtwoinverse(sequence matrix)
    if length(matrix) != 2 or length(matrix[1]) != 2 or length(matrix[2]) != 2 then
        return notok
    else
        return matrix_scalar_multiplication(1/(matrix[1][1]*matrix[2][2] - matrix[1][2] * 
                                               matrix[2][1]),{{matrix[2][2],matrix[1][2]*-1},{
                                                                                              matrix
                                                                                              [
                                                                                               2
                                                                                              ]
                                                                                              [
                                                                                               1
                                                                                              ]
                                                                                              *
                                                                                              -1,
                                                                                              matrix
                                                                                              [
                                                                                               1
                                                                                              ]
                                                                                              [
                                                                                               1
                                                                                              ]
                                                                                             }
                                                             })-- 1/ad*bc * [d,-b]
--																											    [-c,a]
    end if
end function
--------------------------------------------------------------------------------------------
-- Verticies
global function vertex_scalar_subtraction(sequence vertex,atom tominus)
atom sok
    sok = check_atom_vertex(vertex)
    if sok != ok then
        return notok
    end if

    for i = 1 to length(vertex) do
        vertex[i] = vertex[i] - tominus
    end for

    return vertex
end function

global function vertex_multiplication(sequence vertex1, sequence vertex2)
atom vertexok
sequence outvertex
    outvertex = {}
    vertexok = check_atom_vertex(vertex1)
    if vertexok = notok then
        return statnotok
    end if
    vertexok = check_atom_vertex(vertex2)
    if vertexok = notok then
        return statnotok
    end if

    for i = 1 to length(vertex1) do
        outvertex = append(outvertex,vertex1[i]*vertex2[i])
    end for
    return outvertex
end function

global function vertex_sum(sequence vertex)
atom vertexok
atom total
    total = 0
    vertexok = check_atom_vertex(vertex)
    if vertexok != ok then
        return statnotok
    end if
    for i = 1 to length(vertex) do
        total = total + vertex[i]
    end for
    return total
end function

--------------------------------------------------------------------------------------------
-- 3D Data
-- X,Y,Z
--global function writeTRIDDSbox(atom X, atom Y,atom Z,object towrite,sequence TRIDDS)
--	while length(TRIDDS) < X do
--		TRIDDS = append(TRIDDS,{})
--	end while
--	while length(TRIDDS[X]) < Y do
--		TRIDDS[X] = append(TRIDDS[X],{})
--	end while
--	while length(TRIDDS[X][Y]) < Z do
--		TRIDDS[X][Y] = append(TRIDDS[X][Y],-1)
--	end while
--
--	TRIDDS[X][Y][Z] = towrite
--
--	return TRIDDS
--end function
--
--global function readTRIDDSbox(atom X,atom Y,atom Z,sequence TRIDDS)
--	if length(TRIDDS) < X or length(TRIDDS[X]) < Y or length(TRIDDS[X][Y]) < Z then
--		return notok
--	else
--		return TRIDDS[X][Y][Z]
--	end if
--end function
--
--global function extract_vector(atom X,atom Y,atom Z,sequence TRIDDS)
--sequence returnseq
----object readreturn
--	returnseq = {}
--	if X = -1 then
--		for i = 1 to length(TRIDDS) do
--			returnseq = append(returnseq,TRIDDS[i][Y][Z])
--		end for
--		return returnseq
--	end if
--	if Y = -1 then
--		for i = 1 to length(TRIDDS[X]) do
--			returnseq = append(returnseq,TRIDDS[X][i][Z])
--		end for
--		return returnseq
--	end if
--	if Z = -1 then
--		for i = 1 to length(TRIDDS[X][Y]) do
--			returnseq = append(returnseq,TRIDDS[X][Y][i])
--		end for
--		return returnseq
--	end if
--end function
--
--global function extract_matrix(atom X,atom Y,atom Z,sequence TRIDDS)
--sequence returnseq
--	returnseq = {}
--	if X != -1 then
--		return TRIDDS[X]
--	end if
--
--	if Y != -1 then
--		for i = 1 to length(TRIDDS) do
--			returnseq = append(returnseq,TRIDDS[i][Y])
--		end for
--	end if
--
--	if Z != -1 then
----writecell(atom row,atom column,object towrite,sequence CSV)
--		for x = 1 to length(TRIDDS) do
--			for y = 1 to length(TRIDDS[x]) do
--				writecell(x,y,TRIDDS[x][y][Z],returnseq)
--			end for
--		end for
--	end if
--
--	return returnseq
--end function
--
--global function writeTRIDDS(sequence whereto,atom overwrite,sequence TRIDDS) 
--	-- basically just a wrapper.
--atom filehandle
--	filehandle = open(whereto,"rb")
--	if filehandle = -1 or overwrite = on then
--		if filehandle != -1 then
--			close(filehandle)
--		end if
--		filehandle = open(whereto,"wb")
--		if dump = on then
--			send2dump(SMMP({{"u","Write TRIDDS called. "},{"u","filename = "},{"u",whereto},{
--																							 "u",
--																							 " handle = "
--																							},
--							{"r",filehandle},{"u","\n"}}))
--		end if
--		PutObject(filehandle, TRIDDS)
--	else
--		return notok
--	end if
--	close(filehandle)
--	return ok
--end function
--
--global function readTRIDDS(sequence wherefrom)
--atom filehandle
--object returna
--	filehandle = open(wherefrom,"rb")
--	if filehandle != -1 then
--		returna = GetObject(filehandle)
--		close(filehandle)
--		return returna
--	else
--		return notok
--	end if
--end function
---------------------------------------------------------------------------------------------
--global function writeTRIDDS_W_Percent(sequence whereto,atom overwrite,sequence TRIDDS) 
--	-- basically just a wrapper.
--atom filehandle
--	filehandle = open(whereto,"rb")
--	if filehandle = -1 or overwrite = on then
--		if filehandle != -1 then
--			close(filehandle)
--		end if
--		filehandle = open(whereto,"wb")
--		if dump = on then
--			send2dump(SMMP({{"u","Write TRIDDS called. "},{"u","filename = "},{"u",whereto},{
--																							 "u",
--																							 " handle = "
--																							},
--							{"r",filehandle},{"u","\n"}}))
--		end if
--		PutObject_w_Output(filehandle, TRIDDS)
--	else
--		return notok
--	end if
--	close(filehandle)
--	return ok
--end function

--------------------------------Statistics--------------------------------------------------
global function find_average(sequence toaverage)
atom total 
    total = 0
    if length(toaverage) = 0 then
        return statnotok
    end if
    for i = 1 to length(toaverage) do
        if sequence(toaverage[i]) then
            return statnotok
        end if
    end for

    for i = 1 to length(toaverage) do
        total = total + toaverage[i]
    end for

    return (total/length(toaverage))
end function

-------------------------------------------------------------------------------------------
global function stand_dev(sequence tostanddev,object average)
object myaverage
sequence differences
sequence squarediff
atom sum
atom vertexOK

    if length(tostanddev) = 1 then -- how can there be deviation with 1 question?
        return 0
    end if
    vertexOK = check_atom_vertex(tostanddev)
    if vertexOK != ok then
        return statnotok
    end if

    if sequence(average) or average = notok then
        myaverage = find_average(tostanddev)
    elsif atom(average) then
        myaverage = average
    end if

    if sequence(myaverage) then
        return statnotok
    end if

    differences = vertex_scalar_subtraction(tostanddev,myaverage)
    squarediff = vertex_multiplication(differences,differences) -- IE diff^2
    sum = vertex_sum(squarediff)
    sum = sum/(length(tostanddev)-1)
    sum = sqrt(sum)
    return sum
end function

global function min(sequence tofindmin)
atom isok
atom current_pick
    if length(tofindmin) = 0 then
        return statnotok
    end if
    isok = check_atom_vertex(tofindmin)
    if isok = notok then
        return statnotok
    end if

    current_pick = tofindmin[1]
    for i = 2 to length(tofindmin) do
        if current_pick > tofindmin[i] then
            current_pick = tofindmin[i]
        end if
    end for

    return current_pick
end function

global function max(sequence tofindmax)
--atom isok
object current_pick
--isok = check_atom_vertex(tofindmax)
--if isok = notok then
--return statnotok
--end if

    current_pick = tofindmax[1]
    for i = 2 to length(tofindmax) do
        if compare(current_pick,tofindmax[i]) = -1 then
            current_pick = tofindmax[i]
        end if
    end for

    return current_pick
end function
-------------------------------------------------------------------------------------------
global function sortA(sequence tosort)
sequence sorted
object maxreturn
atom searchreturn
    sorted = {}
    while length(tosort) > 0 do
        maxreturn = max(tosort)
        sorted = prepend(sorted,maxreturn)
        searchreturn = search(tosort,maxreturn)
        tosort = delete_element(tosort,searchreturn)
    end while
    return sorted
end function
------------------------------------------------------------------------------------
global function sortD(sequence tosort)
sequence sorted
object minreturn
atom searchreturn
    sorted = {}
    while length(tosort) > 0 do
        minreturn = min(tosort)
        sorted = append(sorted,minreturn)
        searchreturn = search(tosort,minreturn)
        tosort = delete_element(tosort,searchreturn)
    end while
    return sorted
end function
-----------------------------------------------------------------------------------------
global function find_median(sequence data)
    if length(data) = 0 then
        return statnotok
    end if
    data = sortA(data)

    if is_even(length(data)) = notok then
        return data[length(data)/2+.5]
    else
        return (data[length(data)/2] + data[length(data)/2+1])/2
    end if
end function

global function find_Q1(sequence data)
    if length(data) = 0 then
        return statnotok
    end if
    data = sortA(data)

    if is_even(length(data)) = ok then
        return find_median(data[1..length(data)/2])
    else
        return find_median(data[1..length(data)/2-.5])
    end if
end function

global function find_Q3(sequence data)
    if length(data) = 0 then
        return statnotok
    end if
    data = sortD(data)

    if is_even(length(data)) = ok then
        return find_median(data[1..length(data)/2])
    else
        return find_median(data[1..length(data)/2-.5])
    end if
end function
---------------------------------------------------------------------
global procedure make_ASCII_table()
atom foobar

    foobar = open(mydir() & "ASCIITABLE.TXT","w")
    for i = 1 to 255 do
        print(foobar,i)
        puts(foobar,"|")
        puts(foobar,i) 
        puts(foobar,"|")
        puts(foobar,"\n")
    end for
end procedure
------------------------------------------------------------------------------------
global function write_Inf_Var(sequence InfVar,object Var,object towrite,atom overwrite)
atom searchreturn

    if dump = on then
        send2dump(SMMP({{"u","WriteInf> called. "},{"u"," Var = "},{"u",Var},{"u","/"},{"r",
                                                                                        Var},{
                                                                                              "u",
                                                                                              " towrite = "
                                                                                             },
                        {"r",towrite},{"u","\n"}}))
    end if
    searchreturn = search(InfVar[1],Var)

    if searchreturn = notok or overwrite = on then
        InfVar[1] = append(InfVar[1],Var)
        InfVar[2] = append(InfVar[2],towrite)
    end if

    return InfVar
end function
----------------------------------------------------------------------------------------
global function Inf_Lookup(sequence InfVar,object Var)
atom searchreturn
    if dump = on then
        send2dump(SMMP({{"u","InfLookup> called. "},{"u"," Var = "},{"u",Var},{"u","/"},{"r",
                                                                                         Var},
                        {"u","\n"}}))
    end if
    searchreturn = search(InfVar[1],Var)

    if searchreturn != notok then
        if dump = on then
            send2dump(SMMP({{"u","InfLookup> Var Found. Returning "},{"r",InfVar[2][
                                                                                    searchreturn
                                                                                   ]},{"u",
                                                                                       "\n"}})
                     )
        end if
        return InfVar[2][searchreturn]
    else
        if dump = on then
            send2dump(SMMP({{"u","InfLookup> Not Found."},{"u","\n"}}))
        end if
        return notok
    end if
end function
-------------------------------------------------------------------------------------------
global function Rev_Inf_Lookup(sequence InfVar,object Val)
atom searchreturn
    if dump = on then
        send2dump(SMMP({{"u","RevInfLookup> called. "},{"u"," Value = "},{"r",Val},{"u","\n"}}
                      ))
    end if
    searchreturn = search(InfVar[2],Val)

    if searchreturn != notok then
        if dump = on then
            send2dump(SMMP({{"u","RevInfLookup> Value Found. Returning "},{"r",InfVar[1][
                                                                                         searchreturn
                                                                                        ]},{
                                                                                            "u",
                                                                                            "\n"
                                                                                           }})
                     )
        end if
        return InfVar[1][searchreturn]
    else
        if dump = on then
            send2dump(SMMP({{"u","RevInfLookup> Not Found."},{"u","\n"}}))
        end if
        return notok
    end if
end function
-------------------------------------------------------------------------------------------
global function DelInfVar(sequence InfVar,object Var)
atom searchreturn
    if dump = on then
        send2dump(SMMP({{"u","DelVar> called. "},{"u"," Var = "},{"u",Var},{"u","/"},{"r",Var},
                        {"u","\n"}}))
    end if
    searchreturn = search(InfVar[1],Var)

    if searchreturn != notok then
        if dump = on then
            send2dump(SMMP({{"u","DelVar>Var Found. Deleteing... "},{"u","\n"}}))
        end if
        InfVar[1] = delete_element(InfVar[1],searchreturn)
        InfVar[2] = delete_element(InfVar[2],searchreturn)
        return InfVar
    else
        if dump = on then
            send2dump(SMMP({{"u","DelVar> Not Found."},{"u","\n"}}))
        end if
        return InfVar
    end if
end function
------------------------------------------------------------------------------------------- BOOKMARK
global constant No_Compression = 0
global constant CompressoCompression = 1

global function do_compression(sequence data,atom compression_type)
    return data
end function

global function do_decompression(sequence data,atom compression_type)
    return data
end function

-------------------------------------------------------------------------------------------
global function make_unique(sequence seq)
sequence seq2
atom searchreturn
    seq2 = {}
    for i = 1 to length(seq) do
        searchreturn = search(seq2,seq[i])
        if searchreturn = -1 then
            seq2 = append(seq2,seq[i])
        end if
    end for
    return seq2
end function
------------------------------------------------------------------------------------------
global procedure errorandabort(sequence message)
    puts(1,message)
    sleep(3)
    abort(1)
end procedure
----------------------------------------------------------------------------------------
global function round(atom toround,atom toroundto)
atom leftovers
atom output
sequence upordown
    if dump = on then
        send2dump("Round> " & sprint(toround) & " to the nearest " & sprint(toroundto)& "\n")
    end if
    upordown = {-1,-1}
    leftovers = remainder(toround,toroundto)
    output = toround - leftovers 
-- this works
    upordown[1] = (output + toroundto) - leftovers
    upordown[2] = leftovers
    if upordown[1] <= upordown[2] then
        output = output + toroundto
    elsif upordown[1] > upordown[2] then
-- do nothing
    end if
    if dump = on then 
        send2dump("Round> returning " & sprint(output)& "\n")
    end if
    return output
end function
-----------------------------------------------------------Date Handling.....
global function daysinmonthlookup(atom month,atom year)
    if month = 1 or month = 3 or month = 5 or month = 7 or month = 8 or month = 10 or month = 
        12 then
        return 31
    end if
    if month = 4 or month = 6 or month = 9 or month = 11 then
        return 30
    end if
    if month = 2 then
        if remainder(year,4) > 0 then
            return 28
        elsif remainder(year,4) = 0then
            return 29
        end if
    end if

    return notok
end function

global function add_days(atom day, atom month,atom year,atom daystoadd)
    --if dump = on then
    --    send2dump("add_days starting :\n")
    --    send2dump("day : "&sprint(day) & "\n")
    --    send2dump("month : "&sprint(month)& "\n")
    --    send2dump("year : "&sprint(year)& "\n")
    --    send2dump("days2add : "&sprint(daystoadd)& "\n")
    --    send2dump("\n")
    --end if
-- Commented because taking tooooooo much room in dump logs
    while day + daystoadd > daysinmonthlookup(month,year) or month > 12 do
        --if dump = on then
        --    send2dump("add_days looping :\n")
        --    send2dump("day : "&sprint(day) & "\n")
        --    send2dump("month : "&sprint(month)& "\n")
        --    send2dump("year : "&sprint(year)& "\n")
        --    send2dump("days2add : "&sprint(daystoadd)& "\n")
        --    send2dump("\n")
        --end if
        if month > 12 then
            if dump = on then
                send2dump("month > 12 (month = " & sprint(month) &" ) \n")
            end if
            year += 1
            month -= 12
        end if
        if day+daystoadd > daysinmonthlookup(month,year) then
            --if dump = on then
            --send2dump("day + daystoadd ( " &sprint(day) & " + " & sprint(
            --daystoadd
            --                                                                     ) & 
            --     " ) > " 
            --     & sprint(daysinmonthlookup(month,year)) & "\n")
            --end if
            daystoadd -= daysinmonthlookup(month,year)
            month +=1
        end if
    end while
    day += daystoadd
    return {day,month,year}
end function


global function minus_days(atom day, atom month,atom year,atom daystoadd)
    --if dump = on then
    --    send2dump("minus_days starting :\n")
    --    send2dump("day : "&sprint(day) & "\n")
    --    send2dump("month : "&sprint(month)& "\n")
    --    send2dump("year : "&sprint(year)& "\n")
    --    send2dump("days2minus : "&sprint(daystoadd)& "\n")
    --    send2dump("\n")
    --end if
    while day - daystoadd < 1 or month < 1 do
        --if dump = on then
        --    send2dump("minus_days looping :\n")
        --    send2dump("day : "&sprint(day) & "\n")
        --    send2dump("month : "&sprint(month)& "\n")
        --    send2dump("year : "&sprint(year)& "\n")
        --    send2dump("days2minus : "&sprint(daystoadd)& "\n")
        --    send2dump("\n")
        --end if
        if month < 1 then
            if dump = on then
                send2dump("month < 1 (month = " & sprint(month) &" ) \n")
            end if
            year -= 1
            month += 12
        end if
        if day-daystoadd < 1 then
            --if dump = on then
            --    send2dump("day - daystoadd ( " &sprint(day) & " - " & sprint(
            --    daystoadd
            --                                                                         ) & 
            --         " ) < 1\n")
            --end if
            day += daysinmonthlookup(month,year)
            month -=1
        end if
    end while
    day -= daystoadd
    return {day,month,year}
end function

global function Date_Compare(sequence testee,sequence tester)
    if testee[3] != tester[3] then
        if testee[3] < tester[3] then
            return "BEFORE"
        elsif testee[3] > tester[3] then
            return "AFTER"
        end if
    elsif testee[2] != tester[2] then
        if testee[2] < tester[2] then
            return "BEFORE"
        elsif testee[2] > tester[2] then
            return "AFTER"
        end if
    elsif testee[1] != tester[1] then
        if testee[1] < tester[1] then
            return "BEFORE"
        elsif testee[1] > tester[1] then
            return "AFTER"
        end if
    else
        return "SAME"
    end if
end function

global function xrand(integer randn,integer x)
sequence toret
    toret = {}
    for i = 1 to x do
        toret &= rand(randn)
    end for
    return toret
end function

global function fbequal(sequence arg1,sequence arg2)
    send2dump("FBEQUAL> Checking if the first bit of "&strprint(arg1)&" is equal to "&strprint
              (arg2)&"\n")
    if length(arg1) >= length(arg2) and equal(arg1[1..length(arg2)],arg2) then
        send2dump("FBEQUAL> Returned ok\n")
        return 1=1
    else
        send2dump("FBEQUAL> Returned notok\n")
        return 1!=1
    end if
end function

global function trim(sequence string,sequence unneeded)
    if length(unneeded) <= length(string) and equal(string[1..length(unneeded)],unneeded) then
        return string[length(unneeded)+1..length(string)]
    else
        return string
    end if
end function

global function INI_to_Inf(sequence filename)
object getter
sequence output
atom filehandle
atom equals
    output = {{},{}}
    getter = {}
    filehandle = open(filename,"r")
    if filehandle = -1 then
        return {{"Error"},{filename&" could not be opened"}}
    end if
    while not(equal(getter,-1)) do
        getter = gets(filehandle)
        if atom(getter) then
            exit
        end if
        equals = search(getter,'=')
        if equals != -1 then
            if getter[length(getter)] != '\n' then
                output = write_Inf_Var(output,getter[1..equals-1],getter[equals+1..length(
                                                                                          getter
                                                                                         )],on
                                      )
            else
                output = write_Inf_Var(output,getter[1..equals-1],getter[equals+1..length(
                                                                                          getter
                                                                                         )-1],
                                       on)
            end if
        end if
    end while
    return output
end function

global function IPString_to_IP(sequence string)
sequence one, two, three, four
atom finder
    finder = find('.',string)
    if finder = 0 then 
        return -1 
    end if
    one = string[1..finder-1]
    string = string[finder+1..length(string)]
    finder = find('.',string)
    if finder = 0 then 
        return -1 
    end if
    two = string[1..finder-1]
    string = string[finder+1..length(string)]
    finder = find('.',string)
    if finder = 0 then 
        return -1 
    end if
    three = string[1..finder-1]
    string = string[finder+1..length(string)]
    four = string[1..length(string)]
    return {makenumber(one),makenumber(two),makenumber(three),makenumber(four)}
end function

global function SiM(sequence matrix,atom forcerow,atom forcecolumn,object searchfor)
if atom(searchfor) then
    printftodump("SiM>Attempting to find %g in %s\n",{searchfor,sprint(matrix)})
else
    printftodump("SiM>Attempting to find %s in %s\n",{searchfor,sprint(matrix)})
end if
if forcerow then
    if forcecolumn then
        if equal(searchfor,matrix[forcerow][forcecolumn]) then
            return {forcerow,forcecolumn}
        else
            return -1
        end if
    else
        for i = 1 to length(matrix[forcerow]) do
            if equal(matrix[forcerow][i],searchfor) then
                return {forcerow,i}
            end if
        end for
        return -1
    end if
end if
-- forcerow must be disabled
if forcecolumn then
    for i = 1 to length(matrix) do
        if equal(matrix[i][forcecolumn],searchfor) then
            return {i,forcecolumn}
        end if
    end for
    return -1
end if
for i = 1 to length(matrix) do
    for j = 1 to length(matrix[i]) do
        if equal(matrix[i][j],searchfor) then
            return {i,j}
        end if
    end for
end for
return -1
end function

global function get_value(object tovalue) -- wraper for value() that error reports.
sequence valret
valret = value(tovalue)
if valret[1] = GET_SUCCESS then
    return valret[2]
else
    puts(1,"ERROR : Value could not properly value "&strprint(tovalue))
    return {}
end if
-- this will never be reached, either it will return fine or it will exit
end function