<?php
# support/support.php
#
# Shows how much the newbie got installed
#
# 2004/11/14  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

print "<b>Support Page</b><br>";
print "The purpose of this page is to see how many of the requirements are set up properly.<br>";
print "THIS PAGE IS A ROUGH DRAFT OF WHAT IT SHOULD LOOK LIKE.  WE NEED SOME WORKING CODE ADDED TO IT.";
print "<table width=100% border=1>";

print "<tr>";
print "<td><b>Requirement</b></td>";
print "<td><b>Pass</b></td>";
print "<td><B>Fail</b></td>";
print "<td><b>Troubleshooting Notes</b></td>";
print "</tr>";

print "<tr>";
print "<td>Web Server Installed</td>";
# Might add some routine to show what type of webserver it is
print "<td> Pass </td>";
print "<td> </td>";
print "<td> If user can see this page, we know the web server is installed.</td>";
print "</tr>";

print "<tr>";
print "<td>Mysql Installed</td>";
# Might add some routine to show if mysql is installed
print "<td> unknown </td>";
print "<td> unknown </td>";
print "<td> MySQL is requirement.  See www.mysql.com</td>";
print "</tr>";

print "<tr>";
print "<td>Mysql Access</td>";
# Might add some routine to test if server is able to access the aistockbot database
print "<td> unknown </td>";
print "<td> unknown </td>";
print "<td> insert troubleshooting notes here</td>";
print "</tr>";

print "<tr>";
print "<td>PHP Installed</td>";
# Might add some routine to show if php is installed
print "<td> unknown </td>";
print "<td> unknown </td>";
print "<td> PHP is a requirement.  See www.php.com</td>";
print "</tr>";

print "<tr>";
print "<td>PEAR Installed</td>";
# Might add some routine to show if pear is installed
print "<td> unknown </td>";
print "<td> unknown </td>";
print "<td> PEAR is a requirement.  See ???</td>";
print "</tr>";

print "<tr>";
print "<td>JPGRAPH Installed</td>";
# Might add some routine to show if jpgraph is installed
print "<td> unknown </td>";
print "<td> unknown </td>";
print "<td> jpgraph is a requirement.  See ???</td>";
print "</tr>";

print "</table>";
include_once("{$path}include/footer.php");
?>