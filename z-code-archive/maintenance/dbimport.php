<?php

  //echo "dbimport included";

if(empty($path))
  $path = substr($_SERVER[PATH_TRANSLATED],0,-24); 
 
include_once($path.'include/config.php');
include_once($path.'include/functions.php');

if(empty($import_type))
  $import_type = 'i'; // for other options, see notes in functions.php

if(empty($sql_files)){
  $sql_dir = scan_dir($path.'sql/');
  $sql_files = $sql_dir[files];
}

set_time_limit(60);


db_p_open();
// does pconnect save time on connection setup/teardown?


//print_r($sql_files);

foreach($sql_files as $file){
  if(substr($file, -4) == '.sql'){
    if(file_exists($file)){
      echo "Importing $file<br>\n";
      $count = db_process_sql_file($file, $import_type);  // to debug, add 'dnq' to args
      echo "$count rows effected<br>\n";
    } else {
      echo "Could not open file '$file'<br>\n";
    }
  } else {
    echo "File '$file' skipped<br>\n";
  }
}
db_close();

?>
