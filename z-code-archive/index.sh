# index.sh

# These are the things that have to be done in advance because the
# bash shell is unable to run these when the .php script executes the .sh:
# touch /var/www/html/index.wav
# chown nobody:nobody /var/www/html/index.wav
# chmod 777 /var/www/html/index.wav

myvar="$1"

myvar=`echo $1 | sed -e 's/foobarspacer/ /g'` 

#echo "$myvar" | /var/www/html/aistockbot-0.0.2/flite -o /var/www/html/aistockbot-0.0.2/index.wav

echo "$myvar" | ./flite -o ./index.wav
