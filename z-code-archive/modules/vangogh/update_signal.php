<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	learn.php and update_signal.php
#############################################################################
#
# 2004/05/23  FS  Changed for easier use as update_signal.php
# 2004/05/18  FS  Fix a bug insert statement for ai_result
# 2004/05/17  FS  Changed $query_string to $menu_string and use
#                 function extract_menu_string to find the menus
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use settings from ai_config
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/09  MS  2109 lines - Added $path
# 2004/01/22  MS  1965 lines - Adding open, high, low, close, volume for 4 days out
# 2004/01/17  MS  1827 lines - Added variables:  close_2_days_ago, volume_2_days_ago, volume_yesterday
# 2003/07/20  MS  1808 lines - Added mysql_close($dbcnx) at end of file
# 2003/05/27  MS  1777 lines - Putting in Sell side
# 2003/05/26  MS  1596 lines - HTML-Free!
# 2003/05/26  MS  1609 lines - Added $high_254_days and $low_254_days
# 2003/05/12  MS  1527 lines - Changed Reporting
# 2003/05/10  MS  1508 lines - Accumulation/Distribution Line
# 2003/05/05  MS  1459 lines - Added WHERE completed=0 to combinations query
#                 so we can constantly rerun learn.php without having to
#                 erase the results every time.
# 2003/04/30  MS  1445 lines - Added $days_long and $days_short
# 2003/04/19  MS  1414 lines - Added Genetic Algorithm section.  This really
#		  speeds up things!!!
# 2003/04/18  MS  Added the $global_database and database connection changes
#                 per Dominic Porreco
# 2003/04/06  MS  Incorporated the code in update_signal.php into learn.php
#		  so I can simply copy learn.php to update_signal.php and 
#		  quickly uncomment some stuff to get that up to date.
# 2003/04/04  MS  Added Fundamental Data Analysis - 1218 lines
# 2003/04/03  MS  Added "ORDER BY ticker, date" to $sqlhistory query in
#		  learn.php to fix multiple results.
# 2002/11/02  MS  Optimized to analyze only the companies you want.
#		  Zeroed out close_moving_total_x and volume_moving_total_x
# 2002/09/25  MS  Fixed error where last combination was not calculated 
# 2002/09/22  MS  Working on combinations
# 2002/09/19  MS  Split learn.php by adding a formulas.php include statement
# 2002/09/16  MS  Went to "$global_hostname"
# 2002/09/15  MS  Multiple "ruleset_parent" Formula Appending, Code Cleanup
# 2002/09/14  MS  Multiple Stock Capability
# 2002/09/13  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));
$file_name=basename($_SERVER["PHP_SELF"]);

$score=0;
$high_254_days=0;
$low_254_days=9999999999;

print "<table width=100%>";
print "<tr>";
print "<td width=100% background={$path}images/hex.gif>";
print "<font size=-1>";

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
set_time_limit (240000);

print "<table width=100%>";

# Start Flite Speech Synthesis wording
$learn="I am learning right now";

# MYSQL QUERY STATEMENT DEFINITION SECTION:
# Define MySQL Query Statements

# Use this to analyze all quotes and companies
#$sqlhistory=db_query("
#	SELECT		ticker, date, open, high, low, close, volume, recommendation
#	FROM		history
#	ORDER BY 	ticker, date
#	");


# Use this to analyze only the companies you want
$sqlhistory=db_query("
	SELECT		ai_history.ticker AS ticker, ai_history.date AS date, ai_history.open AS open, ai_history.high AS high, ai_history.low AS low, ai_history.close AS close, ai_history.volume AS volume, ai_history.recommendation AS recommendation, ai_company.active AS active
	FROM		ai_history
	INNER JOIN	ai_company
	ON		ai_history.ticker = ai_company.ticker
	WHERE		ai_company.active = 1
	ORDER BY	ticker, date
	");

$sqlrule_parent=db_query("
	SELECT 	formula
	FROM 	ai_rule_parent
	WHERE	active = 1
	");

#$sqlresults=db_query("
#	SELECT	ticker
#	FROM 	ai_result
#	WHERE	ticker = '$ticker';
#	");


#--------------------------------------------------------------------------------------------------------------------------
# WARNING:  THIS SECTION CONTAINS CODE FOR THE update_signal.php VERSION
# THE DIFFERENCE BETWEEN learn.php and update_signal.php is this section
if ($file_name=="learn.php") {
	# FOR learn.php, use:
	print "<b>STEP:  CALCULATE RESULTS</b><br><br>";
	$sqldistinctcombinations=db_query("
		SELECT DISTINCT id
		FROM		ai_combinations
		WHERE		completed = 0
		ORDER BY	id
		");
} elseif ($file_name=="update_signal.php") {
	# FOR update_signal.php, use:
	print "<b>STEP: UPDATE SIGNAL</b><br>";
	$sqldistinctcombinations=db_query("
		SELECT 		combination_id AS id, 
				SUM(score) AS grandtotal, 
				AVG(percentage_of_days_score_above_zero) AS average_percentage_of_days_score_above_zero, 
				AVG(percentage_running_score_above_zero) AS average_percentage_running_score_above_zero
		FROM		ai_result
		GROUP BY	combination_id
		ORDER BY 	grandtotal DESC
		LIMIT		1
		");
}
#--------------------------------------------------------------------------------------------------------------------------

###########
# 05/27/2003:  combinations_sellside table dropped. Old Note: see above.I may need something different for update_signal.php
#$sql_combinations_sellside=db_query("
#	SELECT DISTINCT id
#	FROM		combinations_sellside
#	WHERE		completed = 0
#	ORDER BY	id
#	");
#
###########

$combinations_id=1;

# 05/27/2003:  Added rule_potential_sellside_id to this query:
$sqlcombos=db_query("
	SELECT		id, rule_potential_id, rule_potential_sellside_id
	FROM		ai_combinations
	WHERE		id = $combinations_id
	AND		completed = 0
	");

$sqlrulepotential=db_query("
	SELECT	id, formula 
	FROM 	ai_rule_potential_buyside
	WHERE	id = $combinations_id
	");

# 05/27/2003:  Added this:
$sqlrulepotentialsellside=db_query("
	SELECT	id, formula 
	FROM 	ai_rule_potential_sellside
	WHERE	id = $combinations_id
	");

# FUNCTION DEFINITION SECTION:
function ibuy($running_total)
	{
	global $recom;
	global $running_total;
	/* DEBUG MODE:  OFF
	echo " tomorrow morning i should BUY";
	*/
	global $buystatus;
	$recom="BUY";
	$buystatus=1; # I'm buying
	return $running_total;
	}

function isell($running_total)
	{
	global $recom;
	global $running_total;
	/* DEBUG MODE:  OFF
	echo " tomorrow morning i should SELL";
	*/
	global $buystatus;
	$recom="SELL";
	$buystatus=0; # I'm selling
	return $running_total;
	}


# Set SQL Queries For Fetching
$row=db_fetch_array($sqlhistory);

# FIRST RECORD INITIALIZATION SECTION:
$ticker			=$row["ticker"];
$date			=$row["date"];
$open			=$row["open"];
$high			=$row["high"];
$low			=$row["low"];
$close			=$row["close"];
$volume			=$row["volume"];
$yesterday_close	=$row["close"];
#used below:  $close_2_days_ago	=$row["close"]; # just to initialize it 
$volume_yesterday	=$volume; # just to initialize it

$highest_high_39_days=0;
$lowest_low_39_days=0;
$stochastics_39=0;
$stoch_start=0;
$stoch_end=0;
$stochastics_39_10DMA_delayed_TOTAL=0;
$stochastics_39_10DMA_delayed=0;

$open_1_days_ago = $open; # just to initialize it
$open_2_days_ago = $open; # just to initialize it
$open_3_days_ago = $open; # just to initialize it
$open_4_days_ago = $open; # just to initialize it

$high_1_days_ago = $high; # just to initialize it
$high_2_days_ago = $high; # just to initialize it
$high_3_days_ago = $high; # just to initialize it
$high_4_days_ago = $high; # just to initialize it

$low_1_days_ago = $low; # just to initialize it
$low_2_days_ago = $low; # just to initialize it
$low_3_days_ago = $low; # just to initialize it
$low_4_days_ago = $low; # just to initialize it

$close_1_days_ago = $close; # just to initialize it
$close_2_days_ago = $close; # just to initialize it
$close_3_days_ago = $close; # just to initialize it
$close_4_days_ago = $close; # just to initialize it

$volume_1_days_ago = $volume; # just to initialize it
$volume_2_days_ago = $volume; # just to initialize it
$volume_3_days_ago = $volume; # just to initialize it
$volume_4_days_ago = $volume; # just to initialize it

$running_total		=0;

########################################################
# SET PARENT FORMULA
# MULTIPLE "ruleset_parent" FORMULA APPENDING SECTION:
# Set the rule_parent formula
$getrule_parent=db_fetch_array($sqlrule_parent);
# Initialize Formula
$formula_parent=$getrule_parent["formula"];
$formula_parent="($formula_parent)";
# Loop Through All rule_parent Rulesets
while ($getrule_parent = db_fetch_array($sqlrule_parent) ) {
	$tmpformula=$getrule_parent["formula"];
	# Append Formula With Additional Rulesets
	$formula_parent="$formula_parent and ($tmpformula)";
}
#
########################################################







# LOOP THROUGH EVERY ID IN COMBINATION TABLE
while ($getdistinctcombinations = db_fetch_array($sqldistinctcombinations)) {

########################################################
# GENETIC ALGORITHM SECTION - PART 1 OF 3
# 04/29/2003:  ON HOLD
# 05/05/2003:  REINSTATED
# 05/26/2003:  ON HOLD AGAIN BECAUSE I'VE ADDED THE SELL SIDE
# $last_formula="$formula";
#
########################################################

	################################################
	# SET BUYSIDE FORMULA
	# Get next combination number
	$combinations_id=$getdistinctcombinations["id"];
	$combinations_id=(int)$combinations_id;

	# Initialize $revisedformula
	$tmpform="";
	$revisedformula="$formula_parent";
	#------------------------------------------------------------------------------------------------------------------
	if ($file_name=="learn.php") {
		# For learn.php, use:
		# GET RULE POTENTIAL
		$getrulepotentialid=db_query("
			SELECT	rule_potential_id 
			FROM	ai_combinations 
			WHERE	id=$combinations_id
			AND	completed = 0
			");
	} elseif ($file_name=="update_signal.php") {
		# For update_signal.php, use:
		# GET RULE POTENTIAL
		$getrulepotentialid=db_query("
			SELECT	rule_potential_id 
			FROM	ai_combinations 
			WHERE	id=$combinations_id
			");
	}
	#------------------------------------------------------------------------------------------------------------------
	while ($rowpotid=db_fetch_array($getrulepotentialid)) {	
		$potid=$rowpotid['rule_potential_id'];
		# Get formula from rule_potential
		$getformulafromrulepotential=db_query("
			SELECT 	formula
			FROM 	ai_rule_potential_buyside
			WHERE	id = $potid
			");
		$rowformulafromrulepotential=db_fetch_array($getformulafromrulepotential);
		$tmpform=$rowformulafromrulepotential['formula'];
		if($tmpform) {
			$revisedformula="$revisedformula and ($tmpform)";
		}
	}
	$formula="$revisedformula";
	#
	################################################



	################################################
	# SET SELLSIDE FORMULA
	
	$combinations_sellside_id=$getdistinctcombinations["id"];
	$combinations_sellside_id=(int)$combinations_id;
	$tmpsellsideform="";

	$revised_sellside_formula="";
	$formula_sellside="";

	$revised_sellside_formula="$formula_parent";
	#------------------------------------------------------------------------------------------------------------------
	if ($file_name=="learn.php") {
		# For learn.php, use:
		# GET RULE_POTENTIAL_SELLSIDE_ID
		$getrulepotentialsellsideid=db_query("
			SELECT DISTINCT	rule_potential_sellside_id 
			FROM		ai_combinations 
			WHERE		id=$combinations_sellside_id
			AND		completed = 0
			");
	} elseif ($file_name=="update_signal.php") {
		# For update_signal.php, use:
		# GET RULE_POTENTIAL_SELLSIDE_ID
		$getrulepotentialsellsideid=db_query("
			SELECT DISTINCT	rule_potential_sellside_id 
			FROM		ai_combinations 
			WHERE		id=$combinations_sellside_id
			");
	}
	#------------------------------------------------------------------------------------------------------------------
	while ($rowpotsellsideid=db_fetch_array($getrulepotentialsellsideid)) {	
		$potsellsideid=$rowpotsellsideid['rule_potential_sellside_id'];
		# Get formula from rule_potential
		$getformulafromrulepotentialsellside=db_query("
			SELECT 	formula
			FROM 	ai_rule_potential_sellside
			WHERE	id = $potsellsideid
			");
		$rowformulafromrulepotentialsellside=db_fetch_array($getformulafromrulepotentialsellside);
		$tmpsellsideform=$rowformulafromrulepotentialsellside['formula'];
		if($tmpsellsideform) {
			$revised_sellside_formula="$revised_sellside_formula and ($tmpsellsideform)";
		}
	}
	$formula_sellside="$revised_sellside_formula";

#	while ($row_combinations_sellside = db_fetch_array($sql_combinations_sellside)) {
#		# Get next combination number
#		$combinations_sellside_id=$row_combinations_sellside["id"];
#		$combinations_sellside_id=(int)$combinations_sellside_id;
#		# Initialize $revisedformula
#		$tmpform="";
#		$revised_formula_sellside="$formula_parent";
#		# GET RULE POTENTIAL
#		$sql_combinations_sellside=db_query("
#			SELECT	rule_potential_id 
#			FROM	combinations_sellside
#			WHERE	id=$combinations_sellside_id
#			AND	completed = 0
#			");
#		while ($row_combinations_sellside=db_fetch_array($sql_combinations_sellside)) {	
#			$potid=$row_combinations_sellside['rule_potential_id'];
#			# Get formula from rule_potential
#			$sql_rule_potential_sellside=db_query("
#				SELECT 	formula
#				FROM 	rule_potential_sellside
#				WHERE	id = $potid
#				");
#			$row_rule_potential_sellside=db_fetch_array($sql_rule_potential_sellside);
#			$tmpform=$row_rule_potential_sellside['formula'];
#			$revised_formula_sellside="$revised_formula_sellside and ($tmpform)";
#		}
	# I really don't think I should be closing out this while statement here because if I have several
	# sellside formulas, I really need to be running through every combination of them instead of just one.
# I'm gonna move this down towards the bottom:
#	} # questionable <----

#	$formula_sellside = "$revised_formula_sellside"; # WARNING:  THIS NEEDS TO BE CHANGED IN THE FUTURE!
	#
	################################################




	################################################
	# GENETIC ALGORITHM SECTION - PART 2 OF 3
	# 04/29/2003:  ON HOLD
	# 05/05/2003:  REINSTATED
	# 05/26/2003:  ON HOLD AGAIN BECAUSE I'VE ADDED THE SELL SIDE
	#if ($last_formula==$formula) {
	#	continue; # Restarts the while loop
	#}
	#
	################################################

	################################################
	# SHOW FORMULAS IN THE HEADER:
	print "<tr>";
	print "<td width=100% colspan=10>";
	print "<center><hr width=50%></center>";
	print "<font face=arial color=00000ff>";
	print "Combination:  <b>$combinations_id</b><br>";
	print "</font>";
	print "<font face=arial color=000000>";
	print "Buy Formula:  ";
	print "</font>";
	print "<font face=arial color=006600>";
	print "$formula<br>";
	print "</font>";
	print "<font face=arial color=000000>";
	print "Sell Formula:  ";
	print "</font>";
	print "<font face=arial color=990000>";
	print "$formula_sellside<br>";
	print "</font>";
	print "</td>";
	print "</tr>";
	#
	###########################################

	###########################################
	# DEBUG:
	if (db_query_config("debug_mode")==1) {
		print "<tr>";
		print "<td width=10%><font size=-2>TICKER</font></td>";
		print "<td width=10%><font size=-2>DATE</font></td>";
		print "<td width=10%><font size=-2>STOCHASTICS</font></td>";
		print "<td width=10%><font size=-2>CLOSE</font></td>";
		print "<td width=10%><font size=-2>SCORE</font></td>";
		print "<td width=10%><font size=-2>RECOM</font></td>";
		print "<td width=10%><font size=-2>STOCH_39_10DMA_DELAYED</font></td>";
		print "<td width=10%><font size=-2>INDICATOR 2: VOLUME_YESTERDAY</font></td>";
		print "<td width=10%><font size=-2>INDICATOR 3: VOLUME_2_DAYS_AGO</font></td>";
		print "<td width=10%><font size=-2>&nbsp;</font></td>";
		print "</tr>";
	}
	#
	############################################

	############################################
	# REVIEW HISTORY SECTION:
	# LOOP THROUGH EVERY RECORD IN THE HISTORY TABLE
	while ($row = db_fetch_array($sqlhistory) ) {

		# Check for Ticker change
		if (($row["ticker"] != "$ticker") and ($flag1 != 1)) {
			# P R O C E S S   A   T I C K E R ' S   R E S U L T S 
			# PROCESS RESULTS SECTION:
			# PROCESS PRIOR TICKER RESULTS SINCE WE'RE FINISHED WITH IT
			# Insert data into 'results' table using $insert_results query.
			$tmp_total_days=$tmp_positive_day+$tmp_negative_day;
			$percentage_of_days_score_above_zero=($tmp_positive_day/$tmp_total_days)*100;
			$tmp_total_days=$tmp_positive_running+$tmp_negative_running;
			$percentage_running_score_above_zero=($tmp_positive_running/$tmp_total_days)*100;
			$last_run=date("Y/m/d");	# OK

			###########################################
			# GENETIC ALGORITHM SECTION - PART 3 OF 3
			# If this isn't the best result, it knocks out all combinations downline
			# Status:  New - 04/19/2003 (TESTED)
			# 04/29/2003 STATUS:  REMOVED BECAUSE results.php COULD NOT ACCESS combinations table
			# with the successful records because everything was deleted that was processed.
			# 05/05/2003 STATUS:  ADDED AGAIN because this time, I'm updating the combinations table
			# instead of deleting it.
			# 05/26/2003:  ON HOLD AGAIN BECAUSE I'VE ADDED THE SELL SIDE
#			if ($results_high) {
#				if ($score >= $results_high) {
#					# New leader
#					$results_high = $score;
#					# print "results_high = $results_high ::: score=$score<br>";
#				} else {
#					# Remove future downline combinations from consideration
#					$sql_get_combinations=db_query("
#						SELECT 	rule_potential_id
#						FROM	combinations
#						WHERE	id = $combinations_id
#						");
#					while ($row_sql_get_combinations=db_fetch_array($sql_get_combinations)) {
#						$comboid = $row_sql_get_combinations['rule_potential_id'];
#						# print "combinations_id = $combinations_id ::: comboid = $comboid<br>";
#						$sql_get_comb1=db_query("
#							SELECT 	id
#							FROM	combinations
#							WHERE	rule_potential_id = $comboid
#						");
#						while ($row_sql_get_comb1=db_fetch_array($sql_get_comb1)) {
#							$comboid2=$row_sql_get_comb1['id'];
#							# print "comboid2=$comboid2<br>";
#							# DON'T DO THIS:
#							#db_query("
#							#	DELETE FROM	combinations
#							#	WHERE		id = $comboid2
#							#");
#							# DO THIS:
#							db_query("
#								UPDATE	combinations
#								SET	completed = 1
#								WHERE	id = $comboid2
#							");
#						}
#					}
#				}
#			} else {
#				$results_high = $score;
#			}
			#

# IF I DON'T DO A GENETIC ALGORITHM ABOVE, DO THIS
							db_query("
								UPDATE	ai_combinations
								SET	completed = 1
								WHERE	id = $combinations_id
							");
			########################################

#--------------------------------------------------------------------------------------------------------------------------
			if ($file_name=="learn.php") {
				if ($days_long==0 AND $score != 0) { $score= -999; } 	# Eliminate 100% short
											# everything formulas
								    			# 2004/01/17:  I'm adding 
											#              AND $score != 0 
								  			#              so I don't penalize
											#              systems with 0 trades
# took out			if ($days_short==0 AND $score != 0) { $score= -999; } 	# Eliminate 100% go long 
											# and hold formulas
								     			# 2004/01/17:  I'm adding 
											#              AND $score != 0 
								     			#              so I don't penalize
											#               systems with 0 trades
# 2004/05/17 FS short fix, needs to be rewritten later
				if (empty($days_long)) {
					$days_long=0;
				} // end if (empty($days_long))
				if (empty($days_short)) {
					$days_short=0;
				} // end if (empty($days_short))
# print "#$ticker#$combinations_id#$combinations_sellside_id#$score#$percentage_of_days_score_above_zero#$persentage_running_score_above_zero#$last_run#$days_long#$days_short#$transactions#<br>";
				db_query("
					INSERT INTO ai_result 	(ticker, 
								combination_id, 
								combinations_sellside_id, 
								score, percentage_of_days_score_above_zero, 
								percentage_running_score_above_zero, 
								last_run, days_long, 
								days_short, 
								transactions)
					VALUES 			('$ticker', 
								$combinations_id, 
								$combinations_sellside_id, 
								$score, 
								$percentage_of_days_score_above_zero, 
								$percentage_running_score_above_zero, 
								'$last_run', 
								$days_long, 
								$days_short, 
								$transactions)
					");	
			} elseif ($file_name=="update_signal.php") {
				# For update_signal.php, uncomment these lines:
				# UPDATE HISTORY TABLE WITH BUY/SELL SIGNAL
				if ($recom=="BUY") {
					db_query("
						UPDATE 	ai_history 
						SET 	recommendation=1 
						WHERE 	ticker='$ticker' 
						  AND 	date='$date'");
				} else {
					if ($recom=="SELL") {
						db_query("
							UPDATE 	ai_history 
							SET 	recommendation=0 
							WHERE 	ticker='$ticker' 
							  AND 	date='$date'");
					}
				}
			}
#--------------------------------------------------------------------------------------------------------------------------

			#####################################################
			# Timestamp this process just to see if this computer
			# is capable of performing this task for thousands of
			# companies.
			$timestamp=date("m/d/Y h:i:s");
			#####################################################

			#####################################################			
			#/* DEBUG MODE
			print "<tr>";
			print "<td width=10% colspan=2>";
			print "<font face=arial size=-1>";
			print "$timestamp";
			print "</font>";
			print "</td>";
			print "<td width=10% colspan=2>";
			print "<font face=arial size=-1>";
			print "$ticker";
			print "</font>";
			print "</td>";
			if ($score > 0) {
				print "<td width=10% colspan=3>";
				print "<font face=arial size=-1 color=006600>";
				print "Recommendation: $recom";
				print "</font>";
				print "</td>";
				print "<td width=10% colspan=3>";
				print "<font face=arial size=-1 color=006600>";
				print "Profit: $$score per share"; 
				print "</font>";
				print "</td>";
				# print "  Current recommendation of $recom (Total profit: $$score per share)";
			} else {
				print "<td width=10% colspan=6>";
				print "<font face=arial size=-1 color=ff0000>";
				print "Loss: $$score per share"; 
				print "</font>";
				print "</td>";
				# print "  I lost $$score using this formula.";
			}
			print "</tr>";
			#
			######################################################
			
			#*/
			# NEW TICKER INITIALIZATION SECTION:
			$tmpnewticker= $row["ticker"];
			# print "PLEASE NOTE:   TICKER HAS CHANGED TO $tmpnewticker<br>";
			#used below:      $close_2_days_ago = $yesterday_close; # 2004/01/17 Added $close_2_days_ago
			# $yesterday_close=$row["close"];
			$yesterday_close=$close;

			#used below:      $volume_2_days_ago	=$volume_yesterday; # 2004/01/17 Added
			$volume_yesterday	=$row["volume"];    # 2004/01/17 Added

			$highest_high_39_days=0;
			$lowest_low_39_days=0;
			$stochastics_39=0;
			$stoch_start=0;
			$stoch_end=0;
			$stochastics_39_10DMA_delayed_TOTAL=0;
			$stochastics_39_10DMA_delayed=0;

			# (copied from above)
			$open_1_days_ago = $open; # just to initialize it
			$open_2_days_ago = $open; # just to initialize it
			$open_3_days_ago = $open; # just to initialize it
			$open_4_days_ago = $open; # just to initialize it

			$high_1_days_ago = $high; # just to initialize it
			$high_2_days_ago = $high; # just to initialize it
			$high_3_days_ago = $high; # just to initialize it
			$high_4_days_ago = $high; # just to initialize it
		
			$low_1_days_ago = $low; # just to initialize it
			$low_2_days_ago = $low; # just to initialize it
			$low_3_days_ago = $low; # just to initialize it
			$low_4_days_ago = $low; # just to initialize it
		
			$close_1_days_ago = $close; # just to initialize it
			$close_2_days_ago = $close; # just to initialize it
			$close_3_days_ago = $close; # just to initialize it
			$close_4_days_ago = $close; # just to initialize it

			$volume_1_days_ago = $volume; # just to initialize it
			$volume_2_days_ago = $volume; # just to initialize it
			$volume_3_days_ago = $volume; # just to initialize it
			$volume_4_days_ago = $volume; # just to initialize it

			$yesterday_score=0;
			$running_total=0;
			$tmp_positive_day=0;
			$tmp_positive_running=0;
			$tmp_negative_day=0;
			$tmp_negative_running=0;
			$score=0;
			$days_long=0;
			$days_short=0;

			# Initialize Custom Ruleset Declaration & Calculation Variables
			$x=0;
			$moving_average_total=0;
			$close_moving_average_5_days=0;
			$close_moving_average_10_days=0;
			$close_moving_average_20_days=0;
			$close_moving_average_21_days=0;
			$close_moving_average_22_days=0;
			$close_moving_average_23_days=0;
			$close_moving_average_30_days=0;
			$close_moving_average_45_days=0;
			$close_moving_average_60_days=0;
			$close_moving_average_70_days=0;
			$close_moving_average_90_days=0;
			$close_moving_average_135_days=0;
			$close_moving_average_200_days=0;
			$close_moving_average_260_days=0;
			$close_moving_total_5_days=0;
			$close_moving_total_10_days=0;
			$close_moving_total_20_days=0;
			$close_moving_total_21_days=0;			
			$close_moving_total_22_days=0;
			$close_moving_total_23_days=0;
			$close_moving_total_30_days=0;
			$close_moving_total_45_days=0;
			$close_moving_total_60_days=0;
			$close_moving_total_70_days=0;
			$close_moving_total_90_days=0;
			$close_moving_total_135_days=0;
			$close_moving_total_260_days=0;
			$highest_high_5_days=0;
			$lowest_low_5_days=0;
			$volume_moving_average_5_days=0;
			$volume_moving_average_10_days=0;
			$volume_moving_average_20_days=0;
			$volume_moving_total_5_days=0;
			$volume_moving_total_10_days=0;
			$volume_moving_total_20_days=0;
			$close_exponential_moving_average_5_days=0;
			$yesterday_close_exponential_moving_average_5_days=0;
			$close_exponential_moving_average_12_days=0;
			$yesterday_close_exponential_moving_average_12_days=0;
			$close_exponential_moving_average_26_days=0;
			$yesterday_close_exponential_moving_average_26_days=0;
			$macd_fast=0;
			$macd_slow=0;
			$macd_histogram=0;
			$williams_percent_r_5_days=0;
			$high_254_days=0;
			$low_254_days=9999999999;
			$transactions=0;
			$buystatus=0;
			$entry_price=0;
			$exit_price=0;
		}

		# Drop the XMSR flag
		$flag1 = 0;

		# A N A L Y S I S

		################################################
		# GET TECHNICAL DATA
		# DATA GATHERING SECTION:
		# Get history table data - COMPLETE TABLE
		$ticker		=$row["ticker"];
		$date		=$row["date"];
		$open		=$row["open"];
		$high		=$row["high"];
		$low		=$row["low"];
		$close		=$row["close"];
		$volume		=$row["volume"];
		# NOT USED:  $recommendation	=$row["recommendation"]; 
		#
		################################################


		#################################################
		## GET FUNDAMENTAL DATA
		##
		$sqlfundamental=db_query("
			SELECT		book_value
					, edgar_accession_number
					, edgar_central_index_key
					, shares_outstanding
					, qtr_earnings
					, qtr_sales
			FROM		ai_fundamental
			WHERE		ticker = '$ticker'
			AND		report_date <= '$date'	
			ORDER BY	report_date	DESC
		");

		$rowfundamental=db_fetch_array($sqlfundamental);

		$book_value			= $rowfundamental["book_value"];
		$edgar_accession_number 	= $rowfundamental["edgar_accession_number"];
		$edgar_central_index_key 	= $rowfundamental["edgar_central_index_key"];
		$shares_outstanding		= $rowfundamental["shares_outstanding"];
		$qtr_earnings			= $rowfundamental["qtr_earnings"];
		$qtr_sales			= $rowfundamental["qtr_sales"];
		$equity				= $rowfundamental["equity"];
		# print "ticker=$ticker:::date=$date:::book_value=$book_value:::qtr_earnings=$qtr_earnings<br>";

		# print "<tr><td colspan=10>";
		# print "Date=$date";
		# Generate EDGAR link
		# $edgar_central_index_key_no_leading_zeros = (int)$edgar_central_index_key;
		# $edgar_accession_number_no_dashes = ereg_replace ("-", "", $edgar_accession_number);
		# $edgar_link = "http://edgar.sec.gov/Archives/edgar/data/$edgar_central_index_key_no_leading_zeros/$edgar_accession_number_no_dashes/$edgar_central_index_key-index.htm");
		# print "EDGAR LINK = $edgar_link";
		# DEBUG STUFF
		# print ":: Book Value=$book_value";
		# print ":: EDGAR Accession Number=$edgar_accession_number";
		# print ":: EDGAR Central Index Key=$edgar_central_index_key";
		# print ":: Shares Outstanding=$shares_outstanding";
		# print ":: Current Earnings This Quarter=$qtr_earnings";
		# print ":: Current Sales This Quarter=$qtr_sales";
		# print ":: Earnings One Year Ago=$qtr_ago_4_earnings";
		# print "<br>";
		# print "</td></tr>";
		#
		#################################################





		########################################################################
		########################################################################
		## 	     		  F O R M U L A S                             ##
		########################################################################
 		########################################################################
		# CUSTOM RULESET VARIABLE DECLARATION AND CALCULATION SECTION:         #
		########################################################################
		#  This is a section where I need YOUR help.                           #
		#  I need the full collection of Technical Analysis formulas known.    #
		#  If you provide one, that's great because it gets us further towards #
		#  our goal.  Email me at:  vooch@Lawrenceburg.com with your formulas. #
		#  Please use the format as described below.                           #
		########################################################################
		# GLOBAL VARIABLES - FOR TECHNICAL ANALYSIS                            #
		# These are variables which are used for more than one indicator       #
		########################################################################
		$x=$x+1;			# $x
						# This is the datastream id.
						# When 'x' is 1, it's the first
						# data record we have for a particular
						# ticker symbol.
						# Since it can be used for several
						# formulas below, it is included here.
		#
		$moving_close[$x]=$close;	# $moving_close[$x]
						# This is the closing price of the
						# stock at a particular point in time.
						# Since it can be used for several
						# formulas, it is included in this
						# section.
		#
		$moving_volume[$x]=$volume;	# $moving_volume[$x]
						# This is the volume of the stock
						# at a particular point in time. Since
						# it can be used for several formulas,
						# it is included in this section.
		#
		$moving_high[$x]=$high;		# $moving_high[$x]
						# This is the high of the stock
						# at a particular point in time. Since
						# it can be used for several formulas,
						# it is included in this section.
		#
		$moving_low[$x]=$low;		# $moving_low[$x]
						# This is the low of the stock
						# at a particular point in time. Since
						# it can be used for several formulas,
						# it is included in this section.
		#
		##########################################################################
		# GLOBAL VARIABLES - FOR FUNDAMENTAL ANALYSIS                            #
		##########################################################################
		$moving_qtr_earnings[$x]=$qtr_earnings;	# $moving_qtr_earnings[$x]
						# This is the stock's quarterly earnings
						# at a particular point in time. Since
						# it can be used for several formulas,
						# it is included in this section.
		#
		$moving_qtr_sales[$x]=$qtr_sales;	# $moving_qtr_sales[$x]
						# This is the stock's quarterly sales
						# at a particular point in time. Since
						# it can be used for several formulas,
						# it is included in this section.
		#

		###
		#
		#                  (Add Additional Global Variables Here.)
		#
		#
		##########################################################################
		##########################################################################
		# FORMULAS - TECHNICAL ANALYSIS                                          #  
		##########################################################################
		## FORMULA:		CLOSE EXPONENTIAL MOVING AVERAGE 5-DAYS
		## VARIABLE NAME:	$close_exponential_moving_average_5_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		*** UNVERIFIED ***
		## DESCRIPTION:		Exponential Moving Average:(EMA) = (Ptod * K) + (EMAyest * (1-K))
		##			where K = (2/(N+1))
		##			N = the number of days in the EMA
		##			Ptod = Today's Price
		##			EMAyest = the EMA of yesterday
		##
		if ($x >= 5) {
			# Calculate 5-day Exponential Moving Average
			$close_exponential_moving_average_5_days=($close*(2/(5+1)))+($yesterday_close_exponential_moving_average_5_days*(1-(2/(5+1))));
			$yesterday_close_exponential_moving_average_5_days=$close_exponential_moving_average_5_days;
		} else {
			# Since we do not have enough data points, make it coincide with the actual close.
			$close_exponential_moving_average_5_days=$close;
			$yesterday_close_exponential_moving_average_5_days=$close_exponential_moving_average_5_days;
		}
		##
		#######################################################################
		## FORMULA:		CLOSE EXPONENTIAL MOVING AVERAGE 12-DAYS
		## VARIABLE NAME:	$close_exponential_moving_average_12_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		*** UNVERIFIED ***
		## DESCRIPTION:		Exponential Moving Average:(EMA) = (Ptod * K) + (EMAyest * (1-K))
		##			where K = (2/(N+1))
		##			N = the number of days in the EMA
		##			Ptod = Today's Price
		##			EMAyest = the EMA of yesterday
		##
		if ($x >= 12) {
			# Calculate 12-day Exponential Moving Average
			$close_exponential_moving_average_12_days=($close*(2/(12+1)))+($yesterday_close_exponential_moving_average_12_days*(1-(2/(12+1))));
			$yesterday_close_exponential_moving_average_12_days=$close_exponential_moving_average_12_days;
		} else {
			# Since we do not have enough data points, make it coincide with the actual close.
			$close_exponential_moving_average_12_days=$close;
			$yesterday_close_exponential_moving_average_12_days=$close_exponential_moving_average_12_days;
		}
		##
		#######################################################################
		## FORMULA:		CLOSE EXPONENTIAL MOVING AVERAGE 26-DAYS
		## VARIABLE NAME:	$close_exponential_moving_average_26_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		*** UNVERIFIED ***
		## DESCRIPTION:		Exponential Moving Average:(EMA) = (Ptod * K) + (EMAyest * (1-K))
		##			where K = (2/(N+1))
		##			N = the number of days in the EMA
		##			Ptod = Today's Price
		##			EMAyest = the EMA of yesterday
		##
		if ($x >= 26) {
			# Calculate 26-day Exponential Moving Average
			$close_exponential_moving_average_26_days=($close*(2/(26+1)))+($yesterday_close_exponential_moving_average_26_days*(1-(2/(26+1))));
			$yesterday_close_exponential_moving_average_26_days=$close_exponential_moving_average_26_days;
		} else {
			# Since we do not have enough data points, make it coincide with the actual close.
			$close_exponential_moving_average_26_days=$close;
			$yesterday_close_exponential_moving_average_26_days=$close_exponential_moving_average_26_days;
		}
		##
		#######################################################################
		## FORMULA:		MACD (MOVING AVERAGE CONVERGENCE-DIVERGENCE) FAST LINE
		## VARIABLE NAME:	$macd_fast
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		*** UNVERIFIED ***
		## DESCRIPTION:		$fast_macd = 12dayExponentialMovingAverage-26dayExponentialMovingAverage
		##
		# Calculate MACD Fast
		$macd_fast=$close_exponential_moving_average_12_days-$close_exponential_moving_average_26_days;
		##
		#######################################################################
		## FORMULA:		MACD (MOVING AVERAGE CONVERGENCE-DIVERGENCE) SLOW SIGNAL LINE
		## VARIABLE NAME:	$macd_slow
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		*** UNVERIFIED ***
		## DESCRIPTION:		$slow_macd = 9dayExponentialMovingAverageofMACDFastLine
		##
		# Calculate MACD Slow Signal Line
		if ($x >= 9) {
			# Calculate macd_slow (which is 9-day Exponential Moving Average of MACD Fast Line)
			# $yesterday_macd_fast is calculated at the end of this entire formula section
			$macd_slow=($macd_fast*(2/(9+1)))+($yesterday_macd_fast*(1-(2/(9+1))));
		} else {
			# Since we do not have enough data points, make it coincide with macd_fast.
			$macd_slow=$macd_fast;
		}
		##
		#######################################################################
		## FORMULA:		MACD (MOVING AVERAGE CONVERGENCE-DIVERGENCE) HISTOGRAM
		## VARIABLE NAME:	$macd_histogram
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		*** UNVERIFIED ***
		## DESCRIPTION:		$macd_histogram = $macd_fast - $macd_slow
		##
		# Calculate MACD Histogram
		$macd_histogram=$macd_fast-$macd_slow;
		##
		#######################################################################
		## FORMULA:		VOLUME MOVING AVERAGE 5-DAYS
		## VARIABLE NAME:	$volume_moving_average_5_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs volume the last 5 trading days
		##
		if ($x >= 5) {
			# Calculate 5-day moving average
			$tmp=$x-5;
			$volume_moving_total_5_days=$volume_moving_total_5_days+$moving_volume[$x]-$moving_volume[$tmp];
			$volume_moving_average_5_days=$volume_moving_total_5_days/5;
		} else {
			# Since we do not have enough data points, average the period
			$volume_moving_total_5_days=$volume_moving_total_5_days+$moving_volume[$x];
			$volume_moving_average_5_days=$volume_moving_total_5_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		VOLUME MOVING AVERAGE 10-DAYS
		## VARIABLE NAME:	$volume_moving_average_10_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs volume the last 10 trading days
		##
		if ($x >= 10) {
			# Calculate 10-day moving average
			$tmp=$x-10;
			$volume_moving_total_10_days=$volume_moving_total_10_days+$moving_volume[$x]-$moving_volume[$tmp];
			$volume_moving_average_10_days=$volume_moving_total_10_days/10;
		} else {
			# Since we do not have enough data points, average the period
			$volume_moving_total_10_days=$volume_moving_total_10_days+$moving_volume[$x];
			$volume_moving_average_10_days=$volume_moving_total_10_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		VOLUME MOVING AVERAGE 20-DAYS
		## VARIABLE NAME:	$volume_moving_average_20_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs volume the last 20 trading days
		##
		if ($x >= 20) {
			# Calculate 20-day moving average
			$tmp=$x-20;
			$volume_moving_total_20_days=$volume_moving_total_20_days+$moving_volume[$x]-$moving_volume[$tmp];
			$volume_moving_average_20_days=$volume_moving_total_20_days/20;
		} else {
			# Since we do not have enough data points, average the period
			$volume_moving_total_20_days=$volume_moving_total_20_days+$moving_volume[$x];
			$volume_moving_average_20_days=$volume_moving_total_20_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 5-DAYS
		## VARIABLE NAME:	$close_moving_average_5_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 5 trading days
		## 
		if ($x >= 5) {
			# Calculate Closing Price 5-day Moving Average
			$tmp=$x-5;
			$close_moving_total_5_days=$close_moving_total_5_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_5_days=$close_moving_total_5_days/5;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_5_days=$close_moving_total_5_days+$moving_close[$x];
			$close_moving_average_5_days=$close_moving_total_5_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 10-DAYS
		## VARIABLE NAME:	$close_moving_average_10_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 10 trading days
		##
		if ($x >= 10) {
			# Calculate 10-day moving average
			$tmp=$x-10; 
			$close_moving_total_10_days=$close_moving_total_10_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_10_days=$close_moving_total_10_days/10;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_10_days=$close_moving_total_10_days+$moving_close[$x];
			$close_moving_average_10_days=$close_moving_total_10_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 20-DAYS
		## VARIABLE NAME:	$close_moving_average_20_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 20 trading days
		##
		if ($x >= 20) {
			# Calculate 20-day moving average
			$tmp=$x-20;
			$close_moving_total_20_days=$close_moving_total_20_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_20_days=$close_moving_total_20_days/20;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_20_days=$close_moving_total_20_days+$moving_close[$x];
			$close_moving_average_20_days=$close_moving_total_20_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 21-DAYS
		## VARIABLE NAME:	$close_moving_average_21_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 21 trading days (first 21 days give incorrect number),
		##			otherwise it was last verified accurate 11/02/02
		##
		if ($x >= 21) {
			# Calculate 21-day moving average
			$tmp=$x-21;
			$close_moving_total_21_days=$close_moving_total_21_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_21_days=$close_moving_total_21_days/21;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_21_days=$close_moving_total_21_days+$moving_close[$x];
			$close_moving_average_21_days=$close_moving_total_21_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 22-DAYS
		## VARIABLE NAME:	$close_moving_average_22_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 22 trading days
		##			Approximately 21.74 business days in a month,
		##			so this is a 30-day calendar day moving average
		##
		if ($x >= 22) {
			# Calculate 22-day moving average
			$tmp=$x-22;
			$close_moving_total_22_days=$close_moving_total_22_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_22_days=$close_moving_total_22_days/22;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_22_days=$close_moving_total_22_days+$moving_close[$x];
			$close_moving_average_22_days=$close_moving_total_22_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 23-DAYS
		## VARIABLE NAME:	$close_moving_average_23_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 23 trading days
		##
		if ($x >= 23) {
			# Calculate 23-day moving average
			$tmp=$x-23;
			$close_moving_total_23_days=$close_moving_total_23_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_23_days=$close_moving_total_23_days/23;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_23_days=$close_moving_total_23_days+$moving_close[$x];
			$close_moving_average_23_days=$close_moving_total_23_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 30-DAYS
		## VARIABLE NAME:	$close_moving_average_30_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 30 trading days
		##
		if ($x >= 30) {
			# Calculate 30-day moving average
			$tmp=$x-30;
			$close_moving_total_30_days=$close_moving_total_30_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_30_days=$close_moving_total_30_days/30;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_30_days=$close_moving_total_30_days+$moving_close[$x];
			$close_moving_average_30_days=$close_moving_total_30_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 45-DAYS
		## VARIABLE NAME:	$close_moving_average_45_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 45 trading days
		##
		if ($x >= 45) {
			# Calculate 45-day moving average
			$tmp=$x-45;
			$close_moving_total_45_days=$close_moving_total_45_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_45_days=$close_moving_total_45_days/45;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_45_days=$close_moving_total_45_days+$moving_close[$x];
			$close_moving_average_45_days=$close_moving_total_45_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 60-DAYS
		## VARIABLE NAME:	$close_moving_average_60_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 60 trading days
		##
		if ($x >= 60) {
			# Calculate 60-day moving average
			$tmp=$x-60;
			$close_moving_total_60_days=$close_moving_total_60_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_60_days=$close_moving_total_60_days/60;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_60_days=$close_moving_total_60_days+$moving_close[$x];
			$close_moving_average_60_days=$close_moving_total_60_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 70-DAYS
		## VARIABLE NAME:	$close_moving_average_70_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 70 trading days
		if ($x >= 70) {
			# Calculate 70-day moving average
			$tmp=$x-75;
			$close_moving_total_70_days=$close_moving_total_70_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_70_days=$close_moving_total_70_days/70;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_70_days=$close_moving_total_70_days+$moving_close[$x];
			$close_moving_average_70_days=$close_moving_total_70_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 90-DAYS
		## VARIABLE NAME:	$close_moving_average_90_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 90 trading days
		##
		if ($x >= 90) {
			# Calculate 90-day moving average
			$tmp=$x-90;
			$close_moving_total_90_days=$close_moving_total_90_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_90_days=$close_moving_total_90_days/90;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_90_days=$close_moving_total_90_days+$moving_close[$x];
			$close_moving_average_90_days=$close_moving_total_90_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 135-DAYS
		## VARIABLE NAME:	$close_moving_average_135_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 135 trading days
		##			270 business days in a year, so this is the
		##			6-month closing price moving average
		##
		if ($x >= 135) {
			# Calculate 135-day moving average
			$tmp=$x-135;
			$close_moving_total_135_days=$close_moving_total_135_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_135_days=$close_moving_total_135_days/135;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_135_days=$close_moving_total_135_days+$moving_close[$x];
			$close_moving_average_135_days=$close_moving_total_135_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 200-DAYS
		## VARIABLE NAME:	$close_moving_average_200_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 200 trading days
		##			200 business days in a year
		##
		if ($x >= 200) {
			# Calculate 200-day moving average
			$tmp=$x-200;
			$close_moving_total_200_days=$close_moving_total_200_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_200_days=$close_moving_total_200_days/200;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_200_days=$close_moving_total_200_days+$moving_close[$x];
			$close_moving_average_200_days=$close_moving_total_200_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		CLOSING PRICE MOVING AVERAGE 260-DAYS
		## VARIABLE NAME:	$close_moving_average_260_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs last 260 trading days
		##			260 business days in a year, so this is the
		##			52-week closing price moving average
		##
		if ($x >= 260) {
			# Calculate 260-day moving average
			$tmp=$x-260;
			$close_moving_total_260_days=$close_moving_total_260_days+$moving_close[$x]-$moving_close[$tmp];
			$close_moving_average_260_days=$close_moving_total_260_days/260;
		} else {
			# Since we do not have enough data points, average the period
			$close_moving_total_260_days=$close_moving_total_260_days+$moving_close[$x];
			$close_moving_average_260_days=$close_moving_total_260_days/$x;
		}
		##
		#######################################################################
		## FORMULA:		HIGH 254 DAYS (a.k.a. 52-week High)
		## VARIABLE NAME:	$high_254_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Approx. 254 business days in a year, so this is the 52-week High
		##
		if ($x >= 254) {
			# Calculate 254-day high
			$tmp=$x-254;
			$tmpbeg=(int)$tmp+1;
			$cunt=0;
			if ($moving_high[$tmp]==$high_254_days) {
				# If the high from a year ago is present, see what 
				# the high is from today to year-1day.
				# zero out $close_high_254_days
				$high_254_days=0;
				for($cunt=$tmpbeg;$cunt<$x;$cunt++) {
					if ($moving_high[$cunt] > $high_254_days) {
						$high_254_days = $moving_high[$cunt];
					}
					$cunt++;
				}
			} else {
				if ($moving_high[$x] > $high_254_days) {
					$high_254_days = $moving_high[$x];
				}
			}
			#print "date=$date,high=$high,low=$low,close=$close,high_254_days=$high_254_days<br>";
		} else {
			if ($moving_high[$x] > $high_254_days) {
				$high_254_days = $moving_high[$x];
			}
			#	print "waiting<br>";
			#print "date=$date,high=$high,low=$low,close=$close,high_254_days=$high_254_days<br>";
		}
		##
		#######################################################################
		## FORMULA:		LOW 254 DAYS (a.k.a. 52-week Low)
		## VARIABLE NAME:	$low_254_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Approx. 254 business days in a year, so this is the 52-week Low
		##
		if ($x >= 254) {
			# Calculate 254-day low
			$tmp=$x-254;
			$tmpbeg=(int)$tmp+1;
			$cunt=0;
			if ($moving_low[$tmp]==$low_254_days) {
				# If the high from a year ago is present, see what 
				# the high is from today to year-1day.
				# max out$close_high_254_days
				$low_254_days=9999999999;
				for($cunt=$tmpbeg;$cunt<$x;$cunt++) {
					if ($moving_low[$cunt] < $low_254_days) {
						$low_254_days = $moving_high[$cunt];
					}
					$cunt++;
				}
			} else {
				if ($moving_low[$x] < $low_254_days) {
					$low_254_days = $moving_low[$x];
				}
			}
			# print "date=$date,high=$high,low=$low,close=$close,low_254_days=$low_254_days<br>";
		} else {
			if ($moving_low[$x] < $low_254_days) {
				$low_254_days = $moving_low[$x];
			}
			#	print "waiting<br>";
			# print "<b>date=$date,high=$high,low=$low,close=$close,low_254_days=$low_254_days</b><br>";
		}
		##
		#######################################################################
		## FORMULA:		Accumulation/Distribution Line
		## VARIABLE NAME:   	$accumulation_distribution_line
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		
		#
		# This is a running total
		# Does it need the [$x]?  Or should i remove it?

		
		if (($high-$low)!=0) {
			if ($x > 1) {
				# If this is not the first data point, then...
				$accumulation_distribution_line[$x] = (((($close-$low)-($high-$close))/($high-$low))*$volume)+$accumulation_distribution_line[$x-1];
			} else { 
				# If this is the first data point...
				$accumulation_distribution_line[$x] = ((($close-$low)-($high-$close))/($high-$low))*$volume;
			}
		} else {
		# Correct division by zero error when high equals low
			if ($x > 1) {
				$accumulation_distribution_line[$x] = ((($close-$low)-($high-$close))/(0.000001+$high-$low))*$volume+$accumulation_distribution_line[$x-1];
			} else { 
				$accumulation_distribution_line[$x] = ((($close-$low)-($high-$close))/(0.000001+$high-$low))*$volume;
			}
		}
		#
		##
		#######################################################################
		## FORMULA:		WILLIAMS %R 5-DAYS	
		## VARIABLE NAME:   	$williams_percent_r_5_days
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		
		if ($x >= 5) {
			$highest_high_5_days=$moving_high[$x-4];
			if ($moving_high[$x-3] > $highest_high_5_days) { $highest_high_5_days=$moving_high[$x-3]; }
			if ($moving_high[$x-2] > $highest_high_5_days) { $highest_high_5_days=$moving_high[$x-2]; }
			if ($moving_high[$x-1] > $highest_high_5_days) { $highest_high_5_days=$moving_high[$x-1]; }
			if ($moving_high[$x]   > $highest_high_5_days) { $highest_high_5_days=$moving_high[$x]; }

			$lowest_low_5_days=$moving_low[$x-4];
			if ($moving_low[$x-3] < $lowest_low_5_days) { $lowest_low_5_days=$moving_low[$x-3]; }
			if ($moving_low[$x-2] < $lowest_low_5_days) { $lowest_low_5_days=$moving_low[$x-2]; }
			if ($moving_low[$x-1] < $lowest_low_5_days) { $lowest_low_5_days=$moving_low[$x-1]; }
			if ($moving_low[$x]   < $lowest_low_5_days) { $lowest_low_5_days=$moving_low[$x]; }
			
			# Calculate Williams %R 5-Days
			if ($highest_high_5_days==$lowest_low_5_days) {
				$williams_percent_r_5_days = -100;
			} else {
				$williams_percent_r_5_days = (($highest_high_5_days - $close) / ($highest_high_5_days - $lowest_low_5_days )) * -100;
			}

		} else {
			$williams_percent_r_5_days = -100; # Need to set it to something?
		}
		##
		#######################################################################
		#######################################################################
		## FORMULA:		$yesterday_close	
		## VARIABLE NAME:   (Submit your formulas to vooch@Lawrenceburg.com)
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		
		$yesterday_close = $moving_close[$x-1];
		##
		#######################################################################

                #######################################################################
                ## FORMULA:             STOCHASTICS (39,1)
                ## VARIABLE NAME:       $stochastics_39
                ## AUTHOR:              Vooch
                ## NEW VARIABLES USED:  $highest_high_39_days
                ##                      $lowest_low_39_days
                ##                      $stochastics_39
                ##                      $stochastics_39[$x]
                ## STATUS/BUGS:         unknown
                if ($x >= 39) {
                        $highest_high_39_days=$moving_high[$x-38];
                        if ($moving_high[$x-37] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-37];}
                        if ($moving_high[$x-36] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-36];}
                        if ($moving_high[$x-35] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-35];}
                        if ($moving_high[$x-34] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-34];}
                        if ($moving_high[$x-33] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-33];}
                        if ($moving_high[$x-32] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-32];}
                        if ($moving_high[$x-31] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-31];}
                        if ($moving_high[$x-30] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-30];}
                        if ($moving_high[$x-29] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-29];}
                        if ($moving_high[$x-28] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-28];}
                        if ($moving_high[$x-27] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-27];}
                        if ($moving_high[$x-26] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-26];}
                        if ($moving_high[$x-25] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-25];}
                        if ($moving_high[$x-24] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-24];}
                        if ($moving_high[$x-23] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-23];}
                        if ($moving_high[$x-22] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-22];}
                        if ($moving_high[$x-21] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-21];}
                        if ($moving_high[$x-20] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-20];}
                        if ($moving_high[$x-19] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-19];}
                        if ($moving_high[$x-18] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-18];}
                        if ($moving_high[$x-17] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-17];}
                        if ($moving_high[$x-16] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-16];}
                        if ($moving_high[$x-15] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-15];}
                        if ($moving_high[$x-14] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-14];}
                        if ($moving_high[$x-13] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-13];}
                        if ($moving_high[$x-12] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-12];}
                        if ($moving_high[$x-11] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-11];}
                        if ($moving_high[$x-10] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-10];}
                        if ($moving_high[$x-9] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-9];}
                        if ($moving_high[$x-8] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-8];}
                        if ($moving_high[$x-7] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-7];}
                        if ($moving_high[$x-6] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-6];}
                        if ($moving_high[$x-5] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-5];}
                        if ($moving_high[$x-4] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-4];}
                        if ($moving_high[$x-3] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-3];}
                        if ($moving_high[$x-2] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-2];}
                        if ($moving_high[$x-1] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x-1];}
                        if ($moving_high[$x] > $highest_high_39_days) {$highest_high_39_days=$moving_high[$x];}

                        $lowest_low_39_days=$moving_low[$x-38];
                        if ($moving_low[$x-37] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-37];}
                        if ($moving_low[$x-36] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-36];}
                        if ($moving_low[$x-35] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-35];}
                        if ($moving_low[$x-34] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-34];}
                        if ($moving_low[$x-33] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-33];}
                        if ($moving_low[$x-32] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-32];}
                        if ($moving_low[$x-31] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-31];}
                        if ($moving_low[$x-30] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-30];}
                        if ($moving_low[$x-29] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-29];}
                        if ($moving_low[$x-28] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-28];}
                        if ($moving_low[$x-27] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-27];}
                        if ($moving_low[$x-26] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-26];}
                        if ($moving_low[$x-25] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-25];}
                        if ($moving_low[$x-24] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-24];}
                        if ($moving_low[$x-23] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-23];}
                        if ($moving_low[$x-22] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-22];}
                        if ($moving_low[$x-21] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-21];}
                        if ($moving_low[$x-20] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-20];}
                        if ($moving_low[$x-19] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-19];}
                        if ($moving_low[$x-18] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-18];}
                        if ($moving_low[$x-17] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-17];}
                        if ($moving_low[$x-16] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-16];}
                        if ($moving_low[$x-15] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-15];}
                        if ($moving_low[$x-14] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-14];}
                        if ($moving_low[$x-13] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-13];}
                        if ($moving_low[$x-12] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-12];}
                        if ($moving_low[$x-11] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-11];}
                        if ($moving_low[$x-10] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-10];}
                        if ($moving_low[$x-9] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-9];}
                        if ($moving_low[$x-8] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-8];}
                        if ($moving_low[$x-7] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-7];}
                        if ($moving_low[$x-6] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-6];}
                        if ($moving_low[$x-5] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-5];}
                        if ($moving_low[$x-4] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-4];}
                        if ($moving_low[$x-3] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-3];}
                        if ($moving_low[$x-2] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-2];}
                        if ($moving_low[$x-1] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x-1];}
                        if ($moving_low[$x] < $lowest_low_39_days) {$lowest_low_39_days=$moving_low[$x];}

                        # Calculate Stoch
			$stochastics_39_yesterday=$stochastics_39;
                        $stochastics_39=(($close-$lowest_low_39_days)/($highest_high_39_days-$lowest_low_39_days))*100;
                        $stochastics_39_10DMA_delayed_MOVING[$x]=$stochastics_39;
                } else {
                        $stochastics_39=0;
                }
                ##
                #######################################################################
                ## FORMULA:             Stochastics with a 10-day Moving Average and Delayed by 10 days
                ## VARIABLE NAME:       $stochastics_39_10DMA_delayed
                ## AUTHOR:              Vooch
                ##                      $stoch_start
                ##                      $stoch_end
                ##                      $stochastics_39_10DMA_delayed_TOTAL
                ##                      $stochastics_39_10DMA_delayed
		##			$stochastics_39_10DMA_delayed_MOVING
                ## STATUS/BUGS:
                if ($x >= 20) {
                        # Calculate 10-day moving average
                        $stoch_start=$x-10;
                        $stoch_end=$x-20;
                        $stochastics_39_10DMA_delayed_TOTAL=$stochastics_39_10DMA_delayed_TOTAL+$stochastics_39_10DMA_delayed_MOVING[$stoch_start]-$stochastics_39_10DMA_delayed_MOVING[$stoch_end];
                        $stochastics_39_10DMA_delayed=$stochastics_39_10DMA_delayed_TOTAL/10;
                } else {
                        $stochastics_39_10DMA_delayed=0;
                }
                ##
                #######################################################################




		## FORMULA:		
		## VARIABLE NAME:   (Submit your formulas to vooch@Lawrenceburg.com)
		## AUTHOR:		
		## STATUS/BUGS:		
	
		##
		#######################################################################
		## FORMULA:		
		## VARIABLE NAME:   (Submit your formulas to vooch@Lawrenceburg.com)
		## AUTHOR:		
		## STATUS/BUGS:		
	
		##
		#######################################################################


if (db_query_config("bypass_fundamental_data")==0) {
# If 0, then read it...
		#######################################################################
		# FORMULAS - FUNDAMENTAL ANALYSIS
		#######################################################################
		## FORMULA:		EARNINGS - LAST YEAR
		## VARIABLE NAME:	$earnings_1_year_ago
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs earnings from 1 year ago
		##
		if ($x >= 270) {
			# Calculate 270-day moving average
			$tmp=$x-270;
			$qtr_earnings_1_year_ago=$moving_qtr_earnings[$tmp];
		} else {
			# Since we do not have enough data points, zero it out!
			$qtr_earnings_1_year_ago=0;
		}
		##
		#######################################################################
		## FORMULA:		SALES - LAST YEAR
		## VARIABLE NAME:	$sales_1_year_ago
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Grabs sales from 1 year ago
		##
		if ($x >= 270) {
			# Calculate 270-day moving average
			$tmp=$x-270;
			$qtr_sales_1_year_ago=$moving_qtr_sales[$tmp];
		} else {
			# Since we do not have enough data points, zero it out!
			$qtr_sales_1_year_ago=0;
		}
		##
		#######################################################################
		## FORMULA:		ANNUAL EARNINGS
		## VARIABLE NAME:	$annual_earnings
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Calculates Annual Earnings
		##
		if ($x >= 260) {
			# Calculate Annual Earnings
			$tmp=$x; # Get current quarter earnings
			$annual_earnings = 0;
			$annual_earnings = $moving_qtr_earnings[$tmp];

			$tmp=$x-65; # Get earnings (1 quarter ago)
			$annual_earnings = $annual_earnings + $moving_qtr_earnings[$tmp];
			
			$tmp=$x-130; # Get earnings (2 quarters ago)
			$annual_earnings = $annual_earnings + $moving_qtr_earnings[$tmp];
			
			$tmp=$x-195; # Get earnings (3 quarters ago)
			$annual_earnings = $annual_earnings + $moving_qtr_earnings[$tmp];
		}

		##
		#######################################################################
		## FORMULA:		EARNINGS PER SHARE
		## VARIABLE NAME:	$earnings_per_share
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Calculates Earnings Per Share
		##
		if ($x >= 260) {
			$earnings_per_share = $annual_earnings / $shares_outstanding;
		}
		##
		#######################################################################
		## FORMULA:		PRICE_EARNINGS RATIO
		## VARIABLE NAME:	$pe_ratio
		## AUTHOR:		Vooch (Michael Salvucci)
		## STATUS/BUGS:		Calculates Price-Earnings Ratio
		##
		if ($x >= 260) {
			$pe_ratio = $close / $earnings_per_share;
		}
		##
		#######################################################################
		# E N D   O F   F U N D A M E N T A L   D A T A   S E C T I O N
		#######################################################################
} # end of db_query_config("bypass_fundamental_data")==0 check

		#######################################################################
		#######################################################################
		##
		## TURN TODAY'S STUFF INTO YESTERDAY'S STUFF SO I CAN USE IT DURING
		## THE NEXT LOOP
		##	
		$yesterday_macd_fast=$macd_fast;
		
		##
		##
		#######################################################################
		#######################################################################


		#######################################################################
		#
		#			e n d   o f   f o r m u l a s
		#
		#######################################################################


		##################################
		# EVALUATION PREPARATION SECTION:
		# PREP THE EVALUATION BY CREATING THE IF STATEMENT STRING.
		#	$if = "if ($formula) { echo \"BUY\";  } else { echo \"SELL\"; }";
		$if_buyside = "if ($formula) { ibuy($running_total); } else { isell($running_total); }";
		#
		$if_sellside = "if ($formula_sellside) { isell($running_total); } else { ibuy($running_total); }";
		#
		#################################
	
		# DEPRECATED?...
		# $running_total appears to be a dead variable already.
		# whenever I see it's value, it's always 0.
		# I should probably remove all references to $running_total
		# at some point in the future.
		# echo "$running_total";

		################################
		# SCORING SECTION:
		# If yesterday's rec was a BUY...
		if ($yesterday_recom=="BUY") {
#took out (using entry_price and exit_price instead):			$score=$score+$close-$yesterday_close;
			$days_long=$days_long+1;
			# print " SCORE=$score<br>";
		} else {
#took out (using entry_price and exit_price instead):			$score=$score+$yesterday_close-$close;
#took out (never going short)			$days_short=$days_short+1;
			# print " SCORE=$score<br>";
		}
		#
		################################




		######################################################
		# FORMULA EVALUATION SECTION:                        #
		# VERY IMPORTANT BECAUSE THIS EVALUATES THE RULESETS #
		if ($buystatus==1) {
			# If I already bought, then I want to evaluate the sellside
			eval ($if_sellside);            
		} else {
			# If I already sold, then I want to evaluate the buyside
			eval ($if_buyside);            
		}
		######################################################
		



		#######################################
		# 2004/01/17:  ENTRY/EXIT PRICE CALCULATION
		if ($recom != $yesterday_recom AND $recom=="BUY") {
			$entry_price = $close;
		}
		if ($recom != $yesterday_recom AND $recom=="SELL" AND $entry_price != 0) {
			$exit_price = $close;
			$score = $score + $exit_price - $entry_price;
			$entry_price = 0; # reset
			$exit_price = 0; # reset
		}
		########################################



		#################################
		# $yesterday_close bug
		# For some odd reason, I keep losing the $yesterday_close after AIStockBot makes
		# a buy (the first buy record only was affected).  I think it might lose it during
		# the eval() process, but I don't know for certain.
		# So, I'm setting it here again!
		$yesterday_close = $moving_close[$x-1];
		##################################



		##################################
		# DEBUG AREA
		if (db_query_config("debug_mode")==1) {
			print "<tr>";
			print "<td width=10%>";
				print "<font size=-2>$ticker</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>$date</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>s=$stochastics_39</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>$close</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>$score</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>$recom $entry_price</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>s10dmaD=$stochastics_39_10DMA_delayed</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>";
				print "$volume_yesterday";
				print "</font>";
			print "</td>";
			print "<td width=10%>";
				print "<font size=-2>";
				print "$volume_2_days_ago";
				print "</font>";
			print "</td>";
			print "<td border=1 width=10%>";
				print "<font size=-2>";
				print "PE Ratio=$pe_ratio";
				print "</font>";
			print "</td>";
			print "</tr>";		
		}
		# END OF DEBUG AREA
		######################

		################################
		# COMMISSIONS - THIS IS JUST ROUGHED OUT RIGHT NOW.  WE'LL IMRPOVE IT LATER:
		if ($recom != $yesterday_recom) {
			# This means I made another transaction
			$transactions=$transactions+1; # Count the number of transactions made
			if ($score>0) {
				$score=$score*.98;
			} else {
				$score=$score*1.02;
			}
		}
		################################

		################################
		# RISK MODIFIER - under development
		# adjust score by a risk modifier variable (.999999?) based on a losing day.
		# this could be used to punish the hell out of a losing day.
		#
		################################

#--------------------------------------------------------------------------------------------------------------------------
# WARNING:  THIS SECTION CONTAINS CODE FOR THE update_signal.php VERSION
		if ($file_name=="update_signal.php") {
			# For update_signal.php, uncomment these lines:
			
			# UPDATE HISTORY TABLE WITH BUY/SELL SIGNAL
			if ($recom=="BUY") {
				db_query("
					UPDATE 	ai_history 
					SET 	recommendation=1 
					WHERE 	ticker='$ticker' 
					  AND 	date='$date'");
			} else {
				if ($recom=="SELL") {
					db_query("
						UPDATE 	ai_history 
						SET 	recommendation=0 
						WHERE 	ticker='$ticker' 
						  AND 	date='$date'");
				}
			}
		}
#---------------------------------------------------------------------------------------------------------------------------

		###################################
		# ACCUMULATE PROGRESS SECTION:
		# ACCUMULATE percentage_of_days_score_above_zero FOR results TABLE
		# percentage_of_days_score_above_zero is the percentage of the number
		# of days we made a profit using a particular combination and ticker.
		if ($score >= $yesterday_score) {
			# Increase percentage_of_days_score_above_zero
			$tmp_positive_day=$tmp_positive_day+1;
			# DEBUG MODE: OFF
			# echo "score=$score,yesterday_score=$yesterday_score,tmp_positive_day=$tmp_positive_day";
			
		} else {
			$tmp_negative_day=$tmp_negative_day+1;
			# DEBUG MODE: OFF
			# echo "tmp_negative_day=$tmp_negative_day";
		}
		# ACCUMULATE percentage_running_score_above_zero FOR results TABLE
		# percentage_running_score_above_zero is the percentage of the number
		# of days our running score was above zero using a particular 
		# combination and ticker.
		if ($score >= 0) {
			# Increase percentage_running_score_above_zero
			$tmp_positive_running=$tmp_positive_running+1;
		} else {		
			$tmp_negative_running=$tmp_negative_running+1;
		}
		#
		##################################

		# TOMORROW PREPARATION SECTION:
		# PREPARE TODAY'S STUFF AS YESTERDAY'S STUFF BECAUSE I'M ABOUT TO LOOP	
		# After accumulating my scores, I need to set yesterday's score
		# so I can keep a running total of how often I'm right and wrong.
		# Formula:  "Score" is the same as the amount of profit made on 1 share.
		$yesterday_score=$score;
		# Formula:  Calculate yesterday's recommendation
		$yesterday_recom="$recom";
		# 2004/01/17:    Added $close_2_days_ago
		#used below:  $close_2_days_ago = $yesterday_close;
		# Formula:  Calculate yesterday's close
		$yesterday_close = $close;
		# Volume
		#used below:         $volume_2_days_ago	=$volume_yesterday; # 2004/01/17 Added
		$volume_yesterday	=$volume;    # 2004/01/17 Added

		# NOTICE:  I'M GOING DOWN FROM 4_DAYS TO 1_DAYS
		$open_4_days_ago = $open_3_days_ago; # added 2004/01/22
		$open_3_days_ago = $open_2_days_ago; # 
		$open_2_days_ago = $open_1_days_ago; # 
		$open_1_days_ago = $open; # 

		$high_4_days_ago = $high_3_days_ago; # 
		$high_3_days_ago = $high_2_days_ago; # 
		$high_2_days_ago = $high_1_days_ago; # 
		$high_1_days_ago = $high; # 

		$low_4_days_ago = $low_3_days_ago; # 
		$low_3_days_ago = $low_2_days_ago; # 
		$low_2_days_ago = $low_1_days_ago; # 
		$low_1_days_ago = $low; #

		$close_4_days_ago = $close_3_days_ago; # 
		$close_3_days_ago = $close_2_days_ago; # 
		$close_2_days_ago = $close_1_days_ago; # 
		$close_1_days_ago = $close; # 

		$volume_4_days_ago = $volume_3_days_ago; # 
		$volume_3_days_ago = $volume_2_days_ago; # 
		$volume_2_days_ago = $volume_1_days_ago; # 
		$volume_1_days_ago = $volume; # 

	}

###############################################################
###############################################################
# PROCESS RESULTS SECTION:  (AGAIN)
# THIS SECTION IS AN EXACT DUPLICATE OF THE SECTION ABOVE
# BECAUSE I NEED TO GET THE RESULTS FOR THE LAST TICKER
# THAT IS TESTED:
#
# PROCESS PRIOR TICKER RESULTS
# Insert data into 'results' table using $insert_results query.
$tmp_total_days=$tmp_positive_day+$tmp_negative_day;
$percentage_of_days_score_above_zero=($tmp_positive_day/$tmp_total_days)*100;
$tmp_total_days=$tmp_positive_running+$tmp_negative_running;
$percentage_running_score_above_zero=($tmp_positive_running/$tmp_total_days)*100;
$last_run=date("Y/m/d");	# OK


#--------------------------------------------------------------------------------------------------------------------------
# WARNING:  THIS SECTION IS NOT USED IN THE update_signal.php VERSION
# because update_signal.php would continue to add the results together.
if ($file_name=="learn.php") {
	db_query("
		INSERT INTO ai_result (ticker, 
					combination_id, 
					combinations_sellside_id, 
					score, 
					percentage_of_days_score_above_zero, 
					percentage_running_score_above_zero, 
					last_run, 
					days_long, 
					days_short, 
					transactions)
		VALUES 			('$ticker', 
					$combinations_id, 
					$combinations_sellside_id, 
					$score, 
					$percentage_of_days_score_above_zero, 
					$percentage_running_score_above_zero, 
					'$last_run', 
					$days_long, 
					$days_short, 
					$transactions)
		");	
}
#--------------------------------------------------------------------------------------------------------------------------

# Timestamp this process just to see if this computer
# is capable of performing this task for thousands of
# companies.
$timestamp=date("m/d/Y h:i:s");

##############################
# DEBUG MODE - OLD WAY
#print "<tr>";
#print "<td width=100% colspan=10>";
#
#print "$ticker completed at: $timestamp";
#	if ($score > 0) {
#		print " with current recommendation of $recom (Total profit: $$score per share)";
#	} else {
#		print " the formula I used was a loser: $score";
#	}
#print "</td>";
#print "</tr>";
#
###############################





			#####################################################			
			#/* DEBUG MODE
			print "<tr>";
			print "<td width=10% colspan=2>";
			print "<font face=arial size=-1>";
			print "$timestamp";
			print "</font>";
			print "</td>";
			print "<td width=10% colspan=2>";
			print "<font face=arial size=-1>";
			print "$ticker";
			print "</font>";
			print "</td>";
			if ($score > 0) {
				print "<td width=10% colspan=3>";
				print "<font face=arial size=-1 color=006600>";
				print "Recommendation: $recom";
				print "</font>";
				print "</td>";
				print "<td width=10% colspan=3>";
				print "<font face=arial size=-1 color=006600>";
				print "Profit: $$score per share"; 
				print "</font>";
				print "</td>";
				# print "  Current recommendation of $recom (Total profit: $$score per share)";
			} else {
				print "<td width=10% colspan=6>";
				print "<font face=arial size=-1 color=ff0000>";
				print "Loss: $$score per share"; 
				print "</font>";
				print "</td>";
				# print "  I lost $$score using this formula.";
			}
			print "</tr>";
			#
			######################################################
			




#
# NEW TICKER INITIALIZATION SECTION:
$tmpnewticker= $row["ticker"];
# print "PLEASE NOTE:   TICKER HAS CHANGED TO $tmpnewticker<br>";
# 2004/01/17:  Added $close_2_days_ago
$close_2_days_ago = $yesterday_close;
$yesterday_close=$close; # 2004/01/17 minor change
# was $yesterday_close=$row["close"];
$volume_2_days_ago	=$volume_yesterday; # 2004/01/17 Added
$volume_yesterday	=$row["volume"];    # 2004/01/17 Added

$highest_high_39_days=0;
$lowest_low_39_days=0;
$stochastics_39=0;
$stoch_start=0;
$stoch_end=0;
$stochastics_39_10DMA_delayed_TOTAL=0;
$stochastics_39_10DMA_delayed=0;

##########
# added 2004/01/22:  this stuff added:
$open_1_days_ago = $open; # just to initialize it
$open_2_days_ago = $open; # just to initialize it
$open_3_days_ago = $open; # just to initialize it
$open_4_days_ago = $open; # just to initialize it

$high_1_days_ago = $high; # just to initialize it
$high_2_days_ago = $high; # just to initialize it
$high_3_days_ago = $high; # just to initialize it
$high_4_days_ago = $high; # just to initialize it

$low_1_days_ago = $low; # just to initialize it
$low_2_days_ago = $low; # just to initialize it
$low_3_days_ago = $low; # just to initialize it
$low_4_days_ago = $low; # just to initialize it

$close_1_days_ago = $close; # just to initialize it
$close_2_days_ago = $close; # just to initialize it
$close_3_days_ago = $close; # just to initialize it
$close_4_days_ago = $close; # just to initialize it

$volume_1_days_ago = $volume; # just to initialize it
$volume_2_days_ago = $volume; # just to initialize it
$volume_3_days_ago = $volume; # just to initialize it
$volume_4_days_ago = $volume; # just to initialize it
###########




$yesterday_score=0;
$running_total=0;
$tmp_positive_day=0;
$tmp_positive_running=0;
$tmp_negative_day=0;
$tmp_negative_running=0;
$score=0;
$days_long=0;
$days_short=0;
# Initialize Custom Ruleset Declaration & Calculation Variables
$x=0;
$moving_average_total=0;
$close_moving_average_5_days=0;
$close_moving_average_10_days=0;
$close_moving_average_20_days=0;
$close_moving_average_21_days=0;
$close_moving_average_22_days=0;
$close_moving_average_23_days=0;
$close_moving_average_30_days=0;
$close_moving_average_45_days=0;
$close_moving_average_60_days=0;
$close_moving_average_70_days=0;
$close_moving_average_90_days=0;
$close_moving_average_135_days=0;
$close_moving_average_200_days=0;
$close_moving_average_260_days=0;
$close_moving_total_5_days=0;
$close_moving_total_10_days=0;
$close_moving_total_20_days=0;
$close_moving_total_21_days=0;
$close_moving_total_22_days=0;
$close_moving_total_23_days=0;
$close_moving_total_30_days=0;
$close_moving_total_45_days=0;
$close_moving_total_60_days=0;
$close_moving_total_70_days=0;
$close_moving_total_90_days=0;
$close_moving_total_135_days=0;
$close_moving_total_270_days=0;
$highest_high_5_days=0;
$lowest_low_5_days=0;
$volume_moving_average_5_days=0;
$volume_moving_average_10_days=0;
$volume_moving_average_20_days=0;
$volume_moving_total_5_days=0;
$volume_moving_total_10_days=0;
$volume_moving_total_20_days=0;
$close_exponential_moving_average_5_days=0;
$yesterday_close_exponential_moving_average_5_days=0;
$close_exponential_moving_average_12_days=0;
$yesterday_close_exponential_moving_average_12_days=0;
$close_exponential_moving_average_26_days=0;
$yesterday_close_exponential_moving_average_26_days=0;
$macd_fast=0;
$macd_slow=0;
$macd_histogram=0;
$williams_percent_r_5_days=0;
$high_254_days=0;
$low_254_days=9999999999;
$transactions=0;
$buystatus=0;
$entry_price=0;
$exit_price=0;
#*/
###############################################################
###############################################################

# After running through all the tickers, I need to reset the pointer so I can run through all the tickers again.
# $sqlhistory is a db_result set type, not an array, so I need to use this command instead of reset():
mysql_data_seek($sqlhistory, 0);

# Raise a flag so I don't keep repeating XMSR
$flag1=1;


# combinations_sellside
#}
# combinations_sellside

# combinations....end of "while ($getdistinctcombinations = db_fetch_array($sqldistinctcombinations)) {" line
}
# combinations....end of "while ($getdistinctcombinations = db_fetch_array($sqldistinctcombinations)) {" line

print "</table>";
print "<font face=arial size=-1>";
print "<br><br><br><b>PAST PERFORMANCE IS NOT INDICATIVE OF FUTURE RESULTS</b><br>";

#--------------------------------------------------------------------------------------------------------------------------
# WARNING:  THIS SECTION CONTAINS CODE FOR THE update_signal.php VERSION
# THE DIFFERENCE BETWEEN learn.php and update_signal.php is this section
if ($file_name=="learn.php") {
	# FOR learn.php, use:
	print "<br>";
	print "<a href=results.php?{$menu_string}>Go To Next Step:  Results</a><br>";
	print "<br>";
	print "<b>END OF STEP:  CALCULATE RESULTS</b><br>";
} elseif ($file_name=="update_signal.php") {
	# FOR update_signal.php, use:
	# print "<b>END OF STEP:  UPDATE SIGNAL</b><br>";
}
#--------------------------------------------------------------------------------------------------------------------------
print "</font>";
print "</td></tr></table>";

include_once("{$path}include/footer.php");

?>
