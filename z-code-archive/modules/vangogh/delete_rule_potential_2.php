<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	delete_rule_potential_2.php
# This program gives the buy/sell signal based on the best result.
#############################################################################
#
# 2004/05/13  FS  Changed to use sellside too
# 2004/05/12  FS  Changed to use new functions for the database
#                 and new folder
# 2002/11/03  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

$sqlgetrule_potential=db_query("
	SELECT ID, formula
	FROM	ai_rule_potential_buyside
	");

$sqlgetrule_potential_sellside=db_query("
	SELECT ID, formula
	FROM	ai_rule_potential_sellside
	");

$numrows=db_num_rows($sqlgetrule_potential);
$numrows_sellside=db_num_rows($sqlgetrule_potential_sellside);

print "<font face=Arial size=-1><b>MODIFY POTENTIAL RULES</b><br><br>";
print "<i>You may delete additional rules by clicking the Button located to the left of the formula. When you are finished, move on to the next step.</i></font><br><br>";

#Buyside
$e=1;
while ($e < $numrows+1) {
	$row=db_fetch_array($sqlgetrule_potential);
	$currentID=$row["ID"];
	$formula=$row["formula"];
	# Gotta keep ID in auto-incrementing state or other programs will fail
/*	if ($e != $currentID) {
		db_query("UPDATE ai_rule_potential_buyside SET ID=$e WHERE ID=$currentID");
		$currentID=$e;
	}
*/	print " <font face=Arial size=-1>";
	print "<form name={$currentID} action={$path}modules/vangogh/delete_rule_potential.php?ID=$currentID&$menu_string method=post>";
	print "<input type=submit name=submit value=Buyside>";
	print " &nbsp; {$formula}<br>";
	print "</form>";
	print "</font>";
	$e++;
}

print "<br>";

#Sellside
$e=1;
while ($e < $numrows_sellside+1) {
	$row=db_fetch_array($sqlgetrule_potential_sellside);
	$currentID=$row["ID"];
	$formula=$row["formula"];
	# Gotta keep ID in auto-incrementing state or other programs will fail
/*	if ($e != $currentID) {
		db_query("UPDATE ai_rule_potential_sellside SET ID=$e WHERE ID=$currentID");
		$currentID=$e;
	}
*/	print " <font face=Arial size=-1>";
	print "<form name={$currentID}_sellside action={$path}modules/vangogh/delete_rule_potential.php?ID_sellside=$currentID&$menu_string method=post>";
	print "<input type=submit name=submit value=Sellside>";
	print " &nbsp; {$formula}<br>";
	print "</form>";
	print "</font>";
	$e++;
}

include_once("{$path}include/footer.php");

?>
