<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:     learn.php and update_signal.php
#############################################################################
#
# 2004/06/22  FS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));
$file_name=basename($_SERVER["PHP_SELF"]);

print "<table width=100%>";
print "<tr>";
print "<td width=100% background={$path}images/hex.gif>";
print "<font size=-1>";
print "<table width=100%>";

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
set_time_limit (240000);

#--------------------------------------------------------------------------------------------------------------------------
# WARNING:  THIS SECTION CONTAINS CODE FOR THE update_signal.php VERSION
# THE DIFFERENCE BETWEEN learn.php and update_signal.php is this section
if ($file_name=="learn.php") {
        # FOR learn.php, use:
        print "<b>STEP:  CALCULATE RESULTS</b><br><br>";
        $sqldistinctcombinationsellside=db_query("
                SELECT DISTINCT id
                FROM            ai_combination_sellside
                WHERE           completed = 0
                ORDER BY        id
                ");
	# Start Flite Speech Synthesis wording
	$learn="I am learning right now";
} elseif ($file_name=="update_signal.php") {
        # FOR update_signal.php, use:
        print "<b>STEP: UPDATE SIGNAL</b><br>";
        $sqldistinctcombination=db_query("
                SELECT          combination_id AS id,
                                SUM(score) AS grandtotal,
                                AVG(percentage_of_days_score_above_zero) AS average_percentage_of_days_score_above_zero,
                                AVG(percentage_running_score_above_zero) AS average_percentage_running_score_above_zero
                FROM            ai_result
                GROUP BY        combination_id
                ORDER BY        grandtotal DESC
                LIMIT           1
                ");
	# Start Flite Speech Synthesis wording
	$learn="I update right now";
}
#--------------------------------------------------------------------------------------------------------------------------

// find all used ticker and write it in an array

$sqlticker=db_query("
        SELECT  ticker
        FROM    ai_company
        WHERE   active=1
        ");

if (db_num_rows($sqlticker) > 0) {
        while ($tmpticker=db_fetch_array($sqlticker)) {
                $ticker[]=$tmpticker['ticker'];
        } // end while ($tmpticker=db_fetch_array($sqlticker))
} else {
        print "<b>No ticker activated. Exit!</b><br><br>";
        die ("No ticker activated. Exit!");
} // end if db_num_rows($sqlticker) > 0)

// read history data for all used ticker

$counter=0;
while (!empty($ticker[$counter])) {
        print "Read history data for ticker '{$ticker[$counter]}' ... <br>";

        // Read all history data for ticker in an array
        $sqlhistory=db_query("
                SELECT          date, open, high, low, close, volume
                FROM            ai_history
                WHERE           ticker = '{$ticker[$counter]}'
                ORDER BY        date ASC
                ");
        while ($tmphistory=db_fetch_array($sqlhistory)) {
                $tickerhistory[$tmphistory['date']]=array(
                        'open'  => $tmphistory['open'],
                        'high'  => $tmphistory['high'],
                        'low'   => $tmphistory['low'],
                        'close' => $tmphistory['close'],
                        'volume'=> $tmphistory['volume']
                        );
        } // end while ($tmphistory=db_fetch_array($sqlhistory))
	$history[$ticker[$counter]]=$tickerhistory;
        $counter++;
} // end while (!empty($ticker[$counter]))




print "</table>";
print "<font face=arial size=-1>";
print "<br><br><br><b>PAST PERFORMANCE IS NOT INDICATIVE OF FUTURE RESULTS</b><br>";

#--------------------------------------------------------------------------------------------------------------------------
# WARNING:  THIS SECTION CONTAINS CODE FOR THE update_signal.php VERSION
# THE DIFFERENCE BETWEEN learn.php and update_signal.php is this section
if ($file_name=="learn.php") {
        # FOR learn.php, use:
        print "<br>";
        print "<b>END OF STEP:  CALCULATE RESULTS</b><br><br>";
        print "<a href={$path}/modules/vangogh/results.php?{$menu_string}>Go To Next Step:  Results</a><br>";
} elseif ($file_name=="update_signal.php") {
        # FOR update_signal.php, use:
        # print "<b>END OF STEP:  UPDATE SIGNAL</b><br>";
}
#--------------------------------------------------------------------------------------------------------------------------
print "</font>";
print "</td></tr></table>";

include_once("{$path}include/footer.php");

?>
