<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	calculation.php
#############################################################################
#
# 2004/06/04  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");
include_once("{$path}include/finance_functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));
$file_name=basename($_SERVER["PHP_SELF"]);

print "<table width=100%>";
print "<tr>";
print "<td width=100%>";
print "<font size=-1>";

# Start Flite Speech Synthesis wording
$learn="I am calculate right now";

print "<b>STEP:  CALCULATE USED VARIABLES</b><br><br>";

// find all used variables and write it in an array

$sqlvariableid=db_query("
	SELECT DISTINCT	ai_rule_potential_buyside.variable_left as id 
	FROM		ai_rule_potential_buyside, ai_combination_buyside
	WHERE		ai_combination_buyside.completed = 0
	  AND		ai_combination_buyside.rule_potential_id = ai_rule_potential_buyside.id
	");

$sqlvariablesellsideid=db_query("
	SELECT DISTINCT	ai_rule_potential_sellside.variable_left as id 
	FROM		ai_rule_potential_sellside, ai_combination_sellside
	WHERE		ai_combination_sellside.completed = 0
	  AND		ai_combination_sellside.rule_potential_id = ai_rule_potential_sellside.id
	");

if (db_num_rows($sqlvariableid) > 0 or db_num_rows($sqlvariablesellsideid) > 0) {
	while ($tmpidarray=db_fetch_array($sqlvariableid)) {
		$sqlvariable=db_query("
			SELECT 	name, description, options
			FROM	ai_variable
			WHERE	ID = {$tmpidarray['id']}
			");
		$tmpvariablearray=db_fetch_array($sqlvariable);
		if (empty($tmpvariablearray['options'])) {
			$variable[$tmpidarray['id']]=array($tmpvariablearray['name'], $tmpvariablearray['description']);
		} else {
			$variable[$tmpidarray['id']]=$tmpvariablearray['name']."|".$tmpvariablearray['description']."|".$tmpvariablearray['options'];
			$variable[$tmpidarray['id']]=explode('|', $variable[$tmpidarray['id']]);
		} // end if (empty($tmpvariablearray['options']))
	} // end while ($tmpidarray=db_fetch_array($sqlvariableid))
	while ($tmpidarray=db_fetch_array($sqlvariablesellsideid)) {
		$sqlvariable=db_query("
			SELECT 	name, description, options
			FROM	ai_variable
			WHERE	ID = {$tmpidarray['id']}
			");
		$tmpvariablearray=db_fetch_array($sqlvariable);
		if (empty($tmpvariablearray['options'])) {
			$variable[$tmpidarray['id']]=array($tmpvariablearray['name'], $tmpvariablearray['description']);
		} else {
			$variable[$tmpidarray['id']]=$tmpvariablearray['name']."|".$tmpvariablearray['description']."|".$tmpvariablearray['options'];
			$variable[$tmpidarray['id']]=explode('|', $variable[$tmpidarray['id']]);
		} // end if (empty($tmpvariablearray['options']))
	} // end while ($tmpidarray=db_fetch_array($sqlvariableid))
} else {
	print "<b>No variable activated. Exit!</b><br><br>";
	die ("No variable activated. Exit!");
} // end if (db_num_rows($sqlvariable) > 0)

// find all used ticker and write it in an array

$sqlticker=db_query("
	SELECT	ticker
	FROM	ai_company
	WHERE	active=1
	");

if (db_num_rows($sqlticker) > 0) {
	while ($tmpticker=db_fetch_array($sqlticker)) {
		$ticker[]=$tmpticker['ticker'];
	} // end while ($tmpticker=db_fetch_array($sqlticker))
} else {
	print "<b>No ticker activated. Exit!</b><br><br>";
	die ("No ticker activated. Exit!");
} // end if db_num_rows($sqlticker) > 0)

// calculate every variable for all used ticker

$counter=0;
while (!empty($ticker[$counter])) {
	print "Calculate ticker '{$ticker[$counter]}' ... <br>";

	// Read all history data for ticker in an array
	$sqlhistory=db_query("
		SELECT 		date, open, high, low, close, volume
		FROM 		ai_history
		WHERE		ticker = '{$ticker[$counter]}'
		ORDER BY 	date ASC
		");
	while ($tmphistory=db_fetch_array($sqlhistory)) {
		$history[$tmphistory['date']]=array(
			'open'	=> $tmphistory['open'],
			'high'	=> $tmphistory['high'],
			'low'	=> $tmphistory['low'],
			'close'	=> $tmphistory['close'],
			'volume'=> $tmphistory['volume']
			);
	} // end while ($tmphistory=db_fetch_array($sqlhistory))

	// Calculate each used variable
	reset($variable);
	while (list($key, $val) = each($variable)) {
		print "&nbsp; &nbsp; Calculate variable '{$val[1]}' ... <br>";
		flush();
                db_query("
                        DELETE
                        FROM    ai_calculation
                        WHERE   ticker = '{$ticker[$counter]}'
                          AND   variable = $key
                        ");
		$val[2]($ticker[$counter], $history, $key, $val);
	} // end while (list($key, $val) = each($variable))
	$counter++;
} // end while (!empty($ticker[$counter])) 

print "<br><b>END OF STEP:  CALCULATE USED VARIABLES</b><br><br>";

include_once("{$path}include/footer.php");

print "</font>";
print "</td></tr></table>";

?>
