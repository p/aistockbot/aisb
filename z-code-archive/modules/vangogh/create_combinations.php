<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	create_combinations.php
# Description:  This script clears the "combinations" table, then
#               recreates it using the "rule_potential" records.
#############################################################################
#
# 2004/06/17  FS  Changed set_time_limit because the script needs lots of time
# 2004/05/13  FS  Changed to use sellside too
# 2004/05/12  FS  Changed to use new functions for the database
#                 and new folder
# 2004/04/13  FS  Changed algorithm for skipping useless combinations
# 2004/04/02  FS  Changed algorithm for combinations to find all combinations
# 2004/04/01  FS  Fixed use of $global_database instead of fix dbname
# 2002/11/06  MS  Fixed memory leaks
# 2002/11/01  MS  Corrected algorithm and optimized for shorter formulas first
# 2002/10/28  MS  Increased to 10 minutes 
# 2002/09/25  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# SET EXECUTION TIME TO 20 MINUTES (1200 Seconds)
set_time_limit (86400);

# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlrule_potential=db_query("
	SELECT 	id, formula
	FROM 	ai_rule_potential_buyside
	ORDER BY id
	");

# Initialize variables
$counter_combinations = 1; # Number of Combinations created
$counter_records = 0; # Number of records inserted
$combination_id = 1; # Combination ID

# Set SQL Queries For Fetching
$numrowsrule_potential=db_num_rows($sqlrule_potential);

print "<font face=Arial size=2><b>STEP:  CREATE COMBINATIONS</b><br><br>";

print "<b>Deleting all combination records</b><br><br>";

# Wipe out ai_combination_buyside table
db_query("
	DELETE FROM ai_combination_buyside
");
# Wipe out ai_combination_sellside table
db_query("
	DELETE FROM ai_combination_sellside
");

print "<b>Adding new combination records for buyside</b><br>";

# read ID from ai_rule_potential_buyside
$counter_rule_potential=0;
while ($counter_rule_potential<$numrowsrule_potential) {
	$id_rule_potential_array=db_fetch_array($sqlrule_potential);
	$id_rule_potential[$counter_rule_potential]=$id_rule_potential_array["id"];
	$formula_rule_potential[$counter_rule_potential]=$id_rule_potential_array["formula"];
	$counter_rule_potential++;
} # end while ($counter_rule_potential<$numrowsrule_potential)

# Build combinations
$counter_rule_potential=0;
while ($counter_rule_potential<$numrowsrule_potential) {
	$alldone=0;
	# clean array and set first full combination
	$c=0;
	while ($c<=$counter_rule_potential) {
		$tmparray[$c]=$c+1;
		$c++;
	} # end while ($c<$counter_rule_potential)
	$onedone=1;
	$c=$counter_rule_potential;
	# create combinations and write to db
	while (! $alldone) {
		if ($onedone) {
			# find useless combinations
			$f=0;
			$skip_rule=0;
			while ($f<=$counter_rule_potential and ! $skip_rule) {
				$g=$f+1;
				while ($g<=$counter_rule_potential and ! $skip_rule) {
					$formula1=explode(" ", $formula_rule_potential[$tmparray[$f]-1]);
					$formula2=explode(" ", $formula_rule_potential[$tmparray[$g]-1]);
					if ($formula1[0] == $formula2[0] and $formula1[2] == $formula2[2]) {
						$skip_rule=1;
					} # end if ($formula1[0] == $formula2[0] and $formula1[2] == $formula2[2])
					$g++;
				} # end while ($g<=$counter_rule_potential and ! $skip_rule)
				$f++;	
			} #end while ($f<=$counter_rule_potential and ! $skip_rule)
			if (! $skip_rule) {
				# write good combinations
				$d=0;
				while ($d<=$counter_rule_potential) {
					$e=$id_rule_potential[$tmparray[$d]-1];
					db_query("
					INSERT INTO ai_combination_buyside
						(id, rule_potential_id)
					VALUES
						($combination_id, $e)
					");
					$counter_records++;
					$d++;
				} # end while ($d<=$counter_rule_potential)
				$tmparray[$c]=$tmparray[$c]+1;
				$onedone=0;
				$combination_id++;
			} else {
				$d=0;
				while ($d<=$counter_rule_potential) {
					$d++;
				} # end while $d<=$counter_rule_potential)
				$skip_rule=0;
				$tmparray[$c]=$tmparray[$c]+1;
				$onedone=0;
			} # end if (! $skip_rule)
			if (!($counter_combinations % 1000)) {
				$counter_records--;
				$combination_id--;
				print "{$counter_combinations} analysed, {$combination_id} ({$counter_records} records) added.<br>";
				flush();
				$combination_id++;
				$counter_records++;
			} // end if (!($counter_combinations % 1000))
			$counter_combinations++;
		} elseif ($c<0) {
			$alldone=1;
		} elseif ($tmparray[$c] > $numrowsrule_potential-(($counter_rule_potential+1)-($c+1))) {
			$c=$c-1;
			$tmparray[$c]=$tmparray[$c]+1;
		} else {
			$d=$c;
			while ($d<$counter_rule_potential) {
				$tmparray[$d+1]=$tmparray[$d]+1;
				$d++;
			} # end while ($d<$counter_rule_potential)
			$onedone=1;
			$c=$counter_rule_potential;
		}
	} # end while (! $alldone)
	$counter_rule_potential++;
} # end while ($counter_rule_potential<=$numrowsrule_potential)
$combination_id--;
$counter_combinations--;
$counter_records--;

print "<b>The combination table build for buyside is complete. $counter_combinations analyzed, $combination_id combinations ($counter_records records) added.</b><br><br>";
flush();

# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlrule_potential=db_query("
	SELECT 	id, formula
	FROM 	ai_rule_potential_sellside
	ORDER BY id
	");

# Initialize variables
$counter_combinations = 1; # Number of Combinations created
$counter_records = 0; # Number of records inserted
$combination_id = 1; # Combination ID

# Set SQL Queries For Fetching
$numrowsrule_potential=db_num_rows($sqlrule_potential);

print "<b>Adding new combination records for sellside</b><br>";

# read ID from ai_rule_potential_sellside
$counter_rule_potential=0;
while ($counter_rule_potential<$numrowsrule_potential) {
	$id_rule_potential_array=db_fetch_array($sqlrule_potential);
	$id_rule_potential[$counter_rule_potential]=$id_rule_potential_array["id"];
	$formula_rule_potential[$counter_rule_potential]=$id_rule_potential_array["formula"];
	$counter_rule_potential++;
} # end while ($counter_rule_potential<$numrowsrule_potential)

# Build combinations
$counter_rule_potential=0;
while ($counter_rule_potential<$numrowsrule_potential) {
	$alldone=0;
	# clean array and set first full combination
	$c=0;
	while ($c<=$counter_rule_potential) {
		$tmparray[$c]=$c+1;
		$c++;
	} # end while ($c<$counter_rule_potential)
	$onedone=1;
	$c=$counter_rule_potential;
	# create combinations and write to db
	while (! $alldone) {
		if ($onedone) {
			# find useless combinations
			$f=0;
			$skip_rule=0;
			while ($f<=$counter_rule_potential and ! $skip_rule) {
				$g=$f+1;
				while ($g<=$counter_rule_potential and ! $skip_rule) {
					$formula1=explode(" ", $formula_rule_potential[$tmparray[$f]-1]);
					$formula2=explode(" ", $formula_rule_potential[$tmparray[$g]-1]);
					if ($formula1[0] == $formula2[0] and $formula1[2] == $formula2[2]) {
						$skip_rule=1;
					} # end if ($formula1[0] == $formula2[0] and $formula1[2] == $formula2[2])
					$g++;
				} # end while ($g<=$counter_rule_potential and ! $skip_rule)
				$f++;	
			} #end while ($f<=$counter_rule_potential and ! $skip_rule)
			if (! $skip_rule) {
				# write good combinations
				$d=0;
				while ($d<=$counter_rule_potential) {
					$e=$id_rule_potential[$tmparray[$d]-1];
					db_query("
					INSERT INTO ai_combination_sellside
						(id, rule_potential_id)
					VALUES
						($combination_id, $e)
					");
					$counter_records++;
					$d++;
				} # end while ($d<=$counter_rule_potential)
				$tmparray[$c]=$tmparray[$c]+1;
				$onedone=0;
				$combination_id++;
			} else {
				$d=0;
				while ($d<=$counter_rule_potential) {
					$d++;
				} # end while $d<=$counter_rule_potential)
				$skip_rule=0;
				$tmparray[$c]=$tmparray[$c]+1;
				$onedone=0;
			} # end if (! $skip_rule)
			if (!($counter_combinations % 1000)) {
				$counter_records--;
				$combination_id--;
				print "{$counter_combinations} analysed, {$combination_id} ({$counter_records} records) added.<br>";
				flush();
				$combination_id++;
				$counter_records++;
			} // end if (!($counter_combinations % 1000))
			$counter_combinations++;
		} elseif ($c<0) {
			$alldone=1;
		} elseif ($tmparray[$c] > $numrowsrule_potential-(($counter_rule_potential+1)-($c+1))) {
			$c=$c-1;
			$tmparray[$c]=$tmparray[$c]+1;
		} else {
			$d=$c;
			while ($d<$counter_rule_potential) {
				$tmparray[$d+1]=$tmparray[$d]+1;
				$d++;
			} # end while ($d<$counter_rule_potential)
			$onedone=1;
			$c=$counter_rule_potential;
		}
	} # end while (! $alldone)
	$counter_rule_potential++;
} # end while ($counter_rule_potential<=$numrowsrule_potential)
$combination_id--;
$counter_combinations--;
$counter_records--;

print "<b>The combination table build for sellside is complete. $counter_combinations analyzed, $combination_id combinations ($counter_records records) added.</b><br><br>";

print "<b>END OF STEP:  CREATE COMBINATIONS</b><br>";
print "</b></font>";


include_once("{$path}include/footer.php");

?>
