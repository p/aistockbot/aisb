<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	create_rule_potential.php
# Description:  This script clears the "ai_rule_potential" tables, then
#               recreates it using the "variable" and "operator" records.
#############################################################################
# 
# 2004/05/14  FS  Changed to store variable-ID and operator-ID too
# 2004/05/13  FS  Changed to create sellside too
# 2004/05/12  FS  Changed to use new functions for the database and 
#                 new folders
# 2004/04/01  FS  Fixed use of $global_database instead of fix dbname
# 2003/05/07  MS  Close database fix
# 2002/09/25  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
# set_time_limit (60000);

# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlvariable=db_query("
	SELECT 	ID, name
	FROM 	ai_variable
	WHERE	active=1
	ORDER BY name
	");

$sqloperator=db_query("
	SELECT	ID, operator
	FROM 	ai_operator
	WHERE	active=1
	");

# Initialize variables
$x = 0;

# Set SQL Queries For Fetching
$numrowsvariable=db_num_rows($sqlvariable);
while ($temp_array=db_fetch_array($sqlvariable)) {
	if (isset($temp_array["name"])) {
		$variable_array[]=$temp_array["name"];
		$variable_ID_array[]=$temp_array["ID"];
	}
}
$numrowsoperator=db_num_rows($sqloperator);
while ($temp_array=db_fetch_array($sqloperator)) {
	if (isset($temp_array["operator"])) {
		$operator_array[]=$temp_array["operator"];
		$operator_ID_array[]=$temp_array["ID"];
	}
}

print "<font face=arial size=-1>";
print "<b>STEP: REBUILD POTENTIAL RULES</b><br><br>";

print "<b>Delete all ai_rule_potential_buyside records</b><br>" ;

# Wipe out ai_rule_potential_buyside table
db_query("
	DELETE FROM ai_rule_potential_buyside
	");

print "<b>Delete all ai_rule_potential_sellside records</b><br>" ;

# Wipe out ai_rule_potential_sellside table
db_query("
	DELETE FROM ai_rule_potential_sellside
	");

print "<b>Adding new ai_rule_potential_buyside and ai_rule_potential_sellside records...</b><br>";

# Build left-hand side of equation
$variable_left_counter=0;
while ($variable_left_counter < $numrowsvariable) {
	# Build right-hand side of equation
	$variable_right_counter=0;
	while ($variable_right_counter < $numrowsvariable) {
		if ($variable_left_counter <> $variable_right_counter) {
			# Build operator in middle
			$operator_counter=0;
			while ($operator_counter < $numrowsoperator) {
				$formula = "$variable_array[$variable_left_counter] $operator_array[$operator_counter] $variable_array[$variable_right_counter]";
				# Write formula
				db_query("
				INSERT INTO ai_rule_potential_buyside
					(formula, variable_left, operator, variable_right)
				VALUES
					('$formula', {$variable_ID_array[$variable_left_counter]}, {$operator_ID_array[$operator_counter]}, {$variable_ID_array[$variable_right_counter]})
				");
				# Write formula sellside
				db_query("
				INSERT INTO ai_rule_potential_sellside
					(formula, variable_left, operator, variable_right)
				VALUES
					('$formula', {$variable_ID_array[$variable_left_counter]}, {$operator_ID_array[$operator_counter]}, {$variable_ID_array[$variable_right_counter]})
				");
				# Increment x
				$x++;
				$x++;
				$operator_counter++;
			} #end while ($operator_counter < $numrowsoperator)
		} // end if ($variable_left_counter <> $variable_right_counter)
		$variable_right_counter++;
	} #end while ($variable_right_counter < $numrowsvariable)
	$variable_left_counter++;
} # end while ($variable_left_counter < $numrowsvariable)

print "<b>The ai_rule_potential_buyside and ai_rule_potential_sellside table build is complete. $x records added.</b><br><br>";

//print "<b>To save time, you may delete any potential formula you wish by clicking the Button located to the left of the formula</b><br><br>";

print "<b>END OF STEP:  REBUILD POTENTIAL RULES</b><br>";

print "</b></font><br>";

include("{$path}modules/vangogh/delete_rule_potential_2.php");


include_once("{$path}include/footer.php");

?>
