<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	delete_rule_potential.php
# This program gives the buy/sell signal based on the best result.
#############################################################################
#
# 2004/05/13  FS  Changed to use sellside too
# 2004/05/12  FS  Changed to use new functions for the database
#                 and new folder
# 2002/11/03  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../../";
} // end if (empty($path))
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");

# Grab the URL and get the menus out of it
extract($_POST);
extract($_GET);
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

if (isset($ID)) {
	$sql=db_query("
		DELETE FROM	ai_rule_potential_buyside
		WHERE	ID	= $ID
		");
} elseif (isset($ID_sellside)) {
	$sql=db_query("
		DELETE FROM	ai_rule_potential_sellside
		WHERE	ID	= $ID_sellside
		");
}

HEADER("Location: {$path}modules/vangogh/delete_rule_potential_2.php?&$menu_string");

?>
