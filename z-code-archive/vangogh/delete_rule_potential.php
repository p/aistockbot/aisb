<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	delete_rule_potential.php
# This program gives the buy/sell signal based on the best result.
#############################################################################
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/01/17  MS  Added extract($_GET)
# 2003/05/26  MS  Added rule_potential_sellside
# 2003/11/03  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");

extract($_GET);

$sql=db_query("
	DELETE FROM	ai_rule_potential
	WHERE	ID	= $ID
	");

$sql=db_query("
	DELETE FROM	ai_rule_potential_sellside
	WHERE	ID	= $ID
	");

HEADER("Location: {$path}vangogh/delete_rule_potential_2.php");

?>
