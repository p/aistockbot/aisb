<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	create_rule_potential.php
# Description:  This script clears the "rule_potential" table, then
#               recreates it using the "variable" and "operator" records.
#############################################################################
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2003/05/26  MS  Added INSERT INTO rule_potential_sellside 
# 2003/05/07  MS  Close database fix
# 2002/09/25  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

# SET EXECUTION TIME TO 1,000 MINUTES (60,000 Seconds) or 16.67 hours
# set_time_limit (60000);

# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlvariable=db_query("
	SELECT 	name
	FROM 	ai_variable
	WHERE	active=1
	ORDER BY name
	");

$sqloperator=db_query("
	SELECT	operator
	FROM 	ai_operator
	WHERE	active=1
	");

# Initialize variables
$x = 0;

# Set SQL Queries For Fetching
$numrows=db_num_rows($sqlvariable);
$numrowsoperator=db_num_rows($sqloperator);

print "<font face=arial size=-1>";
print "<b>STEP: REBUILD POTENTIAL RULES</b><br><br>";

# Wipe out rule_potential table
db_query("
	DELETE FROM ai_rule_potential
	");
print "<b>Deleted all rule_potential records</b><br>" ;

# Wipe out rule_potential_sellside table
db_query("
	DELETE FROM ai_rule_potential_sellside
	");
print "<b>Deleted all rule_potential_sellside records</b><br>" ;

print "<b>Adding new rule_potential records...</b><br>";

# Build left-hand side of equation
for ($c=1; $c < $numrows; $c++) {
	$rowc=db_fetch_array($sqlvariable);
	# Build right-hand side of equation
	for ($d=$c+1; $d < $numrows+1; $d++) {
		$rowd=db_fetch_array($sqlvariable);
		# Build operator in middle
		for ($e=0; $e < $numrowsoperator; $e++) {
			$rowe=db_fetch_array($sqloperator);
			$namec=$rowc["name"];
			$named=$rowd["name"];
			$namee=$rowe["operator"];
			$formula = "$namec $namee $named";
			# Write formula
			db_query("
			INSERT INTO ai_rule_potential
				(formula)
			VALUES
				('$formula')
			");

			db_query("
			INSERT INTO ai_rule_potential_sellside
				(formula)
			VALUES
				('$formula')
			");

			# Get ID
			$sqlgetid=db_query("
				SELECT ID
				FROM ai_rule_potential 
				WHERE formula = '$formula'
				");
			$gettheid=db_fetch_array($sqlgetid);
			$ID=$gettheid["ID"];
			print " <a href=\"delete_rule_potential.php?ID=$ID\">-</a> " . $namec . " " . $namee . " " . $named . "<br>";
			# Increment x
			$x++;
		}
		# Reset operator back to 0 so I can loop again
		mysql_data_seek($sqloperator, 0);
	}
	# Reset left-hand side to current value so I can loop right-side again.
	mysql_data_seek($sqlvariable, $c);
}

print "<b>The rule_potential table build is complete. $x records added.</b><br><br>";

print "<b>To save time, you may delete any potential formula you wish by clicking the '-' located to the left of the formula</b><br><br>";

print "<b>END OF STEP:  REBUILD POTENTIAL RULES</b><br>";

print "</b></font>";

# Reset Variables
unset ($numrows);
unset ($c);
unset ($d);

unset ($sqlvariable);
unset ($sqloperator);

unset ($x);

unset ($numrows);
unset ($numrowsoperator);

unset ($rowc);
unset ($rowd);
unset ($rowe);

unset ($namec);
unset ($named);
unset ($namee);

unset ($formula);

include_once("{$path}include/footer.php");
?>
