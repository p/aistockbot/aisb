<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	create_combinations.php
# Description:  This script clears the "combinations" table, then
#               recreates it using the "rule_potential" records.
#############################################################################
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2002/11/06  MS  Fixed memory leaks
# 2002/11/01  MS  Corrected algorithm and optimized for shorter formulas first
# 2002/10/28  MS  Increased to 10 minutes 
# 2002/09/25  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

# SET EXECUTION TIME TO 20 MINUTES (1200 Seconds)
set_time_limit (1200);

# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlrule_potential=db_query("
	SELECT 	id
	FROM 	ai_rule_potential
	ORDER BY id
	");

# SECTION:  DEFINE MYSQL QUERY STATEMENTS
$sqlrule_potential_sellside=db_query("
	SELECT 	id
	FROM 	ai_rule_potential_sellside
	ORDER BY id
	");
$numrowsrule_potential_sellside=db_num_rows($sqlrule_potential_sellside);


# Initialize variables
$x = 0; # Number of records inserted
$y = 0; # Combination ID

# Set SQL Queries For Fetching
$numrowsrule_potential=db_num_rows($sqlrule_potential);

print "<font face=Arial size=2><b>STEP:  REBUILD COMBINATIONS</b><br><br>";

print "<b>Deleting all combinations records</b><br>";

# Wipe out combinations table
db_query("
	DELETE FROM ai_combinations
");
print "<b>Adding new combinations records</b><br>";

# $m is for rule_potential_sellside_id

# Build combinations
for ($m = 1; $m <= $numrowsrule_potential_sellside; $m++) {
	$z=$numrowsrule_potential;
	for ($z = $z; $z >= 1; $z--) {
		for ($c = 1; $c <= $z; $c++) {
			$y++;
			for ($d = $c; $d <= $c+$numrowsrule_potential-$z; $d++) {
				print $y . " " . $d . " " . $m . "<br>";
				# Write formula
				db_query("
				INSERT INTO ai_combinations
					(id, rule_potential_id, rule_potential_sellside_id)
				VALUES
					($y, $d, $m)
				");
				$x++;
			}
		}
	}
}

print "<b>The combinations table build is complete. $x records added.</b><br><br>";
print "<b>END OF STEP:  REBUILD COMBINATIONS</b><br>";
print "</b></font>";

# Flush Variables
unset($x);
unset($y);
unset($numrowsrule_potential);
unset($sqlrule_potential);
unset($c);
unset($z);
unset($d);

include_once("{$path}include/footer.php");
?>


