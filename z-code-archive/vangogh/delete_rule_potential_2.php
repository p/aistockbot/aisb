<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	delete_rule_potential_2.php
# This program gives the buy/sell signal based on the best result.
#############################################################################
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2002/11/03  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

$sqlgetrule_potential=db_query("
	SELECT ID, formula
	FROM	ai_rule_potential
	");

$numrows=db_num_rows($sqlgetrule_potential);

print "<font face=Arial size=-1><b>MODIFY POTENTIAL RULES</b><br>";
print "<i>You may delete additional rules by clicking the '-' located to the left of the formula.  When you are finished, move on to the next step.</i></font><br><br>";

for ($e=1; $e < $numrows+1; $e++) {
	$row=db_fetch_array($sqlgetrule_potential);
	$currentID=$row["ID"];
	$formula=$row["formula"];
	# Gotta keep ID in auto-incrementing state or other programs will fail
	if ($e != $currentID) {
		db_query("UPDATE ai_rule_potential SET ID=$e WHERE ID=$currentID");
		$currentID=$e;
	}
	print " <font face=Arial size=-1><a href=delete_rule_potential.php?ID=$currentID>-</a><b> " . $formula . "</b></font><br>";
}

include_once("{$path}include/footer.php");

?>
