<?php
#
# quantinetics/index.php
#
# 2004-07-15  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

print "<font face=arial size=+0>";

print "<b>AIStockBot Large-Scale Computing Clustered Server Project</b><br>";

print "This section will feature the large-scale computing arm of the AIStockBot project.<br>";
print "While our other bots can run on standalone machines, we hope to feature group experiments which require the use of multiple computers.<br>";
print "<br>";
print "Here are some of my thoughts on how it would work:<br>";
print "1.  User logins to node-0.2-02.quantinetics.com (or somewhere) and downloads list of experiments<br>";
print "2.  User selects an experiment, and downloads the criteria<br>";
print "3.  User downloads the required data<br>";
print "4.  User processes data (via a new bot) and uploads results<br>";
print "5.  User reviews experiment's progress<br>";

#print "<form method=post action=https://secure.quantinetics.com/somewhere>";
#print "<form method=post action=http://localhost/cvs_aistockbot/research/login_200.php>";
print "<form method=post action=index_2.php>";
print "<table width=100%>";

print "<tr>";
print "<td width=30% align=right><b>Quantinetics.com Login</b></td>";
print "<td width=70%><font size=-1> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[<a href=https://secure.quantinetics.com/somewhere>JOIN</a>]</font></td>";
print "</tr>";

print "<tr>";
print "<td width=30% align=right>Username:</td>";
print "<td width=70%><input type=text name=qu value=\"not ready yet\"></td>";
print "</tr>";

print "<tr>";
print "<td width=30% align=right>Password:</td>";
print "<td width=70%><input type=text name=qp value=\"not ready yet\"></td>";
print "</tr>";


print "<tr>";
print "<td width=30% align=right>Version:</td>";
print "<input type=hidden name=version value=".db_query_config("version").">";
print "<td width=70%>AIStockBot ".db_query_config("version")."</td>";
print "</tr>";

print "<tr>";
print "<td width=30% align=right> </td>";
print "<td width=70%><input type=submit value=Submit></td>";
print "</tr>";

print "</table>";

print "I'm not fully sure how this will all work yet, so I'd like to get some ideas.  - Vooch<br>";

print "</form>";

print "</font>";

include_once("{$path}include/footer.php");

?>