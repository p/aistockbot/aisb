<?php
#
# quantinetics/index_3.php
#
# 2004-07-15  MS  Initial Release
#
#
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}include/config.php");

# open database
$dbcnx=@mysql_connect("$global_hostname", "$global_username", "$global_password") or die ("Could not connect to $global_database");
@mysql_select_db("$global_database") or die (mysql_error());

#######################################
# SET UP AI_COMPANY TABLE
print "Deactivating all companies...";
$sql_company=mysql_query("
	UPDATE	ai_company
	SET	active=0
	");
print "done<br>";

$company=$_GET["company"];

# Replace the %2C (comma) with a real comma
$company=urldecode($company);

$array=explode(",",$company);
$size=count($array);
for ($i=0; $i < $size; $i++) {
 	print "Activating company: $array[$i]...";
	$sql_company=mysql_query("
		UPDATE	ai_company
		SET	active=1
		WHERE	ticker='$array[$i]'
	");
	print "done<br>";
}
print "<br>";
#######################################
# SET UP AI_OPERATOR TABLE
print "Deactivating all operators...";
$sql_operator=mysql_query("
	UPDATE	ai_operator
	SET	active=0
	");
print "done<br>";

$operator=$_GET["operator"];
# Gonna have a problem here, so I made the central server convert these to words (eg. GTE is >=, LT is <)
$operator=urldecode($operator);

$array="";
$array=explode(",",$operator);
$size=count($array);
for ($i=0; $i < $size; $i++) {
	$operator="$array[$i]";
	# I found a \n in the last item (FIXED IN index_2.php)
	# $operator=ereg_replace("\n","",$operator);
	#if ($array[$i]=='GTE') { $array[$i]=">="; }
	#if ($array[$i]=='LTE') { $array[$i]="<="; }
	#if ($array[$i]=='GT') { $array[$i]=">"; }
	#if ($array[$i]=='LT') { $array[$i]="<"; }
	#if ($array[$i]=='EQ') { $array[$i]="=="; }
	if ($operator=='GTE') { $operator=">="; }
	if ($operator=='LTE') { $operator="<="; }
	if ($operator=='GT') { $operator=">"; }
	if ($operator=='LT') { $operator="<"; }
	if ($operator=='EQ') { $operator="=="; }
 	print "Activating operator: $operator...";
	$sql_operator=mysql_query("
		UPDATE	ai_operator
		SET	active=1
		WHERE	operator='$operator'
	");
	print "done<br>";
}
print "<br>";
#######################################
# SET UP AI_VARIABLE TABLE
print "Deactivating all variables...";
$sql_variable=mysql_query("
	UPDATE	ai_variable
	SET	active=0
	");
print "done<br>";

$variable=$_GET["variable"];

# Replace the %2C (comma) with a real comma
$variable=urldecode($variable);

$array="";
$array=explode(",",$variable);
$size=count($array);
for ($i=0; $i < $size; $i++) {
 	print "Activating variable: $array[$i]...";
	$sql_variable=mysql_query("
		UPDATE	ai_variable
		SET	active=1
		WHERE	name='$array[$i]'
	");
	print "done<br>";
}
print "<br>";
#######################################

mysql_close($dbcnx);

include_once("{$path}include/footer.php");
?>