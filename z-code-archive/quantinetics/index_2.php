<?php
#
# quantinetics/index_2.php
#
# 2004-07-15  MS  Initial Release
#
# Connect to central server and downloads information on available experiments
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

print "<font face=arial size=+0>";

$qu=$_POST["qu"];
$qp=$_POST["qp"];
$version=$_POST["version"];

# Connect to central server
#
#           CHANGE THIS TO NODEBLAHBLAH.quantinetics.com sometime..........
#
$website="http://localhost/cvs_aistockbot/research/login_200.php?qu=$qu&qp=$qp&version=$version";
$lines = file($website);

print "<b>Large-scale Computing Experiments:</b><br>";
print "When choosing to participate in an experiment, we do the following:<br>";
print " - change the activation levels on your tables based on the experiment's requirements<br>";
print " - download required data<br>";
print " - make the calculations<br>";
print " - upload results<br>";
print "<hr>";
foreach ($lines as $line_num => $line) {
   	# echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
	if ($lines[$line_num] == "eor\n") {
		$id		= $lines[$line_num-8];
		$date_start	= $lines[$line_num-7];
		$date_end	= $lines[$line_num-6];
		$name		= $lines[$line_num-5];
		$description	= $lines[$line_num-4];
		$company	= $lines[$line_num-3];
		$operator	= $lines[$line_num-2];
		$variable	= $lines[$line_num-1];
		# "eor\n" 	= $lines[$line_num];  # end of record
		print "<font size=-1>Experiment</font> # <b>$id</b> - $name<br>";

		# Prep link
		$company=ereg_replace("\n","",$company);
		$operator=ereg_replace("\n","",$operator);
		$variable=ereg_replace("\n","",$variable);
		# Prep link some more
		$company=urlencode($company);
		$operator=ereg_replace(">=","GTE",$operator); # GTE must go first, otherwise it'll replace the >a
		$operator=ereg_replace("<=","LTE",$operator);
		$operator=ereg_replace(">","GT",$operator); # GT=Greater Than
		$operator=ereg_replace("<","LT",$operator);
		$operator=ereg_replace("==","EQ",$operator);
		$operator=urlencode($operator); # for the commas
		$variable=urlencode($variable);
		print "<font size=-1>Please click <a href=index_3.php?qu=$qu&qp=$qp&version=$version&company=$company&operator=$operator&variable=$variable&experiment_id=$id>here</a> to participate in this experiment<br></font>";
		$company=urldecode($company);
		$operator=urldecode($operator);
		$variable=urldecode($variable);

		print "$description<br>";
		print "<font size=-1>Dates:</font>	$date_start to $date_end<br>";
		print "<font size=-1>Companies:</font> $company<br>";
		print "<font size=-1>Operators:</font> $operator<br>";
		print "<font size=-1>Variables:</font> $variable<br>";		
		print "<hr>";
	}
}
print "</font>";
include("{$path}include/footer.php");
?>