<?php
# index.php
#
# 2004/04/27  MS  Fixed $global_sound
# 2004/04/23  FS  Use $global_sound
# 2004/04/10  MS  Placed Portfolio Management System (PMS) on main page and moved Activation Report to menu link
# 2003/05/06  MS  Convert html to 100% php
# 2003/04/10  MS  Added extract($_GET) function
# 2002/10/27  MS  Added variable +/- to turn on/off
# 

# to load database information and location of PEAR
include("include/config.php");

# for PEAR
#ini_set("include_path", $pear_location . PATH_SEPARATOR . ini_get("include_path"));
ini_set("include_path", $pear_location . ":" . ini_get("include_path"));
require_once("Auth/Auth.php");


function loginFunction()
{
  /* change html output
   */
  echo "<form method=\"post\" action=\"" . $_SERVER['PHP_SELF'] . "\">";
  echo "<input type=\"text\" name=\"username\">";
  echo "<input type=\"password\" name=\"password\">";
  echo "<input type=\"submit\">";
  echo "</form>";
}

$params = array(
        "dsn" => "mysql://$global_username:$global_password@$global_hostname/$global_database",
        "table" => "ai_auth",
        "usernamecol" => "username",
        "passwordcol" => "password"
        );

$a = new Auth("DB", $params, "$loginFunction");
$a->start();

if ($a->getAuth()) {

  if ($_GET['action'] == "logout") {
    $a->logout();
    echo "<html>";
    echo "<head>";
    echo "</head>";
    echo "<body>";
    echo "You have been logged out!<br>";
    echo "<a href=\"$PHP_SELF?action=login\">Login</a><br>";

  } else {

    include("include/header.php");

    extract($_GET);

    print "<table width=100%>";
    print "<tr>";
    print "<td width=100% background=images/hex.gif>";
    print "<font face=arial size=-1>";

    print "Welcome, " . $a->getUsername() ."!<br>";
    /*
    print "<br>";
    print "<b>Welcome to AIStockBot Version ".db_query_config("version")."</b><br>";
    print "<br>";
    print "We need help in designing a Portfolio Management System, finding a fundamental data source, writing additional industry-standard technical analysis formulas, and creating charts with our data.  If you can help on any of these things, please do not hesitate to contact me - vooch@Lawrenceburg.com.<br>";
    print "<br>";
    print "I'm looking for new ideas about Artificial Intelligence.  This program that you're seeing it a totally new way to look at the Stock Market and Artificial Intelligence.  Your ideas can be included with future releases.  We do not have to just use my ideas.<br>";
    print "<br>";
    print "Best Regards,<br>";
    print "- <i>Michael \"Vooch\" Salvucci</i><br>";
    print "</font>";
    print "<br>";
    */

# $myvar="Welcome , to , A I Stock Bot , version  ".db_query_config("version")." , My Name is Voo ooch , and I am the A I Stock Bot Project Administrator , A I Stock Bot uses Artificial Intelligence to pick Stock using Bots, im ready to work hard for you, you can either have me learn some new things, do some snazzy calculating, or throw me in the recycle bin, The Choice, Is Yours, Master.";
# Snazzy 2001:Space Odyssey line - LOL
$myvar="Hello , , Dave , Welcome , to , A I Stock Bot , version ".db_query_config("version");



# 2004/04/10: Let's switch from Activation Report to Portfolio Management System on the main page
# include("activation/activation_report.php");
include("pms/portfolio_output_1.php");


    #print "fullpath: $fullpath";
    print "<br>";
    print "<br>";

    if (isset($flag)) {
    } else {
	$myvar=str_replace(" ","foobarspacer",$myvar);
	# echo ("myvar is $myvar");

	# Enable this when I want sound and get php working again
	if (db_query_config("sound")) {
		exec ("./index.sh $myvar");

		# The <OBJECT> Tag is HTML4-compliant and used for Mozilla 1.1.  The other
		# tags will be deprecated.
		# The <EMBED> Tag works in Internet Explorer 4.0x, 5.0x, Navigator 3.0x, 4.0x 
		# The <BGSOUND> Tag works in Internet Explorer 3.0x (best used with the NOEMBED tag... see below)

		# THIS TAG IS NOT WORKING YET, BUT IT'S IN THE HTML 4 RECOMMENDATIONS
		# <OBJECT DATA="index.wav" TYPE="audio/x-wav" height=1 width=1>
		# <PARAM NAME=autostart VALUE=true>
		# <PARAM NAME=hidden VALUE=true>
		# </OBJECT>

		# <!--- Alternative method --->
		print "<EMBED SRC=index.wav AUTOSTART=TRUE LOOP=FALSE height=1 width=1 align=center>";
		print "<NOEMBED>";
		print "<BGSOUND SRC=index.wav LOOP=1>";
		print "</NOEMBED>";
		print "</EMBED>";
	} # end if (db_query_config("sound"))
    } # end in (isset($flag))

    print "<br>";
    print "<br>";

    include("include/footer.php");

    print "</td>";
    print "</tr>";
    print "</table>";

    }

} else {
  print "<center><a href=\"pms/signup_1.php\">JOIN</a><br>";
  print "<center>You must be logged in to view this site.</center>\n";
}

?>

