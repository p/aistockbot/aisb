<?php
#
# activation_operator.php
#
# This program updates the variable in the Activation Report
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/13  MS  Fixed path and header() function
# 2004/01/17  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/database.php");

# Extract data from a link
extract($_GET);

# Update Database
$sql=db_query("
	UPDATE	ai_operator
	SET	active 	= $active
	WHERE	ID	= $ID
	");

HEADER("Location: {$path}activation/activation_report.php?flag=1");
?>
