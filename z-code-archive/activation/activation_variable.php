<?php
#
# activation_variable.php
#
# This program updates the variable in the Activation Report
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.php
# 2004/04/13  MS  Fixed paths and header() function, redid database connection, and added db close
# 2003/04/11  MS  Added '?flag=1' so bot doesn't keep repeating speech
# 2003/04/04  MS  Extract data from a link
# 2003/10/26  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/database.php");

# Extract data from a link
extract($_GET);

# Update Database
$sql=db_query("
	UPDATE	ai_variable
	SET	active 	= $active
	WHERE	ID	= $ID
	");

HEADER("Location: {$path}activation/activation_report.php?flag=1");
?>
