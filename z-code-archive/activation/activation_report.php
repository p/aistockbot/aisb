<html>
<head>
<script LANGUAGE="JavaScript">
	<!-- Begin
		function toForm() {
			document.myform.ticker.focus();
			// Replace ticker with the field name of which you want to place the focus.
		}
	// End -->
</SCRIPT>
</head>
<body onLoad=toForm()>
<?php
# activation_report.php
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/13  MS  Fixed link errors
# 2002/11/06  MS  Split away from index.php
# 

if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/header.php");

print "<table width=100% border=1>";
print "<tr>";
print "<td width=99% colspan=3 align=center>";
print "<font face=Arial size=+0>";
print "<b>A C T I V A T I O N &nbsp; &nbsp; &nbsp; R E P O R T</b><br>";
print "<i>Select the companies, operators, and variables you wish to analyze, then click STEP 1 on one of the bots.</i><br>";
print "</font>";
print "<font face=Arial size=-1>";
print "<font color=006600>Green=Activated</font> and <font color=FF0000>Red=Inactive</font><br>";
print "</font>";
print "</td>";
print "</tr>";
print "<tr>";
print "<td width=38%><font size=+0><b>Companies</b></font></td>";
print "<td width=23%><font size=+0><b>Operators</b> - <i>(used for Van Gogh Bot only)</i></font></td>";
print "<td width=38%><font size=+0><b>Variables</b> - <i>(used for Van Gogh Bot only)</i></font></td>";
print "</tr>";
print "<tr>";
print "<td width=38% valign=top>";


print "<font size=-1>";
print "<form name=myform action={$path}/activation/activation_company.php method=post>";
print "Ticker:&nbsp;&nbsp";
	print "<input type=text name=ticker size=6>";
	print "<input type=hidden name=active value=1>";
	print "<input type=submit name=submit value=Activate><br>";
print "</form>";
#print "<br>";
#print "<form action=update_company_active.php method=post>";
#print "Ticker:&nbsp;&nbsp;";
#	print "<input type=text name=ticker size=6>";
#	print "<input type=hidden name=active value=0>";
#	print "<input type=submit name=submit value=Deactivate>";
#print "</form>";
print "</font>";


$sqlcompany=db_query("
	SELECT ticker, name, active 
	FROM ai_company
	WHERE active=1 
	ORDER BY ticker");
while ($rowsqlcompany=db_fetch_array($sqlcompany)) {
	$ticker=$rowsqlcompany['ticker'];
	$name=$rowsqlcompany['name'];
	$active=$rowsqlcompany['active'];
	if ($active == 1) {
		print "<font color=006600 face=arial size=-2>";
		# reactivated
		$alphabetical=substr($ticker, 0, 1);
		$website="http://biz.yahoo.com/p/$alphabetical/$ticker.html";
		print "<a href={$path}activation/activation_company.php?ticker=$ticker&active=0>-</a>$ticker - $name [<a href=$website>Profile</a>]<br>";
	} else {
		# Do nothing because there's too many companies
		print "<font color=FF0000 face=Arial size=-2>";
	}
	print "</font>";
}

print "</td>";
print "<td width=23% valign=top>";

$sqloperator=db_query("
	SELECT ID, operator, description, active
	FROM ai_operator");
while ($rowsqloperator=db_fetch_array($sqloperator)) {
	$ID	= $rowsqloperator['ID'];
	$operator=$rowsqloperator['operator'];
	$description=$rowsqloperator['description'];
	$active=$rowsqloperator['active'];
	if ($active == 1) {
		# Active operator is on (1), so ask to turn it off (0)
		print "<font color=006600 face=arial size=-2>";
		print "<a href={$path}activation/activation_operator.php?ID=$ID&active=0>-</a>";
	} else {
		# Active operator is off (0), so ask to turn it on (1)
		print "<font color=FF0000 face=Arial size=-2>";
		print "<a href={$path}activation/activation_operator.php?ID=$ID&active=1>+</a>";
	}
	print "$operator $description<br>";
	print "</font>";
}
print "</td>";
print "<td width=38% valign=top>";
$sqlvariable=db_query("
	SELECT id, name, active
	FROM ai_variable
	ORDER BY name");
while ($rowsqlvariable=db_fetch_array($sqlvariable)) {
	$name=$rowsqlvariable['name'];
	$active=$rowsqlvariable['active'];
	$id=$rowsqlvariable['id'];
	if ($active == 1) {
		# Active variable is on (1), so ask to turn it off (0)
		print "<font color=006600 face=arial size=-2>";
		print "<a href={$path}activation/activation_variable.php?ID=$id&active=0>-</a>";
	} else {
		# Active variable is off (0), so ask to turn it on (1)
		print "<font color=FF0000 face=Arial size=-2>";
		print "<a href={$path}activation/activation_variable.php?ID=$id&active=1>+</a>";
	}
	print "$name<br>";
	print "</font>";
}

# Flush Variables
unset ($sqlcompany);
unset ($rowsqlcompany);
unset ($ticker);
unset ($active);
unset ($sqloperator);
unset ($rowsqloperator);
unset ($operator);
unset ($description);
unset ($sqlvariable);
unset ($rowsqlvariable);
unset ($name);
unset ($id);

print "</td></tr></table><br><br>";

include_once("{$path}include/footer.php");
?>
</html>
