<?php
# portfolio_output_2.php
#
# 20061112  MS  Changed length of graph based on number of securities
# 20040509  FS  Changed include mechanism and $path variable
# 20040506  FS  Changed to use functions from include/database.inc
# 20030521  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");
include("{$path}jpgraph-1.12.1/src/jpgraph.php");
include("{$path}jpgraph-1.12.1/src/jpgraph_pie.php");
include("{$path}jpgraph-1.12.1/src/jpgraph_pie3d.php");

extract($_GET);  # Grab the $account_id if needed

if ($account_id!="") {
	$sql_portfolio=db_query("
		SELECT		ticker, SUM(shares) AS total_shares, SUM(value) AS total_value
		FROM		ai_portfolio
		WHERE		sell_date = '0000-00-00 00:00:00'
		AND			account_id = '$account_id'
		AND			username = '$username'		
		GROUP BY	ticker
		ORDER BY	ticker
		");
		# WHERE		sell_cost_basis = 0 # dropped because of expired options - using sell_date instead
} else {
	$sql_portfolio=db_query("
		SELECT		ticker, SUM(shares) AS total_shares, SUM(value) AS total_value
		FROM		ai_portfolio
		WHERE		sell_date = '0000-00-00 00:00:00'
		AND			username = '$username'	
		GROUP BY	ticker
		ORDER BY	ticker
		");
		# WHERE		sell_cost_basis = 0 # dropped because of expired options - using sell_date instead
}

$numrows=db_num_rows($sql_portfolio);
$x=0;

while($row_portfolio = db_fetch_array($sql_portfolio)) {
	$ticker[$x]	= $row_portfolio["ticker"];
	$data[$x]	= (int)$row_portfolio['total_value'];
	$x=$x+1;
}

// Some data
#$data = array(20,27,45,75,90);

// Create the Pie Graph.
# $graph = new PieGraph(550,400,"auto");
# $graph = new PieGraph(275,200,"auto");
# $graph = new PieGraph(350,600,"auto");
# 15 * 33 stocks + 200 = this changes the length of the graph based upon number of securities
# $graph = new PieGraph(500,(15*$numrows)+250,"auto");
$graph = new PieGraph(500,500,"auto");
$graph->SetShadow();

// Set A title for the plot
if($account_id!="") {
	$graph->title->Set("$account_id Portfolio");
} else {
	$graph->title->Set("Consolidated Portfolio");
}
$graph->title->SetFont(FF_VERDANA,FS_BOLD,18); 
$graph->title->SetColor("darkblue");
$graph->legend->Pos(0.6,0.1);

// Create 3D pie plot
$p1 = new PiePlot3d($data);
$p1->SetTheme("goldenegg");  # pie colors:  earth, pastel, water, sand
$p1->SetCenter(0.5);
$p1->SetSize(200);

// Adjust projection angle
$p1->SetAngle(90);

// Adjsut angle for first slice
$p1->SetStartAngle(45);

// Display the slice values
#$p1->value->SetFont(FF_ARIAL,FS_BOLD,9);
$p1->value->SetFont(FF_ARIAL,FS_NORMAL,7);
$p1->value->SetColor("darkred");

// Add colored edges to the 3D pie
// NOTE: You can't have exploded slices with edges!
$p1->SetEdge("navy");

// explode pie
$p1->ExplodeAll(0);

# Comment this out if you want percentages to appear and turn on SetLegends() below also.
$p1->SetLabels($ticker,"auto");
#$p1->SetLegends(array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct"));
#$p1->SetLegends($ticker);


# Maybe we need a newer version of jpgraph to do logos (Ref. http://www.aditus.nu/jpgraph/features_formatting.php )
#$icon = new IconPlot('images/logo.png',0.02,0.02,1,30);
#$icon->SetAnchor('left','top');
#$graph->Add($icon);


$graph->Add($p1);
$graph->Stroke();
?>
