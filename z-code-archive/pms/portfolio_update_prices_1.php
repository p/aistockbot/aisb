<?php
#
# pms/portfolio_update_prices_1.php
#
# 2007/09/13  MS  Yahoo is fucking with the data stream, so I had to create a workaround
# 2007/01/29  MS  Modified for Portfolio Management,"Update Prices" so I would only grab 1 distinct ticker at a time
#                 from yahoo.com and not overload them with multiple requests
#                 Also set max_execution_time=300 in php.ini for 29 stocks for my purposes.  Yours might differ.
# 2005/01/20  MS  Initial Release - taken from quotes/yahoo_delayed_quote_2.php
#
if (empty($path)) {
        $path="../";
} // end if (!$path)
include_once("{$path}include/header.php");
include_once("{$path}include/database.php");

# 2004/07/16:  New Feature:  Update Portfolio values.
# Useful for Portfolio Management System's "Current Holdings - Detail",
# but only if there's no data for the stock in ai_history - I'll have to improve that later.
if(!isset($username)) { $username=""; }
if($username<>"") {
	$sql_portfolio3=db_query("
		SELECT		ticker, shares
		FROM		ai_portfolio
		WHERE		sell_cost_basis = 0
		AND		ticker 		<> 'ACT_CASH'
		AND		username 	= '$username'
		AND		sell_date 	= '0000-00-00 00:00:00'
		ORDER BY	ticker
		");

	$last_ticker = "";
	while ($row_portfolio3 = db_fetch_array($sql_portfolio3)) {
		$ticker		= $row_portfolio3["ticker"];
		$shares		= $row_portfolio3["shares"];

		if($ticker != "$last_ticker") {
			$fp = fopen ("http://finance.yahoo.com/d/quotes.csv?s=$ticker&f=sl1d1t1c1ohgv&e=.csv","r");
			$data = fgetcsv ($fp, 1000, ",");
			#$t1 = microtime();
			sleep(2);
			#$t2 = microtime();
			#print "Updating pricing for:  ticker=$ticker t1=$t1 t2=$t2<br> ";
			print "Updating pricing for:  ticker=$ticker<br> ";

			if($data[1] == "0.00") {
				$fp = fopen ("http://finance.yahoo.com/d/quotes.csv?s=$ticker&f=sl1d1t1c1ohgv&e=.csv","r");
				$data = fgetcsv ($fp, 1000, ",");
				sleep(2);
				if($data[1] == "0.00") {
					$fp = fopen ("http://finance.yahoo.com/d/quotes.csv?s=$ticker&f=sl1d1t1c1ohgv&e=.csv","r");
					$data = fgetcsv ($fp, 1000, ",");
					sleep(2);
					if($data[1] == "0.00") {
						# Alternate Method
						$handle = fopen("http://finance.yahoo.com/q?s=$ticker", "r");
						$contents = '';
						while (!feof($handle)) {
							$contents .= fread($handle, 8192);
						}
						fclose($handle);
						$pos = strpos($contents, 'Last Trade');

						$contents = substr("$contents", $pos, 100);
						
						$pos = strpos($contents, '</tr>');
						$contents = substr($contents, 0, $pos);
						
						$contents = str_replace("Last Trade:", "", $contents);
						
						$contents = strip_tags($contents);
						$data[1] = $contents; # price
						
						#print "contents=$contents";
						#exit();
						
						if(!is_numeric($contents)) {
							print "Found a 0.00 with $ticker<br>";
						}
					}
				}
			}

		}
		$last_ticker = "$ticker";  # set so I don't keep bombing yahoo.com with the same ticker

		$value 		= $shares * $data[1];
		# print "value=$value ::: ticker=$ticker ::: shares=$shares ::: price=$data[1] ::: username=$username<br>";
		$sql=db_query("
			UPDATE 	ai_portfolio 
			SET 	value 		= $value
			WHERE 	ticker		= '$ticker' 
			AND 	shares		= $shares
			AND	username 	= '$username'
			AND	sell_date 	= '0000-00-00 00:00:00'
			");
	}
	print "You should now have current prices.  Click on \"Current Holdings - Detail\" to see the updated prices in your portfolio.<br><br>";
	print "<b>PLEASE NOTE:</b>  If you click to view your portfolio, these current prices will be erased and you will not be able to see them in \"Current Holdings - Detail\" again until you repeat the \"Update Prices\" step.  We do this to avoid mingling end-of-day data with intraday data.<br><br>";
	print "When I want to look at \"Current Holdings - Detail\", I'll click on \"Update Prices\" first, and then \"Current Holdings - Detail\" second.<br><br>";
	print "If you would rather update end-of-day pricing, use Steps 0,1,2 of the Picasso bot.<br><br>";
}
include_once("{$path}include/footer.php");
?>