<?php

#
# transaction_output_1.php
#
# 2004/07/17  MS  Added PHPSESSID.  Commented out $notes because it's better when used with "Current Holdings - Detail" of PMS
# 2004/07/17  MS  Make $notes for easier viewing
# 2004/07/13  MS  Added Filter by Ticker
# 2004/07/12  MS  Added test for $username
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/16  MS  Adding ability to delete from transaction and portfolio table
# 2004/04/15  MS  Initial Release
#
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_GET);

# $start_date and $end_date should appear in the URL, but if it doesn't use this:
if ( !isset($start_date) OR !isset($end_date) ) {
	$start_date=date("Y");
	$start_date="$start_date"."-01-01";
	$end_date=date("Y-m-d");
}

if(!isset($_GET["filter_by_account_id"])) {
	$filter_by_account_id = "";
} else {
	$filter_by_account_id = $_GET["filter_by_account_id"];
}

if(!isset($_GET["filter_by_action"])) {
	$filter_by_action = "";
} else {
	$filter_by_action = $_GET["filter_by_action"];
}

if(!isset($_GET['PHPSESSID'])) { $_GET['PHPSESSID']=""; }
print "<form action={$path}pms/transaction_output_1.php>";
print "<font face=arial size=+0>";
print "<b>Transaction Report<br></b>";
print "</font>";
print "<font face=arial size=-1>";
print "<input type=hidden name=PHPSESSID value=$_GET[PHPSESSID]>";
print "From: <input type=text name=start_date value=$start_date> ";
print " &nbsp; To: <input type=text name=end_date value=$end_date> ";
print "<br>";
print "Filter by Ticker: <input type=text name=ticker><br>";

print "Filter by Action: ";
print "<select name=filter_by_action>";
print "<option value=></option>";
print "<option value=Bought>Bought</option>";
print "<option value=Sold>Sold</option>";
print "<option value=Deposit>Deposit</option>";
print "<option value=Withdrawl>Withdrawl</option>";
print "<option value=\"Interest Income\">Interest Income</option>";
print "<option value=\"Cash Dividend\">Cash Dividend</option>";
print "<option value=\"Option Order Rebate\">Option Order Rebate</option>";
print "<option value=\"Margin Fee\">Margin Fee</option>";
print "<option value=\"Miscellaneous Fee\">Miscellaneous Fee</option>";
print "<option value=\"Stock Split\">Stock Split</option>";
print "</select><br>";

print "Filter by Account: ";
print "<select name=filter_by_account_id> ";
print "<option value=></option>";
$string="
        SELECT  	account_id AS temp
        FROM    	ai_account
        WHERE		username='$username'
	";
$sql_account=db_query("$string");
while ($row_account = db_fetch_array($sql_account) ) {
	$temp	= $row_account["temp"];
	print "<option value=$temp ";
	if($temp==$filter_by_account_id) { print " SELECTED "; }
	print " >$temp</option>";
}
print "</select><br>";

print "<input type=submit value=Submit><br><br>";
print "</form>";

# TABLE: TRANSACTION
# account_id   	varchar(255)	
# date  	date
# time  	time
# action  	varchar(10)
# ticker  	varchar(10)
# shares  	decimal(15,6)
# price  	decimal(15,6)
# commission  	decimal(15,6)
# net_change  	decimal(15,6)
# notes  	longtext

if(!isset($ticker)) {
	# Do nothing
} else {
	# Gotta wipe it out if user doesn't want to Filter by ticker and click that "Submit" button
	if($ticker=="") { unset($ticker); }
}

if(!isset($ticker)) {
	# No filter - just dates
	$string="
		SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
		FROM		ai_transaction
		WHERE		date >= '$start_date'
		AND		date <= '$end_date'
		AND		username='$username'
		";

	if($filter_by_account_id != "") {
		$string .= " AND	account_id='$filter_by_account_id' ";
	}

	if($filter_by_action != "") {
		$string .= " AND	action='$filter_by_action' ";
	}

	$string .= "
		ORDER BY	date DESC, time DESC
		";
	$sql_transaction=db_query("$string");

} else {
	# Filter based on ticker symbol
	$string = "
		SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
		FROM		ai_transaction
		WHERE		date >= '$start_date'
		AND		date <= '$end_date'
		AND		username='$username'
		";

	if($filter_by_account_id != "") {
		$string .= " AND	account_id='$filter_by_account_id' ";
	}

	if($filter_by_action != "") {
		$string .= " AND	action='$filter_by_action' ";
	}

	$string .= "
		AND		ticker = '$ticker'
		ORDER BY	date DESC, time DESC
		";

	$sql_transaction=db_query("$string");

}

# Print Headers
print "<table width=100% border=1>";
print "<tr>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Account ID</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Date/Time</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Action</b></td>";

print "<td width=11% bgcolor=abcdef align=center>";
print "<font face=arial size=-1>";
print "<b>Ticker</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Shares</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Price</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Commission</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Net Change</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b> &nbsp; &nbsp;</b></td>";
print "</tr>";

$i=1; # row counter

while ($row_transaction = db_fetch_array($sql_transaction)) {
	$account_id	= $row_transaction["account_id"];
	$date		= $row_transaction["date"];
	$time		= $row_transaction["time"];
	$action		= $row_transaction["action"];
	$ticker		= $row_transaction["ticker"];
	$shares		= $row_transaction["shares"];
	$price		= $row_transaction["price"];
	$commission	= $row_transaction["commission"];
	$net_change	= $row_transaction["net_change"];
	$notes		= $row_transaction["notes"];

	# Print Detail
	$i++;
	if ($i % 2) { 
      		echo "<tr bgcolor=ffffff>"; 
	} else { 
		echo "<tr bgcolor=eeeeee>"; 
	} 

	print "<td width=11% valign=top>";
	print "<font face=arial size=-1>";
	print " $account_id";
	print "</td>";

	print "<td width=11%>";
	print "<font face=arial size=-2>";
	print "$date $time</td>";

	print "<td width=11%>";
	print "<font face=arial size=-1>";
	print "$action</td>";

	print "<td width=11% align=center>";
	print "<font face=arial size=-1>";
	if ($ticker!="") {
		print "$ticker</td>";
	} else {
		print "&nbsp;";
	}

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$shares</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$$price</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$$commission</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$$net_change</td>";


	print "<td width=11% align=center>";
	print "<font face=arial size=-1>";
	$query_string=$_SERVER["QUERY_STRING"];
	print "<form action={$path}pms/transaction_delete_1.php?$query_string method=post>";
	print "<input type=hidden name=username value=\"$username\">";
	print "<input type=hidden name=account_id value=\"$account_id\">";
	print "<input type=hidden name=date value=$date>";
	print "<input type=hidden name=time value=$time>";
	print "<input type=hidden name=action value=\"$action\">";
	print "<input type=hidden name=ticker value=$ticker>";
	print "<input type=hidden name=shares value=$shares>";
	print "<input type=hidden name=price value=$price>";
	print "<input type=hidden name=commission value=$commission>";
	print "<input type=hidden name=net_change value=$net_change>";
	print "<input type=submit value=Delete>";
	print "</td>";
	# This </form> was placed at the end of the row to tighten the amount of space used.
	# Otherwise, we would have some fat rows
	print "</form>"; 

	# End of row:
	print "</tr>";




# 2004/07/18  MS  I took this out because this feature is now available in "Current Holdings - Detail".  However, I am
#		  keeping it here just in case you want to see it.
#	# Extra row for notes:
#	if (strlen($notes)>0) {
#		print "<font face=arial size=-2>";
#		if ($i % 2) { 
#     			echo "<tr bgcolor=ffffff>"; 
#		} else { 
#			echo "<tr bgcolor=eeeeee>"; 
#		} 
#		print "<td colspan=10>";
#		print "<font face=arial size=-2>";
#		print "$notes";
#		print "</td>";
#		print "</tr>";
#	}
  

}
print "</table>";

include_once("{$path}include/footer.php");
?>

