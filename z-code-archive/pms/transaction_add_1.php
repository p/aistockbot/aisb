<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	transaction_add_1.php
# Authors:	Chris Ptacek and Michael Salvucci
#############################################################################
# Future Plans?:If someone wants to write a javascript Company Name lookup
#		and add a new field for the company name which changes
#		when the ticker symbol changes, that would be great.
#############################################################################
#
# 2005/02/11  MS  Corrected "Margin" to read as "Margin Fee"
# 2004/07/13  MS  Added "Stock Split" feature
# 2004/07/13  MS  Checks to make sure user is logged in first
# 2004/07/12  MS  Added test for $username in $get_account sql query
# 2004/05/14  MS  Added PHPSESSID and $menu_string to form
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use settings from ai_config
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/16  MS  Reversed shares and ticker columns
# 2004/04/09  MS  Added $path
# 2003/05/14  MS  Changed "Wire" to "Deposit".  Added "Withdrawl"
#                 account_id needed </select>
# 2003/05/09  CP&MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add a transaction.<br>";
	exit();
}

# Get Distinct Account Numbers
$get_account=db_query("
	SELECT DISTINCT account_id
	FROM		ai_account
	WHERE		username='$username'
	ORDER BY 	account_id
	");

include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

# Print Form
print "<form action={$path}pms/transaction_add_2.php?$menu_string method=post>";
print "<input type=hidden name=PHPSESSID value=$PHPSESSID>";

print "<font size=-2>";
print "<font face=arial size=+0><b>Add New Transaction</b></font><br>";
print "This form is meant to provide a historical transaction of your trade and to adjust your portfolio.  This form is NOT meant for placing an order with an online broker - check the main page to do that.  Providing notes on each trades gives you powerful insight on what you were thinking at the time you placed your trade.<br><br>";

print "<i>Special Notes About This Form:</i><br>";
print "ALL OF THESE REQUIRE AN ACCOUNT AND THE DATE/TIME<br>";
print "Bought - Skip the 'Total' field - it will auto-calculate it after you enter shares, price, and commissions<br>";
print "Sold - Skip the 'Total' field - it will auto-calculate it after you enter shares, price, and commissions<br>";
print "Deposit - Select Deposit from drop-down box and enter amount in 'Total' field<br>";
print "Withdrawl - Select Withdrawl from drop-down box and enter amount in 'Total' field<br>";
print "Interest Income - Enter amount in 'Total' field<br>";
print "Cash Dividend - Uses the <b>Account ID</b>, <b>Date/Time</b>, <b>Action</b>, <b>Ticker</b>, and <b>Total</b> fields<br>";
print "Option Order Rebate - Enter amount in 'Total' field<br>";
print "Margin - Enter amount in 'Total' field<br>";
print "Miscellaneous Fee - Enter amount in 'Total' field<br>";
print "Stock Split - Uses the Date/Time and Ticker Symbol only (You will process this on the next page).<br>";
print "</font>";
print "<br>";

# Print Column Headings
print "<table width=100% background={$path}images/hex.gif>";
print "<tr>";
print "<td width=10% bgcolor=ccccff><font size=-1>Account ID</td>";
print "<td width=10% bgcolor=ccccff><font size=-1>Date/Time</td>";
#print "<td width=10%><font size=-1>Time</td>"; # combined with date above
print "<td width=10% bgcolor=ccccff><font size=-1>Action</td>";
print "<td width=10% bgcolor=ccccff><font size=-1>Ticker</td>";
print "<td width=9% bgcolor=ccccff><font size=-1>Shares</td>";
print "<td width=11% bgcolor=ccccff><font size=-1>Share Price</td>";
print "<td width=10% bgcolor=ccccff><font size=-1>Commission</td>";
print "<td width=10% bgcolor=ccccff><font size=-1>Total</td>"; 	# This should be called "Net Change", but
								# "Total" is easier for the general public
								# to understand.
#print "<td width=10%><font size=-1>Notes</td>";  # used below instead
print "</tr>";

# Form Field:  Account ID
if(!isset($_GET["account_id"])) {
	$x_account_id = "";
} else {
	$x_account_id = $_GET["account_id"];   # Grab the Account ID from the URL if it was passed from the previous form
}
print "<tr>";
print "<td width=10% valign=top>";
print "<select name=account_id>";
while ($row_account=db_fetch_array($get_account)) {
	$account_id=$row_account['account_id'];
	print "<option ";
	if($x_account_id==$account_id) { print " SELECTED "; }
	print " value=$account_id>$account_id</option>";
}
print "</select>";
print "</td>";

# Form Field:  Date
if(!isset($_GET["date"])) {
	$date=date("Y-m-d");
} else {
	$date = $_GET["date"];   # Grab the date from the URL if it was passed from the previous form
}
print "<td width=10% valign=top>";
print "<input type=text name=date size=10 value=$date><br>";
#print "</td>";

# Form Field:  Time
#print "<td width=10%>";
print "<input type=text name=time size=10 value=00:00:00>";
print "</td>";

# Form Field:  Action
print "<td width=10% valign=top>";
print "<select name=action>";
print "<option>Bought</option>";
print "<option>Sold</option>";
print "<option>Deposit</option>";
print "<option>Withdrawl</option>";
print "<option>Interest Income</option>";
print "<option>Cash Dividend</option>";
print "<option>Option Order Rebate</option>";
print "<option>Margin Fee</option>";
print "<option>Miscellaneous Fee</option>";
print "<option>Stock Split</option>";
print "</select>";
print "</td>";

# Form Field:  Ticker
print "<td width=9% valign=top>";
print "<input type=text name=ticker size=5>";
#	# Get Distinct Tickers
#	$get_company=db_query("
#		SELECT DISTINCT ticker
#		FROM		ai_company
#		ORDER BY	ticker
#		");
#	print "<select name=ticker>";
#	print "<option></option>";
#	while ($row_company=db_fetch_array($get_company)) {
#		$ticker=$row_company['ticker'];
#		print "<option>$ticker</option>";
#	}
print "</td>";

# Form Field:  Shares
print "<td width=10% valign=top>";
print "<input type=text name=shares size=8 align=right>";
print "</td>";

# Form Field:  Price
print "<td width=11% valign=top><font face=arial size=-1>";
print db_query_config("currency")."<input type=text name=price size=8 align=right>";
print "</td>";

# Form Field:  Commission
print "<td width=10% valign=top><font face=arial size=-1>";
print db_query_config("currency")."<input type=text name=commission size=6 align=right>";
print "</td>";

# Form Field:  Net Change
print "<td width=10% valign=top><font face=arial size=-1>";
print db_query_config("currency")."<input type=text name=net_change size=6 align=right>";
print "</td>";

# End of Row
print "</tr>";


# Form Field:  Notes
print "<tr>";
print "<td colspan=8 width=100% valign=top bgcolor=ccccff><font face=arial size=-1>";
print "Notes <i>(This is a great opportunity to describe your reasons for getting IN or OUT of a particular security):</i><br>";
print "</td>";
print "</tr>";

# Notes continued
print "<tr>";
print "<td colspan=8 width=100% valign=top><font face=arial size=+3>";
print "<textarea name=notes rows=7 cols=80></textarea><br>";
print "<input type=submit value=Submit>";
print "</td>";
print "</tr>";

# End of Table
print "</table>";

# End of Form
print "</form>";

print "<br><br><br><br><br><br>";

include_once("{$path}include/footer.php");

?>

