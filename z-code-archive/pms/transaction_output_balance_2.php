<?php

#
# transaction_output_balance_1.php
#
# I used transaction_output_1.php to get started
#
# 2006/03/11  MS  Initial Release
#
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))


include_once("{$path}include/database.php");
include_once("{$path}jpgraph-1.12.1/src/jpgraph.php");
include_once("{$path}jpgraph-1.12.1/src/jpgraph_line.php");
include_once("{$path}jpgraph-1.12.1/src/jpgraph_log.php");


include_once("{$path}include/header_invisible.php");

extract($_GET);

# $start_date and $end_date should appear in the URL, but if it doesn't use this:
if ( !isset($start_date) OR !isset($end_date) ) {
	$start_date=date("Y");
	$start_date="$start_date"."-01-01";
	$end_date=date("Y-m-d");
}

if(!isset($_GET['PHPSESSID'])) { $_GET['PHPSESSID']=""; }

if(isset($_GET["starting_balance"])) {
	$starting_balance = $_GET["starting_balance"];
} else {
	$starting_balance = 0; # default
}





# TABLE: TRANSACTION
# account_id   	varchar(255)	
# date  	date
# time  	time
# action  	varchar(10)
# ticker  	varchar(10)
# shares  	decimal(15,6)
# price  	decimal(15,6)
# commission  	decimal(15,6)
# net_change  	decimal(15,6)
# notes  	longtext

if(!isset($ticker)) {
	# No filter - just dates
	$string="
		SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
		FROM		ai_transaction
		WHERE		date >= '$start_date'
		AND		date <= '$end_date'
		AND		username='$username'
		";

	if (isset($_GET["account_id"])) { 
		$string = $string . " AND	account_id = '$_GET[account_id]' ";
	}

	$string = $string . " ORDER BY	date ASC, time ASC";

	$sql_transaction=db_query("$string");
} else {
	# Filter based on ticker symbol
	$string="
		SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
		FROM		ai_transaction
		WHERE		date >= '$start_date'
		AND		date <= '$end_date'
		AND		username='$username'
		AND		ticker = '$ticker'
		";

	if (isset($_GET["account_id"])) { 
		$string = $string . " AND	account_id = '$_GET[account_id]' ";
	}

	$string = $string . " ORDER BY	date ASC, time ASC";

	$sql_transaction=db_query("$string");
}


$dividends_paid = 0; # default
$interest_income = 0 ; # default
$securities_bought = 0 ; # default
$securities_sold = 0 ; # default
$miscellaneous_fee = 0 ; # default
$balance=$starting_balance;

$cpt=0; # for graph

while ($row_transaction = db_fetch_array($sql_transaction)) {
	$account_id	= $row_transaction["account_id"];
	$date		= $row_transaction["date"];
	$time		= $row_transaction["time"];
	$action		= $row_transaction["action"];
	$ticker		= $row_transaction["ticker"];
	$shares		= $row_transaction["shares"];
	$price		= $row_transaction["price"];
	$commission	= $row_transaction["commission"];
	$net_change	= $row_transaction["net_change"];
	$notes		= $row_transaction["notes"];

	if($action!="Miscellaneous Fee") {
		$balance = $balance + $net_change;
	} else {
		$balance = $balance - $net_change;
	}
	

#  graph stuff:
	$xdata[$cpt]=$date;
	$ydata[$cpt]=$balance; # y-axis (closing price)
	# $y2data[$cpt]=(int)$x; # Example of 2nd y-axis
	$cpt++; # Normal method

}







# In order to get the last 1000 records, I have to sort by date DESC
# Then, I need to reverse the process and count downwards by changing
# $cpt to be numrows, and subtract $cpt instead of $cpt++;
# NOTE: JpGraph can only handle 4,000 data points, so I need to make
# provisions for old companies.

#if (!isset($days)) {
#	$days = 270;
#}

// example of Some data
#$datax=array("2001-04-01", "2001-04-02", "2001-04-03");
#$ydata = array(11,3,8,12,5,1,9,13,5,7); #$ydata is defined above already

// Create the graph. These two calls are always required
$graph = new Graph(700,350,"auto");	
$graph->SetScale("lin");
#$graph->SetY2Scale("lin",0,1);


// Show x-axis grids
$graph->xgrid->Show(true);

// Show y-axis grids
$graph->ygrid->Show(true,true);

// Show Secondary y-axis grids
#$graph->y2grid->Show(true,true);

// Set margin
#(LEFT, RIGHT, TOP, BOTTOM);
$graph->img->SetMargin(50,50,40,70);

# Set shadow background
#$graph->SetShadow();

// Set titles
$graph->title->Set("CASH BALANCE");
$graph->subtitle->Set("This graph only shows cash balance");
#$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->title->SetFont(FF_ARIAL,FS_BOLD,12);
$graph->yaxis->title->Set("Portfolio Cash");

#$graph->y2axis->title->Set("Signal BUY=1 SELL=0");

// Setup X-scale
$graph->xaxis->SetTickLabels($xdata);
$graph->xaxis->SetFont(FF_FONT1);
$graph->xaxis->SetLabelAngle(90);
#$graph->xaxis->title->Set("Date ($days days shown)");

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot->SetColor("blue");
$lineplot->SetWeight(1);
#$lineplot->SetFillColor("red"); # Set Fill Color

#$lineplot2=new LinePlot($y2data); # Secondary y-axis
#$lineplot2->SetColor("orange"); # Secondary y-axis
#$lineplot2->SetWeight(2); # Secondary y-axis
##$lineplot2->SetFillColor("yellow"); # Set Fill Color

// Set Legend
$graph->legend->Pos(0.05,0.05,"right","center");
$lineplot->SetLegend("Cash");
#$lineplot2->SetLegend("Signal");

#$graph->y2scale->ticks->Set(1,0.1);

// Add the plot to the graph
$graph->Add($lineplot);

#$graph->AddY2($lineplot2); # Seconday y-axis


// Display the graph
$graph->Stroke();

?>