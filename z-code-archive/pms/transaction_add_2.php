<?php

#
# Filename:	transaction_add_2.php
# Authors:	Chris Ptacek and Michael Salvucci
#
# 20070825  MS  Require people to add notes because they are fallible
# 20061102  MS  Added account id and date to form button, so the next page can show defaults (the last used)
# 20040721  MS  In $action=SOLD, added username and account_id to the WHERE SQL statement
#		  In $action=SOLD, added column which shows 'Account ID' for clarity
# 20040716  MS  Corrected error in the way $net_change is calculated
# 20040713  MS  Fixed bug.  Renamed 'Misc Fee' to 'Miscellaneous Fee'
# 20040713  MS  Added Stock Splits
# 20040712  MS  Fixed typo
# 20040514  MS  Added $username, $menu_string, and PHPSESSID
# 20040509  FS  Changed include mechanism and $path variable
# 20040506  FS  Changed to use functions from include/database.inc
# 20040416  MS  When updating the cash account, using $net_change is more accurate
#		  than using $value, so I'm going to test for $net_change and use that
# 20040416  MS  Do not process transaction record if sold, do it on the next script
# 20040416  MS  Fix the plus-sign bug on $notes.  For some reason, probably due to
#		  url_de/encoding, $notes gets a '+' sometimes
# 20040415  MS  Put in some allowance for rounding differences between brokers when
#		  comparing $net_change vs. $calculated_net_change.  I had to go to
#		  $0.02 on each side when comparing.  Moved the saving of the 
#		  transaction record until the end of the script in case there's
#		  a problem with selling shares.
# 20030522  MS  I will be dropping the drop-down box for the ticker
#                 symbol on the transaction_add_1.php page because it takes
#		  so long.  To protect data integrity, I'm going to
# 		  error-check and make sure the ticker exists in the company
#		  table here.
# 20030521  MS  'Withdraw' changed to 'Withdrawl'
#		  Added 'Interest Income'
# 20030520  MS  Some validation
# 20030510  CP  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_POST);




##############################################################################
# ERROR CHECKING AND POST-FORM PROCESSING ROUTINES
##############################################################################
# Note, I am working from a manually created database, so I am unsure which 
# of these is allowed to be NULL, we can change this below later.
if(!$notes)
{
    die ("\"I came to the conclusion that basically all our views of the world are somewhat flawed or distorted, and then I 
concentrated on the importance of this distortion in shaping events.\" - George Soros.  Applying that discovery to himself, 
Soros concluded:  \"I am fallible.\"  This was not just an observation;  it became his operational principle and overriding 
belief.<br><br>Because of this, PLEASE PRESS THE BACK BUTTON AND ADD A COUPLE SHORT NOTES ABOUT WHY YOU ARE MAKING YOUR TRADING DECISION.");
}
if(!$account_id)
{
    die ("You must have a valid account id");
}
if(!($date))
{
    die ("You must specify a date");
}
if(!($time))
{
    # so insert works, set this to  00:00:00
    $time = "00:00:00";
}
if(!($action))
{
    die ("You must specify an action");
}

# Cash outlay results in negative net_change
if ($action=="Bought") {
	$net_change = $net_change * -1;
}

if(!($shares))
{
    # so insert works, set this to 0
    $shares = "0";
}
if($shares < 0)
{
    die ("You must specify a postive number of shares");
}

# ticker checks
if(!($ticker))
{
    # so insert works, set this to space
    $ticker = " ";
} else {
	# First, force it to uppercase
	$ticker=strtoupper($ticker);
	# Make sure the ticker is legit
	$sql_company=db_query("
		SELECT	ticker
		FROM	ai_company
		WHERE	ticker='$ticker'
		");
	if (db_num_rows($sql_company)==0)
	{
		die ("The ticker symbol you entered is not in the company table.");
	}
}
###
# price checks
if(!($price))
{
    # so insert works, set this to 0.00
    $price = "0.00";
}
$price = ereg_replace('\$', "", $price);
if(ereg( "[:alpha:]+", $price ) )
{
    die ("Price must be a decimal value");
}
if ( $price < 0 )
{
    die ("You must specify a positive price value");
}
###
if(!($commission))
{
    # so insert works, set this to 0.00
    $commission = "0.00";
}

# Remove dollar sign
$commission = ereg_replace('\$', "", $commission);

# Commission ALWAYS gets subtracted (hehehe) so it has got to be negative
# If commission is positive, make it negative.
if ($commission > 0) {
	$commission = $commission * -1;
}

if(ereg( "[:alpha:]+", $commission ) )
{
    die ("Commission must be a decimal value");
}

###
# net_change
if(!$net_change)
{
	if ($action=="Bought") {
		# 2004/07/16  MS  Since $commission is a negative number, I need to subtract it from the net change
		#		  when I'm buying (two negatives make a positive) which means my cost goes up
		# old way:
		# $net_change = ( ( $shares * $price ) + $commission ) * -1;
		# corrected version:
		$net_change = ( ( $shares * $price ) - $commission ) * -1;
	} elseif ($action=="Sold") {
		$net_change = ( ( $shares * $price ) + $commission ) * 1;
	}
}
if ($action=="Bought") {
	# 2004/07/16  MS  Since $commission is a negative number, I need to subtract it from the net change
	#		  when I'm buying (two negatives make a positive) which means my cost goes up
	# old way:
	# $calculated_net_change = ( ( $shares * $price ) + $commission ) * -1;
	# corrected version:
	$calculated_net_change = ( ( $shares * $price ) - $commission ) * -1;
	#######
	# ORIGINAL VERSION: if($net_change!=$calculated_net_change)
	# 2004/04/15:  NEW VERSION: The checking should match any rounding differences.  Some brokers
	# truncate $0.009 to $0.00 on sells and round on buys, so the net_change should allow for a penny either direction:
	if(($net_change>=$calculated_net_change+0.02) OR ($net_change<=$calculated_net_change-0.02))
	{
	    die ("The net_change you entered <b>$net_change</b> is not equal to calculated_net_change $calculated_net_change by the computer.  Click BACK on your Browser to fix the problem.");
	}
} elseif ($action=="Sold") {
	$calculated_net_change = ( ( $shares * $price ) + $commission ) * 1;
	# ORIGINAL VERSION: if($net_change!=$calculated_net_change)
	# 2004/04/15:  NEW VERSION: The checking should match any rounding differences.  Some brokers
	# truncate $0.009 to $0.00 on sells and round on buys, so the net_change should allow for a penny either direction:
	if(($net_change>=$calculated_net_change+0.02) OR ($net_change<=$calculated_net_change-0.02))
	{
	    die ("The net_change you entered <b>$net_change</b> is not equal to calculated_net_change $calculated_net_change by the computer.  Click BACK on your Browser to fix the problem.");
	}
}
$net_change = ereg_replace('\$', "", $net_change);
if(ereg( "[:alpha:]+", $net_change ) )
{
    die ("net_change: must be a decimal value");
}
###


if(!($notes))
{
    # so insert works, set this to space
    $notes = " ";
}



###########
# ACTION
###########

#depends on the action
# NOTE: These must match those provided in the Action form for transaction_1.php
# Bought              - increment the number of shares, decrement cash
# Sold                - decrement the number of shares, increment cash
# Deposit             - increment cash amount
# Cash Dividend       - increment cash amount
# Option Order Rebate - increment cash amount
# Interest Income     - increment cash amount
# Margin Fee          - decrement cash amount
# Miscellaneous Fee   - decrement cash amount
# Withdraw            - decrement cash amount
# Stock Split	      - do nothing with cash

###############################################################################
# BOUGHT
###############################################################################
if( $action == "Bought")
{
	# 2004/04/17:  Added $time here because the proper purchase time was not appearing in the portfolio
	$buy_date = "$date $time";
	#$buy_share_price = (int)$price;  # Note:  DO NOT use (int) here because it will truncate the cents.
	$buy_share_price = $price;
	$buy_cost_basis = "$net_change";
	# $value = (int)$shares * (int)$buy_share_price; # DO NOT USE (int) here!
	$value = $shares * $buy_share_price;

	# NEW PORTFOLIO METHOD:  ALWAYS ADD THE ENTRY WHEN YOU BUY
	$sqlq = "INSERT INTO ai_portfolio (username,account_id,ticker,shares,value,buy_date,buy_share_price,buy_cost_basis,buy_notes) VALUES ('$username','$account_id','$ticker','$shares','$value','$buy_date','$buy_share_price','$buy_cost_basis','$notes')";
	$result = db_query($sqlq) or die (mysql_error());

    # update cash
    $sqlq = "SELECT value FROM ai_portfolio WHERE ticker='ACT_CASH' AND username='$username' AND account_id='$account_id'";
    $result = db_query($sqlq) or die (mysql_error());

    if( 0 == db_num_rows($result) )
    {
        # no1spec:  no current cash position, hmm, bought with no money.  Oh well reflect it negative I guess.
	# Vooch:    LOL
        $value = 0; 
        $value = $value - ($shares * $price);
	if ($commission > 0) {
		#subtract if commission is positive
	        $value = $value - $commission; 
	} else {
		#add if commission is negative
	        $value = $value + $commission; 
	}

	# 2004/04/16:  Check for $net_change and use that instead when possible
	if ($net_change!=0) {
		# Use the preferred formula
	        # do a new insert into table
	        $sqlq = "INSERT INTO ai_portfolio (username,account_id,ticker,shares,value) VALUES ('$username','$account_id','ACT_CASH','0','$net_change')";
	        $result = db_query($sqlq) or die (mysql_error());
	} else {
		# Use the alternate formula
	        # do a new insert into table
	        $sqlq = "INSERT INTO ai_portfolio (username,account_id,ticker,shares,value) VALUES ('$username','$account_id','ACT_CASH','0','$value')";
	        $result = db_query($sqlq) or die (mysql_error());
	}
    }
    else
    {
        # there will be only one
        $value = db_result($result, 0, "value");

	###
	# 2004/04/16:  Set $cash = $value
	$cash=$value;
	###

        $value = $value - ($shares * $price);
	if ($commission > 0) {
		#subtract if commission is positive
	        $value = $value - $commission; 
	} else {
		#add if commission is negative
	        $value = $value + $commission; 
	}

	# 2004/04/16:  Check for $net_change and use that instead when possible
	if ($net_change!=0) {
		# Use the preferred method
		$cash=$cash+$net_change;
        	# do an update entry
	        $sqlq = "UPDATE ai_portfolio SET value='$cash' WHERE username='$username' AND account_id='$account_id' AND ticker='ACT_CASH'";
	        $result = db_query($sqlq) or die (mysql_error());
	} else {
		# Use the alternate method
        	# do an update entry
	        $sqlq = "UPDATE ai_portfolio SET value='$value' WHERE username='$username' AND account_id='$account_id' AND ticker='ACT_CASH'";
	        $result = db_query($sqlq) or die (mysql_error());
	}
    }


}


##################################################
# SOLD
##################################################
else if( $action == "Sold" )
{
	# Pass everything I know to the form

	# 2004/07/21:  Added username and account_id to the WHERE
	$sql_portfolio=db_query("
		SELECT	id, username, ticker, shares, buy_date, buy_share_price
		FROM	ai_portfolio
		WHERE	username 	= '$username'
		AND	account_id 	= '$account_id'
		AND	ticker 		= '$ticker'
		AND	sell_cost_basis = 0
		");
	print "<b>$date $time</b> : Account: <b>$account_id</b><br>";
	print "Request to sell <b>$shares</b> shares of <b>$ticker</b> at <b>$$price</b> per share and paying <b>$$commission</b> in commissions.<br>";
	print "Please provide the share allocation:";

	print "<table width=100%>";

	print "<tr>";

	print "<td width=16% bgcolor=ffcccc>";
	print "<b>Account ID</b>";
	print "</td>";

	print "<td width=16% bgcolor=ffcccc>";
	print "<b>Ticker</b>";
	print "</td>";

	print "<td width=16% bgcolor=ffcccc>";
	print "<b>Entry Date</b>";
	print "</td>";

	print "<td width=16% bgcolor=ffcccc>";
	print "<b>Entry Share Price</b>";
	print "</td>";

	print "<td width=16% bgcolor=ffcccc>";
	print "<b>Shares Held</b>";
	print "</td>";

	print "<td width=16% bgcolor=ffcccc>";
	print "<b>Shares To Sell</b>";
	print "</td>";

	print "</tr>";

	include_once("{$path}include/functions.php");
	$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

	print "<form action=transaction_add_3.php?$menu_string method=post>";	

	# Prep all the hidden stuff
	print "<input type=hidden name=PHPSESSID value=$PHPSESSID>";
	print "<input type=hidden name=commission value=$commission>";
	print "<input type=hidden name=date value=$date>";
	print "<input type=hidden name=time value=$time>";
	print "<input type=hidden name=action value=$action>";
	print "<input type=hidden name=net_change value=$net_change>";
	print "<input type=hidden name=account_id value=$account_id>";
	$sell_date = "$date $time";
	print "<input type=hidden name=sell_date value=\"$sell_date\">";
	$sell_share_price = $price;
	print "<input type=hidden name=sell_share_price value=$sell_share_price>";
	$sell_cost_basis = "$net_change";
	print "<input type=hidden name=sell_cost_basis value=$sell_cost_basis>";
	$value = ($shares * $sell_share_price);
	print "<input type=hidden name=value value=$value>";
	print "<input type=hidden name=shares value=$shares>";
	print "<input type=hidden name=ticker value=$ticker>";
	print "<input type=hidden name=price value=$price>";
	$notes=urlencode($notes);
	print "<input type=hidden name=notes value=\"$notes\">";

	$counter=0;
	while ($row_portfolio=db_fetch_array($sql_portfolio)) {

		$counter++;

		$id			=$row_portfolio['id'];
		$username		=$row_portfolio['username'];
		$ticker			=$row_portfolio['ticker'];
		$shares			=$row_portfolio['shares'];
		$buy_date		=$row_portfolio['buy_date'];
		$buy_share_price	=$row_portfolio['buy_share_price'];

		print "<tr>";
		print "<input type=hidden name=rowid[$counter] value=$id>";

		print "<td width=16%>";
		print "$account_id";
		print "</td>";

		print "<td width=16%>";
		print "$ticker";
		print "</td>";

		print "<td width=16%>";
		print "$buy_date";
		print "</td>";

		print "<td width=16%>";
		print "$buy_share_price";
		print "</td>";

		print "<td width=16%>";
		print "$shares";
		print "<input type=hidden name=rowshares[$counter] value=$shares>";
		print "</td>";

		print "<td width=16%>";
		print "<input type=text name=rowsharestosell[$counter] size=8 value=$shares>";
		print "</td>";

		print "</tr>";
	}
	print "<tr>";

	print "<td width=16%>";
	print " ";
	print "</td>";

	print "<td width=16%>";
	print " ";
	print "</td>";

	print "<td width=16%>";
	print " ";
	print "</td>";

	print "<td width=16%>";
	print " ";
	print "</td>";

	print "<td width=16%>";
	print " ";
	print "</td>";

	print "<td width=16%>";
	print "<input type=hidden name=counter value=$counter>";
	print "<input type=submit value=Submit>";
	print " ";
	print "</td>";

	$counter++;
	print "</tr>";

	print "</form>";
	print "</table>";
}



##################################################
# STOCK SPLIT
##################################################
else if( $action == "Stock Split" )
{
	# Pass everything I know to the form
	$sql_portfolio=db_query("
		SELECT	id, username, ticker, shares, buy_date, buy_share_price
		FROM	ai_portfolio
		WHERE	sell_cost_basis = 0
		AND	ticker = '$ticker'
		");
	print "<b>$date $time</b> : Account: <b>$account_id</b><br>";
	print "Stock Split for <b>$ticker</b>:<br>";

	include_once("{$path}include/functions.php");
	$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

	print "<form action=transaction_add_3b.php?$menu_string method=post>";	

	# Prep all the hidden stuff
	print "<input type=hidden name=PHPSESSID value=$PHPSESSID>";
#	print "<input type=hidden name=commission value=$commission>";
	print "<input type=hidden name=date value=$date>";
	print "<input type=hidden name=time value=$time>";
	print "<input type=hidden name=action value=\"$action\">";
	print "<input type=hidden name=net_change value=$net_change>";
	print "<input type=hidden name=account_id value=$account_id>";
	$sell_date = "$date $time";
	print "<input type=hidden name=sell_date value=\"$sell_date\">";

#	$sell_share_price = $price;
#	print "<input type=hidden name=sell_share_price value=$sell_share_price>";
#	$sell_cost_basis = "$net_change";
#	print "<input type=hidden name=sell_cost_basis value=$sell_cost_basis>";
#	$value = $shares * $sell_share_price;
#	print "<input type=hidden name=value value=$value>";
#	print "<input type=hidden name=shares value=$shares>";
	print "<input type=hidden name=ticker value=$ticker>";
#	print "<input type=hidden name=price value=$price>";
#	$notes=urlencode($notes);
	print "<input type=hidden name=notes value=\"$notes\">";

	print "What is the amount of the Stock Split (eg. 2 for 1): ";
	print "<input type=text name=shares_split_after size=3> ";
	print " for ";
	print "<input type=text name=shares_split_before size=3> ";
	print " <input type=submit value=Submit><br>";
	print "Reverse Stock Splits appear like this (eg. 1 for 10)<br>";

	print "</form>";
}



#################################################################################
# DEPOSIT  -OR-  CASH DIVIDEND  -OR-  OPTION ORDER REBATE  -OR- INTEREST INCOME
# All 3 of these increment cash
#################################################################################
else if(( $action == "Deposit" ) || ( $action == "Cash Dividend" ) || ( $action =="Option Order Rebate" ) || ( $action == "Interest Income" ))
{
    # basically we are just adding to our cash position
    $sqlq = "SELECT value FROM ai_portfolio WHERE username='$username' AND ticker='ACT_CASH' AND account_id='$account_id'";
    $result = db_query($sqlq) or die (mysql_error());

    if( 0 == db_num_rows($result) )
    {
        # no current cash position
        $value = $net_change;

        # do a new insert into table
        $sqlq = "INSERT INTO ai_portfolio (username, account_id,ticker,shares,value) VALUES ('$username','$account_id','ACT_CASH','0','$value')";
        $result = db_query($sqlq) or die (mysql_error());
    }
    else

    {
        # there will be only one
        $value = db_result($result, 0, "value");
        $value = $value + $net_change;

        # do an update entry
        $sqlq = "UPDATE ai_portfolio SET value='$value' WHERE username='$username' AND account_id='$account_id' AND ticker='ACT_CASH'";
        $result = db_query($sqlq) or die (mysql_error());
    }

}

########################################################
# MARGIN FEE  -OR-  MISCELLANEOUS FEE  -OR-  WITHDRAWL
# All 3 of these decrement cash
########################################################

else if(( $action == "Margin Fee" ) || ( $action == "Miscellaneous Fee" ) || ( $action == "Withdrawl" ))
{
    # basically we are just reducing our cash position
    $sqlq = "SELECT value FROM ai_portfolio WHERE username='$username' AND ticker='ACT_CASH' AND account_id='$account_id'";
    $result = db_query($sqlq) or die (mysql_error());

    if( 0 == db_num_rows($result) )
    {
        # the user should use a positive value, we will correct if they didn't
        # no current cash position
        if($net_change < 0)
        {
            $value = $net_change;
        }
        else
        {
            $value = 0 - $net_change;
        }

        # do a new insert into table
        $sqlq = "INSERT INTO ai_portfolio (username,account_id,ticker,shares,value) VALUES ('$username','$account_id','ACT_CASH','0','$value')";
        $result = db_query($sqlq) or die (mysql_error());
    }
    else
    {
        # there will be only one
        $value = db_result($result, 0, "value");
        if($net_change < 0)
        {
            $value = $value + $net_change;
        }
        else
        {
            $value = $value - $net_change;
        }

        # do an update entry
        $sqlq = "UPDATE ai_portfolio SET value='$value' WHERE username='$username' AND account_id='$account_id' AND ticker='ACT_CASH'";
	$result = db_query($sqlq) or die (mysql_error());
    }
}


###################################################
# PROCESS TRANSACTION
###################################################
# Fix the plus-sign bug on $notes.  For some reason, probably due to url_de/encoding, $notes gets a '+' sometimes
if ($notes=='+') {
	$notes="";
}

# PROCESS EVERYTHING EXCEPT 'Sold' AND 'Stock Split' TRANSACTIONS
if ( ($action!="Sold") AND ($action != "Stock Split") ) {
	# insert data to the database
	$table_columns = "username,account_id,date,time,action,shares,ticker,price,commission,net_change,notes";
	$table_values = "'$username','$account_id','$date','$time','$action','$shares','$ticker','$price','$commission','$net_change','$notes'";
	$sqlq = "INSERT INTO ai_transaction ($table_columns) VALUES ($table_values)";

	# To comply with php-4.3.7 and up
	if(!isset($sqlq)) { $sqlq=""; }

	$result = db_query($sqlq) or die (mysql_error());

	# Show everybody except "Sold" and "Stock Split" actions because these need to be processed on another page
	# else show success
	print "<H2>Transaction Added</H2>";
	print "<HR>";
	printf("account_id:  %s<br>", $account_id);
	printf("date:        %s<br>", $date);
	printf("time:        %s<br>", $time);
	printf("action:      %s<br>", $action);
	printf("shares:      %s<br>", $shares);
	printf("ticker:      %s<br>", $ticker);
	printf("price:       %s<br>", $price);
	printf("commission:  %s<br>", $commission);
	printf("net_change:  %s<br>", $net_change);
	printf("notes:       %s<br>", $notes);
	print "<HR>";
	print "<br><br>";

	# $link="transaction_add_1.php?menu_pms=1&PHPSESSID=$PHPSESSID&account_id=$account_id&date=$date";
	# print "<br><a href=$link>Click here to add another transaction</a>";

	?>
	<SCRIPT LANGUAGE="JavaScript">
		<!-- Begin
			function toForm() {
				document.myform.submitbutton.focus();
				// Replace 'submitbutton' with the field name of which you want to place the focus.
			}
		// End -->
	</SCRIPT>
	<BODY onLoad="toForm()">
	<?php

	print "<form name=myform action=transaction_add_1.php?menu_pms=1&PHPSESSID=$PHPSESSID&account_id=$account_id&date=$date method=post>";
	print " <input name=submitbutton type=submit value=\"Add another transaction\">";
	print "</form>";
}
#include_once("{$path}include/footer.php");
?>