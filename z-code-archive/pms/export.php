<?php
#
# export.php
#
# MAYBE....THE WINDOWS COMMAND 'cacls' HAS TO BE RUN ON THE FILENAME OR FOLDER
# cacls - Displays or modifies access control lists (ACLs) of files
# Check Windows Help for more information.
#
#
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add a transaction.<br>";
	exit();
}



#########################################################################
# Export ai_account table
$get_account=db_query("
	SELECT		* 
	FROM		ai_account
	WHERE		username='$username'
	");

#  Open the file to write
$filename = "export/" . "$username-ai_account-" . date("YmdHis") . ".csv";

$handle = fopen($filename, "w");

print "Writing the file $filename ...<br>";

while($row=mysql_fetch_array($get_account)) {
	$username			= $row["username"];
	$account_id			= $row["account_id"];
	$account_name		= $row["account_name"];
	$description		= $row["description"];
	$contact_name		= $row["contact_name"];
	$phone_number		= $row["phone_number"];
	$broker_username	= $row["broker_username"];
	$broker_password	= $row["broker_password"];
	$notes				= $row["notes"];
	
	$string =  "\"$username\",\"$account_id\",\"$account_name\",\"$description\",\"$contact_name\",\"$phone_number\",\"$broker_username\",\"$broker_password\",\"$notes\"\r";

   # Write $somecontent to our opened file
   if (fwrite($handle, $string) === FALSE) {
       print "Cannot write to file ($filename)";
       exit();
   }
}

# Close the file
fclose($handle);
#########################################################################









#########################################################################
# Export ai_auth table
$get_account=db_query("
	SELECT		* 
	FROM		ai_auth
	WHERE		username='$username'
	");

#  Open the file to write
$filename = "export/" . "$username-ai_auth-" . date("YmdHis") . ".csv";

$handle = fopen($filename, "w");

print "Writing the file $filename ...<br>";

while($row=mysql_fetch_array($get_account)) {
	$username			= $row["username"];
	$password			= $row["password"];
	$email				= $row["email"];
	$name				= $row["name"];
	
	$string =  "\"$username\",\"$password\",\"$email\",\"$name\"\r";

   # Write $somecontent to our opened file
   if (fwrite($handle, $string) === FALSE) {
       print "Cannot write to file ($filename)";
       exit();
   }
}

# Close the file
fclose($handle);
#########################################################################














#########################################################################
# Export ai_document table
$get_account=db_query("
	SELECT		* 
	FROM		ai_document
	WHERE		username='$username'
	");

#  Open the file to write
$filename = "export/" . "$username-ai_document-" . date("YmdHis") . ".csv";

$handle = fopen($filename, "w");

print "Writing the file $filename ...<br>";

while($row=mysql_fetch_array($get_account)) {
	$id					= $row["id"];
	$username			= $row["username"];
	$folder				= $row["folder"];
	$date				= $row["date"];
	$subject			= $row["subject"];
	$comment			= $row["comment"];
	$item_1_filename	= $row["item_1_filename"];
	$ocr_item_1			= $row["ocr_item_1"];
	
	$string =  "\"$id\",\"$username\",\"$folder\",\"$date\",\"$subject\",\"$comment\",\"$item_1_filename\",\"$ocr_item_1\"\r";

   # Write $somecontent to our opened file
   if (fwrite($handle, $string) === FALSE) {
       print "Cannot write to file ($filename)";
       exit();
   }
}

# Close the file
fclose($handle);
#########################################################################



















#########################################################################
# Export ai_portfolio table
$get_account=db_query("
	SELECT		* 
	FROM		ai_portfolio
	WHERE		username='$username'
	");

#  Open the file to write
$filename = "export/" . "$username-ai_portfolio-" . date("YmdHis") . ".csv";

$handle = fopen($filename, "w");

print "Writing the file $filename ...<br>";

while($row=mysql_fetch_array($get_account)) {
	$username			= $row["username"];
	$account_id			= $row["account_id"];
	$ticker				= $row["ticker"];
	$shares				= $row["shares"];
	$value				= $row["value"];
	$buy_date			= $row["buy_date"];
	$buy_share_price	= $row["buy_share_price"];
	$buy_cost_basis		= $row["buy_cost_basis"];
	$buy_notes			= $row["buy_notes"];
	$sell_date			= $row["sell_date"];
	$sell_share_price	= $row["sell_share_price"];
	$sell_cost_basis	= $row["sell_cost_basis"];
	$sell_notes			= $row["sell_notes"];
	
	$string =  "\"$id\",\"$username\",\"$account_id\",\"$ticker\",\"$shares\",\"$value\",\"$buy_date\",\"$buy_share_price\",\"$buy_cost_basis\",\"$buy_notes\",\"$sell_date\",\"$sell_share_price\",\"$sell_cost_basis\",\"$sell_notes\"\r";

   # Write $somecontent to our opened file
   if (fwrite($handle, $string) === FALSE) {
       print "Cannot write to file ($filename)";
       exit();
   }
}

# Close the file
fclose($handle);
#########################################################################












#########################################################################
# Export ai_portfolio table
$get_account=db_query("
	SELECT		* 
	FROM		ai_transaction
	WHERE		username='$username'
	");

#  Open the file to write
$filename = "export/" . "$username-ai_transaction-" . date("YmdHis") . ".csv";

$handle = fopen($filename, "w");

print "Writing the file $filename ...<br>";

while($row=mysql_fetch_array($get_account)) {
	$username			= $row["username"];
	$account_id			= $row["account_id"];
	$date				= $row["date"];
	$time				= $row["time"];
	$action				= $row["action"];
	$ticker				= $row["ticker"];
	$shares				= $row["shares"];
	$price				= $row["price"];
	$commission			= $row["commission"];
	$net_change			= $row["net_change"];
	$notes				= $row["notes"];
	
	$string =  "\"$id\",\"$username\",\"$account_id\",\"$ticker\",\"$shares\",\"$value\",\"$buy_date\",\"$buy_share_price\",\"$buy_cost_basis\",\"$buy_notes\",\"$sell_date\",\"$sell_share_price\",\"$sell_cost_basis\",\"$sell_notes\"\r";

   # Write $somecontent to our opened file
   if (fwrite($handle, $string) === FALSE) {
       print "Cannot write to file ($filename)";
       exit();
   }
}

# Close the file
fclose($handle);
#########################################################################






#########################################################################
# SHOW THE RESULTS
print "The newest file (your file) will be timestamped and appear at the bottom:<br>";

print "<br>";

print "Directory Listing<br>";

$directory_path = 'C:\\Inetpub\\wwwroot\\aistockbot\\aistockbot\\pms\\export\\';
$dir_handle = @opendir($directory_path) or die("Unable to open $directory_path");
# print "Directory Listing of $path<BR>";
while ($file = readdir($dir_handle)) {
	if($file!="." && $file!="..") {
        print "<a href=export/$file>$file</a><br>";
	}
}
closedir($dir_handle);
#########################################################################


print "<br><br><br><br><br><br>";
include_once("{$path}include/footer.php");
?>