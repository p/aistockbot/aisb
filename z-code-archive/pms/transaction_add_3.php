<?php
###################################
# transaction_add_3.php
###################################
#
# I spent a lot of time working on this, so I would prefer if no one makes any changes to
# this script until they talk to me.  This script is still buggy, so it's better if I work
# on it until I get it right.  - Vooch
#
#
# 2006-03-11  MS  Added $net_change_partial = round($net_change_partial,6); to fix some errors
# 2004/12/18  MS  Improved situations with multiple sells - (revise sell_cost_basis so it only takes a fraction of each piece being sold)
# 2004/07/21  MS  Took out $sell_cost_basis calculation here because it should already be calculated.
# 2004/07/13  MS  Added " " around $sumofsharestosell in if() to hopefully fix a strange bug
# 2004/05/14  MS  Added $username to the sql queries
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2003/05/22  MS  Initial Release
#


if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

# Get Data
extract($_POST);

#############################
# STEP 1:  DO ERROR CHECKING
#############################

# Debug area:

/*
print "commission=$commission<br>";
print "account_id=$account_id<br>";
print "sell_date=$sell_date<br>";
print "sell_share_price=$sell_share_price<br>";
print "sell_cost_basis=$sell_cost_basis<br>";
print "value=$value<br>";
print "shares=$shares<br>";
print "ticker=$ticker<br>";
print "price=$price<br>";
print "notes=$notes<br>";
print "net_change=$net_change<br>";
*/

$notes=urldecode($notes);

$sumofsharestosell=0;
for ($x=1;$x<=$counter;$x++) {
	#print "rowid[$x]=$rowid[$x]<br>";
	#print "rowshares[$x]=$rowshares[$x]<br>";
	#print "rowsharestosell[$x]=$rowsharestosell[$x]<br>";
	$sumofsharestosell=$sumofsharestosell+$rowsharestosell[$x];
	# Check for trying to sell more than you've got in a row
	if ($rowsharestosell[$x]>$rowshares[$x]) {
		die ("Error:  You cannot allocate <b>$rowsharestosell[$x]</b> shares when that purchase had only <b>$rowshares[$x]</b> shares available");
	}
}

# Make sure the order matches how many your allocating for sale
# 2004/07/13  MS  Added " " around $sumofsharestosell to hopefully fix a strange bug (ie. selling 0.xxx shares)
if($shares != "$sumofsharestosell") {
	die ("Error:  You allocated <b>$sumofsharestosell</b> shares when you sold <b>$shares</b> shares.  Please reallocate amount of shares by clicking BACK on your browser.");
}

###############################################
# STEP 2:  PREP $net_change FOR MULTIPLE SELLS
###############################################

# This step was added on 2004-12-18:

# Here, I need to grab the $net_change value and manipulate it while I'm selling off multiple buy orders.
# Each time, there's a sale, I need to deduct that from the total sale price and allocate a fraction of 
# the $net_change towards each line item being sold.
# As each item gets sold off, the $net_change_remaining_balance keeps getting reduced until we get to the 
# last item being sold.
# In the end, the goal is that the sum of each partial amount equals the original $net_change

$net_change_original = $net_change;
$net_change_remaining_balance = $net_change;
$net_change_partial = 0; # Initialized to zero


###################################################
# STEP 3:  INSERT RECORD INTO TRANSACTION DATABASE
###################################################

# insert data to the database
for ($x=1;$x<=$counter;$x++) {
/*
	print "<b>x=$x<br>";
	print "counter=$counter<br>";
	print "sell_date=$sell_date<br>";
	print "sell_share_price=$sell_share_price<br>";
	print "sell_cost_basis=$sell_cost_basis<br>";
	print "value=$value<br>";
	print "shares=$shares<br>";
	print "notes=$notes<br>";
	print "<br>";
	print "account_id=$account_id<br>";
	print "date=$date<br>";
	print "time=$time<br>";
	print "action=$action<br>";
	print "rowsharestosell[x]=$rowsharestosell[$x]<br>";
	print "ticker=$ticker<br>";
	print "price=$price<br>";
	print "commission=$commission<br>";
	print "net_change=$net_change<br>";
*/
	# Process only if this row is making the transaction
	if ($rowsharestosell[$x]>0) {

		# 2004-12-18:
		# Manipulate $net_change
		# Since there could be multiple lines of shares to sell, we create partial amounts assign those fractions to each line item
		if ($x==$counter) {
			# Since this is the last item, the remaining balance is used
			$net_change_partial = $net_change_remaining_balance;
		
		} else {
			# Since this is not the last line item, we calculate it manually
			$net_change_partial = ($rowsharestosell[$x] * $price) + (($rowsharestosell[$x]/$shares)*$commission);
			$net_change_partial = round($net_change_partial,6);

			# If this is the last item being sold, but it is not the last item in the list of things to sell, we need to 
			# make sure the entire remaining balance is used.
			$net_change_partial_plus_penny = $net_change_partial+0.01;
			$net_change_partial_minus_penny = $net_change_partial-0.01;				
			if (($net_change_remaining_balance <= $net_change_partial_plus_penny) AND ($net_change_remaining_balance >= $net_change_partial_minus_penny)) {
				$net_change_partial = $net_change_remaining_balance;

			} else {
				# This remains as is:
				# $net_change_partial = $net_change_partial;

				# and we deduct $net_change_partial from the $net_change_remaining_balance
				$net_change_remaining_balance = $net_change_remaining_balance - $net_change_partial;
			}
		}


		# Database Action:
		$table_columns = "username,account_id,date,time,action,shares,ticker,price,commission,net_change,notes";
		$table_values = "'$username','$account_id','$date','$time','$action','$rowsharestosell[$x]','$ticker','$price','$commission','$net_change_partial','$notes'";
		db_query("INSERT INTO ai_transaction ($table_columns) VALUES ($table_values)");
	}
}

#####################################################
# STEP 4:  THEN ADJUST RECORDS IN PORTFOLIO DATABASE
#####################################################

for ($x=1;$x<=$counter;$x++) {
	if($rowsharestosell[$x]>0) {
		# This means that I want to sell all or part of this row.

		# Basically, all I've gotta do is just update the record with
		# sell_date, sell_share_price, and sell_cost_basis.  Wait a minute!
		# That's wrong...	
		# hmm....   if $rowsharestosell[$x] matches the $rowshares[$x]
		# then close out the record by doing the update.
		# otherwise, close out the record AND create a new record (including
		# buy information) with the remaining share quantity.

		if($rowsharestosell[$x]==$rowshares[$x]) {
			# print "rowsharestosell[$x] is equal to rowshares[$x]<br>";
			# If $rowsharestosell[$x] matches the $rowshares[$x], this
			# means that I will become completely finished with that record.
			# All I've got to do now is close out the record by doing a simple
			# record update with the sell_date, sell_share_price, and sell_cost_basis.
			
			#####
			# 2004/07/21 16:36:00  MS  Took out $sell_cost_basis calculation here because 
			#			   it should already be calculated.
			#	#####
			#	# 2004/07/21 15:00:00 MS FIXED BUG:  $commissions are negative, so I must add them in $sell_cost_basis
			#	# Original was:
			#	# $sell_cost_basis = ($rowshares[$x]*$sell_share_price)-$commission;
			#	# Revision is:
			#	$sell_cost_basis = ($rowshares[$x]*$sell_share_price)+$commission;
			#	#####
			#####

			# Calculate value
			$revised_value = $sell_share_price * $rowsharestosell[$x];




			# This is not good enough:
			# $revised_sell_cost_basis = round($revised_sell_cost_basis, 2);
			# so I changed it to this:
			# 2004-12-18:  Revise $revised_sell_cost_basis:
			$temp1 = $rowsharestosell[$x] * $price;  # porportion of the shares
			$temp2 = ($rowsharestosell[$x]/$shares)*$commission;  # porportion of the commission
			$revised_sell_cost_basis = round($temp1, 2) + round($temp2, 2);






	       		$sql_portfolio1 = db_query("
				UPDATE	ai_portfolio
				SET	sell_date		='$sell_date'
					, sell_share_price	= $sell_share_price
					, sell_cost_basis	= $revised_sell_cost_basis
					, value			= $value
					, sell_notes		= '$notes'
				WHERE	id=$rowid[$x]
				");

# Fixed this on 2004-12-18:
#			# I've got to force commission to zero now so I don't keep taking commission out on multiple rows.
# WARNING:::::--->	# Make sure commissions come out of cash
#			$commission=0;


		} else {
			# print "rowsharestosell[$x] is NOT equal to rowshares[$x]<br>";
			# OK...  Here the dude didn't close out the entire record in the database, so we've
			# got to close out the record by adjusting the number of shares, and then creating a new record
			# for the remaining balance.

			# Example of how this portfolio table works:
			# 1. Buy 500 shares of a ticker symbol.  1 record created.
			# 2. Sell 200 shares. 1 record changes from 500 to 200 shares.  New record created for 300 shares.

			# FIRST, LET'S CLOSE OUT THE RECORD
			# print "$rowshares[$x] shares in the record<br>";
			# print "$rowsharestosell[$x] shares wishing to sell<br>";
			$remaining_shares = $rowshares[$x]-$rowsharestosell[$x];
			# Need to generate new buy_cost_basis here....

			# Fetch buy_cost_basis and revise it to reflect the partial appropriation
	       		$sql_portfolio2 = db_query("
				SELECT	shares AS original_shares
					, value AS original_value
					, buy_date
					, buy_share_price
					, buy_cost_basis
					, buy_notes
				FROM	ai_portfolio
				WHERE	id=$rowid[$x]
				");

			while ($row_portfolio2=db_fetch_array($sql_portfolio2)) {
				$original_shares	= $row_portfolio2['original_shares'];
				$original_value		= $row_portfolio2['original_value'];
				$buy_date		= $row_portfolio2['buy_date'];
				$buy_share_price	= $row_portfolio2['buy_share_price'];
				$buy_cost_basis		= $row_portfolio2['buy_cost_basis'];
				$buy_notes		= $row_portfolio2['buy_notes'];
			}
			# Get the buy cost basis per share of the original purchase
			$original_buy_cost_basis_per_share = $buy_cost_basis/$original_shares;
			# Calculate cost for record that's getting closed out
			$revised_buy_cost_basis = $original_buy_cost_basis_per_share * $rowsharestosell[$x];
			# Calculate cost for newly created record
			$buy_cost_basis = $buy_cost_basis - $revised_buy_cost_basis;

			# Calculate the value per share
# old way		$original_value_per_share = $original_value/$original_shares;
#			$original_value_per_share = ($shares * $sell_share_price)/$original_shares;
			
			# Calculate the value for the record that's getting closed out
#			$revised_value = $original_value_per_share * $rowsharestosell[$x];
			# Calculate the value for the newly created record
#			$value = $original_value - $revised_value;

			# Calculate value
			$revised_value = $sell_share_price * $rowsharestosell[$x];



			# 2004-12-18:  Revise $revised_sell_cost_basis:
			$revised_sell_cost_basis =  ($rowsharestosell[$x] * $price) + (($rowsharestosell[$x]/$shares)*$commission);




			# Close out the record.   btw, $buy_date and $sell_date must have single quotes!
	       		$sql_portfolio3 = db_query("
				UPDATE	ai_portfolio
				SET	shares			= $rowsharestosell[$x]
					, value			= $revised_value
					, buy_date		='$buy_date'
					, buy_share_price	= $buy_share_price
					, buy_cost_basis	='$revised_buy_cost_basis'
					, sell_date		='$sell_date'
					, sell_share_price	= $sell_share_price
					, sell_cost_basis	= $revised_sell_cost_basis
					, sell_notes		= '$notes'
				WHERE	id=$rowid[$x]
				");

			# Calculate value...again
			$revised_value = $sell_share_price * $remaining_shares;

			# SECOND, LET'S CREATE A NEW RECORD WITH THE REMAINING BALANCE
			$sql_portfolio4 = db_query("
				INSERT INTO	ai_portfolio 
					(
					username
					,account_id
					,ticker
					,shares
					,value
					,buy_date
					,buy_share_price
					,buy_cost_basis
					,buy_notes
					)
				VALUES
					(
					'$username'
					,'$account_id'
					,'$ticker'
					,'$remaining_shares'
					,'$revised_value'
					,'$buy_date'
					,'$buy_share_price'
					,'$buy_cost_basis'
					,'$notes'
					)
				");

		}
	} else {
		# No action taken because we're keeping this stuff
	}
}

################################
# STEP 5:  FINALLY, UPDATE CASH
################################
    # update cash
    $sqlq = "SELECT value FROM ai_portfolio WHERE ticker='ACT_CASH' AND username='$username' AND account_id='$account_id'";
    $result = db_query($sqlq) or die (mysql_error());
    if( 0 == db_num_rows($result) )
    {
	# Test for $net_change, if there's none, then calculate, but it's better to have the real number, so if it's
	# available, then use that instead.
	if(!$net_change) {
	        # no current cash position
	        $value = $shares * $price;
		if ($commission > 0) {
			#subtract if commission is positive
		        $value = $value - $commission; 
		} else {
			#add if commission is negative
		        $value = $value + $commission; 
		}
	} else {
		$value=$net_change;
	}
        # do a new insert into table
        $sqlq = "INSERT INTO ai_portfolio (username,account_id,ticker,shares,value) VALUES ('$username','$account_id','ACT_CASH','0','$value')";
        $result = db_query($sqlq) or die (mysql_error());
    }
    else
    {
        # there will be only one record for ACT_CASH, so grab the value
        $value = db_result($result, 0, "value");
	# Test for $net_change, if there's none, then calculate, but it's better to have the real number, so if it's
	# available, then use that instead.
	if(!$net_change) {
	        $value = $value + ($shares * $price);
		if ($commission > 0) {
			#subtract if commission is positive
		        $value = $value - $commission; 
		} else {
			#add if commission is negative
		        $value = $value + $commission; 
		}
	} else {
		$value = $value + $net_change;  # Since we're always selling in this script, we add.
	}
        # do an update entry
        $sqlq = "UPDATE ai_portfolio SET value='$value' WHERE username='$username' AND account_id='$account_id' AND ticker='ACT_CASH'";
        $result = db_query($sqlq) or die (mysql_error());
    }

print "<br>";

?>
<SCRIPT LANGUAGE="JavaScript">
	<!-- Begin
		function toForm() {
			document.myform.submitbutton.focus();
			// Replace 'submitbutton' with the field name of which you want to place the focus.
		}
	// End -->
</SCRIPT>
<BODY onLoad="toForm()">
<?php

print "<form name=myform action=transaction_add_1.php?menu_pms=1&PHPSESSID=$PHPSESSID&account_id=$account_id&date=$date method=post>";
print "<input name=submitbutton type=submit value=\"Add another transaction\">";
print "</form>";

 
include_once("{$path}include/footer.php");
?>
