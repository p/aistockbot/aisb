<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	transaction_dump_1.php
# Authors:	Michael Salvucci
#############################################################################
#
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2003/05/14  MS  Initial Release
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");

# NEED TO GET THE LAST TRANSACTION OF LAST YEAR OR THE EARLIEST LAST DATE.

# USE THAT NET_CHANGE AS THE STARTING POINT FOR THIS YEAR.


# Initialize variables
$profit=0;
$cost=0;
$running_total=0;

# THEN...
$cash_beginning=50000; #because i don't know right now
$net_change=$cash_beginning; 
$running_total=$running_total+$net_change;

# Get transaction
$get_transaction=db_query("
	SELECT 		*
	FROM		ai_transaction
	WHERE		date > '2003-01-01'
	AND		date < '2003-12-31'
	ORDER BY	date
	");

while ($row_transaction=db_fetch_array($get_transaction)) {
	$account_id	=$row_transaction['account_id'];
	$date		=$row_transaction['date'];
	$time		=$row_transaction['time'];
	$action		=$row_transaction['action'];
	$shares		=$row_transaction['shares'];
	$ticker		=$row_transaction['ticker'];
	$price		=$row_transaction['price'];
	$commission	=$row_transaction['commission'];
	$net_change	=$row_transaction['net_change'];
	$notes		=$row_transaction['notes'];

	if ($action=='Bought') {
		# Cash decrease
		# Think of it as a cost increase?????????
		$cost = $cost + $net_change;

		# only used to determine real profit later
		$profit = $profit + $net_change;
	}

	if ($action=='Sold') {
		# Cash increase
		# Think of it as a cost decrease?????????

		# only used to determine real profit later
		$profit = $profit + $net_change;  # because $net_change will be a negative value
	}

	if ($action=='Cash Dividend') {
		# Cash increase
		# Profit increase
		$profit = $profit + $net_change;
	}

	if ($action=='Deposit') {
		# Cash increase
		# $net_change is the amount of the Deposit

		# No change to profit

		$cash = $cash + $net_change;
		$running_total=$running_total-$net_change;  # NULLIFY THIS
	}
	
	if ($action=='Withdrawl') {
		# Cash decrease
		# Total cost reduced by amount of withdrawl??????????????????

		# No change to profit
	}

	if ($action=='Option Order Rebate') {
		# Cash increase
		# Cost decrease
	}

	if ($action=='Margin') {
		# This is the margin fee
		# Cash decrease
		# Cost increase
	}

	if ($action=='Miscellaneous Fee') {
		# Cash decrease
		# Cost increase
	}
print "cost=$cost<br>";
print "profit=$profit<br>";
print "cash_beginning=$cash_beginning<br>";
print "cash_ending=$cash_ending<br>";
print "cash=$cash<br>";
print "net_change=$net_change<br>";
print "account_id=$account_id<br>";
print "date=$date<br>";
print "time=$time<br>";
print "action=$action<br>";
print "shares=$shares<br>";
print "ticker=$ticker<br>";
print "price=$price<br>";
print "commission=$commission<br>";
print "net_change=$net_change<br>";
print "notes=$notes<br>";
$running_total=$running_total+$net_change;
print "running_total=$running_total<br>";
print "===============================================================================<br>";

# Zero this out again?
$cash=0;

}

$cash_ending = $cash;


# then subtract beginning cash
$running_total=$running_total-$cash_beginning;
# so now running_total is profit(loss)



# OK, then add in portfolio
# TICKER:  SIRI  2000 shares @ $0.40
$running_total=$running_total+800-9.95;  # S A M P L E

print "running_total=$running_total<br>";
print "cost=$cost<br>";

#include("include/footer.php");
?>
