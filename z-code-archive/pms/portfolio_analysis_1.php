<?php
#
# portfolio_analysis_1.php
#
# 20061029  MS  Fixed Return-Risk Ratio calculation for when ABS(losses) exceeded profits
# 20061011  MS  Added filter by ticker
#		Added sort by Entry or Exit Date
#		Added turnover (adjusted to trades per month)
#               Added CARP Analysis (this section only appears when sort by Ticker or Entry Date in ascending order
# 20050210  MS  Changed $sql_portfolio from "WHERE		sell_cost_basis > 0"
#		  to:  "WHERE		sell_date <> '0000-00-00 00:00:00'"
# 		  so that expired options show up in the report
# 20050210  MS  I'm doing my taxes, so I'm going to have this thing calculate everything
#                 for the accountant!  :)
#                 I'm also adding filtering capability so the user can specify a time period
#                 if they want
# 20040720  MS  Added 2 columns:  Entry Share Price and Exit Share Price
# 20040717  MS  Cleanup and added "Details" link
# 20040714  MS  Added "Annualized Profit" to give you an idea on the time-value of money
# 20040713  MS  Changed $sql_portfolio from (ORDER BY id DESC) to (ORDER BY sell_date DESC)
# 20040712  MS  Added test for $username
# 20040712  MS  Conforms with php-4.3.x and up
# 20040509  FS  Changed include mechanism and $path variable
# 20040506  FS  Changed to use settings from ai_config
# 20040506  FS  Changed to use functions from include/database.inc
# 20040415  MS  Fixed error when nothing sold and/or bought
# 2003????  MS  Initial Release
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
#include_once("{$path}include/header.php"); # need the space!




##########################################################################
# NOTE:  SINCE WE ARE NOT USING HEADER.PHP AND FOOTER.PHP, I MUST INCLUDE
#        ALL OF THIS STUFF:
#
if (!isset($base_dir)){
  $base_dir=substr($_SERVER["PATH_TRANSLATED"],0,-17); //-17 due to length of "include/header.php";
}
include_once("{$path}include/database.php");
include_once("{$path}include/functions.php");
if(isset($_SERVER["QUERY_STRING"])) {
	$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));
} else {
	$menu_string=""; # PHP requires you to initialize a variable, so we'll do it here.
}
if(strstr(PHP_OS,'WIN')){ 
	$pear_location = $base_dir.'PEAR';
	ini_set("include_path", ini_get("include_path").';'.$pear_location); 
	// PHP on windows seems to use *last* pear dir listed.
} else {
	$pear_location = $base_dir.'/PEAR';
	ini_set("include_path", $pear_location . ":" . ini_get("include_path"));
}
require_once("{$path}PEAR/Auth/Auth.php");
$params = array(
        "dsn" => "mysql://$global_username:$global_password@$global_hostname/$global_database",
        "table" => "ai_auth",
        "usernamecol" => "username",
        "passwordcol" => "password"
        );
$a = new Auth("DB", $params, "loginFunction");
$a->start();
$username=$a->getUsername();
#
##########################################################################


include_once("{$path}Date-1.3/Date/Calc.php");

$i=0; # row counter for <tr> background color

print "<table width=100%>";
print "<tr>";
# print "<td width=100% background={$path}images/hex.gif>"; # dropped hex.gif graphic from report
print "<td width=100%>";
print "<font face=\"verdana\" size=+0>";






# $sell_date_start and $sell_date_end should appear in the URL, but if it doesn't use this:
if ( !isset($sell_date_start) OR !isset($sell_date_end) ) {
	if ( !isset($_GET["sell_date_start"]) OR !isset($_GET["sell_date_end"]) ) {
		$sell_date_start=date("Y");
		$sell_date_start="$sell_date_start"."-01-01";
		$sell_date_end=date("Y-m-d");
	} else {
		$sell_date_start=$_GET["sell_date_start"];
		$sell_date_end=$_GET["sell_date_end"];
	}
}

if(!isset($_GET["filter_by_ticker"])) {
	$filter_by_ticker = "";
} else {
	$filter_by_ticker = $_GET["filter_by_ticker"];
}

if(!isset($_GET["filter_by_account_id"])) {
	$filter_by_account_id = "";
} else {
	$filter_by_account_id = $_GET["filter_by_account_id"];
}

if(!isset($_GET["public_consumption"])) {
	$public_consumption = "";
} else {
	$public_consumption = $_GET["public_consumption"];
}

if(!isset($_GET['PHPSESSID'])) { $_GET['PHPSESSID']=""; }
print "<form action={$path}pms/portfolio_analysis_1.php>";
print "<b>PORTFOLIO ANALYSIS</b><br>";
print "<input type=hidden name=PHPSESSID value=$_GET[PHPSESSID]>";
print "From Sell Date Start: <input type=text name=sell_date_start value=$sell_date_start> <br>";
print " &nbsp; To Sell Date End: <input type=text name=sell_date_end value=$sell_date_end> <br>";
print " &nbsp; Filter by Ticker: <input type=text name=filter_by_ticker value=$filter_by_ticker> <br>";

print " &nbsp; Filter by Account: ";
print "<select name=filter_by_account_id> ";
print "<option value=></option>";
$string="
        SELECT  	account_id AS temp
        FROM    	ai_account
        WHERE		username='$username'
	";
$sql_account=db_query("$string");
while ($row_account = db_fetch_array($sql_account) ) {
	$temp	= $row_account["temp"];
	print "<option value=$temp ";
	if($temp==$filter_by_account_id) { print " SELECTED "; }
	print " >$temp</option>";
}
print "</select><br>";

print "<input type=checkbox name=public_consumption"; if($public_consumption!="") { print " CHECKED "; } print "> For Public Consumption <font size=-2>(dollar amounts are removed for privacy).  Print this report as a .pdf and you can show your friends.</font><br>";
print " &nbsp; ";
print "<input type=submit value=Submit><br><br>";
print "</form>";




$string="
        SELECT  	ai_portfolio.account_id AS account_id
			, ai_portfolio.ticker AS ticker
			, ai_portfolio.shares AS shares
			, ai_portfolio.buy_date AS buy_date
			, ai_portfolio.buy_share_price AS buy_share_price
			, ai_portfolio.buy_cost_basis AS buy_cost_basis
			, ai_portfolio.sell_date AS sell_date
			, ai_portfolio.sell_share_price AS sell_share_price
			, ai_portfolio.sell_cost_basis AS sell_cost_basis
			, ai_portfolio.shares AS shares
			, ai_company.name AS name
        FROM    	ai_portfolio
	INNER JOIN	ai_company
	ON		ai_portfolio.ticker = ai_company.ticker
        WHERE		sell_date <> '0000-00-00 00:00:00'
	AND		sell_date > '$sell_date_start 00:00:00'
	AND		sell_date < '$sell_date_end 23:59:59'
	AND		username='$username'
	";

if($filter_by_ticker!="") {
	$string .= "
		AND ai_portfolio.ticker = '$filter_by_ticker' 
		";
}

if($filter_by_account_id!="") {
	$string .= "
		AND ai_portfolio.account_id = '$filter_by_account_id' 
		";
}



if(!isset($_GET["order_by"])) {
	$string .= "
		ORDER BY	sell_date 
	        ";
	$order_by = "";
} else {
	$order_by = $_GET["order_by"];
	$string .= "
		ORDER BY	'$order_by'
		";
}






if(!isset($_GET["sort_order"])) {
	$sort_order = 1;
	$string .= " DESC ";
} else {
	$sort_order = $_GET["sort_order"];
	if($sort_order==1) {
		# 1 = Descending
		# 0 = Ascending
		$string .= " DESC ";
	} else {
		# Sorts accending by default
	}
}
# Switch it to the opposite for the next time
if($sort_order==1) {
	$sort_order = 0;
} else {
	$sort_order = 1;
}

# WHERE		sell_cost_basis > 0






# Secondary sort is forced by ASCending order right now since I don't have that functionality setup.
if(!isset($_GET["order_by_secondary"])) {
	# do nothing
} else {
	$order_by_secondary = $_GET["order_by_secondary"];
	$string .= "
		, '$order_by_secondary'
		";
}




$sql_portfolio=db_query("$string");

$numrows = db_num_rows($sql_portfolio);
if ($numrows==0) {
	print "<b>Portfolio Analysis</b><br>";
	print "You have not sold any of your open positions yet.<br>";
	print "Nothing to report.<br><br><br><br><br><br><br><br><br><br><br><br><br>";
	print "</td></tr></table>";
	#include_once("{$path}include/footer.php");
	exit();
}


#################################################
# PRINT HEADER ROW
print "<table width=100% border=1>";
	print "<tr>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Account</font></td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>";
		# Sort by Ticker, then by Buy Date for CARP Analysis
		print "<a href=http://$_SERVER[HTTP_HOST]"."$_SERVER[SCRIPT_NAME]?"."$_SERVER[QUERY_STRING]&order_by=ticker&order_by_secondary=buy_date&";
		print "sort_order=$sort_order>";
		print "Ticker</font>";
	print "</td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Company Name</font></td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Shares</font></td>";
	print "<td width=5% align=center bgcolor=ffcccc valign=bottom><font size=-2>";
		print "<a href=http://$_SERVER[HTTP_HOST]"."$_SERVER[SCRIPT_NAME]?"."$_SERVER[QUERY_STRING]&order_by=buy_date&";
		print "sort_order=$sort_order>";
		print "Entry Date</font>";
	print "</td>";
	print "<td width=5% align=center bgcolor=ffcccc valign=bottom><font size=-2>Entry Share Price</font></td>";
	if (db_query_config("aistockbotpro")==1) {
		print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>&nbsp; &nbsp; &nbsp; &nbsp;Entry Cost </font></td>";
		print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Exit Date &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </font></td>";
	} else {
		print "<td width=5% align=center bgcolor=ffcccc valign=bottom><font size=-2>Entry Cost</font></td>";
		print "<td width=5% align=center bgcolor=ccffcc valign=bottom><font size=-2>";
		print "<a href=http://$_SERVER[HTTP_HOST]"."$_SERVER[SCRIPT_NAME]?"."$_SERVER[QUERY_STRING]&order_by=sell_date&";

		print "sort_order=$sort_order>";
		print "Exit Date</font></td>";
	}
	print "<td width=5% align=center bgcolor=ccffcc valign=bottom><font size=-2>Exit Share Price</font></td>";
	print "<td width=5% align=center bgcolor=ccffcc valign=bottom><font size=-2>Exit Cost</font></td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Days Held</font></td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Real Profit($)</font></td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Real Profit(%)</font></td>";
	print "<td width=5% align=center bgcolor=ccccff valign=bottom><font size=-2>Annual-<br>ized<br>Profit(%)</font></td>";
	#print "<td width=5%>running_profit_dollars</td>";
	#print "<td width=5%>running_profit_percentage</td>";
	print "</tr>";

	###################################################
	# Initialize variables
	$running_profit_dollars         = 0;
	$running_profit_percentage      = 0;
	$number_of_rows                 = 0;
	$days_held                      = 0;
	$days_held_total		= 0;
	$total_days_held                = 0;
	$total_count			= 0;
	$total_dollar_profits		= 0;
	$total_dollar_losses		= 0;
	$winner				= 0;
	$winner26percent		= 0;
	$loser				= 0;
	$last_buy_share_price		= 0;
	$average_down_count		= 0;
	$average_down_winner		= 0;
	$pyramiding_profits_count	= 0;
	$pyramiding_profits_winner	= 0;
	$cut_the_losers_count		= 0;
	$cut_the_losers_winner		= 0;
	$ride_the_winners_count		= 0;
	$ride_the_winners_winner	= 0;
	$last_ticker			= "";

	while ($row_portfolio = db_fetch_array($sql_portfolio) ) {
		$number_of_rows++;
	        $account_id             =$row_portfolio["account_id"];
	        $ticker                 =$row_portfolio["ticker"];
	        $shares                 =$row_portfolio["shares"];
	        $buy_date               =$row_portfolio["buy_date"];
	        $buy_share_price        =$row_portfolio["buy_share_price"];
	        $buy_cost_basis         =$row_portfolio["buy_cost_basis"];
	        $sell_date              =$row_portfolio["sell_date"];
	        $sell_share_price       =$row_portfolio["sell_share_price"];
	        $sell_cost_basis        =$row_portfolio["sell_cost_basis"];
	        $shares		        =$row_portfolio["shares"];
		$name			=$row_portfolio["name"];

		$buy_date_month = substr("$buy_date", 5, 2);
		$buy_date_day  = substr("$buy_date", 8, 2);
		$buy_date_year = substr("$buy_date", 0, 4);

	        $buy_date_numberofdays = Date_Calc::dateToDays($buy_date_day, $buy_date_month, $buy_date_year);

		$sell_date_month = substr("$sell_date", 5, 2);
		$sell_date_day  = substr("$sell_date", 8, 2);
		$sell_date_year = substr("$sell_date", 0, 4);

	        $sell_date_numberofdays = Date_Calc::dateToDays($sell_date_day, $sell_date_month, $sell_date_year);
		#print "SDNOD=$sell_date_numberofdays<br>";

		$days_held=$sell_date_numberofdays - $buy_date_numberofdays;
		$days_held=(int)$days_held+1;

	        $total_days_held = $total_days_held + ($days_held);

		# To comply with php-4.3.x and up:
		if(!isset($sell_cost_basis_total)) { $sell_cost_basis_total=0; }
		if(!isset($buy_cost_basis_total)) { $buy_cost_basis_total=0; }

	        $sell_cost_basis_total = $sell_cost_basis_total + $sell_cost_basis;
	        $buy_cost_basis_total = $buy_cost_basis_total + $buy_cost_basis;

		# Since $buy_cost_basis is stored as a negative value in the database,
		# I have go do buy*-1 here instead of the usual sell-buy 
	        $profit_dollars = ($sell_cost_basis-($buy_cost_basis*-1));
	        $profit_percentage = ($sell_cost_basis-($buy_cost_basis*-1))/($buy_cost_basis*-1);
	
	        $running_profit_dollars = $running_profit_dollars + $profit_dollars;
		# $running_profit_percentage = $running_profit_percentage + $profit_percentage;

		#######################
		# PRINT DETAIL RECORDS
		$i++;
		if ($i % 2) { 
			echo "<tr bgcolor=eeeeee>";
		} else {
	      		echo "<tr bgcolor=ffffff>";
		}


		# COLUMN:  ACCOUNT
	        print "<td width=5% align=center>&nbsp;";
if($public_consumption=="") {
		print "<font size=-2>";
		print "$account_id";
		print "</font>";
}
		print "</td>";

		# COLUMN:  TICKER
		if (file_exists("../images/logos/$ticker.GIF")) {
			print "<form action=../individual/individual.php method=post>";
			print "<td width=5% align=center>";
			print "<font size=-2>";
			print "<input type=hidden name=days value=21>";
			print "<input type=hidden name=myvar value=$ticker>";
			print "<input type=image height=30 src=../images/logos/$ticker.GIF alt=$ticker>";
			print "</td>";
			print "</font>";
			print "</form>";
		} else {
			print "<td width=5% align=center>";
			print "<font size=-2>";
			print "$ticker";
# 			$buy_date=urlencode($buy_date);
# 			$sell_date=urlencode($sell_date);
# 			print "<a href=portfolio_analysis_2.php?ticker=$ticker&shares=$shares&b=$buy_date&s=$sell_date&$menu_string>";
#			print "<font face=arial size=-3>Details</font>";
# 			print "</a>";
# 			$buy_date=urldecode($buy_date);
# 			$sell_date=urldecode($sell_date);
			print "&nbsp;";
			print "</font>";
			print "</td>";
		}


		# COLUMN:  COMPANY NAME
	        print "<td width=5% align=left>";
		print "<font size=-5>";
		$name=strtoupper($name);
		print "$name";
		print "</font>";
		print "</td>";

		# COLUMN:  SHARES
	        print "<td width=5% align=right>&nbsp;";
		print "<font size=-2>";
if($public_consumption=="") {
		print "$shares";
}
		print "</font>";
		print "</td>";

		# COLUMN:  ENTRY DATE
	        print "<td width=5% align=center>";
		print "<font size=-2>";
		print "$buy_date";
		print "</font>";
		print "</td>";

		# COLUMN:  ENTRY SHARE PRICE
	        $buy_share_price=number_format($buy_share_price,2,".",",");
	        print "<td width=5% align=right>";
		print "<font size=-2>";
		print "$$buy_share_price";
		print "</font>";
		print "</td>";

		# COLUMN:  ENTRY COST
	        $buy_cost_basis=number_format($buy_cost_basis,2,".",",");
	        print "<td width=5% align=right>&nbsp;";
if($public_consumption=="") {
		print "<font size=-2>";
		print "$$buy_cost_basis";
		print "</font>";
}
		print "</td>";

		# COLUMN:  EXIT DATE
	        print "<td width=5% align=center>";
		print "<font size=-2>";
		print "$sell_date";
		print "</font>";
		print "</td>";

		# COLUMN:  EXIT SHARE PRICE
	        $sell_share_price=number_format($sell_share_price,2,".",",");
	        print "<td width=5% align=right>";
		print "<font size=-2>";
		print "$$sell_share_price";
		print "</font>";
		print "</td>";

		# COLUMN:  EXIT COST
	        $sell_cost_basis=number_format($sell_cost_basis,2,".",",");
	        print "<td width=5% align=right>&nbsp;";
if($public_consumption=="") {
		print "<font size=-2>";
		print "$$sell_cost_basis";
		print "</font>";
}
		print "</td>";

		# COLUMN:  DAYS HELD
	        print "<td width=5% align=center>";
		print "<font size=-2>";
		print "$days_held";
		print "</font>";
		print "</td>";

		# COLUMN:  Real PROFIT ($)
	        $profit_dollars=number_format($profit_dollars,2,".",",");
	        print "<td width=5% align=right>&nbsp;";
if($public_consumption=="") {
		if ($profit_dollars>0) {
			print "<font color=000000 size=-2>";
		} else {
			print "<font color=ff0000 size=-2>";
		}
		print "$$profit_dollars";
		print "</font>";
}
		print "</td>";
		if ($profit_dollars>0) {
			$total_dollar_profits = $total_dollar_profits + $profit_dollars;
		} else {
			$total_dollar_losses = $total_dollar_losses + $profit_dollars;
		}

		# COLUMN:  Real PROFIT (%)
	        $profit_percentage=$profit_percentage*100;
	        $profit_percentage=number_format($profit_percentage,2,".",",");
	        print "<td width=5% align=right>";
		if ($profit_percentage>0) {
			print "<font color=000000 size=-2>";
		} else {
			print "<font color=ff0000 size=-2>";
		}
		print "$profit_percentage %";
		print "</font>";
		print "</td>";

	        #print "<td width=5>$running_profit_dollars</td>";
	        #print "<td width=5%>$running_profit_percentage</td>";

		# COLUMN:  ANNUALIZED PROFIT (%)
		$annualized_profit = (365/$days_held) * ($profit_percentage);
	        $annualized_profit=number_format($annualized_profit,2,".",",");
	        print "<td width=5% align=right>";
		if ($annualized_profit>0) {
			print "<font color=000000 size=-2>";
		} else {
			print "<font color=ff0000 size=-2>";
		}
		print "$annualized_profit %";
		print "</font>";
		print "</td>";

	        print "</tr>";

		# Tally accuracy
		if($annualized_profit>0) {
			$winner=$winner+1;
			$total_count=$total_count+1;
		} else {
			$loser=$loser+1;
			$total_count=$total_count+1;			
		}

		# Tally accuracy of 26% Annualized Returns
		if($annualized_profit>26) {
			$winner26percent=$winner26percent+1;
		}

		# Tally days held
		$days_held_total = $days_held_total + $days_held;


		##################################################################
		# Count Averaging Down Accuracy only if these conditions are met:
		# I'm filtering by ticker
		# Buy Date is in Ascending order
		if($ticker==$last_ticker && ($order_by=='ticker' || $order_by=='buy_date') && $sort_order==1 && $last_buy_share_price!=0) {
# && $filter_by_ticker!="" && $order_by=='buy_date'
			# Cut the Losers after -8% loss
			if ($profit_percentage<0 && $profit_percentage > -8.00) {
				$cut_the_losers_winner = $cut_the_losers_winner + 1;
				$cut_the_losers_count = $cut_the_losers_count + 1;
			}
			if ($profit_percentage < -8.00) {
				$cut_the_losers_count = $cut_the_losers_count + 1;
			}
			

			# Averaging Down
			if($buy_share_price<=$last_buy_share_price) {
				$average_down_count = $average_down_count + 1;
				if ($profit_dollars>0) {
					$average_down_winner = $average_down_winner + 1;
				}
			}

			# Ride The Winners
			if ($profit_percentage>0) {
				$ride_the_winners_winner = $ride_the_winners_winner + 1;
				$ride_the_winners_count = $ride_the_winners_count + 1;
			}
			if ($profit_percentage <= 0) {
				$ride_the_winners_count = $ride_the_winners_count + 1;
			}


			# Pyramiding Profits
			if($buy_share_price>=$last_buy_share_price) {
				$pyramiding_profits_count = $pyramiding_profits_count + 1;
				if ($profit_dollars>0) {
					$pyramiding_profits_winner = $pyramiding_profits_winner + 1;
				}
			}
		}
		$last_buy_share_price = $buy_share_price;
		$last_ticker = $ticker;
		#
		##################################################################


	}

	if($ticker==$last_ticker && ($order_by=='ticker' || $order_by=='buy_date') && $sort_order==1 && $last_buy_share_price!=0 ) {
# && $filter_by_ticker!="" && $order_by=='buy_date' 
		print "<b>CARP Analysis (this section only appears when sort by Ticker or Entry Date in ascending order):</b><br>";
		###
		print "Cut The Losers:  $cut_the_losers_winner / $cut_the_losers_count = ";
		if($cut_the_losers_count>0) {
			print Number_Format($cut_the_losers_winner/$cut_the_losers_count*100,2);
		} else {
			# Fixes division by zero error
			print "0.00";
		}
		print "% accuracy<br>";
		###
		print "Averaging Down:  $average_down_winner / $average_down_count = ";
		if($average_down_count>0) {
			print Number_Format($average_down_winner/$average_down_count*100,2);
		} else {
			# Fixes division by zero error
			print "0.00";
		}
		print "% accuracy<br>";
		###
		print "Ride The Winners:  $ride_the_winners_winner / $ride_the_winners_count = ";
		if($ride_the_winners_count>0) {
			print Number_Format($ride_the_winners_winner/$ride_the_winners_count*100,2);
		} else {
			# Fixes division by zero error
			print "0.00";
		}
		print "% accuracy<br>";
		###
		print "Pyramiding Profits:  $pyramiding_profits_winner / $pyramiding_profits_count = ";
		if($pyramiding_profits_count>0) {
			print Number_Format($pyramiding_profits_winner/$pyramiding_profits_count*100,2);
		} else {
			# Fixes division by zero error
			print "0.00";
		}
		print "% accuracy<br>";
		print "<br>";
		###
	}

	print "<b>Real Profit</b> is the true return you received on your investment.<br>";
	print "<b>Annualized Profit</b> is an extrapolated version of your return.  If you make a small profit in a short period of time, repeatedly, you'll end up with huge gains.<br>";

	# Since $buy_cost_basis is stored as a negative value in the database,
	# I have go do buy*-1 here instead of the usual sell-buy 
	$annualized_profit_yield = (($sell_cost_basis_total-($buy_cost_basis_total*-1))/($buy_cost_basis_total*-1))*(((365/($total_days_held))*($number_of_rows)))*100;
	$annualized_profit_yield=number_format($annualized_profit_yield,2,".",",");
	print "<b>Annualized Profit Yield of Round Trips:  $annualized_profit_yield%</b><br>";

if($public_consumption=="") {
	print "<b>Total Buy Cost Basis of Trades:  $" . Number_Format($buy_cost_basis_total, 2) . "</b><br>";
	print "<b>Total Sell Cost Basis of Trades:  $" . Number_Format($sell_cost_basis_total, 2) . "</b><br>";
	$running_profit_dollars=number_format($running_profit_dollars,2,".",",");
	print "<b>Real Profit Dollars of Trades:  $$running_profit_dollars</b><br>";
}

	$profit_percentage = (($sell_cost_basis_total-($buy_cost_basis_total*-1))/($buy_cost_basis_total*-1))*100;
	$profit_percentage=number_format($profit_percentage,2,".",",");
	print "<b>Real Profit Percentage of Trades:  $profit_percentage%<br>";
if($public_consumption=="") {
	print "<br>";

	print "Total Profits:&nbsp; $$total_dollar_profits<br>";
	print "Total Losses:&nbsp; $$total_dollar_losses<br>";
	print "Return-Risk Ratio:&nbsp; ";
	if($total_dollar_losses!=0) {
		if( $total_dollar_profits > ABS($total_dollar_losses) ) {
			print Number_Format(ABS($total_dollar_profits/$total_dollar_losses), 2);
		} else {
			if($total_dollar_profits==0) {
				# Prevents division by zero error
				print "Negative Infinity ";
			} else {
				print Number_Format(($total_dollar_losses/$total_dollar_profits), 2);
			}
		}
		if (ABS($total_dollar_profits/$total_dollar_losses)>5) {
			print ":1 <font color=green>ACCEPTABLE</font>";
		} else {
			print ":1 <font color=red>UNACCEPTABLE - You need to do more homework</font>";
		}
	} else {
		print "Infinity";
	}
	print "<br>";
	print "<small>NOTE:  Buffett's Return-Risk Ratio is over 100:1</small><br>";
	
	print "</b>";

	#########################################
	# Get Dividends
	$sql_dividend_string="
	        SELECT  	ai_transaction.account_id AS account_id
				, ai_transaction.ticker AS ticker
				, ai_transaction.date AS date
				, ai_transaction.net_change AS net_change
				, ai_company.name AS name
	        FROM    	ai_transaction
		INNER JOIN	ai_company
		ON		ai_transaction.ticker = ai_company.ticker
		WHERE		date >= '$sell_date_start'
		AND		date <= '$sell_date_end'
		AND		action = 'Cash Dividend'
		AND		username='$username'
		";

	if($filter_by_ticker!="") {
		$sql_dividend_string .= "
			AND	ai_transaction.ticker = '$filter_by_ticker'
			";
	}

	if($filter_by_account_id!="") {
		$sql_dividend_string .= "
			AND	ai_transaction.account_id = '$filter_by_account_id'
			";
	}

	$sql_dividend_string .= "
		ORDER BY	date
	        ";

	$sql_dividend=db_query("$sql_dividend_string");


	$total_dividends = 0; # Initialize it
	print "<BR><b>Cash Dividends Received:</b><BR>";
	while ($row_dividend = db_fetch_array($sql_dividend) ) {
	        $account_id           	=$row_dividend["account_id"];
	        $ticker               	=$row_dividend["ticker"];
	        $date                 	=$row_dividend["date"];
	        $net_change           	=$row_dividend["net_change"];
	        $name			=$row_dividend["name"];
		print "$account_id $date $ticker " . strtoupper($name) . " $" . Number_Format($net_change, 2) . "<BR>";
		$total_dividends = $total_dividends + $net_change;
	}
	print "<B>TOTAL CASH DIVIDENDS RECEIVED: $" . Number_Format($total_dividends, 2) . "</B><BR>";


	# Get Interest Income
	if($filter_by_ticker=="") {
		# Show this only if I'm not filtering by ticker and account_id
		if($filter_by_account_id=="") {
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Interest Income'
				AND		username='$username'
				ORDER BY	date
			        ");
		} else {
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Interest Income'
				AND		username='$username'
				AND		account_id='$filter_by_account_id'
				ORDER BY	date
			        ");
		}
		$total_interest_income = 0; # Initialize it
		print "<BR><b>Interest Income Received:</b><BR>";
		while ($row_dividend = db_fetch_array($sql_dividend) ) {
		        $account_id           	=$row_dividend["account_id"];
		        $date                 	=$row_dividend["date"];
		        $net_change           	=$row_dividend["net_change"];
			print "$account_id $date " . " $" . Number_Format($net_change, 2) . "<BR>";
			$total_interest_income = $total_interest_income + $net_change;
		}
		print "<B>TOTAL INTEREST INCOME RECEIVED: $" . Number_Format($total_interest_income, 2) . "</B><BR>";
	} else {
		$total_interest_income = 0;  # not used since I'm filtering one stock
	}



	# Get Option Order Rebate
	if($filter_by_ticker=="") {
		if($filter_by_account_id=="") {
			# Show this only if I'm not filtering by ticker and account_id
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Option Order Rebate'
				AND		username='$username'
				ORDER BY	date
			        ");
		} else {
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Option Order Rebate'
				AND		username='$username'
				AND		account_id='$filter_by_account_id'
				ORDER BY	date
			        ");
		}
		$total_option_order_rebate = 0; # Initialize it
		print "<BR><b>Option Order Rebates Received:</b><BR>";
		while ($row_dividend = db_fetch_array($sql_dividend) ) {
		        $account_id           	=$row_dividend["account_id"];
		        $date                 	=$row_dividend["date"];
		        $net_change           	=$row_dividend["net_change"];
			print "$account_id $date " . " $" . Number_Format($net_change, 2) . "<BR>";
			$total_option_order_rebate = $total_option_order_rebate + $net_change;
		}
		print "<B>TOTAL OPTION ORDER REBATES RECEIVED: $" . Number_Format($total_option_order_rebate, 2) . "</B><BR>";
	} else {
		$total_option_order_rebate = 0;  # not used since I'm filtering one stock
	}




	# Get Margin Fee
	if($filter_by_ticker=="") {
		if($filter_by_account_id=="") {
			# Show this only if I'm not filtering by ticker and account_id
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Margin Fee'
				AND		username='$username'
				ORDER BY	date
			        ");
		} else {
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Margin Fee'
				AND		username='$username'
				AND		account_id='$filter_by_account_id'
				ORDER BY	date
			        ");
		}
		$total_margin = 0; # Initialize it
		print "<BR><b>Margin Fees:</b><BR>";
		while ($row_dividend = db_fetch_array($sql_dividend) ) {
		        $account_id           	=$row_dividend["account_id"];
		        $date                 	=$row_dividend["date"];
		        $net_change           	=$row_dividend["net_change"];
			print "$account_id $date " . " $" . Number_Format($net_change, 2) . "<BR>";
			$total_margin = $total_margin - $net_change;
		}
		print "<B>TOTAL MARGIN FEES: $" . Number_Format($total_margin, 2) . "</B><BR>";
	} else {
		$total_margin = 0;  # not used since I'm filtering one stock
	}




	# Get Miscellaneous
	$total_miscellaneous_fee = 0; # Initialize it
	if($filter_by_ticker=="") {
		if($filter_by_account_id=="") {
			# Show this only if I'm not filtering by ticker and account_id
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Miscellaneous Fee'
				AND		username='$username'
				ORDER BY	date
			        ");
		} else {
			$sql_dividend=db_query("
			        SELECT  	ai_transaction.account_id AS account_id
						, ai_transaction.date AS date
						, ai_transaction.net_change AS net_change
			        FROM    	ai_transaction
				WHERE		date >= '$sell_date_start'
				AND		date <= '$sell_date_end'
				AND		action = 'Miscellaneous Fee'
				AND		username='$username'
				AND		account_id='$filter_by_account_id'
				ORDER BY	date
			        ");
		}

		print "<BR><b>Miscellaneous Fee Costs:</b><BR>";
		while ($row_dividend = db_fetch_array($sql_dividend) ) {
		        $account_id           	=$row_dividend["account_id"];
		        $date                 	=$row_dividend["date"];
		        $net_change           	=$row_dividend["net_change"];
			print "$account_id $date " . " $" . Number_Format($net_change, 2) . "<BR>";
			$total_miscellaneous_fee = $total_miscellaneous_fee - $net_change;
		}
		print "<B>TOTAL MISCELLANEOUS FEE COSTS: $" . Number_Format($total_miscellaneous_fee, 2) . "</B><BR>";
	}


}
	# TRADING ACCURACY
	print "<BR>";
	$accuracy=($winner/$total_count)*100;
	$accuracy26percent=($winner26percent/$total_count)*100;
	print "<b>TRADING ACCURACY:  $winner / $total_count = " . Number_Format($accuracy, 2) . "%</b><br><br>";
	# At 26 percent, you double your money every 3 years
	print "<b>TRADING ACCURACY OF 26% ANNUALIZED RETURNS:  $winner26percent / $total_count = " . Number_Format($accuracy26percent, 2) . "%</b><br><br>";

	# AVERAGE DAYS HELD
	$average_days_held = $days_held_total / $total_count;
	print "<b>AVERAGE DAYS HELD:  " . Number_Format($average_days_held, 2) . "</b><br><br>";








	#####################################
	# Calculate turnover per month
	$sell_date_start_month = substr("$sell_date_start", 5, 2);
	$sell_date_start_day  = substr("$sell_date_start", 8, 2);
	$sell_date_start_year = substr("$sell_date_start", 0, 4);

        $sell_date_start_numberofdays = Date_Calc::dateToDays($sell_date_start_day, $sell_date_start_month, $sell_date_start_year);

	$sell_date_end_month = substr("$sell_date_end", 5, 2);
	$sell_date_end_day  = substr("$sell_date_end", 8, 2);
	$sell_date_end_year = substr("$sell_date_end", 0, 4);

        $sell_date_end_numberofdays = Date_Calc::dateToDays($sell_date_end_day, $sell_date_end_month, $sell_date_end_year);

	$days=$sell_date_end_numberofdays - $sell_date_start_numberofdays;
	print "<b>TURNOVER:  " . Number_Format($total_count/($days/365.25*12),2) . " trades per month<br>";





	

if($public_consumption=="") {
	$running_profit_dollars = ereg_replace(",","",$running_profit_dollars);

	# print "'$running_profit_dollars' + '$total_dividends' + '$total_interest_income' + '$total_option_order_rebate' + '$total_margin' + '$total_miscellaneous_fee'<BR>";
	print "<BR><b>NETPROFIT=TradeProfits+Dividends+InterestIncome+OptionOrderRebates-MarginFees-Misc.Fees<br>";
	$net_profit=$running_profit_dollars+$total_dividends+$total_interest_income+$total_option_order_rebate+$total_margin+$total_miscellaneous_fee;
	print "NET PROFIT(LOSS) = $" . Number_Format($net_profit, 2) . " &nbsp; ";
	if($net_profit>=0) { print "+"; } 
	print number_format($net_profit/abs($buy_cost_basis_total)*100,2) . "% &nbsp; (";
	if($net_profit>=0) { print "+"; } 
	print number_format(($net_profit/abs($buy_cost_basis_total)*100)*(365/$total_days_held)*($number_of_rows),2) . "% annualized)</b><bR>";

} # end of public consumption


print "</table>";
print "</td></tr></table>";
print "<font size=-2> Created using <a href=http://www.aistockbot.com>www.aistockbot.com</a></font><br>";
#include_once("{$path}include/footer.php");
?>
