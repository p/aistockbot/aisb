<?php
# portfolio_output_1.php
#
# 2007/08/17  MS  Added Cost Basis
# 2005/01/20  MS  Added another column to split up logo and ticker symbol
# 2004/07/16  MS  Added urlencode($account_id) and urldecode($account_id).   (eg. for Account IDs with spaces in them)
# 2004/07/16  MS  Added sell_date='0000-00-00 00:00:00' query.  Changed '$shares' and '$value' to $shares and $value
# 2004/07/12  MS  Added tests for $username so I don't show everyone's portfolio
# 2004/07/12  MS  Fixed to comply with php-4.3.7
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/30  MS  Added extract($_POST) to get $account_id variable if it's chosen from "View Portfolio" menu item
# 2004/09/10  MS  Checked for SCRIPT_NAME to decide whether to load the header or not, and set $img_src
# 2004/09/09  MS  Added $path
# 2003/05/21  MS  Initial Release
#

$scriptname="portfolio_output_1.php";
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");

# Test for where it's being loaded (eg. as an include (index.php) or standalone (portfolio_output_1.php) )
# Extract reserved PHP variables
extract($_SERVER);
# _SERVER["SCRIPT_NAME"] is a reserved PHP Variable
# Knock off all the superflous subdirectories (eg. /cvs_aistockbot/aistockbot/pms/portfolio_output_1.php)
$script_name = substr(strrchr($SCRIPT_NAME, "/"), 1);
if ($script_name==$scriptname) {
	# Gotta load the header because index.php does it by default.
	include_once("{$path}include/header.php");
}

extract($_POST);  # Extract $account_id if it's chosen from the "View Porfolio" main menu

# Initialize Variables
$grand_total = 0;
$grand_total_profit_loss = 0;
$grand_total_profit_loss_percentage = 0;

# Update Portfolio Values First
$sql_portfolio3=db_query("
	SELECT		ticker, shares
	FROM		ai_portfolio
	WHERE		sell_date = '0000-00-00 00:00:00'
	AND		ticker <> 'ACT_CASH'
	AND		username = '$username'
	");
	# WHERE		sell_cost_basis = 0 # not used because of expired options - used sell_date instead

# 20061111:  Since I use the "Update Prices" feature in AIStockBot, I want to stop doing this 
# because I keep losing current pricing information when I view the main page 
#while ($row_portfolio3 = db_fetch_array($sql_portfolio3)) {
#	$pticker	= $row_portfolio3["ticker"];
#	$shares		= $row_portfolio3["shares"];
#	$sql_history = db_query("
#		SELECT		close, date
#		FROM 		ai_history 
#		WHERE		ticker='$pticker' 
#		ORDER BY 	date DESC
#		LIMIT 1
#		");
#	while ($row_history = db_fetch_array($sql_history)) {
#		$close		= $row_history["close"];
#		# Update Portfolio
#		$value = $shares * $close;
#		# 2004/07/16  MS  Added sell_date='0000-00-00 00:00:00' query.  Changed '$shares' and '$value' to $shares and $value
#		$sql=db_query("
#			UPDATE 	ai_portfolio 
#			SET 	value 	= 	$value
#			WHERE 	ticker	=	'$pticker' 
#			AND 	shares	=	$shares
#			AND	username = 	'$username'
#			AND	sell_date 	= '0000-00-00 00:00:00'
#			");
#	}
#	# After running through all the tickers, I need to reset the pointer so I can run through all the tickers again.
#	# $sqlhistory is a mysql_result set type, not an array, so I need to use this command instead of reset():
#	# mysql_data_seek($sql_history, 0);
#}


# TABLE HEADERS
# LEFT COLUMN - GRAPH
print "<table width=100%>";
print "<tr>";
print "<td width=100% valign=top>";
print "<font face=arial size=+1>";
#print "<b>Portfolio</b>";
print "<font face=arial size=+0>";

if(!isset($account_id)) {
	$account_id="";
}


$account_id=urlencode($account_id);

$username = $a->getUsername();

# Check for folder location
if ($script_name=="{$path}portfolio_output_1.php") {
	$img_src="{$path}portfolio_output_2.php?account_id=$account_id&username=$username";
} else {
	# Looking at it from index.php (main page)
	$img_src="{$path}pms/portfolio_output_2.php?account_id=$account_id&username=$username";
}
$account_id=urldecode($account_id);

#print "<br>";
print "<center>";
print "<img src=$img_src>";
print "</center>";


//	include("{$path}quotes/yahoo_delayed_quote_1.php");

print "</td>";


#print "</tr>";
#print "</table>";
#print "<table width=100%>";
#print "<tr>";



# RIGHT COLUMN - HOLDINGS

print "<td width=50% valign=top>";
print "<table width=100%>";

print "<tr>";
print "<td colspan=7>";
print "<font face=arial size=-1>";
print "<b>Current Holdings - \"The Lowest Average Cost Basis Wins!\" - Bill Miller";
if ($account_id) {
	print " for Portfolio: $account_id";
}
print "</b>";
print "</td>";
print "</tr>";


print "<tr>";

print "<td width=17% bgcolor=ccffcc align=center>";
print "<font face=arial size=-1>";
print "<b>&nbsp;</b>";
print "</font>";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=center>";
print "<font face=arial size=-1>";
print "<b>Ticker</b>";
print "</font>";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=center>";
print "<font face=arial size=-1>";
print "<b>Weight</b>";
print "</font>";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=right>";
print "<font face=arial size=-1>";
print "<b>Shares</b>";
print "</font>";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=right>";
print "<font face=arial size=-1>";
print "<b>Share Price</b>";
print "</font>";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=right>";
print "<font face=arial size=-1>";
print "<b>Cost Basis</b>";
print "</font>";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=right>";
print "<font face=arial size=-1>";
print "<b>Value</b>";
print "</font>";
print "</td>";

print "</tr>";


# Get active stocks
if ($account_id) {
	# For one portfolio
	$sql_portfolio2=db_query("
		SELECT		ticker, SUM(shares) AS total_shares, SUM(value) AS total_value, value/shares AS share_price, SUM(buy_cost_basis) AS total_buy_cost_basis
		FROM		ai_portfolio
		WHERE		sell_date = '0000-00-00 00:00:00'
		AND		ticker <> 'ACT_CASH'
		AND		username='$username'
		AND		account_id = '$account_id'
		GROUP BY	ticker
		");
		# WHERE		sell_cost_basis = 0 # dropped because of expired options - used sell_date instead
} else {
	# Do consolidated report
	$sql_portfolio2=db_query("
		SELECT		ticker, SUM(shares) AS total_shares, SUM(value) AS total_value, value/shares AS share_price, SUM(buy_cost_basis) AS total_buy_cost_basis
		FROM		ai_portfolio
		WHERE		sell_date = '0000-00-00 00:00:00'
		AND		username='$username'
		AND		ticker <> 'ACT_CASH'
		GROUP BY	ticker
		");
		# WHERE		sell_cost_basis = 0 # dropped because of expired options - used sell_date instead
}
# 20061111:  Compute Weight Total first, so I can determine weights.  We'll get the remaining information later
$weight_total=0; # the current amount of cash on hand
$temp=0;
while ($row_portfolio2 = db_fetch_array($sql_portfolio2)) {
	$total_value	= $row_portfolio2["total_value"];
	$weight_total	= $weight_total + $total_value;
	$temp++;
}
if($temp!=0) {
	mysql_data_seek($sql_portfolio2,0);  # resets to beginning of table so I can use it again
}





# Get Cash
if($account_id) {
	# For one portfolio
	$sql_portfolio1=db_query("
		SELECT		SUM(value) AS total_value
		FROM		ai_portfolio
		WHERE		ticker = 'ACT_CASH'
		AND		account_id = '$account_id'
		AND		username='$username'
		");
} else {
	# Do consolidated report
	$sql_portfolio1=db_query("
		SELECT		SUM(value) AS total_value
		FROM		ai_portfolio
		WHERE		ticker = 'ACT_CASH'
		AND		username='$username'
		");
}


while ($row_portfolio1 = db_fetch_array($sql_portfolio1)) {
	$ticker			= 'ACT_CASH';
	# $ticker		= $row_portfolio1["ticker"];
	$total_value		= $row_portfolio1["total_value"];

	$weight_total	= $weight_total + $total_value;  # Add cash into the weight

	print "<tr>";

	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	print "<img height=40 src={$path}images/logos/act_cash_2.gif>";
	print "</font>";
	print "</td>";

	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	print "&nbsp; ";
	print "</font>";
	print "</td>";

	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	$grand_total = $grand_total + $total_value; # This must go above the next 2 lines or addition will get screwed.
	$weight = ($grand_total) / $weight_total;
	print number_format($weight*100,2) . "%";
	print "&nbsp; ";
	print "</font>";
	print "</td>";

	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	print "&nbsp; ";
	print "</font>";
	print "</td>";

	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	print "&nbsp; ";
	print "</font>";
	print "</td>";

	print "<td width=17% align=right>";
	print "<font face=arial size=-1>";
	print "Cash Account:";
	print "</font>";
	print "</td>";

	print "<td width=17% align=right>";
	print "<font face=arial size=-1>";

	$total_value=number_format($total_value,2,".",",");
	print "$$total_value";
	print "</font>";
	print "</td>";

	print "</tr>";
	
	#print "<tr>";
	#print "<td width=100% colspan=5>";
	#print "<hr>";
	#print "</td>";
	#print "</tr>";

}






# GET ACTIVE STOCKS


# Initialize variable
$shares_array = "";
$ticker_array = "";

$i=1; # counter for row color

while ($row_portfolio2 = db_fetch_array($sql_portfolio2)) {
	$ticker		= $row_portfolio2["ticker"];
	$total_shares	= $row_portfolio2["total_shares"];
	$total_value	= $row_portfolio2["total_value"];
	$share_price	= $row_portfolio2["share_price"];
	$total_buy_cost_basis	= $row_portfolio2["total_buy_cost_basis"];

	# Let's take a quick break and make sure the company is activated
	db_query("UPDATE ai_company SET active=1, download=1 WHERE ticker='$ticker'");


#	# The YYYYYY is simply a placecard holder which will be replaced in the next
#	# script with a quotation mark "
#	if ($shares_array=="") {
#		$shares_array   = "$total_shares";
#	} else {
#		$shares_array   = "$shares_array" . "YYYYYY" . "$total_shares";
#	}
#
#	# The ZZZZZZ is simply a placecard holder which will be replaced in the next
#	# script with a quotation mark "
#	if ($ticker_array=="") {
#		$ticker_array   = "ZZZZZZ"."$ticker"."ZZZZZZ";
#	} else {
#		$ticker_array   = "$ticker_array" . "," . "ZZZZZZ" . "$ticker" . "ZZZZZZ";
#	}

	#print "ticker $ticker<br>";
	#print "total shares $total_shares<br>";



	#print "<tr>";
	if ($i % 2) { 
            		echo "<tr bgcolor=eeeeee>"; 
	} else { 
		echo "<tr bgcolor=ffffff>"; 
	} 
	$i++;



	if (file_exists("images/logos/$ticker.GIF")) {
		print "<form action=individual/individual.php method=post>";
		print "<td width=20% align=center bgcolor=ffffff>";
		# print "<img border=0 height=30 src=images/logos/$ticker.GIF alt=$ticker>";
		print "<input type=hidden name=days value=21>";
		print "<input type=hidden name=myvar value=$ticker>";
		print "<input type=image height=30 src=images/logos/$ticker.GIF alt=$ticker>";
		print "</td>";
		print "</form>";
	} else {
		print "<td width=20% height=30 align=center>";
		print "<font face=arial size=-1>";
		print "<a href=pms/add_logo.php?ticker=$ticker target=_blank>Add .GIF Logo</a>";
		print "&nbsp;";
		print "</font>";
		print "</td>";
	}



	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	print "$ticker";
	print "</font>";
	print "</td>";

	print "<td width=17% align=center>";
	print "<font face=arial size=-1>";
	$weight = ($total_shares * $share_price) / $weight_total;
	print number_format($weight*100,2) . "%";
	if($weight>0.40) { print "<font size=-2 color=red><br>Overweighted</font>"; }

	print "</font>";
	print "</td>";

	print "<td width=17% align=right>";
	print "<font face=arial size=-1>";
	print "$total_shares";
	print "</font>";
	print "</td>";

	# Share Price
	print "<td width=17% align=right>";
	print "<font face=arial size=-1>";
	print "$" . number_format($share_price,2,".",",");
	print "</font>";
	print "</td>";


	$grand_total = $grand_total + $total_value; # This must go above the next 2 lines or addition will get screwed.
	$profit_loss = (( ($total_value - ABS($total_buy_cost_basis)) / ABS($total_buy_cost_basis) ) * 100);
	$grand_total_profit_loss = $grand_total_profit_loss + $total_value - ABS($total_buy_cost_basis);

	# Cost Basis
	print "<td width=17% align=right>";
	print "<font face=arial size=-1>";
	$cost_basis = $share_price / (1 + ($profit_loss/100));
	if($cost_basis>$share_price) {
		print "<font color=ff0000>";
	} else {
		print "<font color=006600>";
	}
	print "$" . number_format($cost_basis,2,".",",");
	print "</font>";
	print "</font>";
	print "</td>";

	# Value
	print "<td width=17% align=right>";
	print "<font face=arial size=-1>";
	$total_value=number_format($total_value,2,".",",");
	print "$$total_value<br>";
	if($profit_loss<0) { print "<font color=red size=-2>"; } else { print "<font color=green size=-2>+"; }
	print"" . number_format($profit_loss,2,".",",")  . "%<br>";
	print "</font>";
	print "</font>";
	print "</td>";

	print "</tr>";

	#print "<tr>";
	#print "<td width=100% colspan=4>";
	#print "<hr>";
	#print "</td>";
	#print "</tr>";

}

# Print Grand Total
print "<tr>";

print "<td width=17% bgcolor=ccffcc align=center>";
print "&nbsp; ";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=center>";
print "&nbsp; ";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=center>";
print "&nbsp; ";
print "</td>";

print "<td width=17% bgcolor=ccffcc align=right>";
print "<b>&nbsp; </b>";
print "</td>";

print "<td colspan=2 width=34% bgcolor=ccffcc align=right>";
print "<font face=arial size=-1>";
print "<b>Grand Total: </b><br>";
print "<b>$ Gain (Loss): </b><br>";
print "<b>% Gain (Loss): </b><br>";
print "</font>";
print "</td>";

if($grand_total_profit_loss > 0) {
	$grand_total_profit_loss_percentage = $grand_total_profit_loss / $grand_total * 100;
} else {
	$grand_total_profit_loss_percentage = $grand_total_profit_loss / ($grand_total + (-1 * $grand_total_profit_loss)) * 100;
}
$grand_total=number_format($grand_total,2,".",",");

print "<td width=17% bgcolor=ccffcc align=right>";
print "<font face=arial size=-1><b>$$grand_total</b></font><br>";
print "<font face=arial size=-1><b>$" . number_format($grand_total_profit_loss,2,".",",") . "</b></font><br>";
print "<font face=arial size=-1><b>" . number_format($grand_total_profit_loss_percentage,2,".",",") . "%</b></font>";
print "</td>";

print "</tr>";



print "<tr>";
print "<td width=100% colspan=5>";
print "<hr>";
include("{$path}api/ameritrade/order_form_1.php");
print "<hr>";
include("{$path}quotes/yahoo_delayed_quote_1.php");
print "</td>";
print "</tr>";


# Close tables
print "</table>";

print "</td></tr></table>";

print "<br><br>Note:  This script sets active=1 and download=1 in the company table for each company you own.  Doing this activates the company so it can download current pricing whenever you want.<br>";

if ($script_name==$scriptname) {
	include_once("{$path}include/footer.php");
}
?>
