<?php

#
# transaction_delete_1.php
#
# 2005-02-28  MS  Added ability to backout Miscellaneous Fee transactions
# 2004-12-18  MS  Added "username = '$username'" for several queries
#		  Added LIMIT 1 so the one sql transaction does not delete more than one line item
#		  Added functionality for CASH DIVIDEND  -OR-  OPTION ORDER REBATE  -OR- INTEREST INCOME
# 2004/07/21  MS  Had to clarify some variables to comply with php-4.3.x and up
# 2004/07/13  MS  Fixed so that cash is updated properly for multi-users with multiple portfolios
# 2004/07/12  MS  Had to redo $link because HTTP_REFERER wasn't working for me on php-4.3.7
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/27  MS  Various improvements - still more to do
# 2004/04/16  MS  Initial Release
#
#



#####
# NOTE:
# These are the variables that are passed from transaction_output_1.php:
#
#	print "<input type=hidden name=username value=$username>";
#	print "<input type=hidden name=account_id value=$account_id>";
#	print "<input type=hidden name=date value=$date>";
#	print "<input type=hidden name=time value=$time>";
#	print "<input type=hidden name=action value=$action>";
#	print "<input type=hidden name=ticker value=$ticker>";
#	print "<input type=hidden name=shares value=$shares>";
#	print "<input type=hidden name=price value=$price>";
#	print "<input type=hidden name=commission value=$commission>";
#	print "<input type=hidden name=net_change value=$net_change>";
#
#####

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/database.php");

# Grab the hidden from data from transaction_output_1.php:
extract($_POST);

# 2004/07/21:  To clarify things:
$account_id	= $_POST['account_id'];
$ticker 	= $_POST['ticker'];
$date		= $_POST['date'];
$price		= $_POST['price'];
$net_change	= $_POST['net_change'];
if(!isset($buy_date)) { $buy_date=""; }
if(!isset($buy_share_price)) { $buy_share_price=""; }
if(!isset($buy_cost_basis)) { $buy_cost_basis=""; }



##########
# BOUGHT
##########
if ($action=='Bought') {
	# count rows in portfolio table
	$sql_portfolio_1=db_query("
		SELECT 		id
		FROM		ai_portfolio
	");

	$numrows1=db_num_rows($sql_portfolio_1);

	# check for Bought but not sold yet
	# 2004-12-18:  Added "username = '$username'" and "LIMIT 1" here:
	db_query("
		DELETE FROM	ai_portfolio
		WHERE		username	= '$username'
		AND		account_id	= '$account_id'
		AND		ticker		= '$ticker'
		AND		shares		= '$shares'
		AND		buy_date	= '$date $time'
		AND		buy_share_price	= '$price'
		AND		sell_date	= '0000-00-00 00:00:00'
		AND		sell_share_price= '0.000000'
		AND		sell_cost_basis = '0.000000'
		LIMIT		1
	");
		
	# count rows in portfolio table, again!
	$sql_portfolio_2=db_query("
		SELECT 		id
		FROM		ai_portfolio
	");

	$numrows2=db_num_rows($sql_portfolio_2);

	# This is a lazy way to do it, but it works on low traffic websites
	# note: the ai_portfolio table does not save the time, so I'm using 00:00:00 instead of $time
	if ($numrows1==$numrows2) {
		print "ERROR: numrows1=$numrows1 is equal to numrows2=$numrows2<br>";
		print "No records deleted.<br>";

		print "You must delete the Sold portion of this transaction first.<br>";
		print "The portfolio record does not match a corresponding transaction record.<br>";

		print "data dump:<br>";
		print "account_id=$account_id<br>";
		print "ticker=$ticker<br>";
		print "shares=$shares<br>";
		print "buy_date='$date $time'<br>";
		print "buy_share_price=$price<br>";
		print "buy_cost_basis=$net_change<br>";
		print "<br>";
		print "<br>";
		print "<br>";
		print "<br>";

		exit();
	}

	# delete the transaction
	# 2004-12-18:  Added "username = '$username'" and "LIMIT 1" here:
	db_query("
		DELETE FROM	ai_transaction
		WHERE		username = '$username'
		AND		account_id='$account_id'
		AND		date='$date'
		AND		time='$time'
		AND		action='$action'
		AND		ticker='$ticker'
		AND		shares='$shares'
		AND		price='$price'
		AND		commission='$commission'
		AND		net_change='$net_change'
		LIMIT		1
	");

	# query cash amount
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	$sql_portfolio=db_query("
		SELECT		value
		FROM		ai_portfolio
		WHERE		ticker 		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
		");

	while ($row_portfolio = db_fetch_array($sql_portfolio)) {
		$value		= $row_portfolio["value"];
	}	

	$cash = $value - $net_change;

	# make cash adjustment to portfolio
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	db_query("
		UPDATE		ai_portfolio
		SET		value=$cash
		WHERE		ticker		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
	");


}



###########
# SOLD - Cancel a sale
###########
if ($action=='Sold') {

	# Look into the portfolio and grab the original purchase information
	# 2004-12-18:  Added username = '$username' here:
	$sql_portfolio_sold = db_query("
		SELECT 		buy_date, buy_share_price, buy_cost_basis
		FROM		ai_portfolio
		WHERE		username 	= '$username'
		AND		account_id 	= '$account_id'
		AND		ticker 		= '$ticker'
		AND		shares		= '$shares'
		AND		sell_date 	= '$date $time'
		AND		sell_share_price = '$price'
		");

	while ($row_portfolio = db_fetch_array ($sql_portfolio_sold)) {
		$buy_date	 = $row_portfolio["buy_date"];
		$buy_share_price = $row_portfolio["buy_share_price"];
		$buy_cost_basis  = $row_portfolio["buy_cost_basis"];
	}

/*
	print "<b>DEBUG:  Grabbing original purchase information for:<br>";
	print "DEBUG:  username=$username<br>";
	print "DEBUG:  account_id=$account_id<br>";
	print "DEBUG:  ticker=$ticker<br>";
	print "DEBUG:  shares=$shares<br>";
	print "DEBUG:  sell_date $date $time<br>";
	print "DEBUG:  sell_share_price $price<br>";
	print "DEBUG:  sell_cost_basis $net_change<br>";

	print "DEBUG:  buy_date=$buy_date<br>";
	print "DEBUG:  buy_share_price=$buy_share_price<br>";
	print "DEBUG:  buy_cost_basis=$buy_cost_basis<br>";
#	exit();
*/

	# Check for an existing open UNSOLD item in the portfolio.  If there is one, UPDATE.  If not, INSERT.
	# 2004-12-18:  Added username = '$username' here:
	$sql_portfolio_test=db_query("
		SELECT		shares AS untouched_shares, buy_cost_basis AS untouched_buy_cost_basis
		FROM		ai_portfolio
		WHERE		username	= '$username'
		AND		account_id 	= '$account_id'
		AND		ticker 		= '$ticker'
		AND		buy_date 	= '$buy_date'
		AND		buy_share_price = '$buy_share_price'
		AND		sell_date 	= '0000-00-00 00:00:00'
		AND		sell_share_price = '0.000000'
		AND		sell_cost_basis	= '0.000000'
		");

	$numrows=db_num_rows($sql_portfolio_test);

/*
	print "<b>DEBUG:  Grabbing original purchase information for:<br>";
	print "DEBUG:  account_id=$account_id<br>";
	print "DEBUG:  ticker=$ticker<br>";
	print "DEBUG:  sell_date $date $time<br>";
	print "DEBUG:  sell_share_price $price<br>";
	print "DEBUG:  and sell_cost_basis $net_change<br>";
	print "DEBUG:  buy_date=$buy_date<br>";
	print "DEBUG:  buy_share_price=$buy_share_price<br>";
	print "DEBUG:  buy_cost_basis=$buy_cost_basis<br>";
	print "numrows=$numrows<br>";
	exit();
*/

	if ($numrows==1) {
		# UPDATE
		while ($row_portfolio_test = db_fetch_array ($sql_portfolio_test)) {
			$untouched_shares	  = $row_portfolio_test["untouched_shares"];
			$untouched_buy_cost_basis = $row_portfolio_test["untouched_buy_cost_basis"];
		}

		$revised_shares = $untouched_shares + $shares; # Add the existing unsold shares to the shares I've cancelled
		$buy_cost_basis = $untouched_buy_cost_basis + $buy_cost_basis;


		# Checks for an existing open UNSOLD item in the portfolio.  If there is one, UPDATE.  If not, INSERT.

		##################
		# 2004/07/21:   Made changes to the following SQL statement:
		#		SET   - Removed account_id='$account_id'
		#		WHERE - Added username='$username'
		#		WHERE - Added account_id='$account_id
		db_query("
			UPDATE		ai_portfolio
			SET		shares 		= $revised_shares
					, buy_cost_basis=$buy_cost_basis
			WHERE		username	= '$username'
			AND		account_id 	= '$account_id'
			AND		ticker 		= '$ticker'
			AND		buy_date 	= '$buy_date'
			AND		buy_share_price = '$buy_share_price'
			AND		sell_date 	= '0000-00-00 00:00:00'
			AND		sell_share_price= '0.000000'
			AND		sell_cost_basis	= '0.000000'
			");
	} else {
		# Checks for an existing open UNSOLD item in the portfolio.  If there is one, UPDATE.  If not, INSERT.

		# INSERT
		# Create the new portfolio record 
		# 2004/07/21:   Added username to this SQL statement:
		db_query("
			INSERT INTO	ai_portfolio
				(username
				, account_id
				, ticker
				, shares
				, buy_date
				, buy_share_price
				, buy_cost_basis
				, sell_date
				, sell_share_price
				, sell_cost_basis)
			VALUES
				('$username'
				, '$account_id'
				, '$ticker'
				, '$shares'
				, '$buy_date'
				, '$buy_share_price'
				, '$buy_cost_basis'
				, '0000-00-00 00:00:00'
				, '0.000000'
				, '0.000000')
		");
	}




	# Delete the transaction
	# 2004-12-18:  Added "username = '$username'" and "LIMIT 1" here:
	db_query("
		DELETE FROM	ai_transaction
		WHERE		username = '$username'
		AND		account_id='$account_id'
		AND		date='$date'
		AND		time='$time'
		AND		action='$action'
		AND		ticker='$ticker'
		AND		shares='$shares'
		AND		price='$price'
		AND		commission='$commission'
		AND		net_change='$net_change'
		LIMIT		1
	");

	# query cash amount
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	$sql_portfolio=db_query("
		SELECT		value
		FROM		ai_portfolio
		WHERE		ticker 		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
		");

	while ($row_portfolio = db_fetch_array($sql_portfolio)) {
		$value		= $row_portfolio["value"];
	}	

	$cash = $value - $net_change;

	# make cash adjustment to portfolio
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	db_query("
		UPDATE		ai_portfolio
		SET		value		= $cash
		WHERE		ticker		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
	");


/*
	print "<b>DEBUG:  Grabbing original purchase information for:<br>";
	print "DEBUG:  username=$username<br>";
	print "DEBUG:  account_id=$account_id<br>";
	print "DEBUG:  ticker=$ticker<br>";
	print "DEBUG:  shares=$shares<br>";
	print "DEBUG:  sell_date $date $time<br>";
	print "DEBUG:  sell_share_price $price<br>";
	print "DEBUG:  sell_cost_basis $net_change<br>";

	print "DEBUG:  buy_date=$buy_date<br>";
	print "DEBUG:  buy_share_price=$buy_share_price<br>";
	print "DEBUG:  buy_cost_basis=$buy_cost_basis<br>";
	print "DEBUG:  numrows=$numrows<br>";
#	exit();
*/

	# delete from portfolio
	# 2004-12-18:  Added "username = '$username'" and "account_id = '$account_id'" and "LIMIT 1" here:
	db_query("
		DELETE FROM	ai_portfolio
		WHERE		username 	= '$username'
		AND		account_id 	= '$account_id'
		AND		ticker		= '$ticker'
		AND		shares		= '$shares'
		AND		sell_date	= '$date $time'
		AND		sell_share_price= '$price'
		LIMIT		1
	");

}


#################################################################################
# DEPOSIT  -OR-  CASH DIVIDEND  -OR-  OPTION ORDER REBATE  -OR- INTEREST INCOME
# All 4 of these increment cash
#################################################################################

if(( $action == "Deposit" ) || ( $action == "Cash Dividend" ) || ( $action =="Option Order Rebate" ) || ( $action == "Interest Income" )) {
	# delete the transaction
	# 2004-12-18:  Added "username = '$username'" and "LIMIT 1" here:
	db_query("
		DELETE FROM	ai_transaction
		WHERE		username='$username'
		AND		account_id='$account_id'
		AND		date='$date'
		AND		time='$time'
		AND		action='$action'
		AND		ticker='$ticker'
		AND		shares='$shares'
		AND		price='$price'
		AND		commission='$commission'
		AND		net_change='$net_change'
		LIMIT		1
	");

	# query cash amount
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	$sql_portfolio=db_query("
		SELECT		value
		FROM		ai_portfolio
		WHERE		ticker 		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
		");

	while ($row_portfolio = db_fetch_array($sql_portfolio)) {
		$value		= $row_portfolio["value"];
	}	

	$cash = $value - $net_change;

	# make cash adjustment to portfolio
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	db_query("
		UPDATE		ai_portfolio
		SET		value=$cash
		WHERE		ticker		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
	");
}



#################################################################################
# MISCELLANEOUS FEE
# All 1 of these decrement cash
#################################################################################

if( ($action == "Miscellaneous Fee") || ($action == "Margin Fee") ) {
	# delete the transaction
	# 2004-12-18:  Added "username = '$username'" and "LIMIT 1" here:
	db_query("
		DELETE FROM	ai_transaction
		WHERE		account_id='$account_id'
		AND		username='$username'
		AND		date='$date'
		AND		time='$time'
		AND		action='$action'
		AND		ticker='$ticker'
		AND		shares='$shares'
		AND		price='$price'
		AND		commission='$commission'
		AND		net_change='$net_change'
		LIMIT		1
	");

	# query cash amount
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	$sql_portfolio=db_query("
		SELECT		value
		FROM		ai_portfolio
		WHERE		ticker 		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
		");

	while ($row_portfolio = db_fetch_array($sql_portfolio)) {
		$value		= $row_portfolio["value"];
	}	

	if($net_change>0) {
		$cash = $value + $net_change;
	} else {
		$cash = $value - $net_change;
	}

	# make cash adjustment to portfolio
	# 2004/07/13:  Changed into multi-user/multi-portfolio mode by checking for username and account_id
	db_query("
		UPDATE		ai_portfolio
		SET		value=$cash
		WHERE		ticker		= 'ACT_CASH'
		AND		username	= '$username'
		AND		account_id	= '$account_id'
	");
}








if (($action!='Bought') AND ($action!='Sold') AND ($action!='Deposit') AND ($action!='Cash Dividend') AND ($action!='Option Order Rebate') AND ($action!='Interest Income') AND ($action!='Margin Fee') AND ($action!='Miscellaneous Fee')) {
	# The above functions are already setup	
	include_once("{$path}include/header.php");
	print "Functionality for Action: <b>$action</b> is not setup yet.";
	exit();
} 


# 2004/07/12:   Having problems using HTTP_REFERER on a windows box, so I'm going to
# 		redirect to transaction_output_1.php
#$link=$_SERVER["HTTP_REFERER"];
# print "link=$link";
#header("Location: $link");
#

$query_string=$_SERVER["QUERY_STRING"];

$link="{$path}pms/transaction_output_1.php?$query_string";
header("Location: $link");
?>

