<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	manage_accounts_1.php
# Authors:	Nick Kuechler
#############################################################################
#
# 2004/07/13  MS  Checks to make sure user is logged in first
# 2004/07/13  MS  Changed to pass URL variables to next page (to maintain login)
# 2004/07/12  MS  Conforms to php-4.3.x now
# 2004/05/12  NK  Now displays the portfolio for an individual user.
#                 Must be logged in to use manage accounts system.
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/22  NK  Created file
##############################################################################

# add new brokerage account
# - select broker from dropdown list populated by list of brokers
# - enter account number / account id
# - enter password (store as md5 encrypted, for future use)
# edit brokerage account
# - change password
# delete brokerage account
# - close account

# TABLE ai_account (
#  username varchar(50) NOT NULL default '',
#  account_id varchar(255) NOT NULL default '',
#  account_name varchar(255) NOT NULL default '',
#  description varchar(255) NOT NULL default '',
#  contact_name varchar(255) NOT NULL default '',
#  phone_number varchar(255) NOT NULL default '',
#  broker_username varchar(255) NOT NULL default '',
#  broker_password varchar(255) NOT NULL default '',
#  notes longtext NOT NULL
#) TYPE=MyISAM;


include("../include/config.php");

############
# 2004/07/12:  Vooch took this out because the authentication process is engaged
# when include/header.php is included (as shown below).  php-4.3.x shows numerous
# errors if you attempt to run these lines twice.  We do not need this:
#
#
#ini_set("include_path", $pear_location . ":" . ini_get("include_path"));
#require_once("Auth/Auth.php");

#$params = array(
#         "dsn" => "mysql://$global_username:$global_password@$global_hostname/$global_database",
#        "table" => "ai_auth",
#        "usernamecol" => "username",
#        "passwordcol" => "password"
#        );

#$a = new Auth("DB", $params, "$loginFunction");
#$a->start();

#if ($a->getAuth()) {
#  //print "USER IS ALREADY AUTHORIZED";
#############



if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add an account.<br>";
	exit();
}

//print "USER IS ALREADY AUTHORIZED";
print "<b>Portfolio Accounts for ". $username .":</b><br>";

# Get a list of all accounts
$get_account=db_query("
        SELECT          *
        FROM            ai_account
	WHERE		username = '$username'
        ORDER BY        account_id
        ");

#$show_columns=db_query("
#	SHOW		columns
#	FROM		ai_account
#	");


# display the list of accounts
#echo "<form method=\"post\" action=\"". $_SERVER['PHP_SELF'] ."\">";
#echo "<input type=\"hidden\" name=\"updatesite\" value=\"". $site ."\">";
print "<table border=1>\n";
print "\t<tr>\n";

# display column names
#while ($row = db_fetch_row($show_columns)) {
#  print "\t\t<td><b>$row[0]</b></td>\n";
#  $col_arr[] = $row[0];
#}
#print "\t</tr>\n";



while ($line = mysql_fetch_array($get_account, MYSQL_ASSOC)) {
  print "\t<tr>\n";
  $i = 0;
  $acct_id = $line["account_id"];

  foreach ($line as $col_value) {
    //if ($i >= 2) {
      print "\t\t<td>". $col_value ."</td>\n";
      #print "\t\t<td><input type=\"text\" name=\"update". $col_arr[$i] ."\" value=\"". $col_value ."\"></td>\n";
    //} else {
      //print "\t\t<td>acct_id:". $col_value ."</td>\n";
    //}
    //$i++;
  }
  print "\t\t<td><a href=\"{$path}pms/manage_accounts_2.php?edit=1&acct_id=". $acct_id ."\">Edit</a></td>";
  print "\t\t<td><a href=\"{$path}pms/manage_accounts_2.php?del=1&acct_id=". $acct_id ."\">Del</a></td>";
  print "\t</tr>\n";
}

print "</table>\n";
#echo "<input type=\"submit\" value=\"UPDATE\">";
#echo "</form>";

print "<br><br>";

print "<b>Add New Account</b>:<br>";
$query_string=$_SERVER["QUERY_STRING"];

# To comply with php-4.3.7 and up:
if(!isset($query_string)) { $query_string=""; }

print "<form method=post action={$path}pms/manage_accounts_2.php?addacct=1&$query_string>";
print "<table border=0>";
echo "<tr>";
	echo "<td align=right>";
	echo "Account ID:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"account_id\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Account Name:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"account_name\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Description:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"description\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Contact Name:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"contact_name\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Phone Number:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"phone_number\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Username:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"broker_username\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Password:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"broker_password\"><br>";
	echo "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td align=right>";
	echo "Notes:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"notes\"><br>";
	echo "</td>";
echo "</tr>";
echo "</table>";
echo "<input type=\"submit\" value=\"ADD NEW ACCT\">";
echo "</form>";

include_once("{$path}include/footer.php");

#} else {
#  print "NOT LOGGED IN";
#}
?>
