<?php

#
# portfolio_analysis_2.php
#
# 2004/07/17  MS  Initial Release
#
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

extract($_GET);

# TABLE: TRANSACTION
# account_id   	varchar(255)	
# date  	date
# time  	time
# action  	varchar(10)
# ticker  	varchar(10)
# shares  	decimal(15,6)
# price  	decimal(15,6)
# commission  	decimal(15,6)
# net_change  	decimal(15,6)
# notes  	longtext

$ticker	=$_GET["ticker"];
$shares =$_GET["shares"];
$b	=$_GET["b"]; # Buy Date in format:  YYYY-MM-DD HH:II:SS
$buy_date = substr("$b",0,10); # YYYY-MM-DD
$buy_time = substr("$b",11,8); # HH:II:SS
$s	=$_GET["s"]; # Sell Date in format:  YYYY-MM-DD HH:II:SS
$sell_date = substr("$s",0,10); # YYYY-MM-DD
$sell_time = substr("$s",11,8); # HH:II:SS

# I don't use shares on the buy side, but I use them on the sell side.
$buy_transaction=db_query("
	SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
	FROM		ai_transaction
	WHERE		ticker 	= '$ticker'
	AND		date 	= '$buy_date'
	AND		time 	= '$buy_time'
	AND		username= '$username'
	");

$sell_transaction=db_query("
	SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
	FROM		ai_transaction
	WHERE		ticker 	= '$ticker'
	AND		shares	= $shares
	AND		date 	= '$sell_date'
	AND		time 	= '$sell_time'
	AND		username= '$username'
	");

# Get the shares sold, so I can use it for the Trade Entry side.  Otherwise, I'll be using a bad number.
$sql_shares_sold=db_query("
	SELECT		shares
	FROM		ai_transaction
	WHERE		ticker 	= '$ticker'
	AND		shares	= $shares
	AND		date 	= '$sell_date'
	AND		time 	= '$sell_time'
	AND		username= '$username'
	");
while ($row_sell_transaction = db_fetch_array($sql_shares_sold)) {
	$shares_sold	= $row_sell_transaction["shares"];
}

print "<table width=100% border=1>";

# TRADE ENTRY

while ($row_buy_transaction = db_fetch_array($buy_transaction)) {
	$account_id	= $row_buy_transaction["account_id"];
	$date		= $row_buy_transaction["date"];
	$time		= $row_buy_transaction["time"];
	$action		= $row_buy_transaction["action"];
	$ticker		= $row_buy_transaction["ticker"];
	$shares		= $row_buy_transaction["shares"];
	$price		= $row_buy_transaction["price"];
	$commission	= $row_buy_transaction["commission"];
	# $net_change	= $row_buy_transaction["net_change"];
	$notes		= $row_buy_transaction["notes"];

	print "<tr>";
	print "<td colspan=2 bgcolor=ffeeee><font face=arial size=-1><b>$ticker Trade Entry</b></font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Account ID:</font></td>";
	print "<td width=80%><font face=arial size=-1>$account_id</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Trade Date:</font></td>";
	print "<td width=80%><font face=arial size=-1>$action on $date $time</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Shares:</font></td>";
	print "<td width=80%><font face=arial size=-1>$shares_sold</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Price:</font></td>";
	print "<td width=80%><font face=arial size=-1>$$price</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Commission:</font></td>";
	print "<td width=80%><font face=arial size=-1>$$commission</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Net Change:</font></td>";
	# print "<td width=80%>$net_change</td>"; # incorrect because if you sold a portion of it, you get the wrong number.  This shows the original whole purchase amount.
	$entry_net_change=(($shares_sold*$price)+$commission)*-1;
        $entry_net_change=number_format($entry_net_change,2,".",",");
	print "<td width=80%><font face=arial size=-1>$$entry_net_change</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right valign=top bgcolor=eeeeee><font face=arial size=-1>Notes:</font></td>";
	$notes=ereg_replace("\015","<br>",$notes);
	print "<td width=80%>&nbsp;<font face=arial size=-1>$notes</font></td>";
	print "</tr>";
}

# TRADE EXIT

while ($row_sell_transaction = db_fetch_array($sell_transaction)) {
	$account_id	= $row_sell_transaction["account_id"];
	$date		= $row_sell_transaction["date"];
	$time		= $row_sell_transaction["time"];
	$action		= $row_sell_transaction["action"];
	$ticker		= $row_sell_transaction["ticker"];
	$shares		= $row_sell_transaction["shares"];
	$price		= $row_sell_transaction["price"];
	$commission	= $row_sell_transaction["commission"];
	$net_change	= $row_sell_transaction["net_change"];
	$notes		= $row_sell_transaction["notes"];

	print "<tr>";
	print "<td colspan=2 bgcolor=eeffee><font face=arial size=-1><b>$ticker Trade Exit</b></font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Account ID:</font></td>";
	print "<td width=80%><font face=arial size=-1>$account_id</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Trade Date:</font></td>";
	print "<td width=80%><font face=arial size=-1>$action on $date $time</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Shares:</font></td>";
	print "<td width=80%><font face=arial size=-1>$shares</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Price:</font></td>";
	print "<td width=80%><font face=arial size=-1>$$price</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Commission:</font></td>";
	print "<td width=80%><font face=arial size=-1>$$commission</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right bgcolor=eeeeee><font face=arial size=-1>Net Change:</font></td>";
	# print "<td width=80%>$net_change</td>"; # incorrect because if you sold a portion of it, you get the wrong number.  This shows the original whole purchase amount.
	$exit_net_change=(($shares_sold*$price)+$commission);
        $exit_net_change=number_format($exit_net_change,2,".",",");
	print "<td width=80%><font face=arial size=-1>$$exit_net_change</font></td>";
	print "</tr>";

	print "<tr>";
	print "<td width=20% align=right valign=top bgcolor=eeeeee><font face=arial size=-1>Notes:</font></td>";
	$notes=ereg_replace("\015","<br>",$notes);
	print "<td width=80%>&nbsp;<font face=arial size=-1>$notes</font></td>";
	print "</tr>";
}


print "<tr>";
print "<td colspan=2 bgcolor=eeeeff><font face=arial size=-1><b>Summary</b></font></td>";
print "</tr>";

print "<tr>";
print "<td colspan=2 width=100%><font face=arial size=-1>";
$difference=$exit_net_change+$entry_net_change;
$net=$difference/$entry_net_change*100;
$net=number_format($net,2,".",",");
if ($difference>=0) {
	print "You made $$difference for a total profit of $net%";
} else {
	$net=$net*-1;
	print "<font face=arial color=ff0000><b>You LOST $$difference for a total LOSS of $net%. ";
	if (($net < 0) AND ($net >= -4)) {
		print "AT LEAST YOU USED SOME MONEY MANAGEMENT SKILLS.<br>";
	}
	if (($net < -4) AND ($net >= -14)) {
		print "DO BETTER NEXT TIME!<br>";
	}
	if (($net < -14) AND ($net >= -24)) {
		print "KEEP THIS UP, AND YOU'LL BE BROKE!<br>";
	}
	if (($net < -24) AND ($net >= -34)) {
		print "I HATE WHEN YOU LOSE ME MONEY!<br>";
	}
	if ($net < -34) {
		print "YOU FUCKING PIECE OF SHIT!!!<br>";
	}
	print "</b></font>";
}
print "</td>";
print "</tr>";

print "</table>";

include_once("{$path}include/footer.php");
?>

