<?php

#
# transaction_output_balance_1.php
#
# I used transaction_output_1.php to get started
#
# 2006/03/11  MS  Initial Release
#
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))

include_once("{$path}include/database.php");

include_once("{$path}include/header.php");

extract($_GET);

# $start_date and $end_date should appear in the URL, but if it doesn't use this:
if ( !isset($start_date) OR !isset($end_date) ) {
	$start_date=date("Y");
	$start_date="$start_date"."-01-01";
	$end_date=date("Y-m-d");
}

if(!isset($_GET['PHPSESSID'])) { $_GET['PHPSESSID']=""; }

print "<form action={$path}pms/transaction_output_balance_1.php>";
print "<font face=arial size=+0>";
print "<b>Cash Balance Report<br></b>";
print "</font>";
print "<font face=arial size=-1>";
print "<input type=hidden name=PHPSESSID value=$_GET[PHPSESSID]>";
print "From: <input type=text name=start_date value=$start_date> ";
print " &nbsp; To: <input type=text name=end_date value=$end_date> ";
print " &nbsp; ";
print "<br>";

print "Filter by Ticker: <input type=text name=ticker> &nbsp; ";
print "Filter by Account: ";
print "<select name=account_id>";
print "<option value=></option>";
	$sql_temp=db_query("
		SELECT		DISTINCT account_id
		FROM		ai_account
		WHERE		username='$username'
		");
	while ($row_temp = db_fetch_array($sql_temp)) {
		$temp	= $row_temp["account_id"];
		print "<option value=$temp "; if(isset($_GET["account_id"])) { if ($temp==$_GET["account_id"]) { print " SELECTED "; } }    print ">$temp</option>";
	}
print "</select>";
print " &nbsp; ";
print "<br>";

if(isset($_GET["starting_balance"])) {
	$starting_balance = $_GET["starting_balance"];
} else {
	$starting_balance = 0; # default
}

$balance = $starting_balance;
print "Starting Balance: <input type=text name=starting_balance value=$starting_balance> &nbsp; ";

print "<input type=submit value=Submit><br><br>";
print "</form>";






























# TABLE: TRANSACTION
# account_id   	varchar(255)	
# date  	date
# time  	time
# action  	varchar(10)
# ticker  	varchar(10)
# shares  	decimal(15,6)
# price  	decimal(15,6)
# commission  	decimal(15,6)
# net_change  	decimal(15,6)
# notes  	longtext

if(!isset($ticker)) {
	# Do nothing
} else {
	# Gotta wipe it out if user doesn't want to Filter by ticker and click that "Submit" button
	if($ticker=="") { unset($ticker); }
}

if(!isset($ticker)) {
	# No filter - just dates
	$string="
		SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
		FROM		ai_transaction
		WHERE		date >= '$start_date'
		AND		date <= '$end_date'
		AND		username='$username'
		";

	if (isset($_GET["account_id"])) { 
		$string = $string . " AND	account_id = '$_GET[account_id]' ";
	}

	$string = $string . " ORDER BY	date ASC, time ASC";

	$sql_transaction=db_query("$string");
} else {
	# Filter based on ticker symbol
	$string="
		SELECT		account_id, date, time, action, ticker, shares, price, commission, net_change, notes
		FROM		ai_transaction
		WHERE		date >= '$start_date'
		AND		date <= '$end_date'
		AND		username='$username'
		AND		ticker = '$ticker'
		";

	if (isset($_GET["account_id"])) { 
		$string = $string . " AND	account_id = '$_GET[account_id]' ";
	}

	$string = $string . " ORDER BY	date ASC, time ASC";

	$sql_transaction=db_query("$string");
}

# Print Headers

print "<table width=100% border=1 cellspacing=0 cellpadding=3 bordercolor=dddddd>";
print "<tr>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Account ID</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Date/Time</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Action</b></td>";

print "<td width=11% bgcolor=abcdef align=center>";
print "<font face=arial size=-1>";
print "<b>Ticker</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Shares</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Price</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Commission</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Net Change</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Cash Balance</b></td>";
print "</tr>";

$i=1; # row counter

$dividends_paid = 0; # default
$interest_income = 0 ; # default
$securities_bought = 0 ; # default
$securities_sold = 0 ; # default
$miscellaneous_fee = 0 ; # default

$cpt=0; # for graph

while ($row_transaction = db_fetch_array($sql_transaction)) {
	$account_id	= $row_transaction["account_id"];
	$date		= $row_transaction["date"];
	$time		= $row_transaction["time"];
	$action		= $row_transaction["action"];
	$ticker		= $row_transaction["ticker"];
	$shares		= $row_transaction["shares"];
	$price		= $row_transaction["price"];
	$commission	= $row_transaction["commission"];
	$net_change	= $row_transaction["net_change"];
	$notes		= $row_transaction["notes"];

	# Print Detail
	$i++;
	if ($i % 2) { 
      		echo "<tr bgcolor=ffffff>"; 
	} else { 
		echo "<tr bgcolor=eeeeee>"; 
	} 

	print "<td width=11% valign=top>";
	print "<font face=arial size=-1>";
	print " $account_id";
	print "</td>";

	print "<td width=11%>";
	print "<font face=arial size=-2>";
	print "$date $time</td>";

	print "<td width=11%>";
	print "<font face=arial size=-1>";
	print "$action</td>";

	print "<td width=11% align=center>";
	print "<font face=arial size=-1>";
	if ($ticker!="") {
		print "$ticker</td>";
	} else {
		print "&nbsp;";
	}

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$shares</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$$price</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$$commission</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	if($action!="Miscellaneous Fee") {
		print "$$net_change";
	} else {
		print "$-$net_change";
	}
	if($action=="Cash Dividend") { $dividends_paid = $dividends_paid + $net_change; }
	if($action=="Interest Income") { $interest_income = $interest_income + $net_change; }
	if($action=="Bought") { $securities_bought = $securities_bought + $net_change; }
	if($action=="Sold") { $securities_sold = $securities_sold + $net_change; }
	if($action=="Miscellaneous Fee") { $miscellaneous_fee = $miscellaneous_fee + $net_change; }
	print "</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	if($action!="Miscellaneous Fee") {
		if($action=="Withdrawl") {
			$balance = $balance - $net_change;
		} else {
			$balance = $balance + $net_change;
		}
	} else {
		$balance = $balance - $net_change;
	}

	print "$" . Number_Format($balance,2);
	print "</td>";




#  graph stuff:
	$xdata[$cpt]=$date;
	$ydata[$cpt]=$balance; # y-axis (closing price)
	# $y2data[$cpt]=(int)$x; # Example of 2nd y-axis
	$cpt++; # Normal method



/*
	print "<td width=11% align=center>";
	print "<font face=arial size=-1>";
	$query_string=$_SERVER["QUERY_STRING"];
	print "<form action={$path}pms/transaction_delete_1.php?$query_string method=post>";
	print "<input type=hidden name=username value=\"$username\">";
	print "<input type=hidden name=account_id value=\"$account_id\">";
	print "<input type=hidden name=date value=$date>";
	print "<input type=hidden name=time value=$time>";
	print "<input type=hidden name=action value=\"$action\">";
	print "<input type=hidden name=ticker value=$ticker>";
	print "<input type=hidden name=shares value=$shares>";
	print "<input type=hidden name=price value=$price>";
	print "<input type=hidden name=commission value=$commission>";
	print "<input type=hidden name=net_change value=$net_change>";
	print "<input type=submit value=Delete>";
	print "&nbsp;</td>";
*/
	# This </form> was placed at the end of the row to tighten the amount of space used.
	# Otherwise, we would have some fat rows
	print "</form>"; 

	# End of row:
	print "</tr>";




# 2004/07/18  MS  I took this out because this feature is now available in "Current Holdings - Detail".  However, I am
#		  keeping it here just in case you want to see it.
#	# Extra row for notes:
#	if (strlen($notes)>0) {
#		print "<font face=arial size=-2>";
#		if ($i % 2) { 
#     			echo "<tr bgcolor=ffffff>"; 
#		} else { 
#			echo "<tr bgcolor=eeeeee>"; 
#		} 
#		print "<td colspan=10>";
#		print "<font face=arial size=-2>";
#		print "$notes";
#		print "</td>";
#		print "</tr>";
#	}
  

}

# Force to top of page using this trick:
print "<table width=100% cellspacing=0 cellpadding=0>";
print "<tr><td>";

	print "<br>";
	print "<b>Summary:</b><br>";
	print "Starting Balance = $$starting_balance<br>";
	print "Dividends Paid = $$dividends_paid<br>";
	print "Interest Income = $$interest_income<br>";
	print "Securities Bought = $$securities_bought<br>";
	print "Securities Sold = $$securities_sold<br>";
	print "Miscellaneous Fee = $$miscellaneous_fee<br>";
	print "Ending Balance = $$balance<br>";
	print "<br>";

print "</td>";
print "<td>";

	print "<img align=right src={$path}pms/transaction_output_balance_2.php?PHPSESSID=$_GET[PHPSESSID]&account_id=$account_id&starting_balance=$starting_balance&start_date=$start_date&end_date=$end_date>";

print "</td>";
print "</tr>";
print "</table>";








if($account_id=="") {
	print "<b>ERROR:  You must choose an account using the filter.</b><br><br>";
}

print "</table>";



include_once("{$path}include/footer.php");
?>

