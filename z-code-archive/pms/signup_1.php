<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	signup_1.php
# Authors:	Nick Kuechler
#############################################################################
#
# 2004/07/12  MS  Fixed bug... password2 wasn't specified on the form
#		  Also, checked to make sure the passwords match
# 2004/05/08  NK  Created file
##############################################################################

# TABLE ai_auth (
#  username varchar(50) NOT NULL default '',
#  password varchar(32) NOT NULL default '',
#)

$path="../"; # we're one directory from main
include("{$path}include/header.php");

# Get a list of all accounts
#$get_account=db_query("
#        SELECT          *
#        FROM            ai_account
#        ORDER BY        account_id
#        ");

#$show_columns=db_query("
#	SHOW		columns
#	FROM		ai_account
#	");


if (isset($_GET['addacct'])) {

    $username = $_POST["username"];
    $password = $_POST["password"];
    $password2 = $_POST["password2"];
    $email = $_POST["email"];
    $name = $_POST["name"];

    if ( empty($username) || empty($password) ) {
      print "<br><br>";
      print "Missing username or password.";

    } else {
	if ($password==$password2) {
		$update_account=db_query("
	        	INSERT INTO     ai_auth
	                          (username,
	                          password,
		    		  email,
				  name)
		  	VALUES	  ('". $username ."',
				  md5('". $password ."'),
				  '". $email ."',
				  '". $name ."')
		  	");
	      	print "Added username: ". $username ." to user list.<br>";
	} else {
		print "Passwords do not match.  User not created.<br>";
	}
    }

} else {

  # display the list of accounts
  print "<br><br>";

  print "<b>Add New Account</b>:<br>";
  echo "<form method=\"post\" action=\"signup_1.php?addacct=1&\">";
  print "<table border=0>";
  echo "<tr>";
	echo "<td align=right>";
	echo "Username:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"username\"><br>";
	echo "</td>";
  echo "</tr>";
  echo "<tr>";
	echo "<td align=right>";
	echo "Password:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"password\" name=\"password\"><br>";
	echo "</td>";
  echo "</tr>";
  echo "<tr>";
	echo "<td align=right>";
	echo "Password (again):";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"password\" name=\"password2\"><br>";
	echo "</td>";
  echo "</tr>";
  echo "<tr>";
	echo "<td align=right>";
	echo "E-mail:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"email\"><br>";
	echo "</td>";
  echo "</tr>";
  echo "<tr>";
	echo "<td align=right>";
	echo "Name:";
	echo "</td>";
	echo "<td>";
	echo "<input type=\"text\" name=\"name\"><br>";
	echo "</td>";
  echo "</tr>";
  echo "</table>";
  echo "<input type=\"submit\" value=\"SIGN-UP\">";
  echo "</form>";

  include("{$path}include/footer.php");

}

?>
