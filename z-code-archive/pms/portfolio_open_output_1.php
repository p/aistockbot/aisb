<?php

# portfolio_open_output_1.php
#
# 20070826  MS  Added Positive Returns Probability
# 20070510  MS  Added a "Never sell Your Best Performers to buy Unknowns" recommendation.  Added a new column that calcuates Annualized Gain based upon a 26% annualized profit deman.  Our goal is to double our money every 3 years.  My hope is to alert myself to laggards so I can reinvestigate and see why they are not performing.
# 20061011  MS  Added Monthly Turnover Rate
# 20060515  MS  Fixed problem with $notes appearing on more than one ticker 
# 20041218  MS  Removed repetitive username, account_id, ticker for ticker symbols having multiple listings
# 20040718  MS  Added a "show notes" feature
# 20040716  MS  Added profit/loss percentage
# 20040712  MS  Added username column (just for clarity)
# 20040712  MS  Test for $username now
# 20040712  MS  Added account_id column
# 20040712  MS  Conforms to php-4.3.x standards
# 20040509  FS  Changed include mechanism and $path variable
# 20040506  FS  Changed to use functions from include/database.inc
# 20040429  MS  Removed most of the detail for ACT_CASH since it's irrelevant
#

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");
include_once("{$path}Date-1.3/Date/Calc.php");

# Headers
print "<table width=100% border=1 cellspacing=0 cellpadding=0 bordercolor=dddddd>";



#################################################
# 20060322:  Added this section for Wash Sale Rules, so I don't end up buying something within 30 days of selling it.
print "<tr>";
print "<td colspan=13 align=center>";
print "<font face=arial size=+0>";
print "<b><font color=red>WASH SALE RULES</b><br>";
print "<i>(Do not buy these stocks within 30 days of the sell period if you do not trade marked-to-market. You should wait at least 30 days.  Consult with your accountant or financial planner for more information.)</i></font></b><BR>";

$end_of_today = date("Y-m-d 23:59:59");

$year	= date("Y");
$month	= date("m");  
if ($month==1) { $month=12; $year=$year-1; } else { $month=$month-1; } # one month ago
if ($month==1) { $month=12; $year=$year-1; } else { $month=$month-1; } # two months ago
if ( strlen($month)==1 ) { $month = "0" . "$month"; }
$day	= date("d");

$sixty_days_ago = "$year" . "-". "$month" . "-" . "$day 00:00:00"; 

$sql_wash=db_query("
	SELECT		ticker, date, time, price
	FROM		ai_transaction
	WHERE		date >= '$sixty_days_ago'
	AND		date <= '$end_of_today'
	AND		action = 'Sold'
	AND		username='$username'
	GROUP BY	ticker, date
	ORDER BY	date ASC, time ASC
	");

print " &nbsp; &nbsp; - ";

$turns_sixty_days = 0;					# Used for Monthly Turnover Rate below

while ($row_wash = db_fetch_array($sql_wash)) {
	$wash_ticker	= $row_wash["ticker"];
	$date		= $row_wash["date"];
	$price 		= $row_wash["price"];
	$price = number_format($price,2,".",",");
	print "[ <b><font color=blue>$wash_ticker</font></b> <font size=-2>was sold on</font> <b><font color=green>$date</font></b> <font size=-2>for</font> <b><font color=red>$$price</font></b> ] - ";
	$turns_sixty_days = $turns_sixty_days + 1;  	# Used for Monthly Turnover Rate below
}
print "</font>";
print "</td>";
print "</tr>";




#################################################
# Compare Turnover last 12 months versus last 60 days
$year_ago = $year-1 . "-". "$month" . "-" . "$day 00:00:00"; 

$sql_turns=db_query("
	SELECT		count(ticker) AS turns_year_ago
	FROM		ai_transaction
	WHERE		date >= '$year_ago'
	AND		date <= '$end_of_today'
	AND		action = 'Sold'
	AND		username='$username'
	");
while ($row_turns = db_fetch_array($sql_turns)) {
	$turns_year_ago	= $row_turns["turns_year_ago"];
}
print "<tr>";
print "<td colspan=13 align=center>";
print "<font face=arial size=+0>";
print "<b><font color=red><b>MONTHLY TURNOVER RATE</b><br>";
print "<font face=arial size=-1 color=black>";
print "Last 12 months = " . $turns_year_ago/12 . " trades per month<br>";
print "Last 2 months = " . $turns_sixty_days/2 . " trades per month<br>";
if($turns_sixty_days/2 > $turns_year_ago/12*1.10) {
	print "<b><font color=red>WARNING!<br>";
	print "You might be over trading since your Turnover Rate is rising by more than 10%.<br>";
	print "Try waiting ";
	print ceil(30/($turns_sixty_days-($turns_year_ago/12*2))) . " days before exiting your next trade.<br>";
}

print "</b>";
print "Peter Lynch said it took 3-4 years to get really high returns on some of his picks.<br>";
print "Assuming 4 year holding period (4 years * 12 months = 48 months) and 1% weight for each purchase,<br>";
print "then I should make only 2 trades per month as my Target Monthly Turnover Rate.<br>";

print "</td>";
print "</tr>";




#################################################
print "<tr>";
print "<td colspan=7>";
print "<font face=arial size=-1>";
print "<b>Current Holdings - Detail </b><br>";
if(!isset($show_notes)) { $show_notes=0; }
if($show_notes==1) {
	print "<a href=portfolio_open_output_1.php?show_notes=0$menu_string>hide notes</a>";
} else {
	print "<a href=portfolio_open_output_1.php?show_notes=1$menu_string>show notes</a>";
	print "<font size=-2> - We HIGHLY recommend you view your notes.  You get to see what you thought when you entered your trade and compare it to what you think today.<br>";
}
print "</td>";

print "<td colspan=6 bgcolor=ffaaaaa align=center>";
print "<font face=arial size=-1>";
print "<b>F O C U S &nbsp; O N &nbsp; T H I S &nbsp; S E C T I O N</b>";
print "</td>";
print "</tr>";



print "<tr>";

print "<td width=8% bgcolor=ccffcc align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Username</b>";
print "</font>";
print "</td>";

print "<td width=8% bgcolor=ccffcc align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Account</b>";
print "</font>";
print "</td>";

print "<td width=8% bgcolor=ccffcc align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Ticker</b>";
print "</font>";
print "</td>";

print "<td width=8% bgcolor=ccffcc align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Purchase<br>Date</b>";
print "</font>";
print "</td>";

print "<td width=8% bgcolor=ccffcc align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Shares</b>";
print "</font>";
print "</td>";



print "<td width=9% bgcolor=bcdefa align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Current<br>Share<br>Price</b>";
print "</font>";
print "</td>";

print "<td width=9% bgcolor=bcdefa align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Purchase<br>Share<br>Price</b>";
print "</font>";
print "</td>";




print "<td width=4% bgcolor=badecf align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Days<br>Held<br>ToDate</b>";
print "</font>";
print "</td>";




print "<td width=8% bgcolor=fedcba align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Current Value</b>";
print "</font>";
print "</td>";

print "<td width=8% bgcolor=fedcba align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Purchase<br>Cost<br>Basis</b>";
print "</font>";
print "</td>";


print "<td width=10% bgcolor=fabcde align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Profit/Loss on Paper</b>";
print "</font>";
print "</td>";

print "<td width=10% bgcolor=fabcde align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Annualized Profit/Loss on Paper</b>";
print "</font>";
print "</td>";


print "<td width=10% bgcolor=fabcde align=center valign=bottom>";
print "<font face=arial size=-1>";
print "<b>Annualized Profit/Loss on Paper with Forced 26% Annualized Profit Demands</b>";
print "</font>";
print "</td>";

print "</tr>";

$sql_portfolio=db_query("
	SELECT		account_id, ticker, shares, value, buy_date, buy_share_price, buy_cost_basis
	FROM		ai_portfolio
	WHERE		sell_date = '0000-00-00 00:00:00'
	AND		username='$username'
	ORDER BY	account_id, ticker, buy_date DESC
	");

$i=0; # counter for row color
$profit_loss_total = 0;
$days_held = 0;
$days_held_total = 0; # counter to get average days held across the board
$trades_held_total = 0; # counter to get average days held across the board.  Later, if the account is ACT_CASH, I need to subtract one from this total.
$buy_cost_basis_total = 0;
$winners=0;
$recommendation="";
$notes="";

while ($row_portfolio = db_fetch_array($sql_portfolio)) {
	$account_id		= $row_portfolio["account_id"];
	$ticker			= $row_portfolio["ticker"];
	$shares			= $row_portfolio["shares"];
	$value			= $row_portfolio["value"];
	$buy_date		= $row_portfolio["buy_date"];
	$buy_share_price	= $row_portfolio["buy_share_price"];
	$buy_cost_basis		= $row_portfolio["buy_cost_basis"];

	# Conforms to php-4.3.x standards:
	if (!isset($last_account_id)) { $last_account_id=""; }
	if (!isset($last_ticker)) { $last_ticker=""; }

	if ($last_ticker!=$ticker) { $i++; } # increment counter for row's color

	# Detail
	if ($i % 2) { 
		echo "<tr bgcolor=eeeeee>";
	} else {
      		echo "<tr bgcolor=ffffff>";
	}




	# COLUMN:  USERNAME
	print "<td width=8% align=center>";
	print "<font face=arial size=-2>";
	if (($last_ticker!=$ticker) OR ($ticker=="ACT_CASH")) {
		print "$username";
	} else {
		print "&nbsp;";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  ACCOUNT
	print "<td width=8% align=center>";
	print "<font face=arial size=-2>";
	if (($last_ticker!=$ticker) OR ($last_account_id!=$account_id)) {
		print "$account_id";
	} else {
		print "&nbsp;";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  TICKER
	print "<td width=8% align=center>";
	print "<font face=arial size=-2>";
	if (($last_ticker!=$ticker) OR ($ticker=="ACT_CASH")) {
		print "$ticker";
	} else {
		print "&nbsp;";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  PURCHASE DATE
	print "<td width=8% align=right>&nbsp;";
	print "<font face=arial size=-2>";
	if ($ticker!="ACT_CASH") {
		print "$buy_date";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  SHARES
	print "<td width=8% align=right>&nbsp;";
	print "<font face=arial size=-2>";
	if ($ticker!="ACT_CASH") {
		print "$shares";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  CURRENT SHARE PRICE
	print "<td width=9% align=right>&nbsp;";
	print "<font face=arial size=-2>";
	if ($shares>0) {
		$value_divided_by_shares = $value / $shares;
		$value_divided_by_shares = number_format($value_divided_by_shares,2,".",",");
	} else {
		$value_divided_by_shares = 0; # for ACT_CASH
	}
	if ($ticker!="ACT_CASH") {
		print "<a href=http://finance.yahoo.com/q/ta?s=$ticker&t=1d&l=on&z=l&q=l&p=&a=v&c= target=_blank>$$value_divided_by_shares</a>&nbsp;";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  PURCHASE SHARE PRICE
	print "<td width=9% align=right>&nbsp;";
	print "<font face=arial size=-2>";
	if ($ticker!="ACT_CASH") {
		# $buy_share_price = number_format($buy_share_price,2,".",",");
		print "$$buy_share_price";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  DAYS HELD
	print "<td width=4% align=center>&nbsp;";
	if ($ticker!="ACT_CASH") {
		$buy_date_month = substr("$buy_date", 5, 2);
		$buy_date_day  = substr("$buy_date", 8, 2);
		$buy_date_year = substr("$buy_date", 0, 4);
	        $buy_date_numberofdays = Date_Calc::dateToDays($buy_date_day, $buy_date_month, $buy_date_year);
		$today_date=date("Y-m-d");
		$today_date_month = substr("$today_date", 5, 2);
		$today_date_day  = substr("$today_date", 8, 2);
		$today_date_year = substr("$today_date", 0, 4);
	        $today_date_numberofdays = Date_Calc::dateToDays($today_date_day, $today_date_month, $today_date_year);
		$days_held=$today_date_numberofdays - $buy_date_numberofdays;
		$days_held=(int)$days_held+1;
		if ($days_held>1825) { 
			# Red... over 5 years old.  Time to get out if its a loser
			print "<font face=arial size=-1 color=ff0000><b>";
		} else {
			if($days_held>1460) {
				# Orange
				print "<font face=arial size=-1 color=orange><b>";
			} else {
				if($days_held>1095) {
					# yellow
					print "<font face=arial size=-1 color=yellow><b>";
				} else {
					if($days_held>730) {
						# green
						print "<font face=arial size=-1 color=009900><b>";
					} else {
						if($days_held>365) {
							# blue
							print "<font face=arial size=-1 color=0000FF><b>";
						} else {
							print "<font face=arial size=-1 color=000000>";
						}
					}
				}
			}
		}
		print "$days_held";
		print "</font>";
	}
	print "</font>";
	print "</td>";


	# COLUMN:  CURRENT VALUE
	print "<td width=8% align=right>&nbsp;";
	print "<font face=arial size=-1>";
	$value = number_format($value,2,".",",");
	print "$$value&nbsp;";
	$value = ereg_replace(",","",$value);
	print "</font>";
	print "</td>";


	# COLUMN:  PURCHASE COST BASIS
	print "<td width=8% align=right>&nbsp;";
	print "<font face=arial size=-1>";
	$buy_cost_basis = $buy_cost_basis * -1;
	$buy_cost_basis = number_format($buy_cost_basis,2,".",",");
	if ($ticker!="ACT_CASH") {
		print "$$buy_cost_basis&nbsp;";
	}
	$buy_cost_basis = ereg_replace(",","",$buy_cost_basis);
	print "</font>";
	print "</td>";


	# COLUMN:  PAPER PROFIT/LOSS
	print "<td width=10% align=right>&nbsp;";
	print "<font face=arial size=-1>";
	$profit_loss=$value-$buy_cost_basis;
	if ($profit_loss>=0) {
		print "<font color=007700>";
		if( ($ticker!="ACT_CASH") ) {
			print "+";
		}
	} else {
		print "<font color=ff0000>";
	}
	if ($ticker!="ACT_CASH") {
		print "$" . number_format($profit_loss,2,".",",") . "&nbsp;";

		print "<br>";
		$profit_loss_percentage=($profit_loss/$buy_cost_basis)*100;
		$profit_loss_percentage = number_format($profit_loss_percentage,2,".",",");
		if (($profit_loss_percentage>100) OR ($profit_loss_percentage<-4)) {
			print "<b>$profit_loss_percentage%&nbsp;</b><br>";
		} else {
			if($profit_loss>0) {
				print "+$profit_loss_percentage%&nbsp;<br>";
			} else {
				print "$profit_loss_percentage%&nbsp;<br>";
			}
		}
	}
	print "</font>";
	print "</font>";
	print "</td>";


	# COLUMN:  PAPER ANNUALIZED RETURN (%)
	print "<td width=10% align=right>&nbsp;";
	if ($ticker!="ACT_CASH") {
		print "<font face=arial size=-1>";
		$annualized_profit = (365/$days_held) * ($profit_loss_percentage);
		if ($annualized_profit>=0) {
			print "<font color=007700>+";
		} else {
			print "<font color=ff0000>";
		}
		if (($annualized_profit>200) OR ($annualized_profit<-100)) {
		        $annualized_profit=number_format($annualized_profit,2,".",",");
				print "<b>$annualized_profit%</b>&nbsp;";
		} else {
		        $annualized_profit=number_format($annualized_profit,2,".",",");
			print "$annualized_profit%&nbsp;";
		}
		print "</font>";
	}
	print "</td>";



	# COLUMN:  PAPER ANNUALIZED RETURN (%) BUT THIS TIME DEMANDING A 26% RETURN ON INVESTMENT PER YEAR
	print "<td width=10% align=right>&nbsp;";
	if ($ticker!="ACT_CASH") {
		print "<font face=arial size=-1>";
		$annualized_profit_26 =  (($days_held/365) * -26) + $profit_loss_percentage;
		if ($annualized_profit_26>=0) {
			print "<font color=007700>+";
		} else {
			print "<font color=ff0000>";
		}
		if (($annualized_profit_26 > 200) OR ($annualized_profit_26 < -100)) {
		        $annualized_profit_26=number_format($annualized_profit_26,2,".",",");
				print "<b>$annualized_profit_26%</b>&nbsp;";
		} else {
		        $annualized_profit_26=number_format($annualized_profit_26,2,".",",");
			print "$annualized_profit_26%&nbsp;";
		}
		print "</font>";
	}
	print "</td>";



	# End of row:
	print "</tr>";



	# AISTOCKBOT RECOMMENDATION FOR THIS STOCK
	if ($ticker!='ACT_CASH' && $last_ticker!="") {

		##################################################################
		# Recommendation #1:  Add onto the position for every -18% decline.
		#                     Jeremy Siegel says stocks, on average, have
		#                     a one standard deviation of 18% per year, so
		#                     let's average down only at that level, so we
		#                     average down too many times.
		$get_additions=db_query("
			SELECT		count(ticker) AS additions
			FROM		ai_portfolio
			WHERE		sell_date 	= '0000-00-00 00:00:00'
			AND		username 	= '$username'
			AND		ticker 		= '$ticker'
			");

		while ($row_additions = db_fetch_array($get_additions)) {
			$additions	= $row_additions["additions"];
		}

		$volatility = 12;  # default.  Note:  It's only 7 so it gives several recommendations.

		if(isset($_GET["volatility"])) {
			$volatility = $_GET["volatility"];
		}

		# Average Down recommendation
		if($profit_loss_percentage < ($volatility * -1 * $additions) ) {
			$recommendation = $recommendation . "<font color=ff0000>You've got $additions position(s) open and since you're losing $profit_loss_percentage" . "% on this position, it's time to Average Down by buying another $" . number_format($shares*$buy_share_price,2,".",",") . " worth of $ticker stock.  </font>";
		}

		# Pyramid Profits recommendation
		# NOTE:  At first, I had $annualized_profit < 20, but then I realized that I would always be limiting myself, so I added an OR test with another formula
#		if ( ( $profit_loss_percentage > ($volatility * 0.5 * $additions) ) && ( ($annualized_profit < ($volatility*($additions+1)) || ($profit_loss_percentage<20) || ($annualized_profit<20) )   ) ) {
		if ( ( $profit_loss_percentage > ($volatility * 1.0 * $additions) ) && ( ($annualized_profit < ($volatility*($additions+1)) || ($profit_loss_percentage<20) || ($annualized_profit<20) )   ) ) {

			$recommendation = $recommendation . "<font color=006600>You've got $additions position(s) open and since you're making $profit_loss_percentage" . "% on this position, it's time to Pyramid Profits by buying another $" . number_format($shares*$buy_share_price,2,".",",") . " worth of $ticker stock.  </font>";
		}

		if ($annualized_profit_26 > 0) {
			$recommendation = $recommendation . "  <font color=006600>Never sell your Best Performers, like this stock, to buy Unknowns.</font>";
		}

		if($recommendation!="") {
			if ($i % 2) { 
				echo "<tr bgcolor=eeeeee>";
			} else {
		      		echo "<tr bgcolor=ffffff>";
			}
			print "<td>&nbsp;</td>";
			print "<td>&nbsp;</td>";
			print "<td>&nbsp;</td>";
			print "<td colspan=10>";
			print "<font face=arial size=-2>";
	
			print "RECOMMENDATION (based on volatility=$volatility):<br><font size=-1><b>$recommendation</b></font><br>";
			print "<a href=http://$_SERVER[HTTP_HOST]"."$_SERVER[SCRIPT_NAME]?"."$_SERVER[QUERY_STRING]&volatility=" . ($volatility + 1) . ">less recommendations</a>";
			print " &nbsp; <a href=http://$_SERVER[HTTP_HOST]"."$_SERVER[SCRIPT_NAME]?"."$_SERVER[QUERY_STRING]&volatility=" . ($volatility - 1) . ">more recommendations</a>";
	#phpinfo(); exit();
			print "</font>";
			print "</td>";
			print "</tr>"; 
		}
		$recommendation=""; # reset
	} 



	# Extra row for Notes
	if($show_notes==1) {
		if ($i % 2) { 
			echo "<tr bgcolor=eeeeee>";
		} else {
	      		echo "<tr bgcolor=ffffff>";
		}
		print "<td colspan=12 align=right>";
		print "<font face=arial size=-2>";
		$get_notes=db_query("
			SELECT		notes
			FROM		ai_transaction
			WHERE		username	= '$username'
			AND		account_id 	= '$account_id'
			AND		ticker		= '$ticker'
			AND		shares 		= '$shares'
			AND		price 		= '$buy_share_price'
			AND		action		= 'Bought'
			");
		while ($row_notes = db_fetch_array($get_notes)) {
			$notes	= $row_notes["notes"];
		}
		if ($notes!=" " && $notes!="") {
			print "<form action=notes_update_2.php method=post>";
			print "<input type=hidden name=username value=\"$username\">";
			print "<input type=hidden name=account_id value=\"$account_id\">";
			print "<input type=hidden name=ticker value=\"$ticker\">";
			print "<input type=hidden name=shares value=\"$shares\">";
			print "<input type=hidden name=price value=\"$buy_share_price\">";
			print "<input type=hidden name=action value=\"Bought\">";
			$notes_length = strlen($notes);
			if( (round($notes_length/110,0)+1) > (substr_count($notes, ':')/2) ) {
				$notes_rows = round($notes_length/110,0)+1;
			} else {
				$notes_rows = substr_count($notes, ':')/2;
			}
			print "&nbsp;<textarea name=notes cols=110 rows=$notes_rows>$notes</textarea>&nbsp;";
			print "<input type=submit value=Update>";
			#$notes=ereg_replace("\015","<br>",$notes);
			#print "$notes";
			print "</form>";
		} else {
			# Here, I'm just going to add a space to the note, so it appears it's adding the note.
			print "<form action=notes_update_2.php method=post>";
			print "<input type=hidden name=notes value=\"  \">";
			print "<input type=hidden name=username value=\"$username\">";
			print "<input type=hidden name=account_id value=\"$account_id\">";
			print "<input type=hidden name=ticker value=\"$ticker\">";
			print "<input type=hidden name=shares value=\"$shares\">";
			print "<input type=hidden name=price value=\"$buy_share_price\">";
			print "<input type=hidden name=action value=\"Bought\">";
			print "<input type=submit value=\"AddNote\">";
			print "</form>";
		}
		print "</font>";
		print "</td>";
		print "</tr>";
	}

	$notes = "";  # reset $notes
	$last_account_id=$account_id;
	$last_ticker=$ticker;

	$days_held_total = $days_held_total + $days_held;
	$trades_held_total = $trades_held_total + 1;

	if ($ticker=='ACT_CASH') {
		# Don't count the cash account as a trade for accuracy percentage calculation
		$trades_held_total = $trades_held_total - 1;
	}

	if ($ticker!="ACT_CASH") {
		$profit_loss_total = $profit_loss_total + $profit_loss;
	}

	if ($ticker!="ACT_CASH") {
		$buy_cost_basis_total = $buy_cost_basis_total + $buy_cost_basis;
	} else {
		$buy_cost_basis_total = $buy_cost_basis_total + $value;
	}

	if ($profit_loss>0 && $ticker!='ACT_CASH') {
		$winners = $winners+1;
	}

}

print "</table>";

print "<font face=verdana size=+0>";

if ($trades_held_total>0) {
	$average_days_held = $days_held_total / $trades_held_total;
	#$average_days_held = number_format($average_days_held,2,".",",");
	print "<b>Average Number of Days Held = " . number_format($average_days_held,2,".",",") . " (your target is either 1095, 1460, or 1825 days)</b><br>";
}

$percentage_profit_loss_of_held = $profit_loss_total / $buy_cost_basis_total;
$percentage_profit_loss_of_held=$percentage_profit_loss_of_held*100;

$buy_cost_basis_total = number_format($buy_cost_basis_total,2,".",",");
print "<b>Buy Cost Basis Total (includes Cash) = $ $buy_cost_basis_total</b><br>";

$winning_percentage_of_held = ($winners/$trades_held_total)*100;
if($winning_percentage_of_held < 30) {
	print "<font color=ff0000><b>The market is kicking your ass right now!  You need to fight back!<br>
	Add cash to your account ASAP, start buying stocks, and Averaging Down your positions... Now!</b></font><br>";
}
if($winning_percentage_of_held > 80) {
	print "<font color=009900><b>Your stock picking accuracy is over 80% right now.  Is the market getting overheated?
	If it is, you should start selling some stocks... Now!</b></font><br>";
}
$winning_percentage_of_held = number_format($winning_percentage_of_held,1,".",",");
print "<b>Accuracy of stocks held ($winners for $trades_held_total) = $winning_percentage_of_held%</b>";

if($average_days_held > 1) { $positive_returns_probability = 51.2; }
if($average_days_held > 7) { $positive_returns_probability = 53; }
if($average_days_held > 15) { $positive_returns_probability = 54; }
if($average_days_held > 30) { $positive_returns_probability = 57; }
if($average_days_held > 45) { $positive_returns_probability = 58; }
if($average_days_held > 60) { $positive_returns_probability = 60; }
if($average_days_held > 90) { $positive_returns_probability = 63; }
if($average_days_held > 180) { $positive_returns_probability = 67; }
if($average_days_held > 270) { $positive_returns_probability = 70; }
if($average_days_held > 360) { $positive_returns_probability = 73; }
if($average_days_held > 730) { $positive_returns_probability = 74; }
if($average_days_held > 1095) { $positive_returns_probability = 77; }
if($average_days_held > 1460) { $positive_returns_probability = 80; }
if($average_days_held > 1825) { $positive_returns_probability = 82; }
if($average_days_held > 2190) { $positive_returns_probability = 87; }
if($average_days_held > 2555) { $positive_returns_probability = 90; }
if($average_days_held > 2920) { $positive_returns_probability = 92; }
if($average_days_held > 3285) { $positive_returns_probability = 97; }
if($average_days_held > 3650) { $positive_returns_probability = 99; }

print "<b> (your target is $positive_returns_probability%)</b><br>";

if($winning_percentage_of_held < $positive_returns_probability) {
	print "Since you've held for an average of " . number_format($average_days_held,2,".",",") . " days, your  
		accuracy should be $positive_returns_probability%.
		Unfortunately, you are not beating this target, which means either:<br>
		(1) you are selling your winners too quickly and overpaying for unknowns.  You should Ride The Winners;<br>
		(2) you are not Averaging Down properly.  Buy some stocks with a Margin of Safety now, or;<br>
		(3) you made a mistake (Sell your mistakes), or;<br>
		(4) the market has a negative market risk present.  Mr. Market is depressed.  Suggestions:<br>
		 &nbsp; (a) Buy bonds, Treasuries, or other income-generating assets;<br>
		 &nbsp; (b) Hedge by buying securities in International markets if you're heavily concentrated in your country;<br>
		 &nbsp; (c) Hold and wait it out, or;<br>
		 &nbsp; (d) Sell some stocks if it's a Bear Market (over 45 or 60 days old) since Bear Markets take all stocks down for on average 540 days.<br>";
}
if($winning_percentage_of_held > $positive_returns_probability) {
	print "Since you've held for an average of " . number_format($average_days_held,2,".",",") . " days, your 
		accuracy should be $positive_returns_probability%.
		Fortunately, you are beating this target, however;  you should:<br>
		(1) take some profits here since the market is probably more euphoric than it should be;<br>
		(2) sell your \"higher-priced AND longest held\" profitable holdings to lower your cost basis<br>";
}







if( ($winning_percentage_of_held<50) AND ($trades_held_total>6) ) { 
	print "You'll probably have to hold these longer.  If you have mistakes, sell them now or start selling them slowly.";
}
print "<br>";

if ($percentage_profit_loss_of_held>0) {
	print "<font color=007700>";
	print "<b>Profit/Loss of Stocks Held = $ $profit_loss_total ";
	$percentage_profit_loss_of_held = number_format($percentage_profit_loss_of_held,2,".",",");
	print "$percentage_profit_loss_of_held%</b><br>";
	print "</font>";
} else {
	print "<font color=ff0000>";
	print "<b>Profit/Loss of Stocks Held = $ $profit_loss_total ";
	$percentage_profit_loss_of_held = number_format($percentage_profit_loss_of_held,2,".",",");
	print "($percentage_profit_loss_of_held%)</b><br>";
	print "</font>";
}

print "</font>";

include_once("{$path}include/footer.php");
?>
