<?php
#
# import_2.php
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add a transaction.<br>";
	exit();
}






#############################
# STEP 1:  WRITE THE FILE TO THE HARDDRIVE
if(isset($_FILES['file_ai_auth']['name'])) {
	# file_ai_auth SECTION
	$file_ai_auth_temporary_name = $_FILES['file_ai_auth']['tmp_name']; # temporary filename
	$file_ai_auth_name = $_FILES['file_ai_auth']['name'];  # real filename
	# Verify filetype:
	$file_ai_auth_length = strlen($file_ai_auth_name);
	$file_ai_auth_starting_position = $file_ai_auth_length-4;
	$file_ai_auth_test = substr("$file_ai_auth_name", $file_ai_auth_starting_position, 4);
	print "DEBUG: tmp_name = " . $_FILES['file_ai_auth']['tmp_name'] . "<br>";
	print "DEBUG: name = " . $_FILES['file_ai_auth']['name'] . "<br>";
	print "DEBUG: base_dir = $base_dir<br>";
	if ( ($file_ai_auth_test == ".csv") ) {
	        # OK
	} else {
	        print "<font size=+0><b>File Upload Status = <font color=ff0000>FAILED</font></b></font><br><br>";
	        print "Only <b>.csv</b> files can be uploaded at this time.<br>";
	        print "Also, the max filesize limit is set to 524,288 bytes<br><br></center>"; # per php.ini?
	        # Check /var/log/httpd/error_log for filesize errors if this page does not show on the browser correctly.
	        # Example:  Requested content-length of 888683 is larger than the configured limit of 524288
	        exit();
	}
	
	if (is_uploaded_file($_FILES['file_ai_auth']['tmp_name'])) {
	        move_uploaded_file($file_ai_auth_temporary_name, "$base_dir" . "\\pms\\import\\" . "$file_ai_auth_name");
	        #chmod("$file_ai_auth_name", 0444);
	}
	############
	# STEP 2:  OPEN FILE FOR READING
	if(strstr(PHP_OS,'WIN')){ 
		$filename_import = "import\\" . "$file_ai_auth_name";
	} else {
		$filename_import = "import/" . "$file_ai_auth_name";
	}
	$fp = fopen ("$filename_import", "r");

	while ($data=fgetcsv($fp, 7000, ",")) {
		if ( ($data[0] != "") ) {
			$x_username			= $data[0];
			$x_password 		= $data[1];
			$x_email			= $data[2];
			$x_name		 		= $data[3];
			if($username == "$x_username") {
			# Only allows users to upload their own content
				$table_columns = "username,password,email,name";
				$table_values = "'$x_username','$x_password','$x_email','$x_name'";
				$sqlq = "INSERT INTO ai_auth ($table_columns) VALUES ($table_values)";
				# To comply with php-4.3.7 and up
				if(!isset($sqlq)) { $sqlq=""; }
				$result = db_query($sqlq) or die (mysql_error());
				print "Added...<br>";
				print "DEBUG:  u = $x_username<br>";
				print "DEBUG:  p = $x_password<br>";
				print "DEBUG:  e = $x_email<br>";
				print "DEBUG:  n = $x_name<br>";
			}
		}
	}
	print "Completed Successfully!<BR>";
	fclose($fp);	
}

# to do...........
if(isset($_FILES['file_ai_account']['name'])) {
}

# to do...........
if(isset($_FILES['file_ai_auth']['name'])) {
}

# to do...........
if(isset($_FILES['file_ai_document']['name'])) {
}

# to do...........
if(isset($_FILES['file_ai_portfolio']['name'])) {
}

# to do...........
if(isset($_FILES['file_ai_transaction']['name'])) {
}


print "<br><br><br><br><br><br>";
include_once("{$path}include/footer.php");
?>