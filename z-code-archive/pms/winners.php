<?php 
#
# winners.php
#
# 20061111  MS  Initial Release
#
###############################################
# HEADER INFORMATION
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add a transaction.<br>";
	exit();
}
include_once("{$path}include/functions.php");
$menu_string=extract_menu_string(ereg_replace("&&", "&", $_SERVER["QUERY_STRING"]));

###############################################
# BODY INFORMATION
print "<b>These stocks have given you a profit 100% of the time (at least 3 trades):</b><br><br>";

$get_tickers=db_query("
	SELECT DISTINCT ticker
	FROM		ai_portfolio
	WHERE		username = '$username'
	AND		ticker 		<> 'ACT_CASH'
	ORDER BY	ticker
	");

print "<table width=100%>";
print "<tr>";
print "<td width=10%><b>Ticker</b></td>";
print "<td width=10%><b># Trades</b></td>";
print "<td width=80%><b>Company Name</b></td>";
print "</tr>";
$i=0;
while ($row = db_fetch_array($get_tickers)) {
	$ticker		= $row["ticker"];
	# First we grab a ticker, then we see if there's at least 3 trades in the stock
	$count_trades=db_query("
		SELECT 		ticker
		FROM		ai_portfolio
		WHERE		username = '$username'
		AND		ticker = '$ticker'
		AND		ai_portfolio.buy_date >= '2005-05-03 00:00:00'
		AND		sell_date <> '0000-00-00 00:00:00'
		");
	$numrows = db_num_rows($count_trades);
	if($numrows>=3) {
		$get_detail=db_query("
			SELECT 		ai_portfolio.sell_cost_basis 
					, ai_portfolio.buy_cost_basis
					, ai_company.name AS name
			FROM		ai_portfolio
			INNER JOIN	ai_company
			ON		ai_portfolio.ticker = ai_company.ticker
			WHERE		username = '$username'
			AND		ai_portfolio.ticker = '$ticker'
			    AND     ai_portfolio.buy_date >= '2005-05-03 00:00:00'
		        AND		ai_portfolio.sell_date <> '0000-00-00 00:00:00'
			");
		$ignore = 0;  # Lower Flag
		while ($rowGetDetail = db_fetch_array($get_detail)) {
			$name			= $rowGetDetail["name"];
			$sell_cost_basis 	= $rowGetDetail["sell_cost_basis"];
			$buy_cost_basis 	= $rowGetDetail["buy_cost_basis"] * -1;
			if($buy_cost_basis>$sell_cost_basis) {
				# This holding lost money, so raise the ignore flag
				$ignore=1;
			}
			$name = strtolower($name);
			$name = ucwords($name);
		}
		if($ignore==0) {
			$i++;
			if ($i % 2) { 
				echo "<tr bgcolor=eeeeee>";
			} else {
		      		echo "<tr bgcolor=ffffff>";
			}
			print "<td width=10%>$ticker</td>";
			print "<td width=10%><a href=portfolio_analysis_1.php?PHPSESSID=$PHPSESSID&sell_date_start=1900-01-01&sell_date_end=2050-12-31&filter_by_ticker=$ticker>$numrows</a></td>";
			print "<td width=80%>$name</td>";
			print "</tr>";
		}
	}
}
print "</table>";
print "<br><b>Chances are, these stocks have long-term upward price trends.  You might want to consider NOT selling these in the future, and instead, pyramiding your profits by buying more.  Stocks with long-term upward price trends may continue that trend.  They may also yield excellent long-term returns.</b><br>";
print "<br>";
print "<b>Also be weary of what Charlie Munger calls \"Bias from Pavlovian Association � Misconstruing past correlations as a reliable bias for decision making\".  Here, Mr. Munger is warning that you might misjudge your purchase because you had such great success with it in the past.</b><br>";
###############################################
# FOOTER INFORMATION
print "<br><br><br><br><br><br>";
include_once("{$path}include/footer.php");
?>