<?php

#
# net_worth_1.php
#
# I used transaction_output_1.php to get started
#
# 2006/03/11  MS  Initial Release
#
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))

include_once("{$path}include/database.php");

include_once("{$path}include/header.php");

include("{$path}Date-1.3/Date/Calc.php");

extract($_GET);

# $start_date and $end_date should appear in the URL, but if it doesn't use this:
if ( !isset($start_date) OR !isset($end_date) ) {
	$start_date=date("Y");
	$start_date="$start_date"."-01-01";
	$end_date=date("Y-m-d");
}

if(!isset($_GET['PHPSESSID'])) { $_GET['PHPSESSID']=""; }

print "<form action={$path}pms/transaction_output_balance_1.php>";
print "<font face=arial size=+0>";
print "<b>Cash Balance Report<br></b>";
print "</font>";
print "<font face=arial size=-1>";
print "<input type=hidden name=PHPSESSID value=$_GET[PHPSESSID]>";
print "From: <input type=text name=start_date value=$start_date> ";
print " &nbsp; To: <input type=text name=end_date value=$end_date> ";
print " &nbsp; ";
print "<br>";

print "Filter by Ticker: <input type=text name=ticker> &nbsp; ";
print "Filter by Account: ";
print "<select name=account_id>";
print "<option value=></option>";
	$sql_temp=db_query("
		SELECT		DISTINCT account_id
		FROM		ai_account
		WHERE		username='$username'
		");
	while ($row_temp = db_fetch_array($sql_temp)) {
		$temp	= $row_temp["account_id"];
		print "<option value=$temp "; if(isset($_GET["account_id"])) { if ($temp==$_GET["account_id"]) { print " SELECTED "; } }    print ">$temp</option>";
	}
print "</select>";
print " &nbsp; ";
print "<br>";

if(isset($_GET["starting_balance"])) {
	$starting_balance = $_GET["starting_balance"];
} else {
	$starting_balance = 0; # default
}

$balance = $starting_balance;
print "Starting Balance: <input type=text name=starting_balance value=$starting_balance> &nbsp; ";

print "<input type=submit value=Submit><br><br>";
print "</form>";





























# TABLE: TRANSACTION
# account_id   	varchar(255)	
# date  	date
# time  	time
# action  	varchar(10)
# ticker  	varchar(10)
# shares  	decimal(15,6)
# price  	decimal(15,6)
# commission  	decimal(15,6)
# net_change  	decimal(15,6)
# notes  	longtext

if(!isset($ticker)) {
	# Do nothing
} else {
	# Gotta wipe it out if user doesn't want to Filter by ticker and click that "Submit" button
	if($ticker=="") { unset($ticker); }
}



# Print Headers

print "<table width=100% border=1 cellspacing=0 cellpadding=3 bordercolor=dddddd>";
print "<tr>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Account ID</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Date/Time</b></td>";

print "<td width=11% bgcolor=abcdef>";
print "<font face=arial size=-1>";
print "<b>Buy Date</b></td>";

print "<td width=11% bgcolor=abcdef align=center>";
print "<font face=arial size=-1>";
print "<b>Ticker</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Shares</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>Buy Cost Basis</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>xxxCommission</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>xxxNet Change</b></td>";

print "<td width=11% bgcolor=abcdef align=right>";
print "<font face=arial size=-1>";
print "<b>xxxCash Balance</b></td>";
print "</tr>";

$i=1; # row counter

$dividends_paid = 0; # default
$interest_income = 0 ; # default
$securities_bought = 0 ; # default
$securities_sold = 0 ; # default
$miscellaneous_fee = 0 ; # default

$cpt=0; # for graph



$current_date = "$start_date";




$current_date = "2006-10-15";
$end_date = "2006-10-31";


$total_for_current_date = 0;
while ($current_date < $end_date) {



$day	= substr($current_date,8,2);
$month	= substr($current_date,5,2);
$year	= substr($current_date,0,4);
$current_date = Date_Calc::nextWeekday($day="$day",$month="$month",$year="$year",$format="%Y-%m-%d");





if(!isset($_GET["ticker"])) {
	# No filter - just dates
	$string="
		SELECT		ai_portfolio.account_id		AS account_id
				, ai_portfolio.ticker 		AS ticker
				, ai_portfolio.shares 		AS shares
				, ai_portfolio.buy_date 	AS buy_date
				, ai_portfolio.buy_share_price 	AS buy_share_price
				, ai_portfolio.buy_cost_basis 	AS buy_cost_basis
				, ai_portfolio.sell_date 	AS sell_date
		FROM		ai_portfolio
		INNER JOIN	ai_transaction
		ON		ai_portfolio.buy_date = 'ai_transaction.date ai_transaction.time'
		WHERE		ai_portfolio.buy_date <= '$current_date'
		AND		(
				ai_portfolio.sell_date = '0000-00-00 00:00:00'
		OR		ai_portfolio.sell_date >= '$current_date'
				)
		AND		ai_portfolio.username='$username'
		";

	if (isset($_GET["account_id"])) { 
		$string = $string . " AND	account_id = '$_GET[account_id]' ";
	}

	# $string = $string . " ORDER BY	date ASC, time ASC";

	$sql_portfolio=mysql_query("$string");
} else {
	# Filter based on ticker symbol
	$string="
		SELECT		account_id, ticker, shares, buy_date, buy_share_price, buy_cost_basis, sell_date
		FROM		ai_portfolio
		WHERE		buy_date <= '$current_date'
		AND		
				(
				sell_date = '0000-00-00 00:00:00'
		OR		sell_date >= '$current_date'
				)
		AND		username='$username'
		AND		ticker='$_GET[ticker]'
		";

	if (isset($_GET["account_id"])) { 
		$string = $string . " AND	account_id = '$_GET[account_id]' ";
	}

	# $string = $string . " ORDER BY	date ASC, time ASC";

	$sql_portfolio=db_query("$string");
}





$total_for_current_date = 0;

while ($row_transaction = db_fetch_array($sql_portfolio)) {
	$account_id	= $row_transaction["account_id"];
	$ticker		= $row_transaction["ticker"];
	$shares		= $row_transaction["shares"];
	$buy_date	= $row_transaction["buy_date"];
	$buy_share_price= $row_transaction["buy_share_price"];
	$buy_cost_basis = abs($row_transaction["buy_cost_basis"]);
	$sell_date	= $row_transaction["sell_date"];

	$total_for_current_date = $total_for_current_date + $buy_cost_basis;

	# Print Detail
	$i++;
	if ($i % 2) { 
      		echo "<tr bgcolor=ffffff>"; 
	} else { 
		echo "<tr bgcolor=eeeeee>"; 
	} 

	print "<td width=11% valign=top>";
	print "<font face=arial size=-1>";
	print " $account_id";
	print "</td>";

	print "<td width=11%>";
	print "<font face=arial size=-2>";
	print "$current_date</td>";

	print "<td width=11%>";
	print "<font face=arial size=-1>";
	print "$i</td>";

	print "<td width=11% align=center>";
	print "<font face=arial size=-1>";
	if ($ticker!="") {
		print "$ticker</td>";
	} else {
		print "&nbsp;";
	}

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "bd=$buy_date</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "sd=$sell_date</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";
	print "$$buy_cost_basis</td>";





/*
	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";

	if($action!="Miscellaneous Fee") {
		print "$$net_change";
	} else {
		print "$-$net_change";
	}
	if($action=="Cash Dividend") { $dividends_paid = $dividends_paid + $net_change; }
	if($action=="Interest Income") { $interest_income = $interest_income + $net_change; }
	if($action=="Bought") { $securities_bought = $securities_bought + $net_change; }
	if($action=="Sold") { $securities_sold = $securities_sold + $net_change; }
	if($action=="Miscellaneous Fee") { $miscellaneous_fee = $miscellaneous_fee + $net_change; }

	print "</td>";

	print "<td width=11% align=right>";
	print "<font face=arial size=-1>";

	if($action!="Miscellaneous Fee") {
		if($action=="Withdrawl") {
			$balance = $balance - $net_change;
		} else {
			$balance = $balance + $net_change;
		}
	} else {
		$balance = $balance - $net_change;
	}


	print "$" . Number_Format($balance,2);
	print "</td>";
*/


/*
#  graph stuff:
	$xdata[$cpt]=$date;
	$ydata[$cpt]=$balance; # y-axis (closing price)
	# $y2data[$cpt]=(int)$x; # Example of 2nd y-axis
	$cpt++; # Normal method
*/

/*
	# End of row:
	print "</tr>";
*/



  

}


print "<tr>";
	print "<td width=11%>";
	print "<font face=arial size=-2>";
	print "$current_date</td>";

	print "<td width=11%>";
	print "<font face=arial size=-2>";
	print "$$total_for_current_date</td>";

	print "<td width=11%>";
	print "<font face=arial size=-2>";
	print "$$balance</td>";
print "</tr>";


mysql_data_seek($sql_portfolio, 0);  # reset pointer so I can requery
#mysql_free_result($sql_portfolio);
#$row_transaction = 0;

} # end of while current_date < end_date




# Force to top of page using this trick:
print "<table width=100% cellspacing=0 cellpadding=0>";
print "<tr><td>";

/*
	print "<br>";
	print "<b>Summary:</b><br>";
	print "Starting Balance = $$starting_balance<br>";
	print "Dividends Paid = $$dividends_paid<br>";
	print "Interest Income = $$interest_income<br>";
	print "Securities Bought = $$securities_bought<br>";
	print "Securities Sold = $$securities_sold<br>";
	print "Miscellaneous Fee = $$miscellaneous_fee<br>";
	print "Ending Balance = $$balance<br>";
	print "<br>";
*/

print "</td>";
print "<td>";

#	print "<img align=right src={$path}pms/transaction_output_balance_2.php?PHPSESSID=$_GET[PHPSESSID]&account_id=$account_id&starting_balance=$starting_balance&start_date=$start_date&end_date=$end_date>";

print "</td>";
print "</tr>";
print "</table>";








if($account_id=="") {
	print "<b>ERROR:  You must choose an account using the filter.</b><br><br>";
}

print "</table>";



include_once("{$path}include/footer.php");
?>

