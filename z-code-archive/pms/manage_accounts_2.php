<?php
#############################################################################
# PROGRAMMER'S LOG SECTION:
# Filename:	manage_accounts_2.php
# Authors:	Nick Kuechler
#############################################################################
#
# 2004/07/13  MS  Checks to make sure user is logged in
# 2004/07/13  MS  Changed to pass URL variables to next page (to maintain login)
# 2004/07/12  MS  Bug fix - removed extra '/' in the Manage Accounts link at the bottom
# 2004/05/12  NK  Bug fix
# 2004/05/09  FS  Changed include mechanism and $path variable
# 2004/05/06  FS  Changed to use functions from include/database.inc
# 2004/04/22  NK  Created file
##############################################################################

if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add an account.<br>";
	exit();
}

##################
# Edit an entry
#
if (isset($_GET['edit'])) {
  #(isset($_GET['acct_id'])) {
  $acct_id = $_GET['acct_id'];
  $acct_id_orig = $_GET['acct_id'];

  # get data for account we wish to edit
  $get_account=db_query("
        SELECT          *
        FROM            ai_account
	WHERE		account_id=\"". $acct_id ."\"
        ");

  $show_columns=db_query("
  	SHOW		columns
  	FROM		ai_account
  	");

  
  # display the list of accounts
  print "<form method=\"post\" action=\"{$_SERVER['PHP_SELF']}\">";
  print "<input type=\"hidden\" name=\"updateaccount\" value=\"{$acct_id}\">";
  print "<input type=\"hidden\" name=\"acct_id_orig\" value=\"{$acct_id_orig}\">";

  print "<table border=0>\n";
  #print "\t<tr>\n";

  # display column names
  while ($row = mysql_fetch_row($show_columns)) {
    #print "\t\t<td><b>$row[0]</b></td>\n";
    $col_arr[] = $row[0];
  }
  #print "\t</tr>\n";


  while ($line = mysql_fetch_array($get_account, MYSQL_ASSOC)) {
    #print "\t<tr>\n";
    $i = 0;

    foreach ($line as $col_value) {
      if ($i >= 0) {
	print "\t<tr>\n";
        #print "\t\t<td>". $col_value ."</td>\n";
	#print "\t\t<td><input type=\"text\" name=\"". $col_arr[$i] ."\" value=\"". $col_value ."\"></td>\n";
	print "\t\t<td align=right>$col_arr[$i]</td>\n";
	print "\t\t<td><input type=\"text\" name=\"". $col_arr[$i] ."\" value=\"". $col_value ."\"></td>\n";
	print "\t</tr>\n";
      } else {
	print "\t<tr>\n";
        $acct_id = $col_value;
	#print "\t\t<td><input type=\"text\" name=\"update". $col_arr[$i] ."\" value=\"". $col_value ."\"></td>\n";
	print "\t\t<td>$col_arr[$i]</td>\n";
	print "\t\t<td><input type=\"text\" name=\"". $col_arr[$i] ."\" value=\"". $col_value ."\"></td>\n";
        #print "\t\t<td>". $col_value ."</td>\n";
	print "\t</tr>\n";
      }
      $i++;
    }
    #print "\t\t<td><a href=\"manage_accounts_3.php?acct_id=". $acct_id ."\">Done</a></td>";
    #print "\t</tr>\n";
  }

  print "</table>\n";
  echo "<input type=\"submit\" value=\"UPDATE\">";
  echo "</form>";


##################
# Add an entry
#
} else if (isset($_GET['addacct'])) {
    $account_id = $_POST['account_id'];
    $account_name = $_POST['account_name'];
    $description = $_POST['description'];
    $contact_name = $_POST['contact_name'];
    $phone_number = $_POST['phone_number'];
    $broker_username= $_POST['broker_username'];
    $broker_password = $_POST['broker_password'];
    $notes = $_POST['notes'];

  $update_account=db_query("
        INSERT INTO	ai_account
			(username,
			account_id,
			account_name,
			description,
			contact_name,
			phone_number,
			broker_username,
			broker_password,
			notes)
	VALUES		('". $username ."',
			'". $account_id ."',
			'". $account_name ."',
			'". $description ."',
			'". $contact_name ."',
			'". $phone_number ."',
			'". $broker_username ."',
			'". $broker_password ."',
			'". $notes ."')
	");

  print "Account added.";

##################
# Delete an entry
#
} else if (isset($_GET['del'])) {
  $acct_id = $_GET['acct_id'];

  $update_account=db_query("
        DELETE FROM	ai_account
	WHERE		account_id=\"". $acct_id ."\"
	AND		username=\"". $username ."\"
	");

  print "Account deleted (username= ". $username .").";

##################
# Update edited entry in database
#
} else if (isset($_POST['updateaccount'])) {
  $acct_id = $_POST['updateaccount'];
  $new_acct = $_POST['account_id'];
  $acct_id_orig = $_POST['acct_id_orig'];
  $account_name = $_POST['account_name'];
  $description = $_POST['description'];
  $contact_name = $_POST['contact_name'];
  $phone_number = $_POST['phone_number'];
  $broker_username = $_POST['broker_username'];
  $broker_password = $_POST['broker_password'];
  $notes = $_POST['notes'];

  #print "1 $acct_id 2 $acct_id_orig 3 $new_acct";
  #print "$account_name $description $contact_name $phone_number $username $password $notes";
  $update_account=db_query("
        UPDATE		ai_account
	SET		account_id=\"". $new_acct ."\",
			account_name=\"". $account_name ."\",
			description=\"". $description ."\",
			contact_name=\"". $contact_name ."\",
			phone_number=\"". $phone_number ."\",
			broker_username=\"". $broker_username ."\",
			broker_password=\"". $broker_password ."\"
	WHERE		account_id=\"". $acct_id_orig ."\"
        ");


  print "Update saved.";

} else {
  print "NEED TO EDIT SOMETHING";

}

$query_string=$_SERVER["QUERY_STRING"];
print "<br><br><a href={$path}pms/manage_accounts_1.php?$query_string>Manage Accounts</a><br><br>";

include_once("{$path}include/footer.php");

?>
