<?php
#
# import_1.php
#
if (empty($path)) {
        $path="../";
} // end if (empty($path))
include_once("{$path}include/header.php");

if($username=="") {
	print "You must be logged in before you can add a transaction.<br>";
	exit();
}

print "<form action=import_2.php?$menu_string method=post ENCTYPE=multipart/form-data>";  #  ENCTYPE=multipart/form-data
print "Filename to import (for table ai_auth):  <input type=file name=file_ai_auth size=60><br>";
print "<input type=submit value=Import>";
print "</form>";

print "<br><br><br><br><br><br>";
include_once("{$path}include/footer.php");
?>