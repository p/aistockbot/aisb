<?php
#
# add_logo_2.php
#
# 2007-09-17  MS  Initial Release
#


$ticker = $_POST["ticker"];

#
# photo_1 SECTION
#
$timestamp=date("YmdHis");
$photo_1_temporary_name = $_FILES['photo_1']['tmp_name']; # temporary filename
$photo_1_name = $_FILES['photo_1']['name'];  # real filename
# Now, I need to VERIFY that this is either a .GIF or .gif
$photo_1_length = strlen($photo_1_name);
$photo_1_starting_position = $photo_1_length-4;
$photo_1_test = substr("$photo_1_name", $photo_1_starting_position, 4);

if ($photo_1_test == ".GIF") {
	# OK
} else {
	if ($photo_1_test == ".gif") {
		# OK
	} else {
		if ($photo_1_test == "") {
			# OK - there's nothing
		} else {
			print "<font size=+0><b>File Upload Status = <font color=ff0000>FAILED</font></b></font><br><br>";
			print "Only <b>.GIF</b> and <b>.gif</b> images can be uploaded at this time.<br>";
			print "Also, the max filesize limit is set to 524,288 bytes<br><br></center>"; # per php.ini?
			# Check /var/log/httpd/error_log for filesize errors if this page does not show on the browser correctly.
			# Example:  Requested content-length of 888683 is larger than the configured limit of 524288
			exit();
		}
	}
}
if (is_uploaded_file($_FILES['photo_1']['tmp_name'])) {
	$photo_1_mysql_name = "../images/logos/$ticker.GIF";
	$photo_1_name = "C:\\Inetpub\\wwwroot\\svn_aistockbot\\aistockbot\\images\\logos\\$ticker.GIF";
	move_uploaded_file($photo_1_temporary_name, "$photo_1_name");
} else {
	# This shows if it's blank, so I took it out.
	# echo "Possible file upload attack. Filename: " . $_FILES['photo_1']['name'];
} 




####################
# CREATE THUMBNAIL
// File and new size
# $filename = 'test.jpg';
$filename = "$photo_1_mysql_name"; 
#$percent = 0.20;  # 20 percent of original size

// Content type
header('Content-type: image/gif');

// Get new sizes
list($width, $height) = getimagesize($filename);
#$newwidth = $width * $percent;
$newwidth = 100;
#$newheight = $height * $percent;
$newheight = 40;

// Load
$thumb = imagecreatetruecolor($newwidth, $newheight);
$source = imagecreatefromgif($filename);

// Resize
imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

// Output
# imagegif($thumb);
# $thumb_photo_1 = str_replace(".jpg","_thumb.jpg",$filename);

# imagejpeg($thumb, $thumb_photo_1, 100);
imagegif($thumb);
####################


print "You can close this window now<br>";

?>