<?php
#
# transaction_add_3b.php
#
# Process Stock Splits
#
# 2004/07/13  MS  Initial Release
#
if (empty($path)) { $path="../"; } 
include_once("{$path}include/header.php");

# Get Data
extract($_POST);

# STEP 2:  UPDATE RECORDS IN PORTFOLIO
$get_portfolio=db_query("
	SELECT	username,account_id,ticker,shares,buy_date,buy_share_price
	FROM	ai_portfolio
	WHERE	username	= '$username'
	AND	account_id	= '$account_id'
	AND	ticker		= '$ticker'
	AND	sell_date	= '0000-00-00 00:00:00'
	");

while ($row_portfolio=db_fetch_array($get_portfolio)) {
	$shares		=$row_portfolio['shares'];
	$buy_date	=$row_portfolio['buy_date'];
	$buy_share_price=$row_portfolio['buy_share_price'];

	# Perform split:
	$split_ratio 		= ($shares_split_after/$shares_split_before);
	$cost_split_ratio	= ($shares_split_before/$shares_split_after);
	$new_shares		= $split_ratio * $shares;
	$new_buy_share_price	= $cost_split_ratio * $buy_share_price;

	# Update ai_portfolio
	db_query("
		UPDATE	ai_portfolio
		SET	shares			= $new_shares
			, buy_share_price	= $new_buy_share_price
		WHERE	username 		= '$username'
		AND	account_id		= '$account_id'
		AND	ticker			= '$ticker'
		AND	sell_date		= '0000-00-00 00:00:00'
		AND	shares			= $shares
		AND	buy_date		= '$buy_date'
		AND	buy_share_price		= $buy_share_price
	");

	# used for adjusting notes:
	$split_date=$date;
	$split_time=$time;

	# used for query in testing for date and time:
	$date2=substr($buy_date,0,10); 	# yyyy-mm-dd
	$time2=substr($buy_date,-8); 	# hh:ii:ss

	# Update ai_transaction
	db_query("
		UPDATE	ai_transaction
		SET	shares			= $new_shares
			, price			= $new_buy_share_price
			, notes			= 'Adjusted for $shares_split_after for $shares_split_before stock split on $split_date $split_time'
		WHERE	username 		= '$username'
		AND	account_id		= '$account_id'
		AND	ticker			= '$ticker'
		AND	shares			= $shares
		AND	date			= '$date2'
		AND	time			= '$time2'
		AND	price			= $buy_share_price
	");

	# TO DO LIST:
	# I really need to have this append to my notes instead of overwriting them!
	# Perhaps when I get a chance (or someone else), we can make the note change in a secondary step (ie. fetch the note, then append)
	###


	# Debug...
	print "Splitting $ticker on buy date $buy_date<br>";
	print "The Split Date is $split_date $split_time<br>";
	print "Turning $shares shares into $new_shares<br>";
	print "Original buy price $$buy_share_price/share turned into $$new_buy_share_price/share for tax purposes.<br>";
	print "(based on $shares_split_after for $shares_split_before split)<br>";
	print "-----<br>";
}

print "<br>";
print "<b>Note:  This only makes changes for user $username on Account ID: $account_id only.</b><br>";

include_once("{$path}include/footer.php");
?>
