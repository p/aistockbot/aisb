REM AIStockbot Windows Setup 


REM Changelog:

REM 2004/08/23 EW Depreciated - Please use setup.php
REM 2004/08/18 EW Modified to reflect changes in header.php
REM 2004/08/03 EW Initial Release

REM Notes:

REM Notes: Some parts of this install can be streamlined
REM if we are able to include Windows versions of some basic
REM Unix tools. Might be worth looking into. 
REM Also, would someone doublecheck that the mixed slashes and 
REM backslashes in paths don't cause problems.

@ECHO off
cls
:start

ECHO Setup.bat is no longer the preferred method to configure
ECHO AIStockBot. Please open setup.php in your browser to 
ECHO configure your copy of AIStockBot.
ECHO.
ECHO Thanks,
ECHO AIStockBot Team

pause

goto END

ECHO.
ECHO *Welcome to AIStockBot Setup*

:CHOICE1
ECHO.
ECHO 1. Install new AIStockbot database and configuration
ECHO 2. Just configure, leave database as is.
ECHO 3. Do nothing and exit
set AIchoice=
set /p AIchoice=Please select 1, 2, or 3   

if '%AIchoice%' == '1' goto ENVSETUP
if '%AIchoice%' == '2' goto ENVSETUP
if '%AIchoice%' == '3' goto END
goto CHOICE1

:ENVSETUP

ECHO *Database Setup*
ECHO.
set AIDBHost=
set /p AIDBHost=Enter database host name [localhost] 
if '%AIDBHost%'=='' set AIDBHost=localhost
ECHO Database host set to "%AIDBHost%"
ECHO.
set AIDBName=
set /p AIDBName=Enter database Name [aistockbot] 
if '%AIDBName%'=='' set AIDBName=aistockbot
ECHO Database name set to "%AIDBName%"
ECHO.
set AIDBUser=
set /p AIDBUser=Enter database username [] 
ECHO Database username set to "%AIDBUser%"
ECHO.
set AIDBPass=
set /p AIDBPass=Enter Database user password [] 
ECHO Database password set to "%AIDBPass%"
ECHO.

if '%AIchoice%' == '2' goto CONFIG

:DBSETUP
ECHO Beginning Database Setup
ECHO *Warning - You are about to reinitialize all data in %AIDBName%*
ECHO If you do not wish to proceed press Ctrl-c now
pause
ECHO.
ECHO This will take a moment...
cd sql

REM Need a way to strip ".sql" before drop-table can be automated
REM If someone figures this out, please feel free to replace the 
REM full database drop below.

REM Also, if you find a good way to provide feedback on the process, that would be good too.

REM mysql -u %AIDBUser% --password=%AIDBPass% --exec="DROP DATABASE IF EXISTS %AIDBName%"
REM mysql -u %AIDBUser% --password=%AIDBPass% --exec="CREATE DATABASE %AIDBName%"

for %%s IN (ai*.sql) DO mysql -u %AIDBUser% --password=%AIDBPass% %AIDBName% < %%s

ECHO.
ECHO Done creating tables.

cd ..

:CACHEDIR
mkdir tmp
cd tmp
mkdir jpgraph_cache
cd ..

:CONFIG
set AIBaseDir=%cd%
set AIConfigFile=%AIBaseDir%\include\config.php
set AIJPCacheDir=%AIBaseDIR%\tmp\jpgraph_cache\

ECHO -------------------------------------- > instructions.txt
ECHO Finishing your aistockbot installation >> instructions.txt
ECHO -------------------------------------- >> instructions.txt
ECHO. >>instructions.txt
ECHO 1. Please ensure that %AIConfigFile% contains the following lines: >>instructions.txt
ECHO. >>instructions.txt
ECHO ------>> instructions.txt
ECHO. >>instructions.txt
ECHO # Used by individual.php, learn.php, and stuff in the tools directory >>instructions.txt
ECHO $global_database="%AIDBName%";>>instructions.txt
ECHO $global_hostname="%AIDBHost%";>>instructions.txt
ECHO $global_username="%AIDBUser%";>>instructions.txt
ECHO $global_password="%AIDBPass%";>>instructions.txt
ECHO. >>instructions.txt
ECHO -------------------------------------- >>instructions.txt
ECHO. >>instructions.txt


ECHO Please review instructions.txt to finalize your installation
start instructions.txt
pause

:END

REM Clean up environmentS variables, not that this matters much.
set AIChoice=
set AIDBHost=
set AIDBHame=
set AIDBUser=
set AIDBPass=
set AIBaseDir=
set AIConfigFile=

@ECHO on





